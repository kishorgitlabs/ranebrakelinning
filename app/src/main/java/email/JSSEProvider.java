package email;

import java.security.AccessController;
import java.security.PrivilegedAction;
import java.security.Provider;

public final class JSSEProvider
        extends Provider {
    public JSSEProvider() {
        super("HarmonyJSSE", 1.0D, "Harmony JSSE Provider");
        AccessController.doPrivileged(new PrivilegedAction() {
            public Void run() {
                JSSEProvider.this.put("SSLContext.TLS", "org.apache.harmony.xnet.provider.jsse.SSLContextImpl");
                JSSEProvider.this.put("Alg.Alias.SSLContext.TLSv1", "TLS");
                JSSEProvider.this.put("KeyManagerFactory.X509", "org.apache.harmony.xnet.provider.jsse.KeyManagerFactoryImpl");
                JSSEProvider.this.put("TrustManagerFactory.X509", "org.apache.harmony.xnet.provider.jsse.TrustManagerFactoryImpl");
                return null;
            }
        });
    }
}


/* Location:              F:\Android\decompiler\dex2jar-2.0\classes-dex2jar.jar!\com\brainmagicleather\leatherindia\home\JSSEProvider.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */