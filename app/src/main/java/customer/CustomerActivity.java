package customer;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.brainagic.catalogue.EditProfileActivity;
import com.brainagic.catalogue.catalogue.R;

import about.AboutActivity;
import alertbox.Alertbox;
import engineer.EngineerActivity;
import feedback.FeedbackActivity;
import home.HomeActivity;
import mechanic.MechanicActivity;
import notification.NotificationActivity;
import product.PriceList;
import product.ProductCatalog;
import retailer.RetailerActivity;
import search.SearchMapFragment;
import wsd.WSDActivity;

;

public class CustomerActivity extends Activity {




    private RelativeLayout sales, feedbacklayout, findmech, findretailer,wsdlayout;
    private String Usertype,Useremail,Usermobile;
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    private Alertbox alertbox = new Alertbox(CustomerActivity.this);
    private TextView title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer);
        getActionBar().setLogo(R.drawable.logo);
       /* getSupportActionBar().setDisplayShowHomeEnabled(true);
       // getSupportActionBar().setDisplayHomeAsUpEnabled(true);
       // getSupportActionBar().setTitle("Customer");
        getSupportActionBar().setIcon(R.mipmap.ic_launcher);*/
        title = (TextView) findViewById(R.id.title);
        myshare = getSharedPreferences("Registration", MODE_PRIVATE);
        editor = myshare.edit();
        Usertype = myshare.getString("Usertype","") ;
        Useremail = myshare.getString("Email","") ;
        Usermobile = myshare.getString("MobNo","") ;
        wsdlayout = (RelativeLayout) findViewById(R.id.wsd);
        if(Usertype.equals("Fleet Operators"))
        {
            wsdlayout.setVisibility(View.VISIBLE);
            title.setText("Fleet Operator");
        }
        else
        {
            wsdlayout.setVisibility(View.GONE);
        }



        wsdlayout = (RelativeLayout) findViewById(R.id.wsd);
        wsdlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent find = new Intent(CustomerActivity.this, SearchMapFragment.class);
                find.putExtra("findertype", "Distributor");
                startActivity(find);
            }
        });



        feedbacklayout = (RelativeLayout) findViewById(R.id.feedlay);
        feedbacklayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                    Intent find = new Intent(CustomerActivity.this, FeedbackActivity.class);
                    startActivity(find);
            }
        });
        findmech = (RelativeLayout) findViewById(R.id.findmechaniclayout);
        findmech.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Intent find = new Intent(CustomerActivity.this, SearchMapFragment.class);
                find.putExtra("findertype", "Mechanic");
                startActivity(find);

            }
        });

        findretailer = (RelativeLayout) findViewById(R.id.findretailerlayout);
        findretailer.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Intent find = new Intent(CustomerActivity.this, SearchMapFragment.class);
                find.putExtra("findertype", "Retailer");
                startActivity(find);


            }
        });


        // for home abd back
        TextView home = (TextView) findViewById(R.id.fontDesign1);
        TextView back = (TextView) findViewById(R.id.fontDesign2);
        home.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                Intent ho = new Intent(CustomerActivity.this, HomeActivity.class);
                ho.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(ho);
            }
        });
        back.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                onBackPressed();
            }
        });
    }

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        super.onBackPressed();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.popup, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // ...

        switch (item.getItemId()) {
            case R.id.about:
                // search action
                startActivity(new Intent(CustomerActivity.this, AboutActivity.class));
                return true;
            case R.id.product:
                // location found
                startActivity(new Intent(CustomerActivity.this, ProductCatalog.class));
                return true;
            case R.id.price:
                // refresh
                startActivity(new Intent(CustomerActivity.this, PriceList.class));
                return true;
            case R.id.wsd:
                // help action
                if(!Usertype.equals("Distributor"))
                {
                    alertbox.showAlertbox("You are not allowed to view this page !","auth");
                }
                else
                {
                    startActivity(new Intent(CustomerActivity.this, WSDActivity.class));
                }
                return true;
            case R.id.retailer:
                // check for updates action
                if(!Usertype.equals("Retailer"))
                {
                    alertbox.showAlertbox("You are not allowed to view this page !","auth");
                }
                else {
                    startActivity(new Intent(CustomerActivity.this, RetailerActivity.class));
                }
                return true;
            case R.id.fieldengi:
                // check for updates action

                if(Usertype.equals("Rane Employee") || Usertype.equals("Sales Executives")|| Usertype.equals("MSR"))
                {

                    startActivity(new Intent(CustomerActivity.this, EngineerActivity.class));
                }
                else
                {
                    alertbox.showAlertbox("You are not allowed to view this page !","auth");
                }
                return true;
            case R.id.msr:
                // check for updates action

                if(Usertype.equals("MSR"))
                {

                    startActivity(new Intent(CustomerActivity.this, EngineerActivity.class));
                }
                else
                {
                    alertbox.showAlertbox("You are not allowed to view this page !");
                }
                return true;
            case R.id.mech:
                // check for updates action
                if(!Usertype.equals("Mechanic"))
                {
                    alertbox.showAlertbox("You are not allowed to view this page !","auth");
                }
                else
                {
                    startActivity(new Intent(CustomerActivity.this, MechanicActivity.class));
                }
                return true;
            case R.id.customer:
                // check for updates action

                if((Usertype.equals("Others")) || (Usertype.equals("Fleet Operators")))
                {

                    startActivity(new Intent(CustomerActivity.this, CustomerActivity.class));
                }
                else
                {
                    alertbox.showAlertbox("You are not allowed to view this page !","auth");
                }
                return true;
            case R.id.notification:
                // check for updates action
                startActivity(new Intent(CustomerActivity.this, NotificationActivity.class));
                return true;
            case R.id.edit:
                // check for updates action
                Intent edit = new Intent(CustomerActivity.this,EditProfileActivity.class);
                edit.putExtra("usertype",Usertype);
                edit.putExtra("mobile",Usermobile);
                edit.putExtra("email",Useremail);
                startActivity(edit);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
