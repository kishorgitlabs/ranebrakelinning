package search;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.brainagic.catalogue.EditProfileActivity;
import com.brainagic.catalogue.catalogue.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import about.AboutActivity;
import alertbox.Alertbox;
import customer.CustomerActivity;
import dmax.dialog.SpotsDialog;
import engineer.EngineerActivity;
import home.HomeActivity;
import mechanic.MechanicActivity;
import network.NetworkConnection;
import notification.NotificationActivity;
import persistency.ServerConnection;
import product.PriceList;
import product.ProductCatalog;
import wsd.WSDActivity;

;

@SuppressLint("NewApi")
public class Map_Search extends FragmentActivity implements LocationListener, OnMapReadyCallback {


    private ArrayList<String> LatitudeList, LongtitudeList, ShopnameList, AddressList;
    private Connection connection;
    private Statement stmt;
    private ResultSet rset;
    private Alertbox box = new Alertbox(Map_Search.this);
  
    private String findertype;
    private Spinner statespinner, cityspinner;
    private String SelectedState, SelectedCity;
    private ArrayList<String> StatespinnerList, CityspinnerList;
    private TextView tittletxt;

    private String Usertype,Useremail,Usermobile;
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    // Get Activity
    private Context context;
    // Google map
    private GoogleMap googleMap;
    // Update location
    private LocationManager locationManager;
    // Get coordinates
    Location location;
    // The minimum distance to change Updates in meters
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10; // 10 meters
    // The minimum time between updates in milliseconds
    private static final long MIN_TIME_BW_UPDATES = 1000 * 60 * 1; // 1 minute
    // Get layout
    private View view;
    // Get Map layout
    private MapView mapView;
    private MarkerOptions currentlocationmarker;
    private LatLng currentLatLag;
    private boolean isGPSEnabled, isNetworkEnabled;
    private SpotsDialog progressDialog;
    private Alertbox alertbox = new Alertbox(Map_Search.this);
    private Marker mMarker;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.map_search);

        getActionBar().setLogo(R.drawable.logo);

        myshare = Map_Search.this.getSharedPreferences("Registration", Map_Search.this.MODE_PRIVATE);
        editor = myshare.edit();
        Useremail = myshare.getString("Email","") ;
        Usermobile = myshare.getString("MobNo","") ;
        findertype = myshare.getString("findertype", "");
        Usertype = myshare.getString("Usertype", "");
        tittletxt = (TextView) findViewById(R.id.title);
        //tittletxt.setText("Find " + findertype);

        if(myshare.getString("findertype", "").toString().equals("Distributor"))
        {
            tittletxt.setText("Find Whole Sale Distributor (WSD)");
        }
        else {
            tittletxt.setText("Find " + myshare.getString("findertype", "").toString());
        }

        statespinner = (Spinner) findViewById(R.id.statespinner);
        cityspinner = (Spinner) findViewById(R.id.cityspinner);

        StatespinnerList = new ArrayList<String>();
        CityspinnerList = new ArrayList<String>();

        AddressList = new ArrayList<String>();
        LatitudeList = new ArrayList<String>();
        LongtitudeList = new ArrayList<String>();
        ShopnameList = new ArrayList<String>();


        statespinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long l) {

                SelectedState = parent.getItemAtPosition(position).toString();
                if (!SelectedState.equals("Select State")) {

                    new GetSpinnerCityData().execute(SelectedState);
                }
                else
                {
                    CityspinnerList.clear();
                    cityspinner.setAdapter(new ArrayAdapter<String>(Map_Search.this, R.layout.simple_spinner_item, CityspinnerList));

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        cityspinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long l) {


                SelectedCity = parent.getItemAtPosition(position).toString();


                if (SelectedCity.equals("All")) {
                    String query = "select *  from registration where state =  '" + SelectedState + "' and usertype = '" + findertype + "' and  latttitude != 'null' and  longtitude != 'null'";
                    Log.v("spiner query", query);

                    new GetAddressDetailas().execute(query);
                } else {
                    String query = "select *  from registration  where city = '" + SelectedCity + "' and state =  '" + SelectedState + "' and usertype = '" + findertype + "' and  latttitude != 'null' and  longtitude != 'null'";
                    Log.v("spiner query", query);

                    new GetAddressDetailas().execute(query);

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        // for home abd back
        TextView home = (TextView) findViewById(R.id.fontDesign1);
        TextView back = (TextView) findViewById(R.id.fontDesign2);
        home.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                Intent ho = new Intent(Map_Search.this, HomeActivity.class);
                ho.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(ho);
            }
        });
        back.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                onBackPressed();
            }
        });


        initilizeMap();
        checkInternet();
    }


    /**
     * function to load map. If map is not created it will create it for you
     */
    private void initilizeMap() {
        if (googleMap == null) {

            SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.map);
            mapFragment.getMapAsync(this);


        }
    }


    @Override
    public void onMapReady(GoogleMap map) {
        googleMap = map;

        // Getting GoogleMap object from the fragment
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        googleMap.getUiSettings().setZoomControlsEnabled(true);
        // Get Location
            locationMap();

        // check if map is created successfully or not
        if (googleMap == null) {
            Toast.makeText(getApplicationContext(),
                    "Sorry! unable to create maps", Toast.LENGTH_SHORT)
                    .show();
        }


    }



    private GoogleMap.OnMyLocationChangeListener myLocationChangeListener = new GoogleMap.OnMyLocationChangeListener() {
        @Override
        public void onMyLocationChange(Location location) {
            LatLng loc = new LatLng(location.getLatitude(), location.getLongitude());
            // Adding a marker
             mMarker = googleMap.addMarker(new MarkerOptions().position(loc));
            if(googleMap != null)
            {
                googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(loc, 16.0f));
            }
        }
    };


    public void locationMap() {
        // Check WIFI and network enable or not and get last location updates

        locationManager = (LocationManager)
                getSystemService(Context.LOCATION_SERVICE);
        isGPSEnabled = locationManager
                .isProviderEnabled(LocationManager.GPS_PROVIDER);
        isNetworkEnabled = locationManager
                .isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        if (isGPSEnabled) {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
            if (locationManager != null) {
                location = locationManager
                        .getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);
                // Pass Latitude & Longitude to LatLng
                if (location != null) {
                    currentLatLag = new LatLng(location.getLatitude(),
                            location.getLongitude());

                    String str = "Latitude:" + location.getLatitude() + ", Longitude:" + location.getLongitude();
                    Log.v("Location current ", str);

                    // Adding a marker
                    MarkerOptions marker = new MarkerOptions().position(
                            currentLatLag)
                            .title("You are at");
                    marker.icon(BitmapDescriptorFactory
                            .defaultMarker(BitmapDescriptorFactory.HUE_AZURE));
                    googleMap.addMarker(marker);

                    CameraPosition cameraPosition = new CameraPosition.Builder()
                            .target(currentLatLag).zoom(5).build();

                    googleMap.animateCamera(CameraUpdateFactory
                            .newCameraPosition(cameraPosition));


                }
            }


        } else if (isNetworkEnabled) {
            // Get update settings
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, this);
            // Get last known location update
            if (locationManager != null) {
                location = locationManager
                        .getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);
                // Pass Latitude & Longitude to LatLng
                if (location != null) {
                    currentLatLag = new LatLng(location.getLatitude(),
                            location.getLongitude());
                    // new GetAddressTask().execute(latLng);
                    String str = "Latitude:" + location.getLatitude() + ", Longitude:" + location.getLongitude();
                    Log.v("Location current ", str);
                    // Adding a marker
                    currentlocationmarker = new MarkerOptions().position(
                            currentLatLag)
                            .title("You are at");
                    currentlocationmarker.icon(BitmapDescriptorFactory
                            .defaultMarker(BitmapDescriptorFactory.HUE_AZURE));
                    googleMap.addMarker(currentlocationmarker);

                    CameraPosition cameraPosition = new CameraPosition.Builder()
                            .target(currentLatLag).zoom(11).build();

                    googleMap.animateCamera(CameraUpdateFactory
                            .newCameraPosition(cameraPosition));


                }
            }

        } else {

            alertbox("Gps Status!!", "Your GPS is: OFF");

        }
    }

    /*----------Method to create an AlertBox ------------- */
    protected void alertbox(String title, String mymessage) {
        AlertDialog.Builder builder = new AlertDialog.Builder(Map_Search.this);
        builder.setMessage("Your Device's GPS is Disable")
                .setCancelable(false)
                .setTitle("** Gps Status **")
                .setPositiveButton("Settings",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                dialog.cancel();

                                Intent myIntent = new Intent(
                                        Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                startActivity(myIntent);

                                 //locationMap();

                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // cancel the dialog box
                                dialog.cancel();

                            }
                        });
        AlertDialog alert = builder.create();
        if(!alert.isShowing()) {
            alert.show();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (isNetworkEnabled && isGPSEnabled) {
            locationMap();
        }
    }

    public class GetAddressTask extends AsyncTask<LatLng, Void, Integer> {
        private LatLng loc;
        String addressText;
        String CityName = "";

        @Override
        protected Integer doInBackground(LatLng... params) {
            int mFinalFlag = 0;
            loc = params[0];
            Geocoder geoCoder = new Geocoder(Map_Search.this, new Locale("en", "IN"));
            // Locale.getDefault());
            try {
                List<Address> addresses = geoCoder.getFromLocation(
                        loc.latitude, loc.longitude, 1);

                if (addresses != null && addresses.size() > 0) {
                    Address address = addresses.get(0);
                    addressText = String.format(
                            "%s, %s, %s",
                            address.getMaxAddressLineIndex() > 0 ? address
                                    .getAddressLine(0) : "", address
                                    .getLocality(), address.getCountryName());

                    if (address.getLocality().toString() != null)
                        CityName = address.getLocality().toString();
                }
            } catch (IOException ex) {
            } catch (Exception e2) {
                e2.printStackTrace();
            }
            return mFinalFlag;
        }

        @Override
        protected void onPostExecute(Integer result) {
            super.onPostExecute(result);
            // Add marker in google map


            if (!CityName.equals("")) {
                NetworkConnection net = new NetworkConnection(Map_Search.this);
                if (net.CheckInternet()) {
                    new GetAddressDetailasWithCity().execute(CityName);
                } else {
                    box.showAlertbox(getResources().getString(R.string.nointernetmsg));
                }

            }

        }
    }


    @Override
    public void onLocationChanged(Location location) {
        // TODO Auto-generated method stub
        googleMap.clear();
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(location
                .getLatitude(), location.getLongitude())));
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(
                location.getLatitude(), location.getLongitude()), 16));
        currentLatLag = new LatLng(location.getLatitude(),
                location.getLongitude());

        currentlocationmarker = new MarkerOptions().position(
                currentLatLag)
                .title("You are at");
        currentlocationmarker.icon(BitmapDescriptorFactory
                .defaultMarker(BitmapDescriptorFactory.HUE_AZURE));
        googleMap.addMarker(currentlocationmarker);

        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(currentLatLag).zoom(12).build();

        googleMap.animateCamera(CameraUpdateFactory
                .newCameraPosition(cameraPosition));
        new GetAddressTask().execute(currentLatLag);


    }

    @Override
    public void onProviderDisabled(String provider) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onProviderEnabled(String provider) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        // TODO Auto-generated method stub

    }


    private void checkInternet() {
        NetworkConnection net = new NetworkConnection(Map_Search.this);
        if (net.CheckInternet()) {
            new GetSpinnerStateData().execute();
        } else {
            box.showAlertbox(getResources().getString(R.string.nointernetmsg));
        }
    }


    class GetSpinnerStateData extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new SpotsDialog(Map_Search.this,R.style.Custom);
            progressDialog.setCancelable(false);
            progressDialog.show();

            StatespinnerList.clear();
            StatespinnerList.add("Select State");
        }

        @Override
        protected String doInBackground(String... strings) {
            try {

                ServerConnection server = new ServerConnection();
                connection = server.getConnection();
                stmt = connection.createStatement();
                String query = "select distinct state from  registration  where  usertype = '" + findertype + "'";
                Log.v("Query", query);
                rset = stmt.executeQuery(query);
                while (rset.next()) {
                    StatespinnerList.add(rset.getString("state"));
                }
                if (StatespinnerList.size() == 1) {
                    return "nodata";
                }
                connection.close();
                stmt.close();
                rset.close();
                return "success";
            } catch (Exception e) {
                return "notsuccess";
            }

        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressDialog.dismiss();
            if (s.equals("success")) {

                statespinner.setAdapter(new ArrayAdapter<String>(Map_Search.this, R.layout.simple_spinner_item, StatespinnerList));
            } else if (s.equals("success")) {
                box.showAlertbox(findertype +" details not found !");
            } else {
                box.showAlertbox(getResources().getString(R.string.nointernetmsg));
            }
        }
    }


    class GetSpinnerCityData extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            CityspinnerList.clear();
            CityspinnerList.add("All");
        }

        @Override
        protected String doInBackground(String... strings) {
            try {

                ServerConnection server = new ServerConnection();
                connection = server.getConnection();
                stmt = connection.createStatement();
                String query = "select distinct city from  registration  where usertype = '" + findertype + "' and  state = '" + strings[0] + "'";
                Log.v("Query", query);
                rset = stmt.executeQuery(query);
                while (rset.next()) {
                    CityspinnerList.add(rset.getString("city"));
                }
                if (CityspinnerList.size() == 1) {
                    return "nodata";
                }
                connection.close();
                stmt.close();
                rset.close();
                return "success";
            } catch (Exception e) {
                return "notsuccess";
            }

        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressDialog.dismiss();
            if (s.equals("success")) {


                Log.e("City List", CityspinnerList.toString());

                cityspinner.setAdapter(new ArrayAdapter<String>(Map_Search.this, R.layout.simple_spinner_item, CityspinnerList));
            } else if (s.equals("success")) {
                box.showAlertbox(findertype +" details not found !");
            } else {
                box.showAlertbox(getResources().getString(R.string.nointernetmsg));
            }
        }
    }


    class GetAddressDetailasWithCity extends AsyncTask<String, Void, String> {
        String CityName;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            ShopnameList.clear();
            LatitudeList.clear();
            LongtitudeList.clear();
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                CityName = strings[0];
                ServerConnection server = new ServerConnection();
                connection = server.getConnection();
                stmt = connection.createStatement();
                String query = "select distinct shopname,latttitude,longtitude from registration where usertype = '" + findertype + "' and  latttitude != 'null' and  longtitude != 'null' and city ='" + strings[0] + "'";
                Log.v("Query", query);
                rset = stmt.executeQuery(query);
                while (rset.next()) {
                    ShopnameList.add(rset.getString("shopname"));
                    LatitudeList.add(rset.getString("latttitude"));
                    LongtitudeList.add(rset.getString("longtitude"));
                }
                if (ShopnameList.isEmpty()) {
                    return "nodata";
                }
                connection.close();
                stmt.close();
                rset.close();
                return "success";
            } catch (Exception e) {
                return "notsuccess";
            }

        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            // progressbar.dismiss();
            if (s.equals("success")) {
                try {
                    googleMap.clear();

                    if (currentLatLag != null) {

                        currentlocationmarker = new MarkerOptions().position(
                                currentLatLag)
                                .title("You are at");
                        currentlocationmarker.icon(BitmapDescriptorFactory
                                .defaultMarker(BitmapDescriptorFactory.HUE_AZURE));
                        googleMap.addMarker(currentlocationmarker);

                        CameraPosition cameraPosition = new CameraPosition.Builder()
                                .target(currentLatLag).zoom(4).build();

                        googleMap.animateCamera(CameraUpdateFactory
                                .newCameraPosition(cameraPosition));
                    }

                    for (int i = 0; i < ShopnameList.size(); i++) {
                        LatLng sydney = new LatLng(Float.valueOf(LatitudeList.get(i)), Float.valueOf(LongtitudeList.get(i)));
                        googleMap.addMarker(new MarkerOptions().position(sydney).title(ShopnameList.get(i)));
                        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney, 4));

                    }
                } catch (Exception e) {


                    //box.showAlertbox(findertype + "  location not found !");
                }


            } else if (s.equals("nodata")) {
                //box.showAlertbox(findertype + "  location not found !");

                //new FindLatLongFromAddress().execute(CityName);
            } else {
                box.showAlertbox(getResources().getString(R.string.slow_internet));
            }
        }
    }


    class GetAddressDetailas extends AsyncTask<String, Void, String> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new SpotsDialog(Map_Search.this,R.style.Custom);
            progressDialog.setCancelable(false);
            progressDialog.show();

            ShopnameList.clear();
            LatitudeList.clear();
            LongtitudeList.clear();
        }

        @Override
        protected String doInBackground(String... strings) {
            try {

                ServerConnection server = new ServerConnection();
                connection = server.getConnection();
                stmt = connection.createStatement();
                //String query = "select distinct shopname,latttitude,longtitude from registration where usertype = '" + findertype + "' and  latttitude != 'null' and  longtitude != 'null' ";
                Log.v("Query", strings[0]);
                rset = stmt.executeQuery(strings[0]);
                while (rset.next()) {
                    ShopnameList.add(rset.getString("shopname"));
                    LatitudeList.add(rset.getString("latttitude"));
                    LongtitudeList.add(rset.getString("longtitude"));
                }
                if (ShopnameList.isEmpty()) {
                    return "nodata";
                }
                connection.close();
                stmt.close();
                rset.close();
                return "success";
            } catch (Exception e) {
                return "notsuccess";
            }

        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressDialog.dismiss();
            if (s.equals("success")) {

                try {
                    googleMap.clear();
                    if (currentLatLag != null) {
                        currentlocationmarker = new MarkerOptions().position(
                                currentLatLag)
                                .title("You are at");
                        currentlocationmarker.icon(BitmapDescriptorFactory
                                .defaultMarker(BitmapDescriptorFactory.HUE_AZURE));
                        googleMap.addMarker(currentlocationmarker);

                        CameraPosition cameraPosition = new CameraPosition.Builder()
                                .target(currentLatLag).zoom(4).build();

                        googleMap.animateCamera(CameraUpdateFactory
                                .newCameraPosition(cameraPosition));
                    }
                    for (int i = 0; i < ShopnameList.size(); i++) {


                        Log.v("LatitudeList ++ ", LatitudeList.get(i));
                        Log.v("LongtitudeList ++ ", LongtitudeList.get(i));

                        LatLng sydney = new LatLng(Float.valueOf(LatitudeList.get(i)), Float.valueOf(LongtitudeList.get(i)));
                        googleMap.addMarker(new MarkerOptions().position(sydney).title(ShopnameList.get(i)));
                        // googleMap.animateCamera(CameraUpdateFactory.zoomIn());
                        // googleMap.animateCamera(CameraUpdateFactory.zoomTo(15), 2000, null);
                        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney, 4));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.v("Error +", e.getMessage());
                    // box.showAlertbox(findertype + "  location not found !");
                }

            } else if (s.equals("nodata")) {
                googleMap.clear();
                if (currentLatLag != null) {
                    currentlocationmarker = new MarkerOptions().position(
                            currentLatLag)
                            .title("You are at");
                    currentlocationmarker.icon(BitmapDescriptorFactory
                            .defaultMarker(BitmapDescriptorFactory.HUE_AZURE));
                    googleMap.addMarker(currentlocationmarker);

                    CameraPosition cameraPosition = new CameraPosition.Builder()
                            .target(currentLatLag).zoom(4).build();

                    googleMap.animateCamera(CameraUpdateFactory
                            .newCameraPosition(cameraPosition));
                }

                box.showAlertbox(findertype + "  location not found !");

            } else {
                box.showAlertbox(getResources().getString(R.string.slow_internet));
            }
        }
    }





    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.popup, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // ...

        switch (item.getItemId()) {
            case R.id.about:
                // search action
                startActivity(new Intent(Map_Search.this, AboutActivity.class));
                return true;
            case R.id.product:
                // location found
                startActivity(new Intent(Map_Search.this, ProductCatalog.class));
                return true;
            case R.id.price:
                // refresh
                startActivity(new Intent(Map_Search.this, PriceList.class));
                return true;
            case R.id.wsd:
                // help action
                if(!Usertype.equals("Distributor"))
                {
                    alertbox.showAlertbox("You are not allowed to view this page !");
                }
                else
                {
                    startActivity(new Intent(Map_Search.this, WSDActivity.class));
                }
                return true;
            case R.id.retailer:
                // check for updates action
                if(!Usertype.equals("Retailer"))
                {
                    alertbox.showAlertbox("You are not allowed to view this page !");
                }
                else {
                    startActivity(new Intent(Map_Search.this, Map_Search.class));
                }
                return true;
            case R.id.fieldengi:
                // check for updates action

                if(Usertype.equals("Rane Employee") || Usertype.equals("Sales Executives")|| Usertype.equals("MSR"))
                {

                    startActivity(new Intent(Map_Search.this, EngineerActivity.class));
                }
                else
                {
                    alertbox.showAlertbox("You are not allowed to view this page !");
                }
                return true;
            case R.id.mech:
                // check for updates action
                if(!Usertype.equals("Mechanic"))
                {
                    alertbox.showAlertbox("You are not allowed to view this page !");
                }
                else
                {
                    startActivity(new Intent(Map_Search.this, MechanicActivity.class));
                }
                return true;
            case R.id.customer:
                // check for updates action

                if(!Usertype.equals("Others") || !Usertype.equals("Fleet Operators"))
                {
                    alertbox.showAlertbox("You are not allowed to view this page !");
                }
                else
                {
                    startActivity(new Intent(Map_Search.this, CustomerActivity.class));
                }
                return true;
            case R.id.notification:
                // check for updates action
                startActivity(new Intent(Map_Search.this, NotificationActivity.class));
                return true;
            case R.id.edit:
                // check for updates action
                Intent edit = new Intent(Map_Search.this,EditProfileActivity.class);
                edit.putExtra("usertype",Usertype);
                edit.putExtra("mobile",Usermobile);
                edit.putExtra("email",Useremail);
                startActivity(edit);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }




}


