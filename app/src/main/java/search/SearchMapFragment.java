package search;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.brainagic.catalogue.EditProfileActivity;
import com.brainagic.catalogue.catalogue.R;

import about.AboutActivity;
import alertbox.Alertbox;
import customer.CustomerActivity;
import engineer.EngineerActivity;
import home.HomeActivity;
import mechanic.MechanicActivity;
import notification.NotificationActivity;
import product.PriceList;
import product.ProductCatalog;
import retailer.RetailerActivity;
import wsd.WSDActivity;

;


public class SearchMapFragment extends Activity {
 
    
    private String Usertype,Useremail,Usermobile;
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    private Alertbox alertbox = new Alertbox(SearchMapFragment.this);
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_map);
        // getActionBar().setIcon(R.drawable.logo);
        myshare = getSharedPreferences("Registration", MODE_PRIVATE);
        editor = myshare.edit();
        editor.putString("findertype", getIntent().getStringExtra("findertype").toString()).commit();
        getActionBar().setLogo(R.drawable.logo);
        Useremail = myshare.getString("Email","") ;
        Usermobile = myshare.getString("MobNo","") ;
        Usertype = myshare.getString("Usertype", "");
        TextView title = (TextView) findViewById(R.id.title);
        if(getIntent().getStringExtra("findertype").toString().equals("Distributor"))
        {
            title.setText("Find Whole Sale Distributor (WSD)");
        }
        else {
            title.setText("Find " + getIntent().getStringExtra("findertype").toString());
        }


        RelativeLayout mapbtn = (RelativeLayout) findViewById(R.id.map);
        RelativeLayout tablebtn = (RelativeLayout) findViewById(R.id.table);

        mapbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                startActivity(new Intent(SearchMapFragment.this,Map_Search.class));
            }
        });

        tablebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                startActivity(new Intent(SearchMapFragment.this,Grid_Search.class));
            }
        });


        // for home abd back
        TextView home = (TextView) findViewById(R.id.fontDesign1);
        TextView back = (TextView) findViewById(R.id.fontDesign2);
        home.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                Intent ho = new Intent(SearchMapFragment.this, HomeActivity.class);
                ho.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(ho);
            }
        });
        back.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                onBackPressed();
            }
        });
    }




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.popup, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // ...

        switch (item.getItemId()) {
            case R.id.about:
                // search action
                startActivity(new Intent(SearchMapFragment.this, AboutActivity.class));
                return true;
            case R.id.product:
                // location found
                startActivity(new Intent(SearchMapFragment.this, ProductCatalog.class));
                return true;
            case R.id.price:
                // refresh
                startActivity(new Intent(SearchMapFragment.this, PriceList.class));
                return true;
            case R.id.wsd:
                // help action
                if(!Usertype.equals("Distributor"))
                {
                    alertbox.showAlertbox("You are not allowed to view this page !","auth");
                }
                else
                {
                    startActivity(new Intent(SearchMapFragment.this, WSDActivity.class));
                }
                return true;
            case R.id.retailer:
                // check for updates action
                if(!Usertype.equals("Retailer"))
                {
                    alertbox.showAlertbox("You are not allowed to view this page !","auth");
                }
                else {
                    startActivity(new Intent(SearchMapFragment.this, RetailerActivity.class));
                }
                return true;
            case R.id.fieldengi:
                // check for updates action

                if(Usertype.equals("Rane Employee") || Usertype.equals("Sales Executives")|| Usertype.equals("MSR"))
                {

                    startActivity(new Intent(SearchMapFragment.this, EngineerActivity.class));
                }
                else
                {
                    alertbox.showAlertbox("You are not allowed to view this page !","auth");
                }
                return true;
            case R.id.msr:
                // check for updates action

                if(Usertype.equals("MSR"))
                {

                    startActivity(new Intent(SearchMapFragment.this, EngineerActivity.class));
                }
                else
                {
                    alertbox.showAlertbox("You are not allowed to view this page !");
                }
                return true;
            case R.id.mech:
                // check for updates action
                if(!Usertype.equals("Mechanic"))
                {
                    alertbox.showAlertbox("You are not allowed to view this page !","auth");
                }
                else
                {
                    startActivity(new Intent(SearchMapFragment.this, MechanicActivity.class));
                }
                return true;
            case R.id.customer:
                // check for updates action

                if(!Usertype.equals("Others") || !Usertype.equals("Fleet Operators"))
                {
                    alertbox.showAlertbox("You are not allowed to view this page !","auth");
                }
                else
                {
                    startActivity(new Intent(SearchMapFragment.this, CustomerActivity.class));
                }
                return true;
            case R.id.notification:
                // check for updates action
                startActivity(new Intent(SearchMapFragment.this, NotificationActivity.class));
                return true;
            case R.id.edit:
                // check for updates action
                Intent edit = new Intent(SearchMapFragment.this,EditProfileActivity.class);
                edit.putExtra("usertype",Usertype);
                edit.putExtra("mobile",Usermobile);
                edit.putExtra("email",Useremail);
                startActivity(edit);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }




}




/*editor.putString("findertype", getIntent().getStringExtra("findertype").toString()).commit();

        TabAdapter = new TabPagerAdapter(getSupportFragmentManager());

        Tab = (ViewPager) findViewById(R.id.pager);
        Tab.setOnPageChangeListener(
                new ViewPager.SimpleOnPageChangeListener() {
                    @Override
                    public void onPageSelected(int position) {

                        actionBar = getActionBar();
                        actionBar.setSelectedNavigationItem(position);
                    }
                });
        Tab.setAdapter(TabAdapter);

        actionBar = getActionBar();
        //Enable Tabs on Action Bar
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        ActionBar.TabListener tabListener = new ActionBar.TabListener() {

            @Override
            public void onTabReselected(android.app.ActionBar.Tab tab,
                                        FragmentTransaction ft) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onTabSelected(ActionBar.Tab tab, FragmentTransaction ft) {

                Tab.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(android.app.ActionBar.Tab tab,
                                        FragmentTransaction ft) {
                // TODO Auto-generated method stub

            }
        };


        //Add New Tab
        actionBar.addTab(actionBar.newTab().setText("Map").setTabListener(tabListener));
        actionBar.addTab(actionBar.newTab().setText("Search").setTabListener(tabListener));*/