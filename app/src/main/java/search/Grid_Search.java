package search;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.brainagic.catalogue.EditProfileActivity;
import com.brainagic.catalogue.catalogue.R;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import about.AboutActivity;
import adapter.Grid_Search_Adapter;
import alertbox.Alertbox;
import customer.CustomerActivity;
import dmax.dialog.SpotsDialog;
import engineer.EngineerActivity;
import home.HomeActivity;
import login.LoginActivity;
import mechanic.MechanicActivity;
import multispinner.MultiSelectionSpinner;
import network.NetworkConnection;
import notification.NotificationActivity;
import persistency.ServerConnection;
import product.PriceList;
import product.ProductCatalog;
import registration.UpdateLocation;
import wsd.WSDActivity;

;

public class Grid_Search extends Activity {
    private Context context;
    private TextView searchword;
    private Button searchbtn;

    private ArrayList<String> NameList, MobileList, StateList, CityList, PincodeList, ShopList, EmailList, UsertypeList, LatitudeList, LongtitudeList, DoorList, StreetList, AreaList;

    private ArrayList<String> TrucksBusList, ThreeWList, TwoWist, CarList, LCVList, OthersList, TractorList;

    private Connection connection;
    private Statement stmt;
    private ResultSet rset;
    private ListView listView;
    private String findertype;
    private Spinner statespinner, cityspinner;
    private String SelectedState, SelectedCity;
    private ArrayList<String> StatespinnerList, CityspinnerList;
    private TextView tittletxt, updatetext, catagotytext;
    private boolean islogin;
    private HorizontalScrollView horiList;
    private SpotsDialog progressDialog;
    private String Usertype, Useremail, Usermobile;
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    private Alertbox alertbox = new Alertbox(Grid_Search.this);
    private Alertbox box = new Alertbox(Grid_Search.this);
    private MultiSelectionSpinner multiSelectionSpinner;
    private List<String> typeofmech = new ArrayList<String>();
    private List<Integer> indicesint;
    private boolean truckbus, lcv, car, three, two, tractor, other;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.grid_search);
        getActionBar().setLogo(R.drawable.logo);
        CityList = new ArrayList<String>();
        StateList = new ArrayList<String>();
        box = new Alertbox(Grid_Search.this);


        searchword = (TextView) findViewById(R.id.searchedit);
        listView = (ListView) findViewById(R.id.listView);
        updatetext = (TextView) findViewById(R.id.update);
        catagotytext = (TextView) findViewById(R.id.catagorytxt);

        horiList = (HorizontalScrollView) findViewById(R.id.horilist);
        horiList.setVisibility(View.GONE);

        statespinner = (Spinner) findViewById(R.id.statespinner);
        cityspinner = (Spinner) findViewById(R.id.cityspinner);

        final String[] array = {"Truck & Bus", "LCV", "Car", "3W", "2W", "Tractor", "Others"};
        multiSelectionSpinner = (MultiSelectionSpinner) findViewById(R.id.typeofchspinner);


        StatespinnerList = new ArrayList<String>();
        CityspinnerList = new ArrayList<String>();

        progressDialog = new SpotsDialog(Grid_Search.this, R.style.Custom);
        progressDialog.setCancelable(false);

        myshare = Grid_Search.this.getSharedPreferences("Registration", Grid_Search.this.MODE_PRIVATE);
        editor = myshare.edit();
        findertype = myshare.getString("findertype", "");
        Usertype = myshare.getString("Usertype", "");
        Useremail = myshare.getString("Email", "");
        Usermobile = myshare.getString("MobNo", "");

        if (Usertype.equals("Sales Executives")) {
            updatetext.setVisibility(View.VISIBLE);

        } else {
            updatetext.setVisibility(View.GONE);
        }


        islogin = myshare.getBoolean("islogin", false);

        tittletxt = (TextView) findViewById(R.id.title);

        if (myshare.getString("findertype", "").toString().equals("Distributor")) {
            tittletxt.setText("Find Whole Sale Distributor (WSD)");
            multiSelectionSpinner.setVisibility(View.GONE);
            catagotytext.setVisibility(View.GONE);
        } else {
            tittletxt.setText("Find " + myshare.getString("findertype", "").toString());
            multiSelectionSpinner.setVisibility(View.VISIBLE);
            catagotytext.setVisibility(View.VISIBLE);
        }

        //tittletxt.setText("Find " + findertype);

        context = Grid_Search.this;


        multiSelectionSpinner.setItems(array);
        multiSelectionSpinner.setSelection(new int[]{6});
        multiSelectionSpinner.setDialogName("Select type of " + myshare.getString("findertype", "").toString());
        multiSelectionSpinner.setListener(new MultiSelectionSpinner.OnMultipleItemsSelectedListener() {
            @Override
            public void selectedIndices(List<Integer> indices) {
                indicesint = indices;
            }

            @Override
            public void selectedStrings(List<String> strings) {
                // Toast.makeText(FirstRegistration.this, strings.toString(), Toast.LENGTH_LONG).show();
                typeofmech = strings;


                if (SelectedCity != null) {
                    if (typeofmech.size() != 0) {


                        if (!SelectedCity.equals("All")) {
                            String selectquery = "select *  from registration  where city = '" + SelectedCity + "' and usertype = '" + findertype + "' and (";
                            //  String query = "";


                            for (int i = 0; i < typeofmech.size(); i++) {

                                if (typeofmech.get(i).toString().equals("Truck & Bus")) {

                                    truckbus = true;

                                    selectquery += " IsTruckBus  ='" + truckbus + "' and";

                                }
                                if (typeofmech.get(i).toString().equals("LCV")) {
                                    lcv = true;

                                    selectquery += "  IsLCV = '" + lcv + "' and";

                                }
                                if (typeofmech.get(i).toString().equals("Car")) {
                                    car = true;

                                    selectquery += "   IsCar ='" + car + "' and";

                                }
                                if (typeofmech.get(i).toString().equals("3W")) {
                                    three = true;

                                    selectquery += "  Is3W ='" + three + "' and";

                                }
                                if (typeofmech.get(i).toString().equals("2W")) {
                                    two = true;
                                    selectquery += "   Is2W ='" + two + "' and";

                                }
                                if (typeofmech.get(i).toString().equals("Tractor")) {
                                    tractor = true;
                                    selectquery += "   IsTractor = '" + tractor + "' and";

                                }
                                if (typeofmech.get(i).toString().equals("Others")) {
                                    other = true;
                                    selectquery += "  IsOthers = '" + other + "' and ";

                                }


                            }

                            selectquery += " 1=1) and 1=1";

                            Log.v("query city selected", selectquery);
                            new GetAddressDetailas().execute(selectquery);

                        }
                    } else {

                        String selectquery = "select *  from registration  where  usertype = '" + findertype + "' and state =  '" + SelectedState + "' and city = '" + SelectedCity + "' and (";

                        for (int i = 0; i < typeofmech.size(); i++) {

                            if (typeofmech.get(i).toString().equals("Truck & Bus")) {

                                truckbus = true;

                                selectquery += " IsTruckBus  ='" + truckbus + "' and";

                            }
                            if (typeofmech.get(i).toString().equals("LCV")) {
                                lcv = true;

                                selectquery += "  IsLCV = '" + lcv + "' and";

                            }
                            if (typeofmech.get(i).toString().equals("Car")) {
                                car = true;

                                selectquery += "   IsCar ='" + car + "' and";

                            }
                            if (typeofmech.get(i).toString().equals("3W")) {
                                three = true;

                                selectquery += "  Is3W ='" + three + "' and";

                            }
                            if (typeofmech.get(i).toString().equals("2W")) {
                                two = true;
                                selectquery += "   Is2W ='" + two + "' and";

                            }
                            if (typeofmech.get(i).toString().equals("Tractor")) {
                                tractor = true;
                                selectquery += "   IsTractor = '" + tractor + "' and";

                            }
                            if (typeofmech.get(i).toString().equals("Others")) {
                                other = true;
                                selectquery += "  IsOthers = '" + other + "' and ";

                            }


                        }

                        selectquery += " 1=1) and 1=1";

                        Log.v("query city is all", selectquery);
                        new GetAddressDetailas().execute(selectquery);
                    }

                } else {
                    String selectquery = "select *  from registration  where  usertype = '" + findertype + "' and (";
                    //  String query = "";


                    for (int i = 0; i < typeofmech.size(); i++) {

                        if (typeofmech.get(i).toString().equals("Truck & Bus")) {

                            truckbus = true;

                            selectquery += " IsTruckBus  ='" + truckbus + "' and";

                        }
                        if (typeofmech.get(i).toString().equals("LCV")) {
                            lcv = true;

                            selectquery += "  IsLCV = '" + lcv + "' and";

                        }
                        if (typeofmech.get(i).toString().equals("Car")) {
                            car = true;

                            selectquery += "   IsCar ='" + car + "' and";

                        }
                        if (typeofmech.get(i).toString().equals("3W")) {
                            three = true;

                            selectquery += "  Is3W ='" + three + "' and";

                        }
                        if (typeofmech.get(i).toString().equals("2W")) {
                            two = true;
                            selectquery += "   Is2W ='" + two + "' and";

                        }
                        if (typeofmech.get(i).toString().equals("Tractor")) {
                            tractor = true;
                            selectquery += "   IsTractor = '" + tractor + "' and";

                        }
                        if (typeofmech.get(i).toString().equals("Others")) {
                            other = true;
                            selectquery += "  IsOthers = '" + other + "' and ";

                        }


                    }

                    selectquery += " 1=1) and 1=1";

                    Log.v("query city not select", selectquery);
                    new GetAddressDetailas().execute(selectquery);
                }
            }
        });


        statespinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long l) {

                SelectedState = parent.getItemAtPosition(position).toString();
                if (!SelectedState.equals("Select State")) {
                    new GetSpinnerCityData().execute(SelectedState);
                } else {

                    multiSelectionSpinner.setSelection(new int[]{});

                    CityspinnerList.clear();
                    cityspinner.setAdapter(new ArrayAdapter<String>(Grid_Search.this, R.layout.simple_spinner_item, CityspinnerList));
                    horiList.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        cityspinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long l) {


                SelectedCity = parent.getItemAtPosition(position).toString();


                if (SelectedCity.equals("All")) {


                    if (typeofmech.size() != 0) {
                        String selectquery = "select *  from registration  where state =  '" + SelectedState + "' and usertype = '" + findertype + "' and (";

                        for (int i = 0; i < typeofmech.size(); i++) {

                            if (typeofmech.get(i).toString().equals("Truck & Bus")) {

                                truckbus = true;

                                selectquery += " IsTruckBus  ='" + truckbus + "' and";

                            }
                            if (typeofmech.get(i).toString().equals("LCV")) {
                                lcv = true;

                                selectquery += "  IsLCV = '" + lcv + "' and";

                            }
                            if (typeofmech.get(i).toString().equals("Car")) {
                                car = true;

                                selectquery += "   IsCar ='" + car + "' and";

                            }
                            if (typeofmech.get(i).toString().equals("3W")) {
                                three = true;

                                selectquery += "  Is3W ='" + three + "' and";

                            }
                            if (typeofmech.get(i).toString().equals("2W")) {
                                two = true;
                                selectquery += "   Is2W ='" + two + "' and";

                            }
                            if (typeofmech.get(i).toString().equals("Tractor")) {
                                tractor = true;
                                selectquery += "   IsTractor = '" + tractor + "' and";

                            }
                            if (typeofmech.get(i).toString().equals("Others")) {
                                other = true;
                                selectquery += "  IsOthers = '" + other + "' and ";

                            }


                        }

                        selectquery += " 1=1) and 1=1";

                        Log.v("query city not select", selectquery);
                        new GetAddressDetailas().execute(selectquery);

                    }
                    else
                    {
                        String selectquery = "select *  from registration  where state =  '" + SelectedState + "' and usertype = '" + findertype + "' ";
                        Log.v("query city not select", selectquery);
                        new GetAddressDetailas().execute(selectquery);
                    }
                } else {

                    if (typeofmech.size() != 0) {
                        String selectquery = "select *  from registration  where city = '" + SelectedCity + "' and usertype = '" + findertype + "' and (";

                        for (int i = 0; i < typeofmech.size(); i++) {

                            if (typeofmech.get(i).toString().equals("Truck & Bus")) {

                                truckbus = true;

                                selectquery += " IsTruckBus  ='" + truckbus + "' and";

                            }
                            if (typeofmech.get(i).toString().equals("LCV")) {
                                lcv = true;

                                selectquery += "  IsLCV = '" + lcv + "' and";

                            }
                            if (typeofmech.get(i).toString().equals("Car")) {
                                car = true;

                                selectquery += "   IsCar ='" + car + "' and";

                            }
                            if (typeofmech.get(i).toString().equals("3W")) {
                                three = true;

                                selectquery += "  Is3W ='" + three + "' and";

                            }
                            if (typeofmech.get(i).toString().equals("2W")) {
                                two = true;
                                selectquery += "   Is2W ='" + two + "' and";

                            }
                            if (typeofmech.get(i).toString().equals("Tractor")) {
                                tractor = true;
                                selectquery += "   IsTractor = '" + tractor + "' and";

                            }
                            if (typeofmech.get(i).toString().equals("Others")) {
                                other = true;
                                selectquery += "  IsOthers = '" + other + "' and ";

                            }


                        }

                        selectquery += " 1=1) and 1=1";

                        Log.v("query city not select", selectquery);
                        new GetAddressDetailas().execute(selectquery);
                    } else {
                        String query = "select *  from registration  where city = '" + SelectedCity + "' and usertype = '" + findertype + "'";
                        Log.v("spiner query", query);
                        new GetAddressDetailas().execute(query);
                    }


                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                if (myshare.getString("Usertype", "").toString().equals("Rane Employee")) {


                    if (islogin) {
                        Intent updateIntent = new Intent(context, UpdateLocation.class);
                        updateIntent.putExtra("usertype", UsertypeList.get(position));
                        updateIntent.putExtra("mobile", MobileList.get(position));
                        updateIntent.putExtra("email", EmailList.get(position));
                        startActivity(updateIntent);

                    } else {
                        Intent updateIntent = new Intent(context, LoginActivity.class);
                        updateIntent.putExtra("usertype", UsertypeList.get(position));
                        updateIntent.putExtra("mobile", MobileList.get(position));
                        updateIntent.putExtra("email", EmailList.get(position));
                        startActivity(updateIntent);
                    }
                }

            }
        });


        // for home abd back
        TextView home = (TextView) findViewById(R.id.fontDesign1);
        TextView back = (TextView) findViewById(R.id.fontDesign2);
        home.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                Intent ho = new Intent(Grid_Search.this, HomeActivity.class);
                ho.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(ho);
            }
        });
        back.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                onBackPressed();
            }
        });

        checkInternet();


    }


    private void checkInternet() {
        NetworkConnection net = new NetworkConnection(Grid_Search.this);
        if (net.CheckInternet()) {
            new GetSpinnerStateData().execute();
        } else {
            box.showAlertboxwithback(getResources().getString(R.string.nointernetmsg));
        }
    }


    class GetSpinnerStateData extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            StatespinnerList.clear();

            progressDialog.show();
            StatespinnerList.add("Select State");
        }

        @Override
        protected String doInBackground(String... strings) {
            try {

                ServerConnection server = new ServerConnection();
                connection = server.getConnection();
                stmt = connection.createStatement();
                String query = "select distinct state from  registration  where  usertype = '" + findertype + "'";
                Log.v("Query", query);
                rset = stmt.executeQuery(query);
                while (rset.next()) {
                    StatespinnerList.add(rset.getString("state"));
                }
                if (StatespinnerList.size() == 1) {
                    return "nodata";
                }
                connection.close();
                stmt.close();
                rset.close();
                return "success";
            } catch (Exception e) {
                return "notsuccess";
            }

        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressDialog.dismiss();
            if (s.equals("success")) {
                statespinner.setAdapter(new ArrayAdapter<String>(Grid_Search.this, R.layout.simple_spinner_item, StatespinnerList));
            } else if (s.equals("nodata")) {
                box.showAlertbox(findertype + " details not found !");
            } else {
                box.showAlertboxwithback(getResources().getString(R.string.nointernetmsg));
            }
        }
    }


    class GetSpinnerCityData extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            CityspinnerList.clear();
            CityspinnerList.add("All");
        }

        @Override
        protected String doInBackground(String... strings) {
            try {

                ServerConnection server = new ServerConnection();
                connection = server.getConnection();
                stmt = connection.createStatement();
                String query = "select distinct city from  registration  where usertype = '" + findertype + "' and  state = '" + strings[0] + "'";
                Log.v("Query", query);
                rset = stmt.executeQuery(query);
                while (rset.next()) {
                    CityspinnerList.add(rset.getString("city"));
                }
                if (CityspinnerList.isEmpty()) {
                    return "nodata";
                }
                connection.close();
                stmt.close();
                rset.close();
                return "success";
            } catch (Exception e) {
                return "notsuccess";
            }

        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (s.equals("success")) {
                progressDialog.dismiss();
                cityspinner.setAdapter(new ArrayAdapter<String>(Grid_Search.this, R.layout.simple_spinner_item, CityspinnerList));
            } else if (s.equals("nodata")) {
                box.showAlertbox(findertype + " details not found !");
            } else {
                box.showAlertboxwithback(getResources().getString(R.string.nointernetmsg));
            }
        }
    }


    class GetAddressDetailas extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progressDialog.show();

            NameList = new ArrayList<String>();
            MobileList = new ArrayList<String>();
            EmailList = new ArrayList<String>();
            CityList = new ArrayList<String>();
            ShopList = new ArrayList<String>();
            StateList = new ArrayList<String>();
            PincodeList = new ArrayList<String>();
            UsertypeList = new ArrayList<String>();
            LatitudeList = new ArrayList<String>();
            LongtitudeList = new ArrayList<String>();
            DoorList = new ArrayList<String>();
            StreetList = new ArrayList<String>();
            AreaList = new ArrayList<String>();

            TrucksBusList = new ArrayList<String>();
            ThreeWList = new ArrayList<String>();
            TwoWist = new ArrayList<String>();
            CarList = new ArrayList<String>();
            LCVList = new ArrayList<String>();
            OthersList = new ArrayList<String>();
            TractorList = new ArrayList<String>();


        }

        @Override
        protected String doInBackground(String... strings) {
            try {

                ServerConnection server = new ServerConnection();
                connection = server.getConnection();
                stmt = connection.createStatement();
                //String query ="select * from registration where usertype ='"+findertype+"'";
                Log.v("Grid query", strings[0]);
                rset = stmt.executeQuery(strings[0]);
                while (rset.next()) {

                    NameList.add(rset.getString("name"));
                    MobileList.add(rset.getString("mobileno"));
                    StateList.add(rset.getString("state"));
                    CityList.add(rset.getString("city"));
                    ShopList.add(rset.getString("shopname"));
                    PincodeList.add(rset.getString("pincode"));
                    EmailList.add(rset.getString("email"));
                    UsertypeList.add(rset.getString("usertype"));
                    LatitudeList.add(rset.getString("latttitude"));
                    LongtitudeList.add(rset.getString("longtitude"));
                    DoorList.add(rset.getString("doorno"));
                    StreetList.add(rset.getString("street"));
                    AreaList.add(rset.getString("area"));


                    TrucksBusList.add(rset.getString("IsTruckBus"));
                    ThreeWList.add(rset.getString("Is3W"));
                    TwoWist.add(rset.getString("Is2W"));
                    CarList.add(rset.getString("IsCar"));
                    LCVList.add(rset.getString("IsLCV"));
                    OthersList.add(rset.getString("IsOthers"));
                    TractorList.add(rset.getString("IsTractor"));


                }
                if (NameList.isEmpty()) {
                    return "nodata";
                }
                connection.close();
                stmt.close();
                rset.close();
                return "success";
            } catch (Exception e) {
                e.printStackTrace();
                return "notsuccess";
            }

        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            if (s.equals("success")) {
                listView.setAdapter(new Grid_Search_Adapter(Grid_Search.this, NameList, MobileList, EmailList, ShopList, StateList, CityList, PincodeList, UsertypeList, LatitudeList, LongtitudeList, Usertype, findertype, islogin, DoorList, StreetList, AreaList,
                        TrucksBusList, ThreeWList, TwoWist, CarList, LCVList, OthersList, TractorList));
                progressDialog.dismiss();
                horiList.setVisibility(View.VISIBLE);
            } else if (s.equals("nodata")) {
                progressDialog.dismiss();
                horiList.setVisibility(View.GONE);
                progressDialog.dismiss();
                box.showAlertbox(findertype + " details not found !");
            } else {
                progressDialog.dismiss();
                box.showAlertboxwithback(getResources().getString(R.string.slow_internet));
            }
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.popup, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // ...

        switch (item.getItemId()) {
            case R.id.about:
                // search action
                startActivity(new Intent(Grid_Search.this, AboutActivity.class));
                return true;
            case R.id.product:
                // location found
                startActivity(new Intent(Grid_Search.this, ProductCatalog.class));
                return true;
            case R.id.price:
                // refresh
                startActivity(new Intent(Grid_Search.this, PriceList.class));
                return true;
            case R.id.wsd:
                // help action
                if (!Usertype.equals("Distributor")) {
                    alertbox.showAlertbox("You are not allowed to view this page !", "auth");
                } else {
                    startActivity(new Intent(Grid_Search.this, WSDActivity.class));
                }
                return true;
            case R.id.retailer:
                // check for updates action
                if (!Usertype.equals("Retailer")) {
                    alertbox.showAlertbox("You are not allowed to view this page !", "auth");
                } else {
                    startActivity(new Intent(Grid_Search.this, Grid_Search.class));
                }
                return true;
            case R.id.fieldengi:
                // check for updates action

                if (Usertype.equals("Rane Employee") || Usertype.equals("Sales Executives") || Usertype.equals("MSR")) {

                    startActivity(new Intent(Grid_Search.this, EngineerActivity.class));
                } else {
                    alertbox.showAlertbox("You are not allowed to view this page !", "auth");
                }
                return true;
            case R.id.msr:
                // check for updates action

                if (Usertype.equals("MSR")) {

                    startActivity(new Intent(Grid_Search.this, EngineerActivity.class));
                } else {
                    alertbox.showAlertbox("You are not allowed to view this page !");
                }
                return true;
            case R.id.mech:
                // check for updates action
                if (!Usertype.equals("Mechanic")) {
                    alertbox.showAlertbox("You are not allowed to view this page !");
                } else {
                    startActivity(new Intent(Grid_Search.this, MechanicActivity.class));
                }
                return true;
            case R.id.customer:
                // check for updates action

                if (!Usertype.equals("Others") || !Usertype.equals("Fleet Operators")) {
                    alertbox.showAlertbox("You are not allowed to view this page !", "auth");
                } else {
                    startActivity(new Intent(Grid_Search.this, CustomerActivity.class));
                }
                return true;
            case R.id.notification:
                // check for updates action
                startActivity(new Intent(Grid_Search.this, NotificationActivity.class));
                return true;
            case R.id.edit:
                // check for updates action
                Intent edit = new Intent(Grid_Search.this, EditProfileActivity.class);
                edit.putExtra("usertype", Usertype);
                edit.putExtra("mobile", Usermobile);
                edit.putExtra("email", Useremail);
                startActivity(edit);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


}
