package wsd;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.brainagic.catalogue.EditProfileActivity;
import com.brainagic.catalogue.catalogue.R;

import about.AboutActivity;
import alertbox.Alertbox;
import customer.CustomerActivity;
import engineer.EngineerActivity;
import home.HomeActivity;
import mechanic.MechanicActivity;
import notification.NotificationActivity;
import product.PriceList;
import product.ProductCatalog;
import registration.RegisterRetailerOrMech;
import retailer.RetailerActivity;
import salesteam.SalesTeamActivity;
import search.SearchMapFragment;

;

public class WSDActivity extends Activity {


	private String Usertype,Useremail,Usermobile;
	private SharedPreferences myshare;
	private SharedPreferences.Editor editor;
	private Alertbox alertbox = new Alertbox(WSDActivity.this);
	private Alertbox box = new Alertbox(WSDActivity.this);

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_wsd);
		getActionBar().setIcon(R.drawable.logo);

		myshare = getSharedPreferences("Registration", MODE_PRIVATE);
		editor = myshare.edit();
		Usertype = myshare.getString("Usertype", "");
		Useremail = myshare.getString("Email","") ;
		Usermobile = myshare.getString("MobNo","") ;
		
		RelativeLayout sales = (RelativeLayout) findViewById(R.id.sales); 

		RelativeLayout registerretailer = (RelativeLayout) findViewById(R.id.registretailer);
		RelativeLayout dealer = (RelativeLayout) findViewById(R.id.findretailer);
		RelativeLayout registermechr = (RelativeLayout) findViewById(R.id.registermech);


		sales.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				startActivity(new Intent(WSDActivity.this, SalesTeamActivity.class));

			}
		});

		registerretailer.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent reg = new Intent(WSDActivity.this, RegisterRetailerOrMech.class);
				reg.putExtra("registertype", "Retailer");
				startActivity(reg);

			}
		});
		registermechr.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent reg = new Intent(WSDActivity.this, RegisterRetailerOrMech.class);
				reg.putExtra("registertype", "Mechanic");
				startActivity(reg);

			}
		});
		dealer.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent find = new Intent(WSDActivity.this, SearchMapFragment.class);
				find.putExtra("findertype","Retailer");
				startActivity(find);

			}
		});



		// for home abd back0
				TextView home = (TextView) findViewById(R.id.fontDesign1);
				TextView back = (TextView) findViewById(R.id.fontDesign2);
				home.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub
						Intent ho = new Intent(WSDActivity.this,HomeActivity.class);
						ho.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
						startActivity(ho);
					}
				});
				back.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub
						onBackPressed();
					}
				});

	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.popup, menu);

		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// ...

		switch (item.getItemId()) {
			case R.id.about:
				// search action
				startActivity(new Intent(WSDActivity.this, AboutActivity.class));
				return true;
			case R.id.product:
				// location found
				startActivity(new Intent(WSDActivity.this, ProductCatalog.class));
				return true;
			case R.id.price:
				// refresh
				startActivity(new Intent(WSDActivity.this, PriceList.class));
				return true;
			case R.id.wsd:
				// help action
				if(!Usertype.equals("Distributor"))
				{
					alertbox.showAlertbox("You are not allowed to view this page !","auth");
				}
				else
				{
					startActivity(new Intent(WSDActivity.this, WSDActivity.class));
				}
				return true;
			case R.id.retailer:
				// check for updates action
				if(!Usertype.equals("Retailer"))
				{
					alertbox.showAlertbox("You are not allowed to view this page !","auth");
				}
				else {
					startActivity(new Intent(WSDActivity.this, RetailerActivity.class));
				}
				return true;
			case R.id.fieldengi:
				// check for updates action

				if(Usertype.equals("Rane Employee") || Usertype.equals("Sales Executives")|| Usertype.equals("MSR"))
				{

					startActivity(new Intent(WSDActivity.this, EngineerActivity.class));
				}
				else
				{
					alertbox.showAlertbox("You are not allowed to view this page !","auth");
				}
				return true;
			case R.id.msr:
				// check for updates action

				if(Usertype.equals("MSR"))
				{

					startActivity(new Intent(WSDActivity.this, EngineerActivity.class));
				}
				else
				{
					alertbox.showAlertbox("You are not allowed to view this page !");
				}
				return true;
			case R.id.mech:
				// check for updates action
				if(!Usertype.equals("Mechanic"))
				{
					alertbox.showAlertbox("You are not allowed to view this page !","auth");
				}
				else
				{
					startActivity(new Intent(WSDActivity.this, MechanicActivity.class));
				}
				return true;
			case R.id.customer:
				// check for updates action

				if(!Usertype.equals("Others") || !Usertype.equals("Fleet Operators"))
				{
					alertbox.showAlertbox("You are not allowed to view this page !","auth");
				}
				else
				{
					startActivity(new Intent(WSDActivity.this, CustomerActivity.class));
				}
				return true;
			case R.id.notification:
				// check for updates action
				startActivity(new Intent(WSDActivity.this, NotificationActivity.class));
				return true;
			case R.id.edit:
				// check for updates action
				Intent edit = new Intent(WSDActivity.this,EditProfileActivity.class);
				edit.putExtra("usertype",Usertype);
				edit.putExtra("mobile",Usermobile);
				edit.putExtra("email",Useremail);
				startActivity(edit);
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}
}
