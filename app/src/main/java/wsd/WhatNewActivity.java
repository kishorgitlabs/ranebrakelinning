package wsd;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.brainagic.catalogue.catalogue.R;

import home.HomeActivity;

;

public class WhatNewActivity extends Activity {

	private TextView wh1;
	private TextView wh2;
	private TextView wh3;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_what_new);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setHomeButtonEnabled(true);
		getActionBar().setIcon(R.drawable.logo);
		wh1 = (TextView)findViewById(R.id.wh1);
		wh2 = (TextView)findViewById(R.id.wh2);
		wh3 = (TextView)findViewById(R.id.wh3);

		wh1.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent go = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.rane.co.in/mediapress.html"));

				startActivity(go);
			}
		});

		wh2.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent go = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.rane.co.in/mediapress.html"));

				startActivity(go);
			}
		});
		wh3.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent go = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.rane.co.in/mediapress.html"));

				startActivity(go);
			}
		});

		
		// for home abd back
		TextView home = (TextView) findViewById(R.id.fontDesign1);
		TextView back = (TextView) findViewById(R.id.fontDesign2);
		home.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent ho = new Intent(WhatNewActivity.this,HomeActivity.class);
				ho.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(ho);
			}
		});
		back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				onBackPressed();
			}
		});
	

		

	}



	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == android.R.id.home) {
			onBackPressed();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
