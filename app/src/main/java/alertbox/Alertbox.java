package alertbox;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.brainagic.catalogue.catalogue.R;

;

public class Alertbox {
	Context context;
	
	public Alertbox(Context con) {
		// TODO Auto-generated constructor stub
		this.context = con;
	}
	

	public void showAlertbox(String msg)
	{
		final AlertDialog alertDialog = new Builder(
				context).create();

		LayoutInflater inflater = ((Activity) context).getLayoutInflater();
		View dialogView = inflater.inflate(R.layout.alertbox, null);
		alertDialog.setView(dialogView);

		TextView log = (TextView) dialogView.findViewById(R.id.textView1);
		Button okay = (Button)  dialogView.findViewById(R.id.okay);
		log.setText(msg);
		okay.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				alertDialog.dismiss();
			}
		});
		alertDialog.setCancelable(false);
		alertDialog.show();
	}

	public void showAlertbox(String msg,String from)
	{
		final AlertDialog alertDialog = new Builder(
				context).create();

		LayoutInflater inflater = ((Activity) context).getLayoutInflater();
		View dialogView = inflater.inflate(R.layout.alertbox, null);
		alertDialog.setView(dialogView);

		TextView log = (TextView) dialogView.findViewById(R.id.textView1);
		Button okay = (Button)  dialogView.findViewById(R.id.okay);
		if(from.equals("auth"))
		{
			okay.setText("Back");
		}

		log.setText(msg);
		okay.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				alertDialog.dismiss();
			}
		});
		alertDialog.setCancelable(false);
		alertDialog.show();
	}

	public void showAlertboxwithback(String msg)
	{
		final AlertDialog alertDialog = new Builder(
				context).create();

		LayoutInflater inflater = ((Activity) context).getLayoutInflater();
		View dialogView = inflater.inflate(R.layout.alertbox, null);
		alertDialog.setView(dialogView);

		TextView log = (TextView) dialogView.findViewById(R.id.textView1);
		Button okay = (Button)  dialogView.findViewById(R.id.okay);
		log.setText(msg);
		okay.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				alertDialog.dismiss();
				((Activity) context).onBackPressed();
			}
		});
		alertDialog.setCancelable(false);
		alertDialog.show();
	}
}
