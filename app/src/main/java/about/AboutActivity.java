package about;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.VideoView;

import com.brainagic.catalogue.EditProfileActivity;
import com.brainagic.catalogue.catalogue.R;


import alertbox.Alertbox;
import customer.CustomerActivity;
import engineer.EngineerActivity;
import home.HomeActivity;
import home.HomePageAdapter;
import inticator.CirclePageIndicator;
import mechanic.MechanicActivity;
import notification.NotificationActivity;
import product.PriceList;
import product.ProductCatalog;
import retailer.RetailerActivity;
import wsd.WSDActivity;

;

public class AboutActivity extends Activity { //implements YouTubePlayer.OnInitializedListener {

    private ImageView rblcrimg;
    private ImageView rblflimg;
    private ImageView zoom;
    CirclePageIndicator ulpageindicator;
    ViewPager imagePageViewer;
    private ImageView nextbtn;
    private ImageView prebtn, award,Intro1,Intro2;
    private ImageView expandedImageView;
    private VideoView videoview;
    // Insert your Video URL
    //String VideoURL = "http://www.dailymotion.com/embed/video/x1ade3x";

//    private YouTubePlayerView youTubeView;
    private static final int RECOVERY_DIALOG_REQUEST = 1;


    private String Usertype,Useremail,Usermobile;
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    private Alertbox alertbox = new Alertbox(AboutActivity.this);


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        getActionBar().setLogo(R.drawable.logo);


        imagePageViewer = (ViewPager) findViewById(R.id.viewpager);
        HomePageAdapter adapter = new HomePageAdapter(AboutActivity.this);
        imagePageViewer.setAdapter(adapter);
        imagePageViewer.setCurrentItem(0);


        myshare = getSharedPreferences("Registration", MODE_PRIVATE);
        editor = myshare.edit();
        Usertype = myshare.getString("Usertype", "");
        Useremail = myshare.getString("Email","") ;
        Usermobile = myshare.getString("MobNo","") ;


        ulpageindicator = (CirclePageIndicator) findViewById(R.id.indicator);
        ulpageindicator.setViewPager(imagePageViewer);
        ulpageindicator.setPageColor(R.color.white);
        ulpageindicator.setFillColor(R.color.white);
        ulpageindicator.setStrokeColor(R.color.logocolor);
        //ulpageindicator.set
        nextbtn = (ImageView) findViewById(R.id.next);
        prebtn = (ImageView) findViewById(R.id.previous);


        award = (ImageView) findViewById(R.id.award);

        Intro1 = (ImageView) findViewById(R.id.intreo1);
        Intro2 = (ImageView) findViewById(R.id.intreo2);





        expandedImageView = (ImageView) findViewById(R.id.expanded_image);
        
       // youTubeView = (YouTubePlayerView) findViewById(R.id.youtube_view);

        // Initializing video player with developer key
      //  youTubeView.initialize(Config.DEVELOPER_KEY, AboutActivity.this);

      /*  award.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                expandedImageView.setVisibility(View.VISIBLE);
                expandedImageView.setImageResource(R.drawable.deming);
            }
        });
*/



        Intro1.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                expandedImageView.setVisibility(View.VISIBLE);
                expandedImageView.setImageResource(R.drawable.ranegroup);
            }
        });



        Intro2.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                expandedImageView.setVisibility(View.VISIBLE);
                expandedImageView.setImageResource(R.drawable.groupcompanies);
            }
        });



        expandedImageView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                expandedImageView.setVisibility(View.GONE);

            }
        });

        nextbtn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                imagePageViewer.setCurrentItem(imagePageViewer.getCurrentItem() + 1);
            }
        });


        prebtn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                imagePageViewer.setCurrentItem(imagePageViewer.getCurrentItem() - 1);
            }
        });


        // for home abd back
        TextView home = (TextView) findViewById(R.id.fontDesign1);
        TextView back = (TextView) findViewById(R.id.fontDesign2);
        home.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                Intent ho = new Intent(AboutActivity.this, HomeActivity.class);
                ho.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(ho);
            }
        });
        back.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                onBackPressed();
            }
        });


    }


   /* @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider,
                                        YouTubeInitializationResult errorReason) {
        if (errorReason.isUserRecoverableError()) {
            errorReason.getErrorDialog(this, RECOVERY_DIALOG_REQUEST).show();
        } else {
            String errorMessage = String.format(
                    getString(R.string.error_player), errorReason.toString());
            Toast.makeText(this, errorMessage, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider,
                                        YouTubePlayer player, boolean wasRestored) {
        if (!wasRestored) {

            // loadVideo() will auto play video
            // Use cueVideo() method, if you don't want to play it automatically
            player.loadVideo(Config.YOUTUBE_VIDEO_CODE);

            // Hiding player controls
            player.setPlayerStyle(PlayerStyle.CHROMELESS);
        }
    }
*/
   /* @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RECOVERY_DIALOG_REQUEST) {
            // Retry initialization if user performed a recovery action
            getYouTubePlayerProvider().initialize(Config.DEVELOPER_KEY, this);
        }
    }*/

   /* private YouTubePlayer.Provider getYouTubePlayerProvider() {
        return (YouTubePlayerView) findViewById(R.id.youtube_view);
    }*/



    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        super.onBackPressed();
    }




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.popup, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // ...

        switch (item.getItemId()) {
            case R.id.about:
                // search action
                startActivity(new Intent(AboutActivity.this, AboutActivity.class));
                return true;
            case R.id.product:
                // location found
                startActivity(new Intent(AboutActivity.this, ProductCatalog.class));
                return true;
            case R.id.price:
                // refresh
                startActivity(new Intent(AboutActivity.this, PriceList.class));
                return true;
            case R.id.wsd:
                // help action
                if(!Usertype.equals("Distributor"))
                {
                    alertbox.showAlertbox("You are not allowed to view this page !");
                }
                else
                {
                    startActivity(new Intent(AboutActivity.this, WSDActivity.class));
                }
                return true;
            case R.id.retailer:
                // check for updates action
                if(!Usertype.equals("Retailer"))
                {
                    alertbox.showAlertbox("You are not allowed to view this page !");
                }
                else {
                    startActivity(new Intent(AboutActivity.this, RetailerActivity.class));
                }
                return true;
            case R.id.fieldengi:
                // check for updates action

                if(Usertype.equals("Rane Employee") || Usertype.equals("Sales Executives")|| Usertype.equals("MSR"))
                {

                    startActivity(new Intent(AboutActivity.this, EngineerActivity.class));
                }
                else
                {
                    alertbox.showAlertbox("You are not allowed to view this page !");
                }
                return true;
            case R.id.msr:
                // check for updates action

                if(Usertype.equals("MSR"))
                {

                    startActivity(new Intent(AboutActivity.this, EngineerActivity.class));
                }
                else
                {
                    alertbox.showAlertbox("You are not allowed to view this page !");
                }
                return true;
            case R.id.mech:
                // check for updates action
                if(!Usertype.equals("Mechanic"))
                {
                    alertbox.showAlertbox("You are not allowed to view this page !");
                }
                else
                {
                    startActivity(new Intent(AboutActivity.this, MechanicActivity.class));
                }
                return true;
            case R.id.customer:
                // check for updates action
                if((Usertype.equals("Others")) || (Usertype.equals("Fleet Operators")))
                {

                    startActivity(new Intent(AboutActivity.this, CustomerActivity.class));
                }
                else
                {
                    alertbox.showAlertbox("You are not allowed to view this page !");
                }

                return true;
            case R.id.notification:
                // check for updates action
                startActivity(new Intent(AboutActivity.this, NotificationActivity.class));
                return true;
            case R.id.edit:
                // check for updates action
                Intent edit = new Intent(AboutActivity.this,EditProfileActivity.class);
                edit.putExtra("usertype",Usertype);
                edit.putExtra("mobile",Usermobile);
                edit.putExtra("email",Useremail);
                startActivity(edit);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }



}