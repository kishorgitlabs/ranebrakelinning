package registration;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.brainagic.catalogue.catalogue.R;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.Gson;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import alertbox.Alertbox;
import dmax.dialog.SpotsDialog;
import fcm.Config;
import home.HomeActivity;
import multispinner.MultiSelectionSpinner;
import network.NetworkConnection;
import notification.NotificationUtil;
import persistency.ServerConnection;
import utility.Base64;
import utility.Utility;

import static com.google.android.gms.wearable.DataMap.TAG;

;

public class FirstRegistration extends Activity {

    EditText name, email, mobileno, city, country, dobtxt, shopname, cityname, pincode, latitude, lontitude, address, doorno, area, street;
    Button okay, cancel, browse;
    Spinner usertype, statespinner;
    SpotsDialog progressDialog;
    String Name, Email = "", Usertype, SelectedState;
    String[] types = {"Type of User", "Distributor", "Retailer", "Mechanic", "Rane Employee", "RBL Sales Executive", "MSR", "Fleet Operator", "Others"};
    Connection connection;
    Statement stmt;
    CheckBox checkbox;
    ResultSet resultSet;
    SharedPreferences myshare;
    SharedPreferences.Editor editor;
    String status = "Active";
    String Dbstatus = "";
    String formattedDate;
    String PROJECT_NUMBER = "97108778128";
    String regID, select_email, select_phno, select_name;
    private ArrayAdapter<CharSequence> adapter;
    private TextView termstext;
    private CheckBox checkterms;
    Calendar myCalendar = Calendar.getInstance();
    private ImageView ivImage;
    private int REQUEST_CAMERA = 0, SELECT_FILE = 1;
    private String userChoosenTask;
    Alertbox alertbox = new Alertbox(FirstRegistration.this);
    // simply code
   public String UPLOAD_URL = "http://rbl.brainmagicllc.com/Image/UploadToServer.php";
    //public String UPLOAD_URL = "http://54.255.163.200/Documents/UploadToServer.php";
    //Uri to store the image uri
    private String fileName;
    private String token = "";

    private Bitmap ShopPhoto, bitmap;
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    Alertbox box = new Alertbox(FirstRegistration.this);
    private LinearLayout NamaeAreaLayout, LatLongLayout, Typeofchlayout;
    private Statement stmt2;
    private String mobilenostn;
    private MultiSelectionSpinner multiSelectionSpinner;
    private List<String> typeofmech = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first_registration);

        getActionBar().setIcon(R.drawable.logo);

        name = (EditText) findViewById(R.id.retailernameedittext);
        email = (EditText) findViewById(R.id.emailedittext);
        mobileno = (EditText) findViewById(R.id.phoneedittext);
        city = (EditText) findViewById(R.id.citytext);
        dobtxt = (EditText) findViewById(R.id.dobtext);
        country = (EditText) findViewById(R.id.countryetext);

        shopname = (EditText) findViewById(R.id.shopedittext);
        address = (EditText) findViewById(R.id.address);
        doorno = (EditText) findViewById(R.id.dooredittext);
        area = (EditText) findViewById(R.id.areaedittext);
        street = (EditText) findViewById(R.id.streetedittext);
        pincode = (EditText) findViewById(R.id.pincodetext);
        latitude = (EditText) findViewById(R.id.latitext);
        lontitude = (EditText) findViewById(R.id.longtitext);


        usertype = (Spinner) findViewById(R.id.usertypespinner);
        statespinner = (Spinner) findViewById(R.id.satetspinner);


        NamaeAreaLayout = (LinearLayout) findViewById(R.id.namearea);
        LatLongLayout = (LinearLayout) findViewById(R.id.latlaong);
        Typeofchlayout = (LinearLayout) findViewById(R.id.typeofchlayout);

        //termstext.setPaintFlags(termstext.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        checkterms = (CheckBox) findViewById(R.id.terms);
        termstext = (TextView) findViewById(R.id.termscond);

        okay = (Button) findViewById(R.id.okay);
        cancel = (Button) findViewById(R.id.cancel);
        ivImage = (ImageView) findViewById(R.id.ivImage);
        browse = (Button) findViewById(R.id.browsebutton);


        String[] array = {"Truck & Bus", "LCV", "Car", "3W", "2W", "Tractor", "Others"};
        multiSelectionSpinner = (MultiSelectionSpinner) findViewById(R.id.typeofchspinner);

        multiSelectionSpinner.setItems(array);
        multiSelectionSpinner.setSelection(new int[]{6});
        multiSelectionSpinner.setListener(new MultiSelectionSpinner.OnMultipleItemsSelectedListener() {
            @Override
            public void selectedIndices(List<Integer> indices) {

            }

            @Override
            public void selectedStrings(List<String> strings) {
                // Toast.makeText(FirstRegistration.this, strings.toString(), Toast.LENGTH_LONG).show();
                typeofmech = strings;
            }
        });


        //FCM
        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                // checking for type intent filter
                if (intent.getAction().equals(Config.REGISTRATION_COMPLETE)) {
                    // gcm successfully registered
                    // now subscribe to `global` topic to receive app wide notifications
                    FirebaseMessaging.getInstance().subscribeToTopic(Config.TOPIC_GLOBAL);

                    displayFirebaseRegId();

                } else if (intent.getAction().equals(Config.PUSH_NOTIFICATION)) {
                    // new push notification is received

                    String message = intent.getStringExtra("message");

                    Toast.makeText(getApplicationContext(), "Push Notification: " + message, Toast.LENGTH_LONG).show();

                }
            }
        };

        displayFirebaseRegId();


        usertype.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Usertype = adapterView.getItemAtPosition(i).toString();


                multiSelectionSpinner.setDialogName("Type of " + Usertype);
                multiSelectionSpinner.setPrompt("Type of " + Usertype);

                if (Usertype.equals("RBL Sales Executive")) {
                    Usertype = "Sales Executives";
                }
                if (Usertype.equals("Fleet Operator")) {
                    Usertype = "Fleet Operators";
                }

                if (Usertype.equals("Distributor") || Usertype.equals("Retailer") || Usertype.equals("Mechanic")) {
                    NamaeAreaLayout.setVisibility(View.VISIBLE);
                    LatLongLayout.setVisibility(View.VISIBLE);

                    if (!Usertype.equals("Distributor"))
                        Typeofchlayout.setVisibility(View.VISIBLE);
                    else
                        Typeofchlayout.setVisibility(View.GONE);
                } else {
                    NamaeAreaLayout.setVisibility(View.GONE);
                    LatLongLayout.setVisibility(View.GONE);
                    Typeofchlayout.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        browse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage();
            }
        });


        ivImage.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                selectImage();
            }
        });


        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

        };


        Date d = new Date();
        myCalendar.setTime(d);
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        formattedDate = df.format(myCalendar.getTime());

        progressDialog = new SpotsDialog(FirstRegistration.this, R.style.Custom);
        progressDialog.setCancelable(false);
        progressDialog.setTitle("Rane Brake Line");
        //  progressDialog.show();


        myshare = getSharedPreferences("Registration", MODE_PRIVATE);
        editor = myshare.edit();


        adapter = ArrayAdapter.createFromResource(this, R.array.state_array, R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        statespinner.setAdapter(adapter);

        ArrayAdapter<String> xxx = new ArrayAdapter<String>(FirstRegistration.this, R.layout.simple_spinner_item, types);
        xxx.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        usertype.setAdapter(xxx);


        statespinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                SelectedState = adapterView.getItemAtPosition(i).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        dobtxt.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                // TODO Auto-generated method stub
                if (dobtxt.isFocused()) {
                    new DatePickerDialog(FirstRegistration.this, date, myCalendar
                            .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                            myCalendar.get(Calendar.DAY_OF_MONTH)).show();
                }
            }
        });


        okay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (Usertype.equals("Distributor") || Usertype.equals("Retailer") || Usertype.equals("Mechanic")) {

                    Name = name.getText().toString();
                    Email = email.getText().toString();
                    mobilenostn = mobileno.getText().toString();

                    String PhoneNo = mobileno.getText().toString();
                    String Regex = "[^\\d]";
                    String PhoneDigits = PhoneNo.replaceAll(Regex, "");

                    if (Name.equals("")) {
                        name.setFocusable(true);
                        name.setError("Enter your name");
                    } else if (PhoneDigits.length() != 10) {
                        Toast.makeText(FirstRegistration.this, "Mobile number must be 10 digits", Toast.LENGTH_SHORT).show();
                        mobileno.setError("Invalid mobile number");
                    } else if (Usertype.equals("Type of User")) {
                        Toast.makeText(FirstRegistration.this, "Select User Type", Toast.LENGTH_LONG).show();
                    } else if (shopname.getText().length() == 0) {
                        shopname.setFocusable(true);
                        shopname.setError("Enter shop name");
                        Toast.makeText(FirstRegistration.this, "Enter shop name", Toast.LENGTH_SHORT).show();
                    } else if (doorno.getText().length() == 0) {
                        doorno.setFocusable(true);
                        doorno.setError("Enter door name");
                        Toast.makeText(FirstRegistration.this, "Enter door name", Toast.LENGTH_SHORT).show();
                    } else if (street.getText().length() == 0) {
                        street.setFocusable(true);
                        street.setError("Enter street name");
                        Toast.makeText(FirstRegistration.this, "Enter street name", Toast.LENGTH_SHORT).show();
                    } else if (area.getText().length() == 0) {
                        area.setFocusable(true);
                        area.setError("Enter area name");
                        Toast.makeText(FirstRegistration.this, "Enter area name", Toast.LENGTH_SHORT).show();
                    } else if (city.getText().length() == 0) {
                        city.setFocusable(true);
                        city.setError("Enter city name");
                        Toast.makeText(FirstRegistration.this, "Enter city name", Toast.LENGTH_SHORT).show();
                    } else if (pincode.getText().length() == 0) {
                        pincode.setFocusable(true);
                        pincode.setError("Enter pincode ");
                        Toast.makeText(FirstRegistration.this, "Enter pincode ", Toast.LENGTH_SHORT).show();
                    } else if (SelectedState.equals("Select State")) {
                        Toast.makeText(FirstRegistration.this, "Select State", Toast.LENGTH_SHORT).show();
                    } else if (!checkterms.isChecked()) {
                        Toast.makeText(FirstRegistration.this, "Please agree to terms & conditions !", Toast.LENGTH_LONG).show();
                    } else {

                        if (Usertype.equals("Sales Executives") || Usertype.equals("Rane Employee")) {
                            if (!Email.equals("")) {
                                if ((CheckGateEmail(email.getText().toString()))) {
                                    checkinternet();
                                } else {
                                    email.setFocusable(true);
                                    email.setError("Please provide your Rane email id for registration");

                                    Toast toast = Toast.makeText(FirstRegistration.this, "Please provide your Rane email id for registration", Toast.LENGTH_LONG);
                                    toast.setGravity(Gravity.CENTER, 0, 0);
                                    toast.show();
                                }
                            }

                        } else {
                            if (!Usertype.equals("Distributor")) {
                                if (typeofmech.size() == 0) {
                                    Toast.makeText(FirstRegistration.this, "Please Select type of " + Usertype + "!", Toast.LENGTH_LONG).show();
                                } else {
                                    checkinternet();
                                }
                            } else {
                                checkinternet();
                            }

                        }

                    }
                } else {

                    Name = name.getText().toString();
                    Email = email.getText().toString();
                    mobilenostn = mobileno.getText().toString();

                    String PhoneNo = mobileno.getText().toString();
                    String Regex = "[^\\d]";
                    String PhoneDigits = PhoneNo.replaceAll(Regex, "");

                    if (Name.equals("")) {
                        name.setFocusable(true);
                        name.setError("Enter your name");
                    } else if (PhoneDigits.length() != 10) {
                        //Snackbar.make(findViewById(R.id.parentPanel), "Mobile number must be 10 digits", Snackbar.LENGTH_SHORT).show();
                        mobileno.setError("Invalid mobile number");
                        Toast.makeText(FirstRegistration.this, "Invalid mobile number", Toast.LENGTH_LONG).show();
                    } else if (SelectedState.equals("Select State")) {
                        //Snackbar.make(findViewById(R.id.parentPanel), "Select state", Snackbar.LENGTH_SHORT).show();
                        Toast.makeText(FirstRegistration.this, "Select State", Toast.LENGTH_LONG).show();
                    } else if (Usertype.equals("Type of User")) {
                        Toast.makeText(FirstRegistration.this, "Select user type", Toast.LENGTH_LONG).show();
                        //Snackbar.make(findViewById(R.id.parentPanel), "Select user type", Snackbar.LENGTH_LONG).show();
                    } else if (!checkterms.isChecked()) {
                        Toast.makeText(FirstRegistration.this, "Please agree to terms & conditions !", Toast.LENGTH_LONG).show();
                    } else {

                        if (Usertype.equals("Sales Executives") || Usertype.equals("Rane Employee")) {

                            if ((CheckGateEmail(email.getText().toString()))) {
                                checkinternet();
                            } else {
                                email.setFocusable(true);
                                email.setError("Please provide your Rane email id for registration");

                                Toast.makeText(FirstRegistration.this, "Please provide your Rane email id for registration", Toast.LENGTH_LONG).show();

                            }
                        } else {
                            checkinternet();
                        }

                    }
                }
            }
        });


        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        termstext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog(FirstRegistration.this);
            }

        });


    }


    public void showDialog(Activity activity) {
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.template_popup);


        Button dialogButton = (Button) dialog.findViewById(R.id.okay);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();

    }


    private void updateLabel() {

        String myFormat = "dd/MM/yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        dobtxt.setText(sdf.format(myCalendar.getTime()));
    }

    private boolean CheckGateEmail(String usertype) {


        if (!usertype.equals("")) {


            String[] parts = usertype.split("@");
            String part2 = parts[1];
            if (part2.equals("ranegroup.com") || (part2.equals("rane.co.in"))) {
                return true;
            } else {
                return false;
            }
        } else {
            // Snackbar.make(findViewById(R.id.parentPanel), "Please provide your Rane email id for registration", Snackbar.LENGTH_LONG).show();
            return false;
        }

    }


    // Fetches reg id from shared preferences
    // and displays on the screen
    private void displayFirebaseRegId() {
        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        token = pref.getString("regId",null);
        //Toast.makeText(FirstRegistration.this, token, Toast.LENGTH_SHORT).show();
        // box.showAlertbox(token);
        Log.e(TAG, "Firebase reg id: " + token);

    }


    @Override
    protected void onResume() {
        super.onResume();

        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.REGISTRATION_COMPLETE));

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.PUSH_NOTIFICATION));

        // clear the notification area when the app is opened
        NotificationUtil.clearNotifications(getApplicationContext());
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }


    private void checkinternet() {
        NetworkConnection connection = new NetworkConnection(FirstRegistration.this);

        if (connection.CheckInternet()) {
            progressDialog.show();

            //new BackroundRunning().execute();
            new AlreadyRegister().execute();


        } else {
            // Toast.makeText(FirstRegistration.this,"Not unable to connect please check your Internet connection ! ", Toast.LENGTH_SHORT).show();
            box.showAlertbox(getResources().getString(R.string.nointernetmsg));

        }


    }


    // Shop image


    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case Utility.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (userChoosenTask.equals("Take Photo"))
                        cameraIntent();
                    else if (userChoosenTask.equals("Choose from Library"))
                        galleryIntent();
                } else {
                    //code for deny
                    alertbox.showAlertbox("Rane Brake Lining needs permission to open Camera !");
                }
                break;
        }
    }


    private void selectImage() {
        final CharSequence[] items = {"Take Photo", "Choose from Library",
                "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(FirstRegistration.this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result = Utility.checkPermission(FirstRegistration.this);

                if (items[item].equals("Take Photo")) {
                    userChoosenTask = "Take Photo";
                    if (result)
                        cameraIntent();

                } else if (items[item].equals("Choose from Library")) {
                    userChoosenTask = "Choose from Library";
                    if (result)
                        galleryIntent();

                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void galleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
    }

    private void cameraIntent() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE)
                onSelectFromGalleryResult(data);

            else if (requestCode == REQUEST_CAMERA)
                onCaptureImageResult(data);


        }
    }

    private void onCaptureImageResult(Intent data) {
        ShopPhoto = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        ShopPhoto.compress(Bitmap.CompressFormat.JPEG, 90, bytes);

        File destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");

        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        ivImage.setImageBitmap(ShopPhoto);

    }


    private void onSelectFromGalleryResult(Intent data) {

        ShopPhoto = null;
        if (data != null) {
            try {
                ShopPhoto = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), data.getData());
                Uri path = data.getData();

                File file = new File("" + path);
                fileName = file.getName();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        ivImage.setImageBitmap(ShopPhoto);


    }


    class ImageUploadTask extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            BitmapDrawable drawable = (BitmapDrawable) ivImage.getDrawable();
            ShopPhoto = drawable.getBitmap();
        }

        @SuppressWarnings("unused")
        @Override
        protected String doInBackground(Void... unsued) {
            InputStream is;
            BitmapFactory.Options bfo;
            Bitmap bitmapOrg;
            ByteArrayOutputStream bao;

            bfo = new BitmapFactory.Options();
            bfo.inSampleSize = 2;

            bao = new ByteArrayOutputStream();


            ShopPhoto.compress(Bitmap.CompressFormat.JPEG, 90, bao);
            byte[] ba = bao.toByteArray();
            String ba1 = Base64.encodeBytes(ba);
            ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            nameValuePairs.add(new BasicNameValuePair("image", ba1));
            nameValuePairs.add(new BasicNameValuePair("cmd", mobilenostn + ".jpg"));
            Log.v("log_tag", System.currentTimeMillis() + ".jpg");
            try {
                HttpClient httpclient = new DefaultHttpClient();
                HttpPost httppost = new
                        //  Here you need to put your server file address
                        HttpPost(UPLOAD_URL);
                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                HttpResponse response = httpclient.execute(httppost);
                HttpEntity entity = response.getEntity();
                is = entity.getContent();
                Log.v("log_tag", "In the try Loop");
            }
            catch (Exception e)
            {
                Log.v("log_tag", "Error in http connection " + e.toString());
                return "notsuccess";
            }
            return "Success";

        }


        @Override
        protected void onPostExecute(String sResponse) {
            if (sResponse.equals("Success"))
                new SaveRegisterDatails().execute();
            else
            {
                progressDialog.dismiss();
                box.showAlertbox(getString(R.string.slow_internet));
            }
        }

    }


    class SaveRegisterDatails extends AsyncTask<String, Void, String> {
        private int i = 0, j = 0;
        private String namestrn, emailtrn, shopnametrn, citynametrn, pincodetrn, latitudestrn, lontitudestrn, addressstrn, doornostrn, areastrn, streetstrn, countrystrn, dobeditstrng;
        private boolean truckbus, lcv, car, three, two, tractor, other;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();


            namestrn = name.getText().toString();
            mobilenostn = mobileno.getText().toString();
            emailtrn = email.getText().toString();
            shopnametrn = shopname.getText().toString();
            citynametrn = city.getText().toString();
            pincodetrn = pincode.getText().toString();

            dobeditstrng = dobtxt.getText().toString();

            if (latitude.getText().toString().length() == 0 || lontitude.getText().toString().length() == 0) {
                latitudestrn = "Not Given";
                lontitudestrn = "Not Given";
            } else {
                latitudestrn = latitude.getText().toString();
                lontitudestrn = lontitude.getText().toString();
            }

            Gson gson = new Gson();
            String jsonText = gson.toJson(multiSelectionSpinner.getSelectedIndices());
            editor.putString("typeofmech", jsonText).commit();


            for (int i = 0; typeofmech.size() > i; i++) {

               if(typeofmech.size()!=0) {
                   if (typeofmech.get(i).toString().equals("Truck & Bus")) {
                       truckbus = true;
                   } else if (typeofmech.get(i).toString().equals("LCV")) {
                       lcv = true;
                   } else if (typeofmech.get(i).toString().equals("Car")) {
                       car = true;
                   } else if (typeofmech.get(i).toString().equals("3W")) {
                       three = true;
                   } else if (typeofmech.get(i).toString().equals("2W")) {
                       two = true;
                   } else if (typeofmech.get(i).toString().equals("Tractor")) {
                       tractor = true;
                   } else {
                       other = true;
                   }
               }
            }


            doornostrn = doorno.getText().toString();
            areastrn = area.getText().toString();
            streetstrn = street.getText().toString();
            countrystrn = country.getText().toString();

            addressstrn = shopname.getText() + "," + doorno.getText() + "," + street.getText() + "," + area.getText() + "," + city.getText() + "," + SelectedState + "," + countrystrn;


        }

        @Override
        protected String doInBackground(String... strings) {

            try {
                ServerConnection server = new ServerConnection();
                connection = server.getConnection();
                stmt = connection.createStatement();
                stmt2 = connection.createStatement();

                String query;
                // save user_registration for notification
                String insertquery = "insert into user_registration(name,mobileno,email,state,city,country,usertype,date,Status,dateofbirth,shopphoto,appid) values('" + namestrn + "','" + mobilenostn + "','" + emailtrn + "','" + SelectedState + "','" + citynametrn + "','" + countrystrn + "','" + Usertype + "','" + formattedDate + "','" + status + "','" + dobeditstrng + "','" + mobilenostn + ".jpg" + "','" + token + "')";
                Log.v("Insert query", insertquery);

                j = stmt2.executeUpdate(insertquery);

                if (Dbstatus.equals("Active")) {
                    query = "update registration set name = '" + namestrn + "', mobileno = '" + mobilenostn + "',email = '" + emailtrn + "',dateofbirth = '" + dobeditstrng + "',address = '" + addressstrn + "',area  = '" + areastrn + "',shopname = '" + shopnametrn + "',doorno = '" + doornostrn + "',street = '" + streetstrn + "',pincode = '" + pincodetrn + "',city = '" + citynametrn + "',state = '" + SelectedState + "',country = '" + countrystrn + "',latttitude = '" + latitudestrn + "',longtitude = '" + lontitudestrn + "',usertype = '" + Usertype + "',Status = 'Active',insertdate = '" + formattedDate + "' , shopphoto ='" + mobilenostn + ".jpg" + "' , IsTruckBus = '" + truckbus + "' , IsLCV = '" + lcv + "' ,IsCar = '" + car + "' ,Is3W = '" + three + "' ,Is2W = '" + two + "' ,IsTractor = '" + tractor + "' ,IsOthers = '" + other + "'  where usertype = '" + Usertype + "' and mobileno ='" + mobilenostn + "' ";
                } else {
                    query = "insert into registration(name,mobileno,email,dateofbirth,address,area,shopname,doorno,street,pincode,city,state,country,latttitude,longtitude,usertype,Status,insertdate, shopphoto,appid,IsTruckBus,IsLCV,IsCar,Is3W,Is2W,IsTractor,IsOthers) values('" + namestrn + "','" + mobilenostn + "','" + emailtrn + "','" + dobeditstrng + "','" + addressstrn + "','" + areastrn + "','" + shopnametrn + "','" + doornostrn + "','" + streetstrn + "','" + pincodetrn + "','" + citynametrn + "','" + SelectedState + "','" + countrystrn + "','" + latitudestrn + "','" + lontitudestrn + "','" + Usertype + "','Active','" + formattedDate + "','" + mobilenostn + ".jpg" + "' ,'" + token + "','" + truckbus + "','" + lcv + "','" + car + "','" + three + "','" + two + "','" + tractor + "','" + other + "')";
                }

                Log.v("Insert query ", query);
                i = stmt.executeUpdate(query);

                if (i == 0 && j == 0) {
                    connection.close();
                    stmt.close();
                    return "notinsert";
                } else {
                    connection.close();
                    stmt.close();
                    return "insert";
                }
            } catch (Exception e) {
                e.printStackTrace();
                return "error";
            }


        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressDialog.dismiss();
            if (s.equals("insert")) {

                final AlertDialog alertDialogbox = new AlertDialog.Builder(
                        FirstRegistration.this).create();

                LayoutInflater inflater = FirstRegistration.this.getLayoutInflater();
                View dialogView = inflater.inflate(R.layout.alertbox, null);
                alertDialogbox.setView(dialogView);

                TextView log = (TextView) dialogView.findViewById(R.id.textView1);
                Button okay = (Button) dialogView.findViewById(R.id.okay);
                log.setText("Registered Successfully");
                okay.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View arg0) {
                        // TODO Auto-generated method stub
                        editor.putBoolean("isregister", true);
                        editor.putString("useroremp", "emp");
                        editor.putString("Name", Name);
                        editor.putString("Email", Email);
                        editor.putString("MobNo", mobilenostn);
                        editor.putString("Usertype", Usertype);
                        editor.commit();
                        Intent go = new Intent(FirstRegistration.this, HomeActivity.class);
                        go.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(go);
                        alertDialogbox.dismiss();
                    }
                });
                alertDialogbox.setCancelable(false);
                alertDialogbox.show();

            } else if (s.equals("notinsert")) {
                progressDialog.dismiss();
                alertbox.showAlertbox("Registration was not successful !. Please try again later");
            } else {
                progressDialog.dismiss();
                box.showAlertbox(getString(R.string.slow_internet));
            }

        }
    }


    class BackroundRunning extends AsyncTask<String, Void, String> {
        private String namestrng, mobilestrng, emailstrng, citystrng, countrystrng, dobstrng;


        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            namestrng = name.getText().toString();
            mobilenostn = mobileno.getText().toString();
            emailstrng = email.getText().toString();
            citystrng = city.getText().toString();
            countrystrng = country.getText().toString();
            dobstrng = dobtxt.getText().toString();
        }

        @Override
        protected String doInBackground(String... strings) {

            int i = 0;
            try {
                ServerConnection server = new ServerConnection();
                connection = server.getConnection();
                stmt = connection.createStatement();

                if (Dbstatus.equals("Active")) {
                    Log.v("query", "update user_registration set name = '" + namestrng + "',mobileno = '" + mobilenostn + "',email = '" + emailstrng + "',city = '" + citystrng + "',state = '" + SelectedState + "',date = '" + formattedDate + "',usertype = '" + Usertype + "', dateofbirth= '" + dobstrng + "',  shopphoto ='" + mobilenostn + ".jpg" + "' where  mobileno ='" + mobilenostn + "' ");

                    i = stmt.executeUpdate("update user_registration set name = '" + namestrng + "',mobileno = '" + mobilenostn + "',email = '" + emailstrng + "',city = '" + citystrng + "',state = '" + SelectedState + "',date = '" + formattedDate + "',usertype = '" + Usertype + "', dateofbirth= '" + dobstrng + "' , shopphoto ='" + mobilenostn + ".jpg" + "', appid= '" + token + "' where  mobileno ='" + mobilenostn + "' ");
                } else {
                    String insertquery = "insert into user_registration(name,mobileno,email,state,city,country,usertype,date,Status,dateofbirth,shopphoto,appid) values('" + namestrng + "','" + mobilenostn + "','" + emailstrng + "','" + SelectedState + "','" + citystrng + "','" + countrystrng + "','" + Usertype + "','" + formattedDate + "','" + status + "','" + dobstrng + "','" + mobilenostn + ".jpg" + "','" + token + "')";
                    Log.v("Insert query", insertquery);
                    i = stmt.executeUpdate(insertquery);
                }

                if (i == 0) {
                    connection.close();
                    stmt.close();
                    return "notinsert";
                } else {
                    connection.close();
                    stmt.close();
                    return "inserted";
                }

            } catch (Exception e) {
                e.printStackTrace();
                return "error_email";
            }


        }


        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressDialog.dismiss();
            if (s.equals("inserted")) {


                final AlertDialog alertDialogbox = new AlertDialog.Builder(
                        FirstRegistration.this).create();

                LayoutInflater inflater = FirstRegistration.this.getLayoutInflater();
                View dialogView = inflater.inflate(R.layout.alertbox, null);
                alertDialogbox.setView(dialogView);

                TextView log = (TextView) dialogView.findViewById(R.id.textView1);
                Button okay = (Button) dialogView.findViewById(R.id.okay);
                log.setText("Registered Successfully");
                okay.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View arg0) {
                        // TODO Auto-generated method stub
                        editor.putBoolean("isregister", true);
                        editor.putString("useroremp", "user");
                        editor.putString("Name", Name);
                        editor.putString("Email", Email);
                        editor.putString("MobNo", mobilenostn);
                        editor.putString("Usertype", Usertype);
                        editor.commit();
                        Intent go = new Intent(FirstRegistration.this, HomeActivity.class);
                        go.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(go);
                        alertDialogbox.dismiss();
                    }
                });
                alertDialogbox.setCancelable(false);
                alertDialogbox.show();
            } else if (s.equals("notinsert")) {
                progressDialog.dismiss();
                box.showAlertbox("Registration was not successful !. Please try again later");
            } else {
                progressDialog.dismiss();
                box.showAlertbox(getString(R.string.slow_internet));
            }
        }

    }


    class AlreadyRegister extends AsyncTask<String, Void, String> {
        String selectquery;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            select_email = email.getText().toString();
            select_phno = mobileno.getText().toString();
            select_name = name.getText().toString();

        }

        @Override
        protected String doInBackground(String... strings) {

            ServerConnection server = new ServerConnection();

            try {

                connection = server.getConnection();

                stmt = connection.createStatement();

                if (Usertype.equals("Distributor") || Usertype.equals("Retailer") || Usertype.equals("Mechanic")) {

                    selectquery = "SELECT Status,mobileno,email from registration where  mobileno='" + select_phno + "'";
                } else {
                    selectquery = "SELECT Status,mobileno,email from user_registration where  mobileno='" + select_phno + "'";
                }

                resultSet = stmt.executeQuery(selectquery);


                if (resultSet.next()) {

                    Dbstatus = resultSet.getString("Status");
                    mobilenostn = resultSet.getString("mobileno");

                    if (Dbstatus.equals("Active")) {
                        resultSet.close();
                        connection.close();
                        stmt.close();
                        return "Registered";
                    } else {

                        resultSet.close();
                        connection.close();
                        stmt.close();

                        return "UnRegister";

                    }

                } else {
                    Dbstatus = "";
                    resultSet.close();
                    connection.close();
                    stmt.close();
                    return "UnRegister";

                }
            } catch (Exception e) {
                e.printStackTrace();
                return "error";
            }

        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (Dbstatus.equals("Active")) {
                // new BackroundRunning().execute();
                if (Usertype.equals("Distributor") || Usertype.equals("Retailer") || Usertype.equals("Mechanic")) {
                    new ImageUploadTask().execute();
                } else {
                    new BackroundRunning().execute();
                }

            } else if (s.equals("UnRegister")) {
                //  new BackroundRunning().execute();
                if (Usertype.equals("Distributor") || Usertype.equals("Retailer") || Usertype.equals("Mechanic")) {
                    new ImageUploadTask().execute();
                } else {
                    new BackroundRunning().execute();
                }
            } else {
                progressDialog.dismiss();
                box.showAlertbox(getString(R.string.slow_internet));
            }


        }
    }


}



