package registration;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.brainagic.catalogue.EditProfileActivity;
import com.brainagic.catalogue.catalogue.R;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import about.AboutActivity;
import alertbox.Alertbox;
import customer.CustomerActivity;
import dmax.dialog.SpotsDialog;
import engineer.EngineerActivity;
import home.HomeActivity;
import mechanic.MechanicActivity;
import multispinner.MultiSelectionSpinner;
import network.NetworkConnection;
import notification.NotificationActivity;
import persistency.ServerConnection;
import product.PriceList;
import product.ProductCatalog;
import retailer.RetailerActivity;
import utility.Base64;
import utility.Utility;
import wsd.WSDActivity;

;

public class RegisterRetailerOrMech extends Activity {

    private EditText name, email, mobileno, shopname, cityname, pincode, latitude, lontitude, address, doorno, area, street, country, dobedit;
    private Button okay, cancel, browse;
    private Spinner usertype, satespinner;
    private SpotsDialog progressDialog;
    private String Name, Email, Mobno, Usertype, SelectedState;
    private String[] types = {"Type of User", "Distributor", "Retailer", "Mechanic", "Rane Employee", "Others"};
    private Connection connection;
    private Statement stmt;
    private CheckBox checkbox;
    private ResultSet resultSet;
    private String status = "Active";
    private String Dbstatus = "";
    private String formattedDate;
    private String PROJECT_NUMBER = "97108778128";
    private String regID, select_email, select_phno, dis_select_phno, select_name;
    private ArrayAdapter<CharSequence> adapter;
    private Alertbox alertbox = new Alertbox(RegisterRetailerOrMech.this);
    private String registertype;
    private LinearLayout typeofmechlayout;
    Calendar myCalendar = Calendar.getInstance();
    private CheckBox pv, lcv, hcv, cv;
    private ImageView ivImage;
    private String userChoosenTask;
    private Bitmap ShopPhoto, bitmap;

   public String UPLOAD_URL = "http://rbl.brainmagicllc.com/Image/UploadToServer.php";
   // public String UPLOAD_URL = "http://http://54.255.163.200/Documents/UploadToServer.php";
    //Uri to store the image uri
    private String fileName;
    private int REQUEST_CAMERA = 0, SELECT_FILE = 1;
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    private String Useremail, Usermobile;
    private String mobilenostn;
    private MultiSelectionSpinner multiSelectionSpinner;
    private List<String> typeofmech;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_retailerormech);

        getActionBar().setIcon(R.drawable.logo);

        registertype = getIntent().getStringExtra("registertype");

        name = (EditText) findViewById(R.id.retailernameedittext);
        email = (EditText) findViewById(R.id.emailedittext);
        mobileno = (EditText) findViewById(R.id.phoneedittext);

        cityname = (EditText) findViewById(R.id.citytext);
        pincode = (EditText) findViewById(R.id.pincodetext);
        country = (EditText) findViewById(R.id.countrytext);
        dobedit = (EditText) findViewById(R.id.dobtext);

        latitude = (EditText) findViewById(R.id.latitext);
        lontitude = (EditText) findViewById(R.id.longtitext);

        shopname = (EditText) findViewById(R.id.shopedittext);
        address = (EditText) findViewById(R.id.address);
        doorno = (EditText) findViewById(R.id.dooredittext);
        area = (EditText) findViewById(R.id.areaedittext);
        street = (EditText) findViewById(R.id.streetedittext);
        typeofmechlayout = (LinearLayout) findViewById(R.id.mechtypelayour);

        okay = (Button) findViewById(R.id.okay);
        cancel = (Button) findViewById(R.id.cancel);
        usertype = (Spinner) findViewById(R.id.usertypespinner);
        satespinner = (Spinner) findViewById(R.id.satetspinner);


        ivImage = (ImageView) findViewById(R.id.ivImage);
        browse = (Button) findViewById(R.id.browsebutton);

        typeofmech = new ArrayList<String>();

        browse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage();
            }
        });


        ivImage.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                selectImage();
            }
        });


        String[] array = {"Truck & Bus", "LCV", "Car", "3W", "2W", "Tractor", "Others"};
        multiSelectionSpinner = (MultiSelectionSpinner) findViewById(R.id.typeofchspinner);
        multiSelectionSpinner.setPrompt("Select type of "+registertype);
        multiSelectionSpinner.setItems(array);
        //multiSelectionSpinner.setSelection(new int[]{6});
        multiSelectionSpinner.setListener(new MultiSelectionSpinner.OnMultipleItemsSelectedListener() {
            @Override
            public void selectedIndices(List<Integer> indices) {


            }

            @Override
            public void selectedStrings(List<String> strings) {
                // Toast.makeText(FirstRegistration.this, strings.toString(), Toast.LENGTH_LONG).show();
                typeofmech = strings;
            }
        });


        String[] types = {registertype};

        Date d = new Date();
        myCalendar.setTime(d);
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        formattedDate = df.format(myCalendar.getTime());


        progressDialog = new SpotsDialog(RegisterRetailerOrMech.this, R.style.Custom);
        progressDialog.setCancelable(false);
        // progressDialog.show();

        myshare = getSharedPreferences("Registration", MODE_PRIVATE);
        editor = myshare.edit();
        Usertype = myshare.getString("Usertype", "");
        Useremail = myshare.getString("Email", "");
        Usermobile = myshare.getString("MobNo", "");
       /* if(registertype.equals("Mechanic"))
        {
            typeofmechlayout.setVisibility(View.VISIBLE);
        }
        else {
            typeofmechlayout.setVisibility(View.GONE);
        }*/


        adapter = ArrayAdapter.createFromResource(this, R.array.state_array, R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        satespinner.setAdapter(adapter);


        ArrayAdapter<String> xxx = new ArrayAdapter<String>(RegisterRetailerOrMech.this, R.layout.simple_spinner_item, types);
        xxx.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        usertype.setAdapter(xxx);


        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

        };


        dobedit.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (dobedit.isFocused()) {
                    new DatePickerDialog(RegisterRetailerOrMech.this, date, myCalendar
                            .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                            myCalendar.get(Calendar.DAY_OF_MONTH)).show();
                }
            }
        });


        usertype.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                Usertype = adapterView.getItemAtPosition(i).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        satespinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                SelectedState = adapterView.getItemAtPosition(i).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        okay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Name = name.getText().toString();
                Email = email.getText().toString();
                Mobno = mobileno.getText().toString();

                String PhoneNo = mobileno.getText().toString();
                String Regex = "[^\\d]";
                String PhoneDigits = PhoneNo.replaceAll(Regex, "");

                if (Name.equals("")) {
                    name.setFocusable(true);
                    name.setError("Enter your mame");
                } else if (PhoneDigits.length() != 10) {
                    Toast.makeText(RegisterRetailerOrMech.this, "Mobile Number Must be 10 digits", Toast.LENGTH_SHORT).show();
                    mobileno.setError("Invalid mobile number");
                } else if (Usertype.equals("Type of User")) {
                    Toast.makeText(RegisterRetailerOrMech.this, "Select User Type", Toast.LENGTH_LONG).show();
                } else if (shopname.getText().length() == 0) {
                    shopname.setFocusable(true);
                    shopname.setError("Enter shop name");
                    Toast.makeText(RegisterRetailerOrMech.this, "Enter shop name", Toast.LENGTH_LONG).show();
                } else if (doorno.getText().length() == 0) {
                    doorno.setFocusable(true);
                    doorno.setError("Enter door name");
                    Toast.makeText(RegisterRetailerOrMech.this, "Enter door name", Toast.LENGTH_LONG).show();
                } else if (street.getText().length() == 0) {
                    street.setFocusable(true);
                    street.setError("Enter street name");
                    Toast.makeText(RegisterRetailerOrMech.this, "Enter street name", Toast.LENGTH_LONG).show();
                } else if (area.getText().length() == 0) {
                    area.setFocusable(true);
                    area.setError("Enter area name");
                    Toast.makeText(RegisterRetailerOrMech.this, "Enter area name", Toast.LENGTH_LONG).show();
                } else if (cityname.getText().length() == 0) {
                    cityname.setFocusable(true);
                    cityname.setError("Enter city name");
                    Toast.makeText(RegisterRetailerOrMech.this, "Enter city name", Toast.LENGTH_LONG).show();
                } else if (SelectedState.equals("Select State")) {
                    Toast.makeText(RegisterRetailerOrMech.this, "Select State", Toast.LENGTH_SHORT).show();
                } else if(typeofmech.isEmpty())
                {

                    Toast.makeText(RegisterRetailerOrMech.this, "Select type of "+registertype, Toast.LENGTH_SHORT).show();
                }
                else {

                    if (Usertype.equals("Rane Employee")) {
                        if (!Email.equals("")) {
                            if ((CheckGateEmail(email.getText().toString()))) {
                                checkinternet();
                            } else {
                                email.setFocusable(true);
                                email.setError("Please provide your Rane email id for registration");

                                Toast toast = Toast.makeText(RegisterRetailerOrMech.this, "Please provide your Rane email id for registration", Toast.LENGTH_LONG);
                                toast.setGravity(Gravity.CENTER, 0, 0);
                                toast.show();
                            }
                        }

                    } else {
                        checkinternet();
                    }

                }
            }
        });

// for home abd back
        TextView home = (TextView) findViewById(R.id.fontDesign1);
        TextView back = (TextView) findViewById(R.id.fontDesign2);
        home.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                Intent ho = new Intent(RegisterRetailerOrMech.this, HomeActivity.class);
                ho.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(ho);
            }
        });
        back.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                onBackPressed();
            }
        });


        cancel.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                onBackPressed();
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.popup, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // ...

        switch (item.getItemId()) {
            case R.id.about:
                // search action
                startActivity(new Intent(RegisterRetailerOrMech.this, AboutActivity.class));
                return true;
            case R.id.product:
                // location found
                startActivity(new Intent(RegisterRetailerOrMech.this, ProductCatalog.class));
                return true;
            case R.id.price:
                // refresh
                startActivity(new Intent(RegisterRetailerOrMech.this, PriceList.class));
                return true;
            case R.id.wsd:
                // help action
                if (!Usertype.equals("Distributor")) {
                    alertbox.showAlertbox("You are not allowed to view this page !", "auth");
                } else {
                    startActivity(new Intent(RegisterRetailerOrMech.this, WSDActivity.class));
                }
                return true;
            case R.id.retailer:
                // check for updates action
                if (!Usertype.equals("Retailer")) {
                    alertbox.showAlertbox("You are not allowed to view this page !", "auth");
                } else {
                    startActivity(new Intent(RegisterRetailerOrMech.this, RetailerActivity.class));
                }
                return true;
            case R.id.fieldengi:
                // check for updates action

                if (Usertype.equals("Rane Employee") || Usertype.equals("Sales Executives") || Usertype.equals("MSR")) {

                    startActivity(new Intent(RegisterRetailerOrMech.this, EngineerActivity.class));
                } else {
                    alertbox.showAlertbox("You are not allowed to view this page !", "auth");
                }
                return true;
            case R.id.msr:
                // check for updates action

                if (Usertype.equals("MSR")) {

                    startActivity(new Intent(RegisterRetailerOrMech.this, EngineerActivity.class));
                } else {
                    alertbox.showAlertbox("You are not allowed to view this page !");
                }
                return true;
            case R.id.mech:
                // check for updates action
                if (!Usertype.equals("Mechanic")) {
                    alertbox.showAlertbox("You are not allowed to view this page !", "auth");
                } else {
                    startActivity(new Intent(RegisterRetailerOrMech.this, MechanicActivity.class));
                }
                return true;
            case R.id.customer:
                // check for updates action

                if ((Usertype.equals("Others")) || (Usertype.equals("Fleet Operators"))) {

                    startActivity(new Intent(RegisterRetailerOrMech.this, CustomerActivity.class));
                } else {
                    alertbox.showAlertbox("You are not allowed to view this page !", "auth");
                }
                return true;
            case R.id.notification:
                // check for updates action
                startActivity(new Intent(RegisterRetailerOrMech.this, NotificationActivity.class));
                return true;
            case R.id.edit:
                // check for updates action
                Intent edit = new Intent(RegisterRetailerOrMech.this, EditProfileActivity.class);
                edit.putExtra("usertype", Usertype);
                edit.putExtra("mobile", Usermobile);
                edit.putExtra("email", Useremail);
                startActivity(edit);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    private void updateLabel() {

        String myFormat = "dd/MM/yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        dobedit.setText(sdf.format(myCalendar.getTime()));
    }

    private boolean CheckGateEmail(String usertype) {

        String[] parts = usertype.split("@");
        String part2 = parts[1];
        if (part2.equals("ranegroup.com") || (part2.equals("rane.co.in"))) {
            return true;
        } else {
            return false;
        }

    }


    private void checkinternet() {
        NetworkConnection connection = new NetworkConnection(RegisterRetailerOrMech.this);

        if (connection.CheckInternet()) {


            new AlreadyRegister().execute();

        } else {
            // Toast.makeText(RegisterRetailerOrMech.this,"Not unable to connect please check your Internet connection ! ", Toast.LENGTH_SHORT).show();
            alertbox.showAlertbox(getResources().getString(R.string.nointernetmsg));


        }


    }

    private void selectImage() {
        final CharSequence[] items = {"Take Photo", "Choose from Library",
                "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(RegisterRetailerOrMech.this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result = Utility.checkPermission(RegisterRetailerOrMech.this);

                if (items[item].equals("Take Photo")) {
                    userChoosenTask = "Take Photo";
                    if (result)
                        cameraIntent();

                } else if (items[item].equals("Choose from Library")) {
                    userChoosenTask = "Choose from Library";
                    if (result)
                        galleryIntent();

                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void galleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
    }

    private void cameraIntent() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE)
                onSelectFromGalleryResult(data);

            else if (requestCode == REQUEST_CAMERA)
                onCaptureImageResult(data);


        }
    }

    private void onCaptureImageResult(Intent data) {
        ShopPhoto = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        ShopPhoto.compress(Bitmap.CompressFormat.JPEG, 90, bytes);

        File destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");

        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        ivImage.setImageBitmap(ShopPhoto);

    }


    private void onSelectFromGalleryResult(Intent data) {

        ShopPhoto = null;
        if (data != null) {
            try {
                ShopPhoto = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), data.getData());
                Uri path = data.getData();
                fileName = path.getLastPathSegment();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        ivImage.setImageBitmap(ShopPhoto);


    }


    class ImageUploadTask extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            BitmapDrawable drawable = (BitmapDrawable) ivImage.getDrawable();
            ShopPhoto = drawable.getBitmap();
        }

        @SuppressWarnings("unused")
        @Override
        protected String doInBackground(Void... unsued) {
            InputStream is;
            BitmapFactory.Options bfo;
            Bitmap bitmapOrg;
            ByteArrayOutputStream bao;

            bfo = new BitmapFactory.Options();
            bfo.inSampleSize = 2;

            bao = new ByteArrayOutputStream();


            ShopPhoto.compress(Bitmap.CompressFormat.JPEG, 90, bao);
            byte[] ba = bao.toByteArray();
            String ba1 = Base64.encodeBytes(ba);
            ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            nameValuePairs.add(new BasicNameValuePair("image", ba1));
            nameValuePairs.add(new BasicNameValuePair("cmd", mobilenostn + ".jpg"));
            Log.v("log_tag", System.currentTimeMillis() + ".jpg");
            try {
                HttpClient httpclient = new DefaultHttpClient();
                HttpPost httppost = new
                        //  Here you need to put your server file address
                        HttpPost(UPLOAD_URL);
                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                HttpResponse response = httpclient.execute(httppost);
                HttpEntity entity = response.getEntity();
                is = entity.getContent();
                Log.v("log_tag", "In the try Loop");
            } catch (Exception e) {
                Log.v("log_tag", "Error in http connection " + e.toString());
                return "notsuccess";
            }
            return "Success";

        }


        @Override
        protected void onPostExecute(String sResponse) {
            if (sResponse.equals("Success"))
                new SaveRegisterDatails().execute();
            else {
                progressDialog.dismiss();
                alertbox.showAlertbox(getString(R.string.slow_internet));
            }
        }
    }


    class AlreadyRegister extends AsyncTask<String, Void, String> {
        String selectquery;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();


            select_name = name.getText().toString();
            select_phno = mobileno.getText().toString();
            progressDialog.show();

        }

        @Override
        protected String doInBackground(String... strings) {

            ServerConnection server = new ServerConnection();
            try {
                connection = server.getConnection();

                stmt = connection.createStatement();

                selectquery = "SELECT Status,mobileno,email from registration where name ='" + select_name + "' and mobileno ='" + select_phno + "' and  usertype = '" + registertype + "'";


                resultSet = stmt.executeQuery(selectquery);


                if (resultSet.next()) {

                    Dbstatus = resultSet.getString("Status");
                    mobilenostn = resultSet.getString("mobileno");
                    if (Dbstatus.equals("Active")) {
                        resultSet.close();
                        connection.close();
                        stmt.close();
                        return "Registered";
                    } else {

                        resultSet.close();
                        connection.close();
                        stmt.close();

                        return "UnRegister";

                    }

                } else {
                    resultSet.close();
                    connection.close();
                    stmt.close();

                    return "UnRegister";

                }
            } catch (Exception e) {
                e.printStackTrace();
                return "error";
            }

        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            if (Dbstatus.equals("Active") && s.equals("Registered")) {
                progressDialog.dismiss();
                alertbox.showAlertbox("User Already Exist");
            } else if (s.equals("UnRegister")) {
                new ImageUploadTask().execute();
            } else {
                progressDialog.dismiss();
                alertbox.showAlertboxwithback(getResources().getString(R.string.slow_internet));
            }


        }
    }


    class SaveRegisterDatails extends AsyncTask<String, Void, String> {
        private int i = 0;
        private String namestrn, emailtrn, shopnametrn, citynametrn, pincodetrn, latitudestrn, lontitudestrn, addressstrn, doornostrn, areastrn, streetstrn, countrystrn, dobeditstrng;
        private boolean truckbus, lcv, car, three, two, tractor, other;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();


            namestrn = name.getText().toString();
            mobilenostn = mobileno.getText().toString();
            emailtrn = email.getText().toString();
            shopnametrn = shopname.getText().toString();
            citynametrn = cityname.getText().toString();
            pincodetrn = pincode.getText().toString();

            dobeditstrng = dobedit.getText().toString();
            if (latitude.getText().toString().length() == 0 || lontitude.getText().toString().length() == 0) {
                latitudestrn = "Not Given";
                lontitudestrn = "Not Given";
            } else {
                latitudestrn = latitude.getText().toString();
                lontitudestrn = lontitude.getText().toString();}


            for (int i = 0; typeofmech.size() > i; i++) {
                if (typeofmech.get(i).toString().equals("Truck & Bus")) {
                    truckbus = true;
                } else if (typeofmech.get(i).toString().equals("LCV")) {
                    lcv = true;
                } else if (typeofmech.get(i).toString().equals("Car")) {
                    car = true;
                } else if (typeofmech.get(i).toString().equals("3W")) {
                    three = true;
                } else if (typeofmech.get(i).toString().equals("2W")) {
                    two = true;
                } else if (typeofmech.get(i).toString().equals("Tractor")) {
                    tractor = true;
                } else {
                    other = true;
                }
            }

            doornostrn = doorno.getText().toString();
            areastrn = area.getText().toString();
            streetstrn = street.getText().toString();
            countrystrn = country.getText().toString();

            addressstrn = shopname.getText() + "," + doorno.getText() + "," + street.getText() + "," + area.getText() + "," + cityname.getText() + "," + SelectedState + "," + countrystrn;


        }

        @Override
        protected String doInBackground(String... strings) {

            try {


                ServerConnection server = new ServerConnection();
                connection = server.getConnection();
                stmt = connection.createStatement();
                String query = "insert into registration(name,mobileno,email,dateofbirth,address,area,shopname,doorno,street,pincode,city,state,country,latttitude,longtitude,usertype,appid,Status,insertdate, shopphoto,IsTruckBus,IsLCV,IsCar,Is3W,Is2W,IsTractor,IsOthers) " +
                        "values('" + namestrn + "','" + mobilenostn + "','" + emailtrn + "','" + dobeditstrng + "','" + addressstrn + "','" + areastrn + "','" + shopnametrn + "','" + doornostrn + "','" + streetstrn + "','" + pincodetrn + "','" + citynametrn + "','" + SelectedState + "','" + countrystrn + "','" + latitudestrn + "','" + lontitudestrn + "','" + Usertype + "','no','Active','" + formattedDate + "','" + mobilenostn + ".jpg" + "' ,'" + truckbus + "','" + lcv + "','" + car + "','" + three + "','" + two + "','" + tractor + "','" + other + "' )";
                Log.v("Insert query ", query);
                i = stmt.executeUpdate(query);

                if (i == 0) {
                    connection.close();
                    stmt.close();
                    return "notinsert";
                } else {
                    connection.close();
                    stmt.close();
                    return "insert";
                }
            } catch (Exception e) {
                e.printStackTrace();
                return "error";
            }


        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressDialog.dismiss();
            if (s.equals("insert")) {
                alertbox.showAlertboxwithback("Registered successfully !");

            } else if (s.equals("notinsert")) {

                alertbox.showAlertbox("Registration was not successful !. Please try again later");
            } else {
                alertbox.showAlertbox(getResources().getString(R.string.slow_internet));
            }

        }
    }


}



/* final AlertDialog alertDialog = new AlertDialog.Builder(
                        RegisterRetailerOrMech.this).create();

                LayoutInflater inflater = getLayoutInflater();
                View dialogView = inflater.inflate(R.layout.alertbox, null);
                alertDialog.setView(dialogView);
                LinearLayout mail = (LinearLayout) dialogView.findViewById(R.id.mail);
                TextView log = (TextView) dialogView.findViewById(R.id.call_mail_text);
                LinearLayout call = (LinearLayout) dialogView.findViewById(R.id.call);
                log.setText("You Need To Call or Mail to Gates");
                call.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View arg0) {
                        // TODO Auto-generated method stub
                        String number = "04447115100";
                        Intent intent = new Intent(Intent.ACTION_DIAL);
                        intent.setData(Uri.parse("tel:" + number));
                        startActivity(intent);
                        alertDialog.dismiss();
                    }
                });
                mail.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        *//*Intent email = new Intent(Intent.ACTION_SEND);
                        email.putExtra(Intent.EXTRA_EMAIL, new String[]{ "krishnan@brainmagic.info"});

                        email.putExtra(Intent.EXTRA_SUBJECT, "");
                        email.putExtra(Intent.EXTRA_TEXT, "");

                       // startActivity(Intent.createChooser(email, "Choose an Email client :"));

                        Intent i = new Intent(Intent.ACTION_SEND);
                        i.setType("message/rfc822");
                        i.putExtra(Intent.EXTRA_EMAIL  , new String[]{"krishnan@brainmagic.info"});
                        i.putExtra(Intent.EXTRA_SUBJECT, "subject of email");
                        i.putExtra(Intent.EXTRA_TEXT   , "body of email");
                        try {
                            startActivity(Intent.createChooser(i, "Send mail..."));
                        } catch (android.content.ActivityNotFoundException ex) {
                            Toast.makeText(RegisterRetailerOrMech.this, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
                        }*//*

                        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                                "mailto", "CC-AAMI@gates.com", null));
                        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Need to Register");
                        emailIntent.putExtra(Intent.EXTRA_TEXT, "Dear Gates, \n Unfortunately my mobile has some problem, so Gates App was Uninstalled. \n Now i want to register my profile " +
                                "so please Give me Permission \n\n Regards \n  Name         : " + select_name + "\n Email        :" + select_email + "\n Mobile No        : " + select_phno + "\n ");
                        startActivity(Intent.createChooser(emailIntent, "Send email..."));


                        alertDialog.dismiss();
                    }
                });*/


               /* final AlertDialog alertDialog_already_exists = new AlertDialog.Builder(
                        RegisterRetailerOrMech.this).create();

                LayoutInflater already_exists_inflater = getLayoutInflater();
                View already_exists_dialogView = already_exists_inflater.inflate(R.layout.alertbox, null);
                alertDialog_already_exists.setView(already_exists_dialogView);
                ImageView sucess = (ImageView) already_exists_dialogView.findViewById(R.id.success);
                TextView already_exists_log = (TextView) already_exists_dialogView.findViewById(R.id.textView1);
                Button okay = (Button) already_exists_dialogView.findViewById(R.id.okay);
                already_exists_log.setText("User Already Exist ! ");
                sucess.setImageResource(R.drawable.nointernet);
                okay.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View arg0) {
                        // TODO Auto-generated method stub

                        alertDialog_already_exists.dismiss();
                        alertDialog.show();
                    }
                });
                alertDialog_already_exists.show();*/

/*

	private void selectImage() {
		final CharSequence[] items = { "Take Photo", "Choose from Gallery",
				"Cancel" };

		AlertDialog.Builder builder = new Builder(RegisterRetailerOrMech.this);
		builder.setTitle("Add Photo!");
		builder.setItems(items, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int item) {
				boolean result= Utility.checkPermission(RegisterRetailerOrMech.this);

				if (items[item].equals("Take Photo")) {
					userChoosenTask ="Take Photo";
					if(result)
						cameraIntent();

				} else if (items[item].equals("Choose from Gallery")) {
					userChoosenTask ="Choose from Gallery";
					if(result)
						galleryIntent();

				} else if (items[item].equals("Cancel")) {
					dialog.dismiss();
				}
			}
		});
		builder.show();
	}

	private void galleryIntent()
	{
		Intent intent = new Intent();
		intent.setType("image*/
/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
		startActivityForResult(Intent.createChooser(intent, "Select File"),SELECT_FILE);
	}

	private void cameraIntent()
	{
		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		startActivityForResult(intent, REQUEST_CAMERA);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if (resultCode == Activity.RESULT_OK) {
			if (requestCode == SELECT_FILE)
				onSelectFromGalleryResult(data);
			else if (requestCode == REQUEST_CAMERA)
				onCaptureImageResult(data);
		}
	}

	private void onCaptureImageResult(Intent data) {
		Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
		ByteArrayOutputStream bytes = new ByteArrayOutputStream();
		thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);

		File destination = new File(Environment.getExternalStorageDirectory(),
				System.currentTimeMillis() + ".jpg");

		FileOutputStream fo;
		try {
			destination.createNewFile();
			fo = new FileOutputStream(destination);
			fo.write(bytes.toByteArray());
			fo.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		fullpath = destination.getAbsolutePath().toString();
		ivImage.setImageBitmap(thumbnail);
		filename = destination.getName().toString();




	}

	@SuppressWarnings("deprecation")
	private void onSelectFromGalleryResult(Intent data) {

		Bitmap bm=null;
		if (data != null) {
			try {
				bm = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), data.getData());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		ivImage.setImageBitmap(bm);
	}

*/
