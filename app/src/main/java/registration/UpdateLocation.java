package registration;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.brainagic.catalogue.EditProfileActivity;
import com.brainagic.catalogue.catalogue.R;
import com.squareup.picasso.Picasso;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import about.AboutActivity;
import alertbox.Alertbox;
import customer.CustomerActivity;
import dmax.dialog.SpotsDialog;
import engineer.EngineerActivity;
import home.HomeActivity;
import mechanic.MechanicActivity;
import network.NetworkConnection;
import notification.NotificationActivity;
import persistency.ServerConnection;
import product.PriceList;
import product.ProductCatalog;
import retailer.RetailerActivity;
import search.Grid_Search;
import utility.Base64;
import utility.Utility;
import wsd.WSDActivity;

;

public class UpdateLocation extends Activity {


    private  EditText name, email, mobileno, shopname, cityname, pincode, latitude, lontitude, address, doorno, area, street, country,dobedit;
    private Button okay, cancel,browse;
    private Spinner usertype, satespinner;
    private  SpotsDialog progressDialog;
    private String Name, Email, Mobno, Usertype, SelectedState;

    private Connection connection;
    private  Statement stmt;
    private CheckBox checkbox;
    private ResultSet resultSet;
    private  SharedPreferences myshare;
    private  SharedPreferences.Editor editor;
    private  String status = "Active";
    private  String Dbstatus = "";
    private  String formattedDate;
    private  String PROJECT_NUMBER = "97108778128";
    private String regID, select_email, select_phno, dis_select_phno, select_name;
    private ArrayAdapter<CharSequence> adapter;
    private  Alertbox alertbox = new Alertbox(UpdateLocation.this);
    private String updateusertype, updatemobile, updateemail;
    Calendar myCalendar = Calendar.getInstance();
    private CheckBox pv, lcv, hcv, cv;
    private LinearLayout typeofmechlayout;
    private ImageView ivImage;
    private String userChoosenTask;
    private Bitmap ShopPhoto,bitmap;

    public String UPLOAD_URL  = "http://rbl.brainmagicllc.com/Image/UploadToServer.php";
    //Uri to store the image uri
    private String fileName;
    private int REQUEST_CAMERA = 0, SELECT_FILE = 1;
    private boolean islogin;
    private String Useremail,Usermobile;
    private String mobilestrn;
    private TextView title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_location);

        getActionBar().setIcon(R.drawable.logo);

        updateusertype = getIntent().getStringExtra("usertype");
        updatemobile = getIntent().getStringExtra("mobile");
        updateemail = getIntent().getStringExtra("email");

        name = (EditText) findViewById(R.id.retailernameedittext);
        email = (EditText) findViewById(R.id.emailedittext);
        mobileno = (EditText) findViewById(R.id.phoneedittext);
        shopname = (EditText) findViewById(R.id.shopedittext);
        cityname = (EditText) findViewById(R.id.citytext);
        pincode = (EditText) findViewById(R.id.pincodetext);
        country = (EditText) findViewById(R.id.countrytext);
        dobedit = (EditText) findViewById(R.id.dobtext);

        latitude = (EditText) findViewById(R.id.latitext);
        lontitude = (EditText) findViewById(R.id.longtitext);

        address = (EditText) findViewById(R.id.address);
        doorno = (EditText) findViewById(R.id.dooredittext);
        area = (EditText) findViewById(R.id.areaedittext);
        street = (EditText) findViewById(R.id.streetedittext);
        typeofmechlayout = (LinearLayout) findViewById(R.id.mechtypelayour);


        title = (TextView) findViewById(R.id.profile);
        title.setText("Update "+updateusertype);

        okay = (Button) findViewById(R.id.okay);
        cancel = (Button) findViewById(R.id.cancel);
        usertype = (Spinner) findViewById(R.id.usertypespinner);
        satespinner = (Spinner) findViewById(R.id.satetspinner);

        ivImage = (ImageView) findViewById(R.id.ivImage);
        browse = (Button) findViewById(R.id.browsebutton);



        myshare = getSharedPreferences("Registration", MODE_PRIVATE);
        editor = myshare.edit();
        islogin = myshare.getBoolean("islogin", false);
        Usertype = myshare.getString("Usertype", "");
        Useremail = myshare.getString("Email","") ;
        Usermobile = myshare.getString("MobNo","") ;
        browse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage();
            }
        });


        ivImage.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                selectImage();
            }
        });



        pv = (CheckBox) findViewById(R.id.pvm);
        hcv = (CheckBox) findViewById(R.id.hcvm);
        lcv = (CheckBox) findViewById(R.id.lcvm);
        cv = (CheckBox) findViewById(R.id.cvm);

        String[] types = {updateusertype};

        Date d = new Date();
        myCalendar.setTime(d);
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        formattedDate = df.format(myCalendar.getTime());


        progressDialog = new SpotsDialog(UpdateLocation.this,R.style.Custom);
        progressDialog.setCancelable(false);
        progressDialog.show();


        myshare = getSharedPreferences("Registration", MODE_PRIVATE);
        editor = myshare.edit();

       /* if(registertype.equals("Mechanic"))
        {
            typeofmechlayout.setVisibility(View.VISIBLE);
        }
        else {
            typeofmechlayout.setVisibility(View.GONE);
        }*/


        adapter = ArrayAdapter.createFromResource(this, R.array.state_array, R.layout.simple_spinner_item);
         adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        satespinner.setAdapter(adapter);


        ArrayAdapter<String> xxx = new ArrayAdapter<String>(UpdateLocation.this, R.layout.simple_spinner_item, types);
       xxx.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        usertype.setAdapter(xxx);


        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

        };


        dobedit.setOnFocusChangeListener(new View.OnFocusChangeListener()
        {
            @Override
            public void onFocusChange(View v, boolean hasFocus)
            {
                if (dobedit.isFocused())
                {
                    new DatePickerDialog(UpdateLocation.this, date, myCalendar
                            .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                            myCalendar.get(Calendar.DAY_OF_MONTH)).show();
                }
            }
        });


        usertype.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                Usertype = adapterView.getItemAtPosition(i).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        satespinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                SelectedState = adapterView.getItemAtPosition(i).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        okay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Name = name.getText().toString();
                Email = email.getText().toString();
                Mobno = mobileno.getText().toString();

                String PhoneNo = mobileno.getText().toString();
                String Regex = "[^\\d]";
                String PhoneDigits = PhoneNo.replaceAll(Regex, "");

                if (Name.equals("")) {
                    name.setFocusable(true);
                    name.setError("Enter Your Name");
                } else if (PhoneDigits.length() != 10) {
                    Toast.makeText(UpdateLocation.this, "Mobile Number Must be 10 digits", Toast.LENGTH_SHORT).show();
                    mobileno.setError("Invalid mobile number");
                } else if (Usertype.equals("Type Of User")) {
                    Toast.makeText(UpdateLocation.this, "Select User Type", Toast.LENGTH_LONG).show();
                } else if (shopname.getText().equals("")) {
                    shopname.setFocusable(true);
                    shopname.setError("enter shop name");
                } else if (doorno.getText().equals("")) {
                    doorno.setFocusable(true);
                    doorno.setError("Enter door name");
                } else if (street.getText().equals("")) {
                    street.setFocusable(true);
                    street.setError("Enter street name");
                } else if (area.getText().equals("")) {
                    area.setFocusable(true);
                    area.setError("Enter area name");
                } else if (cityname.getText().equals("")) {
                    cityname.setFocusable(true);
                    cityname.setError("Enter city name");
                }
                else if (SelectedState.equals("Select sate")) {
                    Toast.makeText(UpdateLocation.this, "Mobile Number Must be 10 digits", Toast.LENGTH_SHORT).show();
                } else {

                    if (Usertype.equals("Rane Employee")) {
                        if (!Email.equals("")) {
                            if ((CheckGateEmail(email.getText().toString()))) {
                                checkinternet();
                            } else {
                                email.setFocusable(true);
                                email.setError("Please provide your Rane email id for registration");

                                Toast toast = Toast.makeText(UpdateLocation.this, "Please provide your Rane email id for registration", Toast.LENGTH_LONG);
                                toast.setGravity(Gravity.CENTER, 0, 0);
                                toast.show();
                            }
                        }

                    }

                    else {
                        checkinternet();
                    }

                }
            }
        });

// for home abd back
        TextView home = (TextView) findViewById(R.id.fontDesign1);
        TextView back = (TextView) findViewById(R.id.fontDesign2);
        home.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                Intent ho = new Intent(UpdateLocation.this, HomeActivity.class);
                ho.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(ho);
            }
        });
        back.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub

              // startActivity(new Intent(UpdateLocation.this, Grid_Search.class));
                onBackPressed();
            }
        });


        cancel.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub

                //startActivity(new Intent(UpdateLocation.this, Grid_Search.class));
                onBackPressed();
            }
        });




    checkinternetforgetUser();
    }






    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.popup, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // ...

        switch (item.getItemId()) {
            case R.id.about:
                // search action
                startActivity(new Intent(UpdateLocation.this, AboutActivity.class));
                return true;
            case R.id.product:
                // location found
                startActivity(new Intent(UpdateLocation.this, ProductCatalog.class));
                return true;
            case R.id.price:
                // refresh
                startActivity(new Intent(UpdateLocation.this, PriceList.class));
                return true;
            case R.id.wsd:
                // help action
                if(!Usertype.equals("Distributor"))
                {
                    alertbox.showAlertbox("You are not allowed to view this page !","auth");
                }
                else
                {
                    startActivity(new Intent(UpdateLocation.this, WSDActivity.class));
                }
                return true;
            case R.id.retailer:
                // check for updates action
                if(!Usertype.equals("Retailer"))
                {
                    alertbox.showAlertbox("You are not allowed to view this page !","auth");
                }
                else {
                    startActivity(new Intent(UpdateLocation.this, RetailerActivity.class));
                }
                return true;
            case R.id.fieldengi:
                // check for updates action

                if(Usertype.equals("Rane Employee") || Usertype.equals("Sales Executives")|| Usertype.equals("MSR"))
                {

                    startActivity(new Intent(UpdateLocation.this, EngineerActivity.class));
                }
                else
                {
                    alertbox.showAlertbox("You are not allowed to view this page !","auth");
                }
                return true;
            case R.id.msr:
                // check for updates action

                if(Usertype.equals("MSR"))
                {

                    startActivity(new Intent(UpdateLocation.this, EngineerActivity.class));
                }
                else
                {
                    alertbox.showAlertbox("You are not allowed to view this page !");
                }
                return true;
            case R.id.mech:
                // check for updates action
                if(!Usertype.equals("Mechanic"))
                {
                    alertbox.showAlertbox("You are not allowed to view this page !","auth");
                }
                else
                {
                    startActivity(new Intent(UpdateLocation.this, MechanicActivity.class));
                }
                return true;
            case R.id.customer:
                // check for updates action

                if((Usertype.equals("Others")) || (Usertype.equals("Fleet Operators")))
                {

                    startActivity(new Intent(UpdateLocation.this, CustomerActivity.class));
                }
                else
                {
                    alertbox.showAlertbox("You are not allowed to view this page !","auth");
                }
                return true;
            case R.id.notification:
                // check for updates action
                startActivity(new Intent(UpdateLocation.this, NotificationActivity.class));
                return true;
            case R.id.edit:
                // check for updates action
                Intent edit = new Intent(UpdateLocation.this,EditProfileActivity.class);
                edit.putExtra("usertype",Usertype);
                edit.putExtra("mobile",Usermobile);
                edit.putExtra("email",Useremail);
                startActivity(edit);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }




    private void selectImage() {
        final CharSequence[] items = {"Take Photo", "Choose from Library",
                "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(UpdateLocation.this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result = Utility.checkPermission(UpdateLocation.this);

                if (items[item].equals("Take Photo")) {
                    userChoosenTask = "Take Photo";
                    if (result)
                        cameraIntent();

                } else if (items[item].equals("Choose from Library")) {
                    userChoosenTask = "Choose from Library";
                    if (result)
                        galleryIntent();

                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void galleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
    }

    private void cameraIntent() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE)
                onSelectFromGalleryResult(data);

            else if (requestCode == REQUEST_CAMERA)
                onCaptureImageResult(data);


        }
    }

    private void onCaptureImageResult(Intent data) {
        ShopPhoto = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        ShopPhoto.compress(Bitmap.CompressFormat.JPEG, 90, bytes);

        File destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");

        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        ivImage.setImageBitmap(ShopPhoto);

    }


    private void onSelectFromGalleryResult(Intent data) {

        ShopPhoto = null;
        if (data != null) {
            try {
                ShopPhoto = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), data.getData());
                Uri path =  data.getData();
                fileName = path.getLastPathSegment();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        ivImage.setImageBitmap(ShopPhoto);


    }




    class ImageUploadTask extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            BitmapDrawable drawable = (BitmapDrawable) ivImage.getDrawable();
            ShopPhoto = drawable.getBitmap();

            progressDialog = new SpotsDialog(UpdateLocation.this,R.style.Custom);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @SuppressWarnings("unused")
        @Override
        protected String doInBackground(Void... unsued) {
            InputStream is;
            BitmapFactory.Options bfo;
            Bitmap bitmapOrg;
            ByteArrayOutputStream bao ;

            bfo = new BitmapFactory.Options();
            bfo.inSampleSize = 2;

            bao = new ByteArrayOutputStream();


            ShopPhoto.compress(Bitmap.CompressFormat.JPEG, 90, bao);
            byte [] ba = bao.toByteArray();
            String ba1 = Base64.encodeBytes(ba);
            ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            nameValuePairs.add(new BasicNameValuePair("image",ba1));
            nameValuePairs.add(new BasicNameValuePair("cmd",mobilestrn+".jpg"));
            Log.v("log_tag", System.currentTimeMillis()+".jpg");
            try{
                HttpClient httpclient = new DefaultHttpClient();
                HttpPost httppost = new
                        //  Here you need to put your server file address
                        HttpPost(UPLOAD_URL);
                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                HttpResponse response = httpclient.execute(httppost);
                HttpEntity entity = response.getEntity();
                is = entity.getContent();
                Log.v("log_tag", "In the try Loop" );
            }catch(Exception e){
                Log.v("log_tag", "Error in http connection "+e.toString());
            }
            return "Success";

        }



        @Override
        protected void onPostExecute(String sResponse)
        {
            new SaveRegisterDatails().execute();
        }

    }


    @Override
    public void onBackPressed() {
        startActivity(new Intent(UpdateLocation.this, Grid_Search.class));
    }

    private void updateLabel() {

        String myFormat = "dd/MM/yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        dobedit.setText(sdf.format(myCalendar.getTime()));
    }


    private boolean CheckGateEmail(String usertype) {

        String[] parts = usertype.split("@");
        String part2 = parts[1];
        if (part2.equals("rane.com")) {
            return true;
        } else {
            return false;
        }

    }



    private void checkinternet() {
        NetworkConnection connection = new NetworkConnection(UpdateLocation.this);

        if (connection.CheckInternet()) {

            new ImageUploadTask().execute();

        } else {
            // Toast.makeText(UpdateLocation.this,"Not unable to connect please check your Internet connection ! ", Toast.LENGTH_SHORT).show();
            alertbox.showAlertboxwithback( getResources().getString(R.string.nointernetmsg));


        }
    }


    private void checkinternetforgetUser() {
        NetworkConnection connection = new NetworkConnection(UpdateLocation.this);

        if (connection.CheckInternet()) {

            new AlreadyRegister().execute();

        } else {
            // Toast.makeText(UpdateLocation.this,"Not unable to connect please check your Internet connection ! ", Toast.LENGTH_SHORT).show();
            alertbox.showAlertboxwithback(getResources().getString(R.string.nointernetmsg));


        }

    }



    class AlreadyRegister extends AsyncTask<String, Void, String> {
        String selectquery;
        private String namestrn, emailtrn, shopnametrn, citynametrn, pincodetrn, latitudestrn, lontitudestrn, addressstrn, doornostrn, areastrn, streetstrn, countrystrn,dobstrn;
        private ArrayList<String> state = new ArrayList<String>();
        String[] types = {""};
        String imageURL = "http://rbl.brainmagicllc.com/Image/";
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... strings) {

            ServerConnection server = new ServerConnection();
            try {
                connection = server.getConnection();

                stmt = connection.createStatement();

                selectquery = "SELECT * from registration where  mobileno='" + updatemobile + "' and  usertype = '" + updateusertype + "'  ";

                resultSet = stmt.executeQuery(selectquery);


                if (resultSet.next()) {

                    Dbstatus = resultSet.getString("Status");
                    namestrn = resultSet.getString("name");
                    emailtrn = resultSet.getString("email");
                    shopnametrn = resultSet.getString("shopname");
                    citynametrn = resultSet.getString("city");
                    pincodetrn = resultSet.getString("pincode");
                    addressstrn = resultSet.getString("address");
                    doornostrn = resultSet.getString("doorno");
                    areastrn = resultSet.getString("area");
                    streetstrn = resultSet.getString("street");
                    mobilestrn = resultSet.getString("mobileno");
                    countrystrn = resultSet.getString("country");
                    dobstrn = resultSet.getString("dateofbrith");
                    latitudestrn = resultSet.getString("latttitude");
                    lontitudestrn = resultSet.getString("longtitude");
                   types[0] = resultSet.getString("usertype");
                    state.add(resultSet.getString("state"));
                    imageURL = imageURL+ resultSet.getString("shopphoto");
                    resultSet.close();
                    connection.close();
                    stmt.close();

                    return "data";

                } else {
                    resultSet.close();
                    connection.close();
                    stmt.close();

                    return "nodata";

                }
            } catch (Exception e) {
                e.printStackTrace();
                return "error";
            }

        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressDialog.dismiss();
            if (s.equals("data")) {


                name.setText(namestrn);
                email.setText(emailtrn);
                mobileno.setText(mobilestrn);
                shopname.setText(shopnametrn);
                cityname.setText(citynametrn);
                pincode.setText(pincodetrn);
                country.setText(countrystrn);
                address.setText(addressstrn);
                doorno.setText(doornostrn);
                area.setText(areastrn);
                street.setText(streetstrn);
                dobedit.setText(dobstrn);

                if(latitudestrn.equals("Not Given") && lontitudestrn.equals("Not Given") )
                {
                    latitude.setHint(latitudestrn);
                    lontitude.setHint(lontitudestrn);
                }
                else
                {
                    latitude.setText(latitudestrn);
                    lontitude.setText(lontitudestrn);
                }



                Picasso.with(UpdateLocation.this).load(imageURL).into(ivImage);



                ArrayAdapter<String> stadapter = new ArrayAdapter<String>(UpdateLocation.this, R.layout.simple_spinner_item, state);
                satespinner.setAdapter(stadapter);


                ArrayAdapter<String> xxx = new ArrayAdapter<String>(UpdateLocation.this, R.layout.simple_spinner_item, types);
                usertype.setAdapter(xxx);


            } else if (s.equals("nodata")) {
                alertbox.showAlertbox("Data not found !");
            }  else {
                alertbox.showAlertboxwithback(getResources().getString(R.string.slow_internet));
            }


        }
    }


    class SaveRegisterDatails extends AsyncTask<String, Void, String> {
        private int i = 0;
        private String namestrn, emailtrn, shopnametrn, citynametrn, pincodetrn, latitudestrn, lontitudestrn, addressstrn, doornostrn, areastrn, streetstrn, countrystrn,dobstrn;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();


            latitudestrn = latitude.getText().toString();
            lontitudestrn = lontitude.getText().toString();


            if(latitude.getText().length()==0 && lontitude.getText().length()==0 )
            {
                latitude.setText("Not Given");
                lontitude.setText("Not Given");
            }

            namestrn = name.getText().toString();
            emailtrn = email.getText().toString();
            mobilestrn = mobileno.getText().toString();
            shopnametrn = shopname.getText().toString();
            citynametrn = cityname.getText().toString();
            pincodetrn = pincode.getText().toString();
            countrystrn = country.getText().toString();
            addressstrn = address.getText().toString();
            doornostrn = doorno.getText().toString();
            areastrn = area.getText().toString();
            streetstrn = street.getText().toString();
            dobstrn = dobedit.getText().toString();




        }

        @Override
        protected String doInBackground(String... strings) {

            try {

                ServerConnection server = new ServerConnection();
                connection = server.getConnection();
                stmt = connection.createStatement();

                String query = "update registration set name = '" + namestrn + "', mobileno = '" + mobilestrn + "',email = '" + emailtrn + "',dateofbrith = '" + dobstrn + "',address = '" + addressstrn + "',area  = '" + areastrn + "',shopname = '" + shopnametrn + "',doorno = '" + doornostrn + "',street = '" + streetstrn + "',pincode = '" + pincodetrn + "',city = '" + citynametrn + "',state = '" + SelectedState + "',country = '" + countrystrn + "',latttitude = '" + latitudestrn + "',longtitude = '" + lontitudestrn + "',usertype = '" + Usertype + "',Status = 'Active',insertdate = '" + formattedDate + "' , shopphoto ='" +  mobilestrn+".jpg" + "' where usertype = '" + Usertype + "' and mobileno ='" + Mobno + "' ";

                Log.v("Update  query ", query);
                i = stmt.executeUpdate(query);

                if (i == 0) {
                    connection.close();
                    stmt.close();
                    return "notinsert";
                } else {
                    connection.close();
                    stmt.close();
                    return "insert";
                }
            } catch (Exception e) {
                e.printStackTrace();
                return "error";
            }


        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressDialog.dismiss();
            if (s.equals("insert")) {
                alertbox.showAlertboxwithback("Details updated successfully !");
            } else if (s.equals("notinsert")) {
                alertbox.showAlertbox("Updation was not successful. Please try again later !");
            } else
            {
                alertbox.showAlertboxwithback(getResources().getString(R.string.slow_internet));
            }

        }
    }


}