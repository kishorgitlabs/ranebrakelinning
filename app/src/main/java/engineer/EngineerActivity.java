package engineer;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.brainagic.catalogue.EditProfileActivity;
import com.brainagic.catalogue.catalogue.R;

import about.AboutActivity;
import alertbox.Alertbox;
import customer.CustomerActivity;
import home.HomeActivity;
import mechanic.MechanicActivity;
import notification.NotificationActivity;
import product.PriceList;
import product.ProductCatalog;
import registration.RegisterRetailerOrMech;
import retailer.RetailerActivity;
import salesteam.SalesTeamActivity;
import search.SearchMapFragment;
import wsd.WSDActivity;

;

public class EngineerActivity extends Activity {


    private SharedPreferences checkSharedPreferences;
    private String Usertype,Useremail,Usermobile;
    private Button findaddress;
    private RelativeLayout regisretailer, findretailer, findmechanic, regisetmech,sales,logoutlayout,wsdlayout;
    SharedPreferences myshare;
    SharedPreferences.Editor editor;
    private boolean islogin;
    Alertbox alertbox = new Alertbox(EngineerActivity.this);
    private TextView tittletxt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_engineer);


        getActionBar().setIcon(R.drawable.logo);

        tittletxt = (TextView) findViewById(R.id.title);

        regisetmech = (RelativeLayout) findViewById(R.id.registmech);
        regisretailer = (RelativeLayout) findViewById(R.id.registretailer);

        findretailer = (RelativeLayout) findViewById(R.id.findretailerlayout);
        findmechanic = (RelativeLayout) findViewById(R.id.findmechaniclayout);
        sales= (RelativeLayout) findViewById(R.id.salesteam);
        logoutlayout= (RelativeLayout) findViewById(R.id.logoutlay);

        myshare = getSharedPreferences("Registration", MODE_PRIVATE);
        editor = myshare.edit();
        Usertype = myshare.getString("Usertype","") ;
        Useremail = myshare.getString("Email","") ;
        Usermobile = myshare.getString("MobNo","") ;
        islogin = myshare.getBoolean("islogin", false);

        tittletxt.setText(Usertype);


        if(islogin)
        {
            logoutlayout.setVisibility(View.VISIBLE);
        }
        else
        {
            logoutlayout.setVisibility(View.GONE);
        }


        regisretailer.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent regiIntent = new Intent(EngineerActivity.this, RegisterRetailerOrMech.class);
                regiIntent.putExtra("registertype", "Retailer");
                startActivity(regiIntent);
            }
        });
        regisetmech.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent regiIntent = new Intent(EngineerActivity.this, RegisterRetailerOrMech.class);
                regiIntent.putExtra("registertype", "Mechanic");
                startActivity(regiIntent);
            }
        });

        findretailer.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent adminIntent = new Intent(EngineerActivity.this, SearchMapFragment.class);
                adminIntent.putExtra("findertype", "Retailer");
                startActivity(adminIntent);

            }

        });

        findmechanic.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent adminIntent = new Intent(EngineerActivity.this, SearchMapFragment.class);
                adminIntent.putExtra("findertype", "Mechanic");
                startActivity(adminIntent);

            }

        });

        sales.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                startActivity(new Intent(EngineerActivity.this, SalesTeamActivity.class));

            }
        });

        wsdlayout = (RelativeLayout) findViewById(R.id.wsd);
        wsdlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent find = new Intent(EngineerActivity.this, SearchMapFragment.class);
                find.putExtra("findertype", "Distributor");
                startActivity(find);
            }
        });

        logoutlayout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {



                final AlertDialog alertDialogbox = new AlertDialog.Builder(
                        EngineerActivity.this).create();

                LayoutInflater inflater = getLayoutInflater();
                View dialogView = inflater.inflate(R.layout.alertbox, null);
                alertDialogbox.setView(dialogView);

                TextView log = (TextView) dialogView.findViewById(R.id.textView1);
                Button okay = (Button) dialogView.findViewById(R.id.okay);
                log.setText("Logout Successfull");
                okay.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View arg0) {
                        // TODO Auto-generated method stub

                        editor.putBoolean("islogin", false);
                        editor.commit();
                        logoutlayout.setVisibility(View.GONE);
                        alertDialogbox.dismiss();
                    }
                });
                alertDialogbox.setCancelable(false);
                alertDialogbox.show();



            }
        });


        // for home abd back
        TextView home = (TextView) findViewById(R.id.fontDesign1);
        TextView back = (TextView) findViewById(R.id.fontDesign2);
        home.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                Intent ho = new Intent(EngineerActivity.this, HomeActivity.class);
                ho.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(ho);
            }
        });
        back.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                onBackPressed();
            }
        });
    }

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        super.onBackPressed();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(islogin)
        {
            logoutlayout.setVisibility(View.VISIBLE);
        }
        else
        {
            logoutlayout.setVisibility(View.GONE);
        }
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.popup, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // ...

        switch (item.getItemId()) {
            case R.id.about:
                // search action
                startActivity(new Intent(EngineerActivity.this, AboutActivity.class));
                return true;
            case R.id.product:
                // location found
                startActivity(new Intent(EngineerActivity.this, ProductCatalog.class));
                return true;
            case R.id.price:
                // refresh
                startActivity(new Intent(EngineerActivity.this, PriceList.class));
                return true;
            case R.id.wsd:
                // help action
                if(!Usertype.equals("Distributor"))
                {
                    alertbox.showAlertbox("You are not allowed to view this page !","auth");
                }
                else
                {
                    startActivity(new Intent(EngineerActivity.this, WSDActivity.class));
                }
                return true;
            case R.id.retailer:
                // check for updates action
                if(!Usertype.equals("Retailer"))
                {
                    alertbox.showAlertbox("You are not allowed to view this page !","auth");
                }
                else {
                    startActivity(new Intent(EngineerActivity.this, RetailerActivity.class));
                }
                return true;
            case R.id.fieldengi:
                // check for updates action

                if(Usertype.equals("Rane Employee") || Usertype.equals("Sales Executives")|| Usertype.equals("MSR"))
                {

                    startActivity(new Intent(EngineerActivity.this, EngineerActivity.class));
                }
                else
                {
                    alertbox.showAlertbox("You are not allowed to view this page !","auth");
                }
                return true;
            case R.id.msr:
                // check for updates action

                if(Usertype.equals("MSR"))
                {

                    startActivity(new Intent(EngineerActivity.this, EngineerActivity.class));
                }
                else
                {
                    alertbox.showAlertbox("You are not allowed to view this page !");
                }
                return true;
            case R.id.mech:
                // check for updates action
                if(!Usertype.equals("Mechanic"))
                {
                    alertbox.showAlertbox("You are not allowed to view this page !","auth");
                }
                else
                {
                    startActivity(new Intent(EngineerActivity.this, MechanicActivity.class));
                }
                return true;
            case R.id.customer:
                // check for updates action

                if((Usertype.equals("Others")) || (Usertype.equals("Fleet Operators")))
                {

                    startActivity(new Intent(EngineerActivity.this, CustomerActivity.class));
                }
                else
                {
                    alertbox.showAlertbox("You are not allowed to view this page !","auth");
                }
                return true;
            case R.id.notification:
                // check for updates action
                startActivity(new Intent(EngineerActivity.this, NotificationActivity.class));
                return true;
            case R.id.edit:
                // check for updates action
                Intent edit = new Intent(EngineerActivity.this,EditProfileActivity.class);
                edit.putExtra("usertype",Usertype);
                edit.putExtra("mobile",Usermobile);
                edit.putExtra("email",Useremail);
                startActivity(edit);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


}