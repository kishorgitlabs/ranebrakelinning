package feedback;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.brainagic.catalogue.EditProfileActivity;
import com.brainagic.catalogue.catalogue.R;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import about.AboutActivity;
import alertbox.Alertbox;
import customer.CustomerActivity;
import dmax.dialog.SpotsDialog;
import email.Mail;
import engineer.EngineerActivity;
import home.HomeActivity;
import mechanic.MechanicActivity;
import network.NetworkConnection;
import notification.NotificationActivity;
import persistency.ServerConnection;
import product.PriceList;
import product.ProductCatalog;
import retailer.RetailerActivity;
import wsd.WSDActivity;

;

public class FeedbackActivity extends Activity {

    EditText name, email, mobileno, subject, decription;
    Button submit, cancel;

    SpotsDialog progressDialog;

    Connection connection;
    Statement stmt;
    ResultSet rset;
    SharedPreferences myshare;
    SharedPreferences.Editor editor;
    String formattedDate;
    Alertbox alertbox = new Alertbox(FeedbackActivity.this);
    private String Email;
    private String Usertype,nameString,emailString,mobileString;
    private String Useremail,Usermobile;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);
        getActionBar().setLogo(R.drawable.logo);

        name = (EditText) findViewById(R.id.nameedittext);
        email = (EditText) findViewById(R.id.emailedittext);
        mobileno = (EditText) findViewById(R.id.phoneedittext);
        subject = (EditText) findViewById(R.id.subjectedittext);
        decription = (EditText) findViewById(R.id.descriptionedittext);





        submit = (Button) findViewById(R.id.submit);
        cancel = (Button) findViewById(R.id.cancel);


        Date d = new Date();

        Calendar c = Calendar.getInstance();
        c.setTime(d);

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        formattedDate = df.format(c.getTime());


        myshare = getSharedPreferences("Registration", MODE_PRIVATE);
        editor = myshare.edit();

        Usertype = myshare.getString("Usertype", "");
        nameString = myshare.getString("Name", "");
        emailString = myshare.getString("Email", "");
        mobileString = myshare.getString("MobNo", "");



        Usertype = myshare.getString("Usertype","") ;
        Useremail = myshare.getString("Email","") ;
        Usermobile = myshare.getString("MobNo","") ;

        name.setText(nameString);
        mobileno.setText(mobileString);
        email.setText(emailString);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                Email = email.getText().toString();

                if (name.getText().length() == 0)
                {
                    name.setFocusable(true);
                    name.setError("Enter your name");
                }
                else if (mobileno.getText().length() != 10)
                {
                    Toast.makeText(FeedbackActivity.this, "Mobile number must be 10 digits", Toast.LENGTH_SHORT).show();
                    mobileno.setError("Invalid mobile number");
                }
                else if (decription.getText().length()==0)
                {
                    decription.setFocusable(true);
                    decription.setError("enter decription of Feedback");
                }
                else if (email.getText().length()==0 && !isValidEmail(email.getText().toString()))
                {
                    Toast.makeText(FeedbackActivity.this, "Enter your email !", Toast.LENGTH_LONG).show();
                    //Snackbar.make(findViewById(R.id.parentPanel), "Select user type", Snackbar.LENGTH_LONG).show();
                }
                else
                {
                    checkinternet();
                }
            }
        });


        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

      //  checkinternetforValue();


        // for home abd back
        TextView home = (TextView) findViewById(R.id.fontDesign1);
        TextView back = (TextView) findViewById(R.id.fontDesign2);
        home.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                Intent ho = new Intent(FeedbackActivity.this, HomeActivity.class);
                ho.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(ho);
            }
        });
        back.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                onBackPressed();
            }
        });

    }


    public final static boolean isValidEmail(CharSequence target) {
        if (TextUtils.isEmpty(target)) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    private void checkinternet() {
        NetworkConnection connection = new NetworkConnection(FeedbackActivity.this);

        if (connection.CheckInternet()) {

            //new BackroundRunning().execute();
            new SaveFeedbackDatails().execute();

        } else {
            // Toast.makeText(FirstRegistration.this,"Not unable to connect please check your Internet connection ! ", Toast.LENGTH_SHORT).show();
            alertbox.showAlertboxwithback(getResources().getString(R.string.nointernetmsg));


        }


    }



    class SaveFeedbackDatails extends AsyncTask<String, Void, String> {
        private int i = 0;
        private String namestrn, emailtrn, subjectstrn, descriptionstrn, mobilestrn;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new SpotsDialog(FeedbackActivity.this,R.style.Custom);
            progressDialog.setCancelable(false);
            progressDialog.show();

            namestrn = name.getText().toString();
            emailtrn = email.getText().toString();
            mobilestrn = mobileno.getText().toString();
            subjectstrn = subject.getText().toString();
            descriptionstrn = decription.getText().toString();


        }

        @Override
        protected String doInBackground(String... strings) {

            try {

                ServerConnection server = new ServerConnection();
                connection = server.getConnection();
                stmt = connection.createStatement();
                String query = "insert into Feedback(name,mobile,email,subject,description,insertdate,user_Type) values('" + namestrn + "','" + mobilestrn + "','" + emailtrn + "','" + subjectstrn + "','" + descriptionstrn + "','" + formattedDate + "','" + Usertype + "')";
                Log.v("Insert query ", query);
                i = stmt.executeUpdate(query);

                if (i == 0) {
                    connection.close();
                    stmt.close();
                    return "notinsert";
                } else {

                    StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                    StrictMode.setThreadPolicy(policy);
                    String mail = "muthu@brainmagic.info,raghuvamsan@brainmagic.info,g.prabhakaran@ranegroup.com";
                    // String mail = "muthu@brainmagic.info";
                    Mail localMail1 = new Mail("ranebrakeline@gmail.com", "ranegroup");
                    String[] arrayOfString = mail.split(",");

                    localMail1.setTo(arrayOfString);
                    localMail1.setFrom("ranebrakeline@gmail.com");
                    String subject = "Rane Brake Line: "+subjectstrn;
                    localMail1.setSubject(subject);

                    String body =" Name     : "+namestrn +" \n Mobile   : "+mobilestrn + " \n Email    : "+emailtrn+" \n Usertype :"+Usertype +"" +
                            "\n\n Description :              "+descriptionstrn;

                    Log.v("body",body);

                    localMail1.setBody(body);
                    if (localMail1.send())
                    {
                        connection.close();
                        stmt.close();
                        return "insert";
                    }
                    else
                    {
                        connection.close();
                        stmt.close();
                        return "notsend";
                    }

                }
            } catch (Exception e) {
                e.printStackTrace();
                return "error";
            }


        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressDialog.dismiss();
            if (s.equals("insert")) {
                alertbox.showAlertbox("Feedback submitted successfully !");
            } else if (s.equals("notinsert"))
            {
                alertbox.showAlertbox("Feedback not submitted. Please try again !");
            }
            else
            {
                alertbox.showAlertbox(getResources().getString(R.string.slow_internet));
            }

        }
    }


    /*class GetDetails extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progressDialog = new ProgressDialog(FeedbackActivity.this);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setCancelable(false);
            progressDialog.setMessage("Loading data...");
            progressDialog.show();

        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                ServerConnection server = new ServerConnection();
                connection = server.getConnection();
                stmt = connection.createStatement();
                Log.v("State Query ++","select name,mobileno,email from user_registration where ");
                rset = stmt.executeQuery(strings[0]);
                if (rset.next()) {

                    nameString = rset.getString("name");
                    mobileString = rset.getString("mobileno");
                    emailString = rset.getString("email");

                    connection.close();
                    stmt.close();
                    rset.close();
                    return "success";
                }
                else {
                    connection.close();
                    stmt.close();
                    rset.close();
                    return "empty";
                }
            } catch (Exception e) {
                e.printStackTrace();
                return "notsuccess";
            }


        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            progressDialog.dismiss();
            if (s.equals("success")) {

            } else if (s.equals("empty")) {
                alertbox.showAlertbox("No Data Found !");
            } else {
                alertbox.showAlertbox(getResources().getString(R.string.nointernetmsg));
            }

        }
    }*/




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.popup, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // ...

        switch (item.getItemId()) {
            case R.id.about:
                // search action
                startActivity(new Intent(FeedbackActivity.this, AboutActivity.class));
                return true;
            case R.id.product:
                // location found
                startActivity(new Intent(FeedbackActivity.this, ProductCatalog.class));
                return true;
            case R.id.price:
                // refresh
                startActivity(new Intent(FeedbackActivity.this, PriceList.class));
                return true;
            case R.id.wsd:
                // help action
                if(!Usertype.equals("Distributor"))
                {
                    alertbox.showAlertbox("You are not allowed to view this page !","auth");
                }
                else
                {
                    startActivity(new Intent(FeedbackActivity.this, WSDActivity.class));
                }
                return true;
            case R.id.retailer:
                // check for updates action
                if(!Usertype.equals("Retailer"))
                {
                    alertbox.showAlertbox("You are not allowed to view this page !","auth");
                }
                else {
                    startActivity(new Intent(FeedbackActivity.this, RetailerActivity.class));
                }
                return true;
            case R.id.fieldengi:
                // check for updates action

                if(Usertype.equals("Rane Employee") || Usertype.equals("Sales Executives")|| Usertype.equals("MSR"))
                {

                    startActivity(new Intent(FeedbackActivity.this, EngineerActivity.class));
                }
                else
                {
                    alertbox.showAlertbox("You are not allowed to view this page !","auth");
                }
                return true;
            case R.id.msr:
                // check for updates action

                if(Usertype.equals("MSR"))
                {

                    startActivity(new Intent(FeedbackActivity.this, EngineerActivity.class));
                }
                else
                {
                    alertbox.showAlertbox("You are not allowed to view this page !");
                }
                return true;
            case R.id.mech:
                // check for updates action
                if(!Usertype.equals("Mechanic"))
                {
                    alertbox.showAlertbox("You are not allowed to view this page !","auth");
                }
                else
                {
                    startActivity(new Intent(FeedbackActivity.this, MechanicActivity.class));
                }
                return true;
            case R.id.customer:
                // check for updates action

                if((Usertype.equals("Others")) || (Usertype.equals("Fleet Operators")))
                {

                    startActivity(new Intent(FeedbackActivity.this, CustomerActivity.class));
                }
                else
                {
                    alertbox.showAlertbox("You are not allowed to view this page !","auth");
                }
                return true;
            case R.id.notification:
                // check for updates action
                startActivity(new Intent(FeedbackActivity.this, NotificationActivity.class));
                return true;
            case R.id.edit:
                // check for updates action
                Intent edit = new Intent(FeedbackActivity.this,EditProfileActivity.class);
                edit.putExtra("usertype",Usertype);
                edit.putExtra("mobile",Usermobile);
                edit.putExtra("email",Useremail);
                startActivity(edit);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }




}
