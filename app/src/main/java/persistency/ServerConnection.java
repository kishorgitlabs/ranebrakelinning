package persistency;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import android.util.Log;

public class ServerConnection {
	Connection connection;

	public Connection getConnection() throws InstantiationException,
			IllegalAccessException, ClassNotFoundException, SQLException {
		String driver = "net.sourceforge.jtds.jdbc.Driver";
		Class.forName(driver).newInstance();
		String url="jdbc:jtds:sqlserver://45.127.102.162/RaneBreakLine";
		String user="sa";
		String password ="HT-exwE[cc$%@#DFf";
		DriverManager.setLoginTimeout(10);
		connection=DriverManager.getConnection(url,user,password);
		Log.w("Connection", "connection open");
		return connection;
	}
}
