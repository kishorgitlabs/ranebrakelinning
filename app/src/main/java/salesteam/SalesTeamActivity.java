package salesteam;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.brainagic.catalogue.EditProfileActivity;
import com.brainagic.catalogue.catalogue.R;

import java.util.ArrayList;

import about.AboutActivity;
import adapter.SalesTeamAdapter;
import alertbox.Alertbox;
import customer.CustomerActivity;
import database.DBHelper;
import dmax.dialog.SpotsDialog;
import engineer.EngineerActivity;
import home.HomeActivity;
import mechanic.MechanicActivity;
import notification.NotificationActivity;
import product.PriceList;
import product.ProductCatalog;
import wsd.WSDActivity;

;

public class SalesTeamActivity extends Activity {

    private TextView selectTypetxt;
    private Spinner selectTypespinner;
    private Spinner searchspinner;
    private SQLiteDatabase db;
    private ListView listview;

    private ArrayList<String> searchtypeList;
    private String TypeString;
    private ArrayList<String> selectedspinerList;
    private String searchword;
    private ArrayList<String> NameList;
    private ArrayList<String> MobileList;
    private ArrayList<String> EmailList;
    private ArrayList<String> AddressList;
    private ArrayList<String> StateList;
    private ArrayList<String> CityList;
    private ArrayList<String> DestinationList, zoneList,PincodeList;

    private SalesTeamAdapter adapter;
    private SpotsDialog progressDialog;
    private ArrayList<String> SpinnerStateList,ManegerList;
    private Spinner stateListspinner,cityListspinner;
    private String SelectedState,SelectedCity,ManagerName,ManagerState,ManagerCity,ManagerAddress,Region,ManagerEmail,ManagerTown,ManagerMobile,ManagerPincode,ManagerDestination;
    private TextView headtext,rnametxt,rdestintxt,rmaobiletxt,remailtxt;
    private LinearLayout headlayout;
    Alertbox box = new Alertbox(SalesTeamActivity.this);
    private String Usertype,Useremail,Usermobile;
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    private Alertbox alertbox = new Alertbox(SalesTeamActivity.this);
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sales_team);

        getActionBar().setIcon(R.drawable.logo);
        listview = (ListView) findViewById(R.id.listview);

        NameList = new ArrayList<String>();
        MobileList = new ArrayList<String>();
        EmailList = new ArrayList<String>();
        AddressList = new ArrayList<String>();
        StateList = new ArrayList<String>();
        CityList = new ArrayList<String>();
        DestinationList = new ArrayList<String>();
        SpinnerStateList = new ArrayList<String>();
        zoneList = new ArrayList<String>();
        ManegerList = new ArrayList<String>();
        PincodeList= new ArrayList<String>();


        myshare = getSharedPreferences("Registration", MODE_PRIVATE);
        editor = myshare.edit();
        Usertype = myshare.getString("Usertype", "");
        Useremail = myshare.getString("Email","") ;
        Usermobile = myshare.getString("MobNo","") ;
        
        
        stateListspinner = (Spinner) findViewById(R.id.statespinner);
        //cityListspinner = (Spinner) findViewById(R.id.statespinner);


        headtext = (TextView) findViewById(R.id.headtext);
        rnametxt = (TextView) findViewById(R.id.rnametxt);
        rdestintxt = (TextView) findViewById(R.id.rdestintxt);
        rmaobiletxt = (TextView) findViewById(R.id.rmaobiletxt);
        remailtxt = (TextView) findViewById(R.id.remailtxt);

        headlayout = (LinearLayout) findViewById(R.id.headlayout);


        checkInternet();




        stateListspinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                SelectedState = parent.getItemAtPosition(position).toString();
                if(!SelectedState.equals("Select state"))
                {
                    String query = "SELECT distinct * FROM salesteam INNER JOIN areamanager ON areamanager.state='"+SelectedState+"' and salesteam.managername = '"+ManegerList.get(position-1)+"'";
                    new GetAddressDetails().execute(query);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });



        // for home abd back
        TextView home = (TextView) findViewById(R.id.fontDesign1);
        TextView back = (TextView) findViewById(R.id.fontDesign2);
        home.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                Intent ho = new Intent(SalesTeamActivity.this, HomeActivity.class);
                ho.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(ho);
            }
        });
        back.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                onBackPressed();
            }
        });
    }

    private void checkInternet()
    {

            String query = "select distinct state,managername from areamanager order by state asc";
            new GetStateandCity().execute(query);

    }


    class GetStateandCity extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progressDialog = new SpotsDialog(SalesTeamActivity.this,R.style.Custom);
            progressDialog.setCancelable(false);
            progressDialog.show();
            SpinnerStateList.clear();
            ManegerList.clear();
            SpinnerStateList.add("Select state");

        }

        @Override
        protected String doInBackground(String... strings) {
            try {


                DBHelper dbhelper = new DBHelper(SalesTeamActivity.this);
                db = dbhelper.readDataBase();

                Log.v("Query" , strings[0]);
                Cursor cursor = db.rawQuery(strings[0], null);

                if (cursor.moveToFirst())
                {
                    do
                    {
                        SpinnerStateList.add(cursor.getString(cursor.getColumnIndex("state")));
                        // CityList.add(rset.getString("city"));
                        ManegerList.add(cursor.getString(cursor.getColumnIndex("managername")));
                    }while (cursor.moveToNext());

                }

                if (SpinnerStateList.size()==1) {
                    cursor.close();
                    dbhelper.close();
                    db.close();
                    return "empty";
                }
                else {
                    cursor.close();
                    dbhelper.close();
                    db.close();
                    return "success";
                }
            } catch (Exception e) {
                e.printStackTrace();
                return "notsuccess";
            }


        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            progressDialog.dismiss();
            if (s.equals("success")) {
                stateListspinner.setAdapter(new ArrayAdapter<String>(SalesTeamActivity.this,R.layout.simple_spinner_item,SpinnerStateList));
               // cityListspinner.setAdapter(new ArrayAdapter<String>(SalesTeamActivity.this,R.layout.simple_spinner_item,CityList));
            } else if (s.equals("empty")) {
                box.showAlertbox("No Data Found !");
            } else {
                box.showAlertboxwithback("No Data Found !");
            }

        }
    }



    class GetAddressDetails extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            // instantiate new progress dialog
            progressDialog = new SpotsDialog(SalesTeamActivity.this,R.style.Custom);
            progressDialog.setCancelable(false);
            progressDialog.show();

            NameList.clear();
            MobileList.clear();
            EmailList.clear();
            AddressList.clear();
            StateList.clear();
            CityList.clear();
            DestinationList.clear();
            zoneList.clear();
            PincodeList.clear();

        }

        @Override
        protected String doInBackground(String... strings) {
            try {


                DBHelper dbhelper = new DBHelper(SalesTeamActivity.this);
                db = dbhelper.readDataBase();
                Cursor cursor = db.rawQuery(strings[0], null);
                Log.v("Query ",strings[0]);
                if (cursor.moveToFirst())
                {
                    do
                    {
                        NameList.add(cursor.getString(cursor.getColumnIndex("name")));
                        MobileList.add(cursor.getString(cursor.getColumnIndex("mobile")));
                        EmailList.add(cursor.getString(cursor.getColumnIndex("email")));
                        AddressList.add(cursor.getString(cursor.getColumnIndex("address")));
                        StateList.add(cursor.getString(cursor.getColumnIndex("state")));
                        CityList.add(cursor.getString(cursor.getColumnIndex("city")));
                        PincodeList.add(cursor.getString(cursor.getColumnIndex("pincode")));
                        DestinationList.add(cursor.getString(cursor.getColumnIndex("designation")));


                        Region = cursor.getString(cursor.getColumnIndex("managerregion"));
                        ManagerName =cursor.getString(cursor.getColumnIndex("managername"));
                        ManagerDestination =cursor.getString(cursor.getColumnIndex("managerdesignation"));
                        ManagerTown =cursor.getString(cursor.getColumnIndex("managercity"));
                        ManagerAddress =cursor.getString(cursor.getColumnIndex("manageraddress"));
                        ManagerState =cursor.getString(cursor.getColumnIndex("managerstate"));
                        ManagerEmail =cursor.getString(cursor.getColumnIndex("manageremail"));
                        ManagerMobile =cursor.getString(cursor.getColumnIndex("managermobile"));

                    }while (cursor.moveToNext());

                }
                if (NameList.isEmpty())
                {
                    cursor.close();
                    dbhelper.close();
                    db.close();
                    return "empty";
                }
                cursor.close();
                dbhelper.close();
                db.close();
                return "success";
            }
            catch (Exception e)
            {
                return "notsuccess";
            }


        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            progressDialog.dismiss();
            if (s.equals("success")) {
                headlayout.setVisibility(View.VISIBLE);
                headtext.setText("Head - "+Region);
                remailtxt.setText(ManagerEmail);
                rdestintxt.setText(ManagerDestination);
                rmaobiletxt.setText(ManagerMobile);
                rnametxt.setText(ManagerName);

                adapter = new SalesTeamAdapter(SalesTeamActivity.this, NameList, MobileList, EmailList, AddressList, StateList, CityList, DestinationList,PincodeList);
                adapter.setNotifyOnChange(true);
                listview.setAdapter(adapter);



            } else if (s.equals("empty")) {
                box.showAlertbox("No Data Found !");
                headlayout.setVisibility(View.GONE);
                adapter = new SalesTeamAdapter(SalesTeamActivity.this, NameList, MobileList, EmailList, AddressList, StateList, CityList, DestinationList,PincodeList);
                adapter.setNotifyOnChange(true);
                listview.setAdapter(adapter);
            } else
            {
                box.showAlertboxwithback("Not able to connect. Please check your network connection and try again.");
            }

        }
    }




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.popup, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // ...

        switch (item.getItemId()) {
            case R.id.about:
                // search action
                startActivity(new Intent(SalesTeamActivity.this, AboutActivity.class));
                return true;
            case R.id.product:
                // location found
                startActivity(new Intent(SalesTeamActivity.this, ProductCatalog.class));
                return true;
            case R.id.price:
                // refresh
                startActivity(new Intent(SalesTeamActivity.this, PriceList.class));
                return true;
            case R.id.wsd:
                // help action
                if(!Usertype.equals("Distributor"))
                {
                    alertbox.showAlertbox("You are not allowed to view this page !","auth");
                }
                else
                {
                    startActivity(new Intent(SalesTeamActivity.this, WSDActivity.class));
                }
                return true;
            case R.id.retailer:
                // check for updates action
                if(!Usertype.equals("Retailer"))
                {
                    alertbox.showAlertbox("You are not allowed to view this page !","auth");
                }
                else {
                    startActivity(new Intent(SalesTeamActivity.this, SalesTeamActivity.class));
                }
                return true;
            case R.id.fieldengi:
                // check for updates action

                if(Usertype.equals("Rane Employee") || Usertype.equals("Sales Executives")|| Usertype.equals("MSR"))
                {

                    startActivity(new Intent(SalesTeamActivity.this, EngineerActivity.class));
                }
                else
                {
                    alertbox.showAlertbox("You are not allowed to view this page !","auth");
                }
                return true;
            case R.id.msr:
                // check for updates action

                if(Usertype.equals("MSR"))
                {

                    startActivity(new Intent(SalesTeamActivity.this, EngineerActivity.class));
                }
                else
                {
                    alertbox.showAlertbox("You are not allowed to view this page !");
                }
                return true;
            case R.id.mech:
                // check for updates action
                if(!Usertype.equals("Mechanic"))
                {
                    alertbox.showAlertbox("You are not allowed to view this page !","auth");
                }
                else
                {
                    startActivity(new Intent(SalesTeamActivity.this, MechanicActivity.class));
                }
                return true;
            case R.id.customer:
                // check for updates action

                if(!Usertype.equals("Others") || !Usertype.equals("Fleet Operators"))
                {
                    alertbox.showAlertbox("You are not allowed to view this page !","auth");
                }
                else
                {
                    startActivity(new Intent(SalesTeamActivity.this, CustomerActivity.class));
                }
                return true;
            case R.id.notification:
                // check for updates action
                startActivity(new Intent(SalesTeamActivity.this, NotificationActivity.class));
                return true;
            case R.id.edit:
                // check for updates action
                Intent edit = new Intent(SalesTeamActivity.this,EditProfileActivity.class);
                edit.putExtra("usertype",Usertype);
                edit.putExtra("mobile",Usermobile);
                edit.putExtra("email",Useremail);
                startActivity(edit);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }



}
