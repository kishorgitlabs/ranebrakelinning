package home;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;

import com.brainagic.catalogue.catalogue.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

;

public class HomePageAdapter extends PagerAdapter {
	Context con;
	private List<Integer> servicesImageList;
	LayoutInflater layoutInflater;
	
	public HomePageAdapter(Context con) {
		// TODO Auto-generated constructor stub
		this.con =con;
		
		
				servicesImageList = new ArrayList<Integer>();
				// Image Resources
				Integer[] webServicesImageResourcesList = { R.drawable.prod0,R.drawable.prod1,
						R.drawable.prod2, R.drawable.prod3, R.drawable.prod4,
						 R.drawable.prod5
						 };
				servicesImageList = Arrays.asList(webServicesImageResourcesList);
		
	}
	
	@Override
	public Object instantiateItem(View container, int position) {
		// TODO Auto-generated method stub
		layoutInflater = (LayoutInflater) con.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View view = layoutInflater.inflate(R.layout.viewpager_activity, null);
		ImageView image = (ImageView) view
				.findViewById(R.id.viewpager_imageView);
		image.setImageResource(servicesImageList.get(position));


		image.setTag(position);
		((ViewPager) container).addView(view, 0);
		return view;
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return servicesImageList.size();
	}

	@Override
	public boolean isViewFromObject(View arg0, Object arg1) {
		// TODO Auto-generated method stub
		return arg1 ==((View) arg0);
	}
	@Override
	public void destroyItem(View container, int position, Object object) {
		// TODO Auto-generated method stub
		 ((ViewPager) container).removeView((View) object);
	}

}
