package home;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import com.brainagic.catalogue.catalogue.R;

import registration.FirstRegistration;


public class MainActivity extends Activity {

	SharedPreferences myshare;
	SharedPreferences.Editor editor;
	boolean isregister;
	private int versionCode;
	private final int SPLASH_DISPLAY_LENGTH = 2000;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		//getsActionBar().setIcon(R.drawable.logo);


		myshare = getSharedPreferences("Registration",MODE_PRIVATE);
		editor = myshare.edit();

		isregister = myshare.getBoolean("isregister",false);


		PackageManager pm = getPackageManager();
		PackageInfo pi = null;
		try {
			pi = pm.getPackageInfo(getPackageName(), 0);
			versionCode = pi.versionCode;
			Log.v("Version name", Integer.toString(versionCode));
		} catch (PackageManager.NameNotFoundException e) {
			e.printStackTrace();
		}

		editor.putInt("VERSION", versionCode).commit();

		//editor.putInt("VERSION",15).commit();

		 /* New Handler to start the Menu-Activity
         * and close this Splash-Screen after some seconds.*/
		new Handler().postDelayed(new Runnable(){
			@Override
			public void run() {

				if (isregister)
				{
					Intent mainIntent = new Intent(MainActivity.this,HomeActivity.class);
					startActivity(mainIntent);
					finish();
				}
				else
				{
					Intent mainIntent = new Intent(MainActivity.this, FirstRegistration.class);
					mainIntent.addFlags(Intent.FLAG_FROM_BACKGROUND);
					startActivity(mainIntent);
					finish();
				}


                /* Create an Intent that will start the Menu-Activity. */

			}
		}, SPLASH_DISPLAY_LENGTH);
	}





	@Override
	protected void onPause() {
		super.onPause();
		finish();
	}
}


