package home;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.brainagic.catalogue.EditProfileActivity;
import com.brainagic.catalogue.FAQActivity;
import com.brainagic.catalogue.catalogue.R;
import com.brainagic.ranegroup.catalogue.VideoList;

import java.util.Timer;

import about.AboutActivity;
import alertbox.Alertbox;
import customer.CustomerActivity;
import engineer.EngineerActivity;
import inticator.LinePageIndicator;
import mechanic.MechanicActivity;
import notification.NotificationActivity;
import product.PriceList;
import product.ProductCatalog;
import retailer.RetailerActivity;
import wsd.WSDActivity;

;

public class HomeActivity extends Activity {

    LinePageIndicator ulpageindicator;
    ViewPager imagePageViewer;
    Timer swipeTimer;
    int currentPage = 1;
    int NUM_PAGES = 5;
    private static float MIN_SCALE = 0.85f;
    private static float MIN_ALPHA = 0.5f;
    SharedPreferences myshare;
    SharedPreferences.Editor editor;
    private String Usertype,Useremail,Usermobile;
    Alertbox alertbox = new Alertbox(HomeActivity.this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        getActionBar().setLogo(R.drawable.logo);


        myshare = getSharedPreferences("Registration", MODE_PRIVATE);
        editor = myshare.edit();
        Usertype = myshare.getString("Usertype","") ;
        Useremail = myshare.getString("Email","") ;
        Usermobile = myshare.getString("MobNo","") ;


        //Home Menu
        LinearLayout aboutlayout = (LinearLayout) findViewById(R.id.about);
        LinearLayout engineerlayout = (LinearLayout) findViewById(R.id.engin);
        LinearLayout mechlayout = (LinearLayout) findViewById(R.id.mechanic);
        LinearLayout customerlayout = (LinearLayout) findViewById(R.id.customer);
        LinearLayout wsdlayout = (LinearLayout) findViewById(R.id.wsd);
        LinearLayout retailerlayout = (LinearLayout) findViewById(R.id.retailer);
        LinearLayout productlayout = (LinearLayout) findViewById(R.id.productcatalog);
        LinearLayout pricelistlayout = (LinearLayout) findViewById(R.id.price);
        LinearLayout notificationtlayout = (LinearLayout) findViewById(R.id.notification);
        LinearLayout editprofilelayout = (LinearLayout) findViewById(R.id.editprofile);
        LinearLayout faqlayout = (LinearLayout) findViewById(R.id.faq);
        LinearLayout msrlayout = (LinearLayout) findViewById(R.id.msr);
        LinearLayout video = (LinearLayout) findViewById(R.id.video);



        if(Usertype.equals("Rane Employee") || Usertype.equals("Sales Executives"))
        {
            retailerlayout.setVisibility(View.GONE);
            msrlayout.setVisibility(View.GONE);
            wsdlayout.setVisibility(View.GONE);
            mechlayout.setVisibility(View.GONE);
            customerlayout.setVisibility(View.GONE);

        }
        else   if(Usertype.equals("Retailer"))
        {
            msrlayout.setVisibility(View.GONE);
            wsdlayout.setVisibility(View.GONE);
            mechlayout.setVisibility(View.GONE);
            customerlayout.setVisibility(View.GONE);
            engineerlayout.setVisibility(View.GONE);
        }
        else if((Usertype.equals("Others")) || (Usertype.equals("Fleet Operators")))
        {
            msrlayout.setVisibility(View.GONE);
            wsdlayout.setVisibility(View.GONE);
            mechlayout.setVisibility(View.GONE);
            retailerlayout.setVisibility(View.GONE);
            engineerlayout.setVisibility(View.GONE);
        }
        else   if(Usertype.equals("Mechanic"))
        {
            msrlayout.setVisibility(View.GONE);
            wsdlayout.setVisibility(View.GONE);
            retailerlayout.setVisibility(View.GONE);
            engineerlayout.setVisibility(View.GONE);
            customerlayout.setVisibility(View.GONE);
        }
        else   if(Usertype.equals("Distributor"))
        {
            msrlayout.setVisibility(View.GONE);
            mechlayout.setVisibility(View.GONE);
            retailerlayout.setVisibility(View.GONE);
            engineerlayout.setVisibility(View.GONE);
            customerlayout.setVisibility(View.GONE);
        }
        else
        {
            wsdlayout.setVisibility(View.GONE);
            mechlayout.setVisibility(View.GONE);
            retailerlayout.setVisibility(View.GONE);
            engineerlayout.setVisibility(View.GONE);
            customerlayout.setVisibility(View.GONE);
        }




        engineerlayout.setOnClickListener(new OnClickListener()
        {

            @Override
            public void onClick(View arg0)
            {
                // TODO Auto-generated method stub

                if(Usertype.equals("Rane Employee") || Usertype.equals("Sales Executives"))
                {

                    startActivity(new Intent(HomeActivity.this, EngineerActivity.class));
                }
                else
                {
                    alertbox.showAlertbox("You are not allowed to view this page !","auth");
                }
            }
        });

        video.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub


                    startActivity(new Intent(HomeActivity.this, VideoList.class));

            }
        });
        retailerlayout.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                if(!Usertype.equals("Retailer"))
                {
                    alertbox.showAlertbox("You are not allowed to view this page !","auth");
                }
                else {
                    startActivity(new Intent(HomeActivity.this, RetailerActivity.class));
                }
            }
        });
        aboutlayout.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                startActivity(new Intent(HomeActivity.this, AboutActivity.class));
            }
        });
        customerlayout.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub

                if((Usertype.equals("Others")) || (Usertype.equals("Fleet Operators")))
                {

                    startActivity(new Intent(HomeActivity.this, CustomerActivity.class));
                }
                else
                {
                    alertbox.showAlertbox("You are not allowed to view this page !","auth");
                }


            }
        });
        mechlayout.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                if(!Usertype.equals("Mechanic"))
                {
                    alertbox.showAlertbox("You are not allowed to view this page !","auth");
                }
                else
                {
                    startActivity(new Intent(HomeActivity.this, MechanicActivity.class));
                }
            }
        });
        wsdlayout.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if(!Usertype.equals("Distributor"))
                {
                    alertbox.showAlertbox("You are not allowed to view this page !","auth");
                }
                else
                {
                    startActivity(new Intent(HomeActivity.this, WSDActivity.class));
                }
            }
        });
        productlayout.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                startActivity(new Intent(HomeActivity.this, ProductCatalog.class));
            }
        });
        pricelistlayout.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                startActivity(new Intent(HomeActivity.this, PriceList.class));
            }
        });
        notificationtlayout.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                startActivity(new Intent(HomeActivity.this, NotificationActivity.class));
            }
        });


        editprofilelayout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Intent edit = new Intent(HomeActivity.this,EditProfileActivity.class);
                edit.putExtra("usertype",Usertype);
                edit.putExtra("mobile",Usermobile);
                edit.putExtra("email",Useremail);
                startActivity(edit);
            }
        });


        faqlayout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(HomeActivity.this, FAQActivity.class));
            }
        });


        msrlayout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!Usertype.equals("MSR"))
                {
                    alertbox.showAlertbox("You are not allowed to view this page !","auth");
                }
                else
                {
                    startActivity(new Intent(HomeActivity.this, EngineerActivity.class));
                }
            }
        });

        // for home and back
        TextView back = (TextView) findViewById(R.id.fontDesign2);

        back.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                onBackPressed();
            }
        });


    }





    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.popup, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // ...

        switch (item.getItemId()) {
            case R.id.about:
                // search action
                startActivity(new Intent(HomeActivity.this, AboutActivity.class));
                return true;
            case R.id.product:
                // location found
                startActivity(new Intent(HomeActivity.this, ProductCatalog.class));
                return true;
            case R.id.price:
                // refresh
                startActivity(new Intent(HomeActivity.this, PriceList.class));
                return true;
            case R.id.wsd:
                // help action
                if(!Usertype.equals("Distributor"))
                {

                    alertbox.showAlertbox("You are not allowed to view this page !","auth");
                }
                else
                {
                    startActivity(new Intent(HomeActivity.this, WSDActivity.class));
                }
                return true;
            case R.id.retailer:
                // check for updates action
                if(!Usertype.equals("Retailer"))
                {
                    alertbox.showAlertbox("You are not allowed to view this page !","auth");
                }
                else {
                    startActivity(new Intent(HomeActivity.this, RetailerActivity.class));
                }
                return true;
            case R.id.fieldengi:
                // check for updates action

                if(Usertype.equals("Rane Employee") || Usertype.equals("Sales Executives")|| Usertype.equals("MSR"))
                {

                    startActivity(new Intent(HomeActivity.this, EngineerActivity.class));
                }
                else
                {
                    alertbox.showAlertbox("You are not allowed to view this page !","auth");
                }
                return true;
            case R.id.msr:
                // check for updates action

                if(Usertype.equals("MSR"))
                {

                    startActivity(new Intent(HomeActivity.this, EngineerActivity.class));
                }
                else
                {
                    alertbox.showAlertbox("You are not allowed to view this page !","auth");
                }
                return true;
            case R.id.mech:
                // check for updates action
                if(!Usertype.equals("Mechanic"))
                {
                    alertbox.showAlertbox("You are not allowed to view this page !","auth");
                }
                else
                {
                    startActivity(new Intent(HomeActivity.this, MechanicActivity.class));
                }
                return true;
            case R.id.customer:
                // check for updates action

                if(!Usertype.equals("Others") || !Usertype.equals("Fleet Operators"))
                {
                    alertbox.showAlertbox("You are not allowed to view this page !","auth");
                }
                else
                {
                    startActivity(new Intent(HomeActivity.this, CustomerActivity.class));
                }
                return true;
            case R.id.notification:
                // check for updates action
                startActivity(new Intent(HomeActivity.this, NotificationActivity.class));
                return true;
            case R.id.edit:
                // check for updates action
                Intent edit = new Intent(HomeActivity.this,EditProfileActivity.class);
                edit.putExtra("usertype",Usertype);
                edit.putExtra("mobile",Usermobile);
                edit.putExtra("email",Useremail);
                startActivity(edit);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
