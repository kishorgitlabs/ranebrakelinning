package adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.brainagic.catalogue.ZoomActivity;
import com.brainagic.catalogue.catalogue.R;
import com.squareup.picasso.Picasso;
import com.tooltip.Tooltip;

import java.util.ArrayList;

import product.Product_More_Details;
import touchImageview.TouchImageView;

;


public class Product_Adapter extends ArrayAdapter<String> {

    Context context;
    private ArrayList<String>  VehicleModelList, VehicleTypeList, PartList, PartTypeList, SizeList,FrontList, FrontReartList, ProductList,UniqTypeList,ImageList;
    TouchImageView zoom;
    String navgationstring;
    private Tooltip tooltip;

    public Product_Adapter(Context context, ArrayList<String> oemList, ArrayList<String> vehicleModelList, ArrayList<String> vehicleTypeList, ArrayList<String> partNoList, ArrayList<String> partTypeList, ArrayList<String> frontReartList, ArrayList<String> productList, ArrayList<String> UniqTypeList,ArrayList<String> SizeList , ArrayList<String> imageList, String navgationstring) {
        super(context, R.layout.product_adapter, oemList);
        this.context = context;
        this.VehicleModelList = vehicleModelList;
        this.PartList = partNoList;
        this.PartTypeList = partTypeList;
        this.VehicleTypeList = vehicleTypeList;
        this.FrontReartList = frontReartList;
        this.ProductList = productList;
        this.ImageList =imageList;
        this.UniqTypeList =UniqTypeList;
        this.SizeList =SizeList;
        this.navgationstring = navgationstring;

        Log.v("Image names",ImageList.toString());

    }



    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        Log.v("VehicleModelList", VehicleModelList.toString());
        convertView = null;
        ProductHolder productHolder;
        if (convertView == null) {
            convertView = ((LayoutInflater) context.getSystemService("layout_inflater")).
                    inflate(R.layout.product_adapter, parent, false);

            productHolder = new ProductHolder();
            productHolder.parttypetxt = (TextView) convertView.findViewById(R.id.parttypetxt);
            productHolder.frontreartxt = (TextView) convertView.findViewById(R.id.frtxt);
            productHolder.partnotxt = (TextView) convertView.findViewById(R.id.partnotxt);
            productHolder.producttxt = (TextView) convertView.findViewById(R.id.producttxt);
            productHolder.imageView = (ImageView) convertView.findViewById(R.id.image);
            productHolder.uniqtxt= (TextView) convertView.findViewById(R.id.uniquetxt);
            productHolder.sizetxt= (TextView) convertView.findViewById(R.id.sizetxt);
            convertView.setTag(productHolder);

        } else {
            productHolder = (ProductHolder) convertView.getTag();
        }


        productHolder.parttypetxt.setText(PartTypeList.get(position));
        productHolder.frontreartxt.setText(FrontReartList.get(position));
        productHolder.partnotxt.setText(PartList.get(position));
        productHolder.producttxt.setText(ProductList.get(position));
        productHolder.uniqtxt.setText(UniqTypeList.get(position));
        productHolder.sizetxt.setText(SizeList.get(position));

        if (ImageList.get(position).equals("")) {
            Picasso.with(context).load("file:///android_asset/noimagefound.jpg").into(productHolder.imageView);
        } else {
            Picasso.with(context).load("file:///android_asset/" + ImageList.get(position) + ".jpg").into(productHolder.imageView);
        }

        if (PartTypeList.get(position).equals("Premium")) {


            productHolder.parttypetxt.setPaintFlags(productHolder.parttypetxt.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            productHolder.parttypetxt.setTextColor(Color.WHITE);

            productHolder.parttypetxt.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    if(motionEvent.getAction()==MotionEvent.ACTION_DOWN)
                    {
                        Typeface font = Typeface.createFromAsset(
                                context.getAssets(),
                                "fonts/SourceSansPro-Regular.otf");

                         tooltip = new Tooltip.Builder(view)
                                .setText(context.getResources().getString(R.string.Premium))
                                 .setBackgroundColor(Color.WHITE)
                                 .setTextColor(context.getResources().getColor(R.color.logocolor))
                                 .setTypeface(font)
                                 .setGravity(Gravity.TOP)
                                .show();

                        return true;
                    }
                    else if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                        tooltip.dismiss();
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                }
            });
        }


        productHolder.partnotxt.setPaintFlags(productHolder.partnotxt.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        productHolder.partnotxt.setTextColor(Color.WHITE);

        productHolder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (ImageList.get(position).equals("")) {

                    Toast.makeText(context, ImageList.get(position) + " Image not found ", Toast.LENGTH_LONG).show();
                } else {
                    Intent goZoom = new Intent(context, ZoomActivity.class);
                    goZoom.putExtra("ImageName", ImageList.get(position));
                    goZoom.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                    context.startActivity(goZoom);
                }


            }


        });

        productHolder.partnotxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent gotoMore = new Intent(context, Product_More_Details.class);
                gotoMore.putExtra("partno", PartList.get(position).toString());
                gotoMore.putExtra("navigation", navgationstring);
                gotoMore.putExtra("model", VehicleModelList.get(position).toString());
                context.startActivity(gotoMore);

            }
        });



        /*if (position % 2 == 1) {
            convertView.setBackgroundColor(Color.GRAY);
        } else {
            convertView.setBackgroundColor(Color.LTGRAY);
        }
*/
        return convertView;
    }

}

class ProductHolder {

    TextView parttypetxt, frontreartxt, partnotxt, producttxt,uniqtxt,sizetxt;
    ImageView imageView;

}
