package adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.brainagic.catalogue.catalogue.R;

import java.util.ArrayList;

import alertbox.Alertbox;
import login.LoginActivity;
import map.MapsActivity;
import registration.UpdateLocation;
import search.Grid_Search;
import touchImageview.TouchImageView;

;import static com.brainagic.catalogue.catalogue.R.id.catagorytxt;

/**
 * Created by system01 on 12/16/2016.
 */

public class Grid_Search_Adapter extends ArrayAdapter<String> {


    private final Alertbox box;
    private Context context;
    private ArrayList<String> nameList, mobileList, emailList, shopList, stateList, cityList, pincodeList, usertype, latitude, langtitude, doorList, streetList, areaList;
    private TouchImageView zoom;
    private String findertype, Usertype;
    private boolean islogin;
    ArrayList<String> trucksBusList, threeWList, twoWist, carList, lcvList, othersList, tractorList;
    private String catagoryName;

    public Grid_Search_Adapter(Context context, ArrayList<String> nameList, ArrayList<String> mobileList, ArrayList<String> emailList, ArrayList<String> shopList, ArrayList<String> stateList, ArrayList<String> cityList, ArrayList<String> pincodeList, ArrayList<String> usertype, ArrayList<String> latitude, ArrayList<String> logtitude, String Usertype, String findertype, boolean islogin,
                               ArrayList<String> doorList, ArrayList<String> streetList, ArrayList<String> areaList,
                               ArrayList<String> trucksBusList, ArrayList<String> threeWList, ArrayList<String> twoWist, ArrayList<String> carList, ArrayList<String> lcvList, ArrayList<String> othersList, ArrayList<String> tractorList) {
        super(context, R.layout.grid_search_list, nameList);
        this.context = context;
        this.nameList = nameList;
        this.mobileList = mobileList;
        this.emailList = emailList;
        this.shopList = shopList;
        this.stateList = stateList;
        this.cityList = cityList;
        this.pincodeList = pincodeList;
        this.usertype = usertype;
        this.latitude = latitude;

        this.doorList = doorList;
        this.streetList = streetList;
        this.areaList = areaList;

        this.langtitude = logtitude;
        this.findertype = findertype;
        this.Usertype = Usertype;
        this.islogin = islogin;
        box = new Alertbox(context);


        this.trucksBusList = trucksBusList;
        this.threeWList = threeWList;
        this.twoWist = twoWist;
        this.carList = carList;
        this.lcvList = lcvList;
        this.othersList = othersList;
        this.tractorList = tractorList;

    }


    @NonNull
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        convertView = null;
        AddressHolder addressHolder;
        if (convertView == null) {
            convertView = ((LayoutInflater) context.getSystemService("layout_inflater")).
                    inflate(R.layout.grid_search_list, parent, false);

            addressHolder = new AddressHolder();

            addressHolder.nametxt = (TextView) convertView.findViewById(R.id.nametxt);
            addressHolder.mobiletxt = (TextView) convertView.findViewById(R.id.mobiletxt);
            addressHolder.catagorytxt = (TextView) convertView.findViewById(R.id.catagorytxt);
            addressHolder.emailtxt = (TextView) convertView.findViewById(R.id.emailtxt);
            addressHolder.shoptxt = (TextView) convertView.findViewById(R.id.shoptxt);
            addressHolder.statetxt = (TextView) convertView.findViewById(R.id.statetxt);
            addressHolder.citytxt = (TextView) convertView.findViewById(R.id.citytxt);
            addressHolder.pincodetxt = (TextView) convertView.findViewById(R.id.pincodetxt);
            addressHolder.latitudetxt = (TextView) convertView.findViewById(R.id.latitxt);
            addressHolder.longtitudetxt = (TextView) convertView.findViewById(R.id.longtxt);
            addressHolder.doortxt = (TextView) convertView.findViewById(R.id.doortxt);
            addressHolder.streettxt = (TextView) convertView.findViewById(R.id.streettxt);
            addressHolder.areatxt = (TextView) convertView.findViewById(R.id.areatxt);
            addressHolder.map = (ImageView) convertView.findViewById(R.id.map);
            addressHolder.update = (Button) convertView.findViewById(R.id.update);


            if (Usertype.equals("Sales Executives")) {
                addressHolder.update.setVisibility(View.VISIBLE);
            } else {
                addressHolder.update.setVisibility(View.GONE);
            }
            convertView.setTag(addressHolder);

        } else {
            addressHolder = (AddressHolder) convertView.getTag();

        }
        addressHolder.nametxt.setText(nameList.get(position));
        addressHolder.mobiletxt.setText(mobileList.get(position));
        addressHolder.emailtxt.setText(emailList.get(position));
        addressHolder.shoptxt.setText(shopList.get(position));
        addressHolder.statetxt.setText(stateList.get(position));
        addressHolder.pincodetxt.setText(pincodeList.get(position));

        addressHolder.doortxt.setText(doorList.get(position));
        addressHolder.streettxt.setText(streetList.get(position));
        addressHolder.areatxt.setText(areaList.get(position));

        addressHolder.latitudetxt.setText(latitude.get(position));
        addressHolder.longtitudetxt.setText(langtitude.get(position));

        addressHolder.citytxt.setText(cityList.get(position));


        catagoryName = "";

        if (trucksBusList.get(position).equals("1")) {
            catagoryName += "Truck & Bus,";
        }
        if (lcvList.get(position).equals("1")) {
            catagoryName += "LCV,";
        }
        if (carList.get(position).equals("1")) {
            catagoryName += "Car,";
        }
        if (threeWList.get(position).equals("1")) {
            catagoryName += "3W,";
        }
        if (twoWist.get(position).equals("1")) {
            catagoryName += "2W,";
        }
        if (tractorList.get(position).equals("1")) {
            catagoryName += "Tractor,";
        }
        if (othersList.get(position).equals("1")) {
            catagoryName += "Others,";
        }


        if (!catagoryName.equals(""))
            addressHolder.catagorytxt.setText(catagoryName.substring(0, catagoryName.length() - 1));
            else
            addressHolder.catagorytxt.setText("No Category");

        addressHolder.update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Usertype.equals("Sales Executives")) {
                    if (islogin) {
                        Intent updateIntent = new Intent(context, UpdateLocation.class);
                        updateIntent.putExtra("usertype", findertype);
                        updateIntent.putExtra("mobile", mobileList.get(position));
                        updateIntent.putExtra("email", emailList.get(position));
                        context.startActivity(updateIntent);

                    } else {
                        Intent updateIntent = new Intent(context, LoginActivity.class);
                        updateIntent.putExtra("usertype", findertype);
                        updateIntent.putExtra("mobile", mobileList.get(position));
                        updateIntent.putExtra("email", emailList.get(position));
                        updateIntent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                        context.startActivity(updateIntent);
                    }
                } else {
                    box.showAlertbox("Your are not Authorized !");
                }
            }
        });


        addressHolder.map.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (latitude.get(position).equals("Not Given") && langtitude.get(position).equals("Not Given") || latitude.get(position).equals("") && langtitude.get(position).equals("")) {
                    box.showAlertbox("Location not found !");
                } else {
                    Intent updateIntent = new Intent(context, MapsActivity.class);
                    updateIntent.putExtra("latitude", latitude.get(position));
                    updateIntent.putExtra("logtitude", langtitude.get(position));
                    updateIntent.putExtra("name", nameList.get(position));
                    context.startActivity(updateIntent);

                }

                   /* Intent searchAddress = new Intent(Intent.ACTION_VIEW, Uri.parse("geo:0,0?q=" + adress.get(position).toString()+","+city_list.get(position).toString() +","+ state_list.get(position).toString()+","+country_list.get(position).toString()));
                    context.startActivity(searchAddress);*/

            }
        });

        return convertView;
    }


}

class AddressHolder {
    TextView nametxt, mobiletxt, emailtxt, shoptxt, statetxt, citytxt, pincodetxt, latitudetxt, longtitudetxt, doortxt, streettxt, areatxt, catagorytxt;
    ImageView map;
    Button update;
}