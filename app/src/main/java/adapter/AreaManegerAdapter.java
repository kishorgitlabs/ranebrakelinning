package adapter;

import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.brainagic.catalogue.catalogue.R;

import java.util.ArrayList;
import java.util.List;

;

/**
 * Created by e on 7/28/2016.
 */
public class AreaManegerAdapter extends ArrayAdapter<String> {
    ArrayList<String> nameList;
    ArrayList<String> mobilrList;
    ArrayList<String> emailList;
    ArrayList<String> addressList;
    ArrayList<String> sataeList;
    ArrayList<String> cityList;
    ArrayList<String> pinList;
    ArrayList<String> latList;
    ArrayList<String> longList;
    ArrayList<String> landList;
    Context context;
    Geocoder coder;
    List<Address> address;
    private String lat,lon;

    public AreaManegerAdapter(Context context, ArrayList<String> nameList, ArrayList<String> mobilrList, ArrayList<String> emailList, ArrayList<String> addressList, ArrayList<String> sataeList,
                            ArrayList<String> cityList, ArrayList<String> pinList){//,ArrayList<String> landList) {
        super(context, R.layout.areamaneger_text_list, nameList);
        this.context = context;
        this.nameList = nameList;
        this.mobilrList = mobilrList;
        this.emailList = emailList;
        this.addressList = addressList;
        this.sataeList = sataeList;
        this.cityList = cityList;
        this.pinList = pinList;
        this.landList= landList;
        coder = new Geocoder(context);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        convertView = null;
        AreaHolder areaHolder;
        if (convertView == null)
        {
            areaHolder = new AreaHolder();
            convertView = ((LayoutInflater) this.context.getSystemService("layout_inflater")).
                    inflate(R.layout.areamaneger_text_list, parent, false);

           // areaHolder.sno = (TextView)convertView.findViewById(R.id.snotxt);
            areaHolder.name = (TextView)convertView.findViewById(R.id.nametxt);
            areaHolder.mobile = (TextView)convertView.findViewById(R.id.mobiletxt);
            areaHolder.email = (TextView)convertView.findViewById(R.id.emailtxt);
            areaHolder.address = (TextView)convertView.findViewById(R.id.addtxt);
            areaHolder.state= (TextView)convertView.findViewById(R.id.satettxt);
            areaHolder.city = (TextView)convertView.findViewById(R.id.citytxt);
            areaHolder.pin = (TextView)convertView.findViewById(R.id.pintxt);
            areaHolder.map = (Button) convertView.findViewById(R.id.map);
           // areaHolder.land =(TextView)convertView.findViewById(R.id.landtxt);


        }
        else
        {
            areaHolder = (AreaHolder) convertView.getTag();
        }

       // areaHolder.sno.setText(Integer.toString(position+1)+".");
        areaHolder.name.setText(nameList.get(position));
        areaHolder.mobile.setText(mobilrList.get(position));
        areaHolder.email.setText(emailList.get(position));
        areaHolder.address.setText(addressList.get(position));
        areaHolder.state.setText(sataeList.get(position));
        areaHolder.city.setText(cityList.get(position));
        areaHolder.pin.setText(pinList.get(position));
       // areaHolder.land.setText(landList.get(position));
        areaHolder.map.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

               /* Intent gomap = new Intent(context, SalesMapsActivity.class);
                gomap.putExtra("title",nameList.get(position));
                gomap.putExtra("Latitude",latList.get(position));
                gomap.putExtra("Longtitude",longList.get(position));
                context.startActivity(gomap);
                */
                Intent searchAddress = new Intent(Intent.ACTION_VIEW, Uri.parse("geo:0,0?q=" + addressList.get(position)));
                context.startActivity(searchAddress);


            }
        });


        return convertView;
    }

    public  void getLocationFromAddress(String strAddress) {

        Geocoder coder = new Geocoder(context);
        List<Address> address;


        try {
            address = coder.getFromLocationName(strAddress,5);
            if (address == null) {

            }
            Address location = address.get(0);
            lat = Double.toString(location.getLatitude());
            lon = Double.toString(location.getLongitude());

            Log.v("lat",lat+"lon"+lon);

        }catch (Exception ex){

        }
    }




}

class AreaHolder
{
    TextView sno;
    TextView name;
    TextView mobile;
    TextView email;
    TextView address;
    TextView state;
    TextView city;
    TextView pin;
    TextView land;
    Button map;
}
