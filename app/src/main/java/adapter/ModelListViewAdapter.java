package adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.brainagic.catalogue.catalogue.R;

import java.util.ArrayList;

;


public class ModelListViewAdapter extends ArrayAdapter {

    Context context;
    ArrayList<String> oem_name  = new ArrayList<String>();
    ArrayList<String> model_name  = new ArrayList<String>();




    public ModelListViewAdapter(Context context, ArrayList<String> oem_name,ArrayList<String> model_name) {
        super(context, R.layout.model_list_text, oem_name);
        this.context = context;
        this.oem_name = oem_name;
        this.model_name = model_name;

    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        ModelListHolder  holder = new ModelListHolder();
        convertView=null;
        if (convertView == null) {

            convertView = ((LayoutInflater) context.getSystemService("layout_inflater")).
                    inflate(R.layout.model_list_text, parent, false);


            holder.oem_name = (TextView) convertView.findViewById(R.id.oemname);
            holder.oem_name.setText(oem_name.get(position));
            holder.model_name = (TextView) convertView.findViewById(R.id.modelname);
            holder.model_name.setText(model_name.get(position));

            convertView.setTag(holder);
        }
        else
        {
            holder = (ModelListHolder) convertView.getTag();
        }
       



        return convertView;
    }



}
class ModelListHolder
{
    TextView oem_name;
    TextView model_name;
}
