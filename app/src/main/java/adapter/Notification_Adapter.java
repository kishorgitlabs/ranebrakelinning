package adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.brainagic.catalogue.catalogue.R;

import java.util.ArrayList;

;



public class Notification_Adapter extends ArrayAdapter<String> {

    Context context;
    private ArrayList<String> DiscriptionList;


    public Notification_Adapter(Context context, ArrayList<String> DiscriptionList) {
        super(context, R.layout.notification_list, DiscriptionList);
        this.context = context;
        this.DiscriptionList = DiscriptionList;

    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub

        convertView = null;
        NotificationHolder notificationHolder;
        if (convertView == null) {
            convertView = ((LayoutInflater) context.getSystemService("layout_inflater")).
                    inflate(R.layout.notification_list, parent, false);
            notificationHolder = new NotificationHolder();


            notificationHolder.disctxt = (TextView) convertView.findViewById(R.id.no_descb);


            // Picasso.with(context).load(ImageList.get(position)).resize(100,100).into(image);
            convertView.setTag(notificationHolder);

        } else {
            notificationHolder = (NotificationHolder) convertView.getTag();
        }
        notificationHolder.disctxt.setText(DiscriptionList.get(position));

        return convertView;
    }

}

class NotificationHolder {
    TextView disctxt;
}



