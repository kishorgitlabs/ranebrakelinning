package adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.brainagic.catalogue.catalogue.R;

import java.util.ArrayList;

;

/**
 * Created by e on 7/28/2016.
 */
public class SalesTeamAdapter extends ArrayAdapter<String> {
    ArrayList<String> nameList;
    ArrayList<String> mobilrList;
    ArrayList<String> emailList;
    ArrayList<String> addressList;
    ArrayList<String> sataeList;
    ArrayList<String> cityList;
    ArrayList<String> DestinationList,PincodeList;

    Context context;

    public SalesTeamAdapter(Context context, ArrayList<String> nameList, ArrayList<String> mobilrList, ArrayList<String> emailList, ArrayList<String> addressList,
                            ArrayList<String> sataeList,
                            ArrayList<String> cityList, ArrayList<String> DestinationList,ArrayList<String> PincodeList) {
        super(context, R.layout.salesteam_text_list, nameList);
        this.context = context;
        this.nameList = nameList;
        this.mobilrList = mobilrList;
        this.emailList = emailList;
        this.addressList = addressList;
        this.sataeList = sataeList;
        this.cityList = cityList;
        this.DestinationList = DestinationList;
        this.PincodeList  = PincodeList;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        convertView = null;
        SalesHolder salesHolder;
        if (convertView == null)
        {
            salesHolder = new SalesHolder();
            convertView = ((LayoutInflater) this.context.getSystemService("layout_inflater")).
                    inflate(R.layout.salesteam_text_list, parent, false);

            salesHolder.name = (TextView)convertView.findViewById(R.id.nametxt);
            salesHolder.mobile = (TextView)convertView.findViewById(R.id.mobiletxt);
            salesHolder.email = (TextView)convertView.findViewById(R.id.emailtxt);
            salesHolder.address = (TextView)convertView.findViewById(R.id.addtxt);
            salesHolder.state= (TextView)convertView.findViewById(R.id.satettxt);
            salesHolder.city = (TextView)convertView.findViewById(R.id.citytxt);
            salesHolder.pin = (TextView)convertView.findViewById(R.id.pincodetxt);
            salesHolder.des = (TextView)convertView.findViewById(R.id.destxt);

        }
        else
        {
            salesHolder = (SalesHolder) convertView.getTag();
        }


        salesHolder.name.setText(nameList.get(position));
        salesHolder.mobile.setText(mobilrList.get(position));
        salesHolder.email.setText(emailList.get(position));
        salesHolder.address.setText(addressList.get(position));
        salesHolder.state.setText(sataeList.get(position));
        salesHolder.city.setText(cityList.get(position));
        salesHolder.des.setText(DestinationList.get(position));
        salesHolder.pin.setText(PincodeList.get(position));


        return convertView;
    }
}
class SalesHolder
{
    TextView name;
    TextView mobile;
    TextView email;
    TextView address;
    TextView state;
    TextView city;
    TextView pin;
    TextView des;

}
