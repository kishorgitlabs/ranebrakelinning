package adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.brainagic.catalogue.catalogue.R;
import com.brainagic.ranegroup.catalogue.VideoList;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class TrainingVideoAdapter extends ArrayAdapter {

    private Context context;
    private String thumb;
    private ArrayList<String> videourl,videodescription,videoname;

    public TrainingVideoAdapter(@NonNull Context context, ArrayList<String> videoname, ArrayList<String> videodescription, ArrayList<String> videourl) {
        super(context, R.layout.videolistadapter);
        this.context=context;
        this.videourl=videourl;
        this.videoname=videoname;
        this.videodescription=videodescription;
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        convertView=null;
        if (convertView==null){

            convertView = ((LayoutInflater) this.context.getSystemService("layout_inflater")).
                    inflate(R.layout.videolistadapter, parent, false);

            ImageView videoimage=convertView.findViewById(R.id.videoimage);
            TextView videotitle=convertView.findViewById(R.id.videotitle);
            TextView videodescrption=convertView.findViewById(R.id.videodescrption);
            LinearLayout videolayout=convertView.findViewById(R.id.videolayout);
            videotitle.setText(videoname.get(position));
            videodescrption.setText(videodescription.get(position));

             String thumbnail=(videourl.get(position));
            try {
                 thumb=extractYoutubeId(thumbnail);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
//                Picasso.with(context)
//                        .load("http://img.youtube.com/vi/"+thumb+"/0.jpg")
//                        .resize(100, 100)
//                        .centerCrop()
//                        .error(R.drawable.logo)
//                        .into(videoimage);

            Picasso.with(context)
                    .load("http://img.youtube.com/vi/"+thumb+"/0.jpg")
                    .memoryPolicy(MemoryPolicy.NO_CACHE )
                    .networkPolicy(NetworkPolicy.NO_CACHE)
                    .error(R.drawable.logo)
                    .into(videoimage);

            videolayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new  Intent(Intent.ACTION_VIEW);
                    intent.setPackage("com.google.android.youtube");
                    intent.setData(Uri.parse(videourl.get(position)));
                   context.startActivity(intent);
                }
            });
        }
        return convertView;

    }

    private String extractYoutubeId(String url) throws MalformedURLException {
        String query = new URL(url).getQuery();
        String[] param = query.split("&");
        String id = null;
        for (String row : param) {
            String[] param1 = row.split("=");
            if (param1[0].equals("v")) {
                id = param1[1];
            }
        }
        return id;
    }

    @Override
    public int getCount() {
        return videourl.size();
    }
}
