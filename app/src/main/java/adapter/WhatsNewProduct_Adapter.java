package adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.brainagic.catalogue.catalogue.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import product.WhatsNewProduct_More_Details;
import touchImageview.TouchImageView;

;



public class WhatsNewProduct_Adapter extends ArrayAdapter<String> {

    Context context;
    private ArrayList<String> oemList, vehicleModelList, vehicleTypeList, partList, descriptionList, frontPartList, rearPartList,productList,ImageList;
    TouchImageView zoom;


    public WhatsNewProduct_Adapter(Context context, ArrayList<String> oemList, ArrayList<String> vehicleTypeList, ArrayList<String> vehicleModelList, ArrayList<String> partList, ArrayList<String> frontPartList, ArrayList<String> rearPartList, ArrayList<String> descriptionList
            , ArrayList<String> productList,ArrayList<String> ImageList) {
        super(context, R.layout.whatsnew_product_list, oemList);
        this.context = context;
        this.descriptionList = descriptionList;
        this.partList = partList;
        this.frontPartList = frontPartList;
        this.rearPartList = rearPartList;
        this.vehicleTypeList = vehicleTypeList;
        this.vehicleModelList = vehicleModelList;
        this.oemList = oemList;
        this.productList=productList;
       this.ImageList=ImageList;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub

        convertView = null;
        WhatsnewHolder whatsnewHolder;
        if (convertView == null) {
            convertView = ((LayoutInflater) context.getSystemService("layout_inflater")).
                    inflate(R.layout.whatsnew_product_list, parent, false);
            whatsnewHolder = new WhatsnewHolder();
            whatsnewHolder. snotxt = (TextView) convertView.findViewById(R.id.snotxt);
            whatsnewHolder. parttxt = (TextView) convertView.findViewById(R.id.parttxt);
            whatsnewHolder. desctxt = (TextView) convertView.findViewById(R.id.desctxt);
            whatsnewHolder. fronttxt = (TextView) convertView.findViewById(R.id.fronttxt);
            whatsnewHolder. reartxt = (TextView) convertView.findViewById(R.id.reartxt);
            whatsnewHolder. oemtxt = (TextView) convertView.findViewById(R.id.vmanutxt);
            whatsnewHolder. modeltxt = (TextView) convertView.findViewById(R.id.vmodeltxt);
            whatsnewHolder. typetxt = (TextView) convertView.findViewById(R.id.vtypetxt);
            whatsnewHolder. producttxt = (TextView) convertView.findViewById(R.id.producttxt);
            whatsnewHolder. image = (ImageView) convertView.findViewById(R.id.image);

            Log.v("image link",ImageList.toString());

            whatsnewHolder.snotxt.setText(Integer.toString(position + 1));
            whatsnewHolder. parttxt.setText(partList.get(position));
            whatsnewHolder.desctxt.setText(descriptionList.get(position));
            whatsnewHolder.fronttxt.setText(frontPartList.get(position));
            whatsnewHolder.reartxt.setText(rearPartList.get(position));
            whatsnewHolder.oemtxt.setText(oemList.get(position));
            whatsnewHolder. modeltxt.setText(vehicleModelList.get(position));
            whatsnewHolder. typetxt.setText(vehicleTypeList.get(position));
            whatsnewHolder. producttxt.setText(productList.get(position));

            Picasso.with(context).load(ImageList.get(position)).resize(100,100).into(whatsnewHolder.image);


            if (!frontPartList.get(position).equals("NA")) {
                whatsnewHolder.fronttxt.setPaintFlags(   whatsnewHolder.fronttxt.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
                whatsnewHolder. fronttxt.setTextColor(Color.BLUE);
            }

            if (!rearPartList.get(position).equals("NA")) {
                whatsnewHolder.reartxt.setPaintFlags(   whatsnewHolder.fronttxt.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
                whatsnewHolder.reartxt.setTextColor(Color.BLUE);
            }


            whatsnewHolder. fronttxt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!rearPartList.get(position).equals("NA")) {
                        Intent gotoMore = new Intent(context, WhatsNewProduct_More_Details.class);
                        gotoMore.putExtra("partno", partList.get(position).toString());
                        context.startActivity(gotoMore);
                    }
                }
            });

            whatsnewHolder. reartxt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (!rearPartList.get(position).equals("NA")) {
                        Intent gotoMore = new Intent(context, WhatsNewProduct_More_Details.class);
                        gotoMore.putExtra("partno", partList.get(position).toString());
                        context.startActivity(gotoMore);
                    }
                }
            });
            convertView.setTag(whatsnewHolder);

        }else {
            whatsnewHolder = (WhatsnewHolder) convertView.getTag();

        }
        return convertView;


    }

}

class WhatsnewHolder
{
    TextView snotxt,parttxt,desctxt,fronttxt,reartxt,oemtxt,modeltxt,typetxt,producttxt;
    ImageView image;


}



