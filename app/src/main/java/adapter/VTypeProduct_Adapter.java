package adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.support.annotation.StyleRes;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.brainagic.catalogue.ZoomActivity;
import com.brainagic.catalogue.catalogue.R;
import com.squareup.picasso.Picasso;
import com.tooltip.Tooltip;

import java.util.ArrayList;

import alertbox.Alertbox;
import product.Product_More_Details;
import touchImageview.TouchImageView;

;


public class VTypeProduct_Adapter extends ArrayAdapter<String> {

    private ArrayList<String> UniqTypeList;
    Context context;
    private ArrayList<String> VehicleModelList, VehicleTypeList, PartList,SizeList, PartTypeList, FrontList, FrontReartList, ProductList, ImageList;
    TouchImageView zoom;
    String navgationstring;
    Alertbox alertbox;
    private Tooltip tooltip;

    public VTypeProduct_Adapter(Context context, ArrayList<String> oemList, ArrayList<String> vehicleModelList, ArrayList<String> vehicleTypeList, ArrayList<String> partNoList, ArrayList<String> partTypeList, ArrayList<String> frontReartList, ArrayList<String> productList,ArrayList<String> UniqTypeList,ArrayList<String> SizeList, ArrayList<String> imageList, String navgationstring) {
        super(context, R.layout.vtype_product_adapter, oemList);
        this.context = context;
        this.VehicleModelList = vehicleModelList;
        this.PartList = partNoList;
        this.PartTypeList = partTypeList;
        this.VehicleTypeList = vehicleTypeList;
        this.FrontReartList = frontReartList;
        this.ProductList = productList;
        this.ImageList = imageList;
        this.navgationstring = navgationstring;
        this.UniqTypeList =UniqTypeList;
        this.SizeList =SizeList;
        alertbox  = new Alertbox(context);
        Log.v("Image names",ImageList.toString());
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        Log.v("VehicleModelList", VehicleModelList.toString());
        convertView = null;
        VehicleTypeHolder vehicleTypeHolder;
        if (convertView == null) {
            convertView = ((LayoutInflater) context.getSystemService("layout_inflater")).
                    inflate(R.layout.vtype_product_adapter, parent, false);

            vehicleTypeHolder = new VehicleTypeHolder();
            vehicleTypeHolder.parttypetxt = (TextView) convertView.findViewById(R.id.parttypetxt);
            vehicleTypeHolder.frontreartxt = (TextView) convertView.findViewById(R.id.frtxt);
            vehicleTypeHolder.partnotxt = (TextView) convertView.findViewById(R.id.partnotxt);
            vehicleTypeHolder.producttxt = (TextView) convertView.findViewById(R.id.producttxt);
            vehicleTypeHolder.imageView = (ImageView) convertView.findViewById(R.id.image);
            vehicleTypeHolder.vmodeltxt = (TextView) convertView.findViewById(R.id.vmodeltxt);
            vehicleTypeHolder.uniqtxt= (TextView) convertView.findViewById(R.id.uniquetxt);
            vehicleTypeHolder.sizetxt = (TextView) convertView.findViewById(R.id.sizetxt);
            convertView.setTag(vehicleTypeHolder);

        } else {
            vehicleTypeHolder = (VehicleTypeHolder) convertView.getTag();
        }


        vehicleTypeHolder.parttypetxt.setText(PartTypeList.get(position));
        vehicleTypeHolder.frontreartxt.setText(FrontReartList.get(position));
        vehicleTypeHolder.partnotxt.setText(PartList.get(position));
        vehicleTypeHolder.producttxt.setText(ProductList.get(position));
        vehicleTypeHolder.vmodeltxt.setText(VehicleModelList.get(position));
        vehicleTypeHolder.uniqtxt.setText(UniqTypeList.get(position));
        vehicleTypeHolder.sizetxt.setText(SizeList.get(position));
        if (ImageList.get(position).equals("")) {
            Picasso.with(context).load("file:///android_asset/noimagefound.jpg").into(vehicleTypeHolder.imageView);
        } else {
            Picasso.with(context).load("file:///android_asset/" + ImageList.get(position) + ".jpg").into(vehicleTypeHolder.imageView);
        }
        if (PartTypeList.get(position).equals("Premium")) {

            vehicleTypeHolder.parttypetxt.setPaintFlags(vehicleTypeHolder.parttypetxt.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            vehicleTypeHolder.parttypetxt.setTextColor(Color.WHITE);

            vehicleTypeHolder.parttypetxt.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    if(motionEvent.getAction()==MotionEvent.ACTION_DOWN)
                    {
                        Typeface font = Typeface.createFromAsset(
                                context.getAssets(),
                                "fonts/SourceSansPro-Regular.otf");

                        tooltip = new Tooltip.Builder(view)
                                .setText(context.getResources().getString(R.string.Premium))
                                .setBackgroundColor(Color.WHITE)
                                .setTextColor(context.getResources().getColor(R.color.logocolor))
                                .setTypeface(font)
                                .setGravity(Gravity.TOP)
                                .show();
                        return true;
                    }
                    else if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                        tooltip.dismiss();
                        return true;
                    }
                    return false;
                }
            });

        }






        vehicleTypeHolder.partnotxt.setPaintFlags(vehicleTypeHolder.partnotxt.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        vehicleTypeHolder.partnotxt.setTextColor(Color.BLACK);

        vehicleTypeHolder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (ImageList.get(position).equals("")) {

                    Toast.makeText(context, ImageList.get(position) + " Image not found ", Toast.LENGTH_LONG).show();
                } else {
                    Intent goZoom = new Intent(context, ZoomActivity.class);
                    goZoom.putExtra("ImageName", ImageList.get(position));
                    goZoom.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                    context.startActivity(goZoom);
                }


            }


        });
        vehicleTypeHolder.partnotxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent gotoMore = new Intent(context, Product_More_Details.class);
                gotoMore.putExtra("partno", PartList.get(position).toString());
                gotoMore.putExtra("navigation", navgationstring);
                gotoMore.putExtra("model", VehicleModelList.get(position).toString());
                context.startActivity(gotoMore);

            }
        });



       /* if (position % 2 == 1) {
            convertView.setBackgroundColor(Color.GRAY);
        } else {
            convertView.setBackgroundColor(Color.LTGRAY);
        }*/

        return convertView;
    }

}

class VehicleTypeHolder {

    TextView parttypetxt, frontreartxt, partnotxt, producttxt, vmodeltxt,uniqtxt,sizetxt;
    ImageView imageView;

}



