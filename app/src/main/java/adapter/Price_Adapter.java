package adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.brainagic.catalogue.ZoomActivity;
import com.brainagic.catalogue.catalogue.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import touchImageview.TouchImageView;

;



/**
 * Created by system01 on 12/16/2016.
 */

public class Price_Adapter extends ArrayAdapter<String> {


    Context context;
    ArrayList<String> partTypeList, partNumberList, sizeList, gradeList, priceList, oemList, vmodelList, FRpartList, RpartList, ImageList,UniquetList;
    private TouchImageView zoom;

    public Price_Adapter(Context context, ArrayList<String> oemList, ArrayList<String> vmodelList, ArrayList<String> partNumberList, ArrayList<String> partTypeList, ArrayList<String> gradeList, ArrayList<String> sizeList, ArrayList<String> priceList, ArrayList<String> FRpartList,
                         ArrayList<String> RpartList, ArrayList<String> UniquetList, ArrayList<String> ImageList) {
        super(context, R.layout.pricelist_details_adapter, partTypeList);
        this.context = context;

        this.partTypeList = partTypeList;
        this.partNumberList = partNumberList;
        this.sizeList = sizeList;
        this.gradeList = gradeList;
        this.priceList = priceList;
        this.oemList = oemList;
        this.vmodelList = vmodelList;
        this.FRpartList = FRpartList;
        this.RpartList = RpartList;
        this.ImageList = ImageList;
        this.UniquetList=UniquetList;
        Log.v("FRList",FRpartList.toString());
    }

    @NonNull
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        convertView = null;
        PriceHolder priceHolder;
        if (convertView == null) {

            priceHolder = new PriceHolder();
            convertView = ((LayoutInflater) context.getSystemService("layout_inflater")).
                    inflate(R.layout.pricelist_details_adapter, parent, false);

            priceHolder.vModelText = (TextView) convertView.findViewById(R.id.vmodeltxt);
            priceHolder.partType = (TextView) convertView.findViewById(R.id.parttypetxt);
            priceHolder.parttxt = (TextView) convertView.findViewById(R.id.partnotxt);
            priceHolder.sizetxt = (TextView) convertView.findViewById(R.id.sizetxt);
            priceHolder.gradetxt = (TextView) convertView.findViewById(R.id.gradetxt);
            priceHolder.pricetxt = (TextView) convertView.findViewById(R.id.pricetxt);
            priceHolder.fparttxt = (TextView) convertView.findViewById(R.id.frtxt);
            priceHolder.uniquetxt = (TextView) convertView.findViewById(R.id.uniguetxt);

            priceHolder.productimage = (ImageView) convertView.findViewById(R.id.image);
            convertView.setTag(priceHolder);

            //Log.v("Price ",context.getResources().getString(R.string.Rs)+" "+priceList.get(position).toString());

        }
        else {
            priceHolder = (PriceHolder) convertView.getTag();

        }

        if (ImageList.get(position).equals(""))
        {
            Picasso.with(context).load("file:///android_asset/noimagefound.jpg").into(priceHolder.productimage);
        }
           else
            {
     Picasso.with(context).load("file:///android_asset/" + ImageList.get(position) + ".jpg").into(priceHolder.productimage);
        }

        priceHolder.vModelText.setText(vmodelList.get(position));
        priceHolder.parttxt.setText(partNumberList.get(position));
        priceHolder.partType.setText(partTypeList.get(position));
        priceHolder.sizetxt.setText(sizeList.get(position));
        priceHolder.gradetxt.setText(gradeList.get(position));
        priceHolder.fparttxt.setText(FRpartList.get(position));
        priceHolder.pricetxt.setText(context.getResources().getString(R.string.Rs) + " " + priceList.get(position));
        priceHolder.uniquetxt.setText(UniquetList.get(position));


        priceHolder.productimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (ImageList.get(position).equals("")) {

                    Toast.makeText(context, ImageList.get(position) + " Image not found ", Toast.LENGTH_LONG).show();
                }
                else {
                    Intent goZoom = new Intent(context, ZoomActivity.class);
                    goZoom.putExtra("ImageName", ImageList.get(position));
                    goZoom.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                    context.startActivity(goZoom);
                }


            }


        });

        return convertView;
    }


}
class PriceHolder {
    TextView  oemText, vModelText, partType, parttxt, sizetxt, gradetxt, pricetxt, fparttxt,uniquetxt;
    ImageView productimage;
}
