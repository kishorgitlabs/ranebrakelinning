package adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.brainagic.catalogue.catalogue.R;

import java.util.ArrayList;

;




public class ListViewAdapter extends ArrayAdapter {

    Context context;
    ArrayList<String> n_name  = new ArrayList<String>();




    public ListViewAdapter(Context context, ArrayList<String> n_name) {
        super(context, R.layout.list_text, n_name);
        this.context = context;
        this.n_name = n_name;

    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        ListHolder  holder = new ListHolder();
        convertView=null;
        if (convertView == null) {

            convertView = ((LayoutInflater) context.getSystemService("layout_inflater")).
                    inflate(R.layout.list_text, parent, false);


            holder.no_name = (TextView) convertView.findViewById(R.id.item_text);
            holder.no_name.setText(n_name.get(position));

            convertView.setTag(holder);
        }
        else
        {
            holder = (ListHolder) convertView.getTag();
        }
       



        return convertView;
    }



}
class ListHolder
{
    public TextView no_name;
}
