package adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.brainagic.catalogue.ZoomActivity;
import com.brainagic.catalogue.catalogue.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import touchImageview.TouchImageView;

;

/**
 * Created by system01 on 12/16/2016.
 */

public class Product_details_Adapter extends ArrayAdapter<String> {


    Context context;
    ArrayList<String> partTypeList, partNumberList, sizeList, gradeList, imageList, priceList, FRList;
    private TouchImageView zoom;
    private RelativeLayout mRelativeLayout;
    private View customView;
    private PopupWindow mPopupWindow;
    private ImageView expanded_image;

    public Product_details_Adapter(Context context, ArrayList<String> partTypeList, ArrayList<String> partNumberList, ArrayList<String> sizeList, ArrayList<String> gradeList, ArrayList<String> priceList, ArrayList<String> imageList
            , ArrayList<String> FRList) {
        super(context, R.layout.product_details_adapter, partTypeList);
        this.context = context;
        this.partTypeList = partTypeList;
        this.partNumberList = partNumberList;
        this.sizeList = sizeList;
        this.gradeList = gradeList;
        this.imageList = imageList;
        this.priceList = priceList;
        this.FRList = FRList;
    }

    @NonNull
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        convertView = null;
        View view = convertView;
        ProductDetailsHolder productDetailsHolder;
        if (convertView == null) {
            convertView = ((LayoutInflater) context.getSystemService("layout_inflater")).
                    inflate(R.layout.product_details_adapter, parent, false);

            productDetailsHolder = new ProductDetailsHolder();

            //productDetailsHolder.snotxt = (TextView) convertView.findViewById(R.id.snotxt);
            productDetailsHolder.partType = (TextView) convertView.findViewById(R.id.parttypetxt);
            productDetailsHolder.parttxt = (TextView) convertView.findViewById(R.id.partnotxt);
            productDetailsHolder.sizetxt = (TextView) convertView.findViewById(R.id.sizetxt);
            productDetailsHolder.gradetxt = (TextView) convertView.findViewById(R.id.gradetxt);
            productDetailsHolder.pricetxt = (TextView) convertView.findViewById(R.id.pricetxt);


            convertView.setTag(productDetailsHolder);


        } else {
            productDetailsHolder = (ProductDetailsHolder) convertView.getTag();
        }



        //productDetailsHolder.snotxt.setText(Integer.toString(position + 1));

        if (FRList.get(position).equals("F")) {
            productDetailsHolder.parttxt.setText("Front");
        } else if (FRList.get(position).equals("R")) {
            productDetailsHolder.parttxt.setText("Rear");
        } else {
            productDetailsHolder.parttxt.setText(FRList.get(position));
        }


        productDetailsHolder.partType.setText(partTypeList.get(position));
        productDetailsHolder.sizetxt.setText(sizeList.get(position));
        productDetailsHolder.gradetxt.setText(gradeList.get(position));
        productDetailsHolder.pricetxt.setText(context.getResources().getString(R.string.Rs) + " " + priceList.get(position));

        ImageView productimage = (ImageView) convertView.findViewById(R.id.image);

        if (imageList.get(position).equals("")) {
            Picasso.with(context).load("file:///android_asset/noimagefound.jpg").into(productimage);
        } else {
            Picasso.with(context).load("file:///android_asset/" + imageList.get(position) + ".jpg").into(productimage);
        }

        //   Picasso.with(context).load("file:///android_asset/" + imageList.get(position) + ".jpg").into(productimage);

        productimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (imageList.get(position).equals("")) {

                    Toast.makeText(context, imageList.get(position) + " Image not found ", Toast.LENGTH_LONG).show();
                } else {
                    Intent goZoom = new Intent(context, ZoomActivity.class);
                    goZoom.putExtra("ImageName", imageList.get(position));
                    goZoom.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                    context.startActivity(goZoom);
                }


            }


        });


        return convertView;
    }
}

class ProductDetailsHolder {
    TextView partType, parttxt, sizetxt, gradetxt, pricetxt;


}

