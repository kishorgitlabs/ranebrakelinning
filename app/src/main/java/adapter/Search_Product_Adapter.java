package adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.brainagic.catalogue.catalogue.R;

import java.util.ArrayList;

import product.Product_More_Details;

;


public class Search_Product_Adapter extends ArrayAdapter<String> {

    Context context;
    private ArrayList<String> OemList,VehicleModelList,VehicleTypeList,PartList,DescriptionList,FrontRearList,ProductList;
    private String navgationstring;


    public Search_Product_Adapter(Context context, ArrayList<String> OemList, ArrayList<String> VehicleModelList, ArrayList<String> VehicleTypeList, ArrayList<String> PartList, ArrayList<String> FrontRearList, ArrayList<String> ProductList)
    {
        super(context, R.layout.search_product_adapter, VehicleModelList);
        this.context = context;
        this.VehicleModelList = VehicleModelList;
        this.OemList = OemList;
        this.PartList = PartList;
        this.FrontRearList = FrontRearList;
        this.VehicleTypeList = VehicleTypeList;
        this.ProductList=ProductList;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        Log.v("VehicleModelList", VehicleModelList.toString());
        convertView = null;
        SearchHolder searchHolder;
        if (convertView == null)
        {
            convertView = ((LayoutInflater) context.getSystemService("layout_inflater")).
                    inflate(R.layout.search_product_adapter, parent, false);

            searchHolder = new SearchHolder();

            searchHolder. vmanu = (TextView) convertView.findViewById(R.id.vmanu);
            searchHolder. vtype = (TextView) convertView.findViewById(R.id.vtype);
            searchHolder. vmodel = (TextView) convertView.findViewById(R.id.vmodel);
            searchHolder. vproduct = (TextView) convertView.findViewById(R.id.vproduct);
            searchHolder. frpart = (TextView) convertView.findViewById(R.id.frpart);
            searchHolder. partno = (TextView) convertView.findViewById(R.id.partno);


            convertView.setTag(searchHolder);

    } else {
            searchHolder = (SearchHolder) convertView.getTag();

    }

        searchHolder.vmanu.setText(OemList.get(position));
        searchHolder.vtype.setText(VehicleTypeList.get(position));
        searchHolder.vmodel.setText(VehicleModelList.get(position));
        searchHolder.frpart.setText(FrontRearList.get(position));
        searchHolder.vproduct.setText(ProductList.get(position));
        searchHolder.partno.setText(PartList.get(position));




        searchHolder.partno.setPaintFlags(searchHolder.partno.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        searchHolder.partno.setTextColor(Color.BLACK);


        searchHolder.partno.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                    navgationstring = OemList.get(position).toString()+"  >> "+VehicleModelList.get(position)+"  >> ";

                    Intent gotoMore = new Intent(context,Product_More_Details.class);
                    gotoMore.putExtra("partno",PartList.get(position).toString());
                    gotoMore.putExtra("navigation", navgationstring);
                    gotoMore.putExtra("model", VehicleModelList.get(position).toString());



                    context.startActivity(gotoMore);
                }

        });





        return convertView;
    }


}

class  SearchHolder
{
    TextView vmanu,vtype,vmodel,vproduct,frpart,partno;


}



