package adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.brainagic.catalogue.catalogue.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import touchImageview.TouchImageView;

;

/**
 * Created by system01 on 12/16/2016.
 */

public class WhatNewProduct_details_Adapter extends ArrayAdapter<String> {


    Context context;
    ArrayList<String> partTypeList, partNumberList, sizeList, gradeList, imageList, priceList;
    private TouchImageView zoom;

    public WhatNewProduct_details_Adapter(Context context, ArrayList<String> partTypeList, ArrayList<String> partNumberList, ArrayList<String> sizeList, ArrayList<String> gradeList, ArrayList<String> priceList, ArrayList<String> imageList) {
        super(context, R.layout.product_details_adapter, partTypeList);
        this.context = context;
        this.partTypeList = partTypeList;
        this.partNumberList = partNumberList;
        this.sizeList = sizeList;
        this.gradeList = gradeList;
        this.imageList = imageList;
        this.priceList = priceList;
    }

    @NonNull
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        convertView = null;
        WhatsnewDetailsHolder whatsnewDetailsHolder;
        if (convertView == null) {
            convertView = ((LayoutInflater) context.getSystemService("layout_inflater")).
                    inflate(R.layout.product_details_adapter, parent, false);

            whatsnewDetailsHolder = new WhatsnewDetailsHolder();

            whatsnewDetailsHolder. snotxt = (TextView) convertView.findViewById(R.id.snotxt);
            whatsnewDetailsHolder. partType = (TextView) convertView.findViewById(R.id.parttypetxt);
            whatsnewDetailsHolder. parttxt = (TextView) convertView.findViewById(R.id.partnotxt);
            whatsnewDetailsHolder. sizetxt = (TextView) convertView.findViewById(R.id.sizetxt);
            whatsnewDetailsHolder. gradetxt = (TextView) convertView.findViewById(R.id.gradetxt);
            whatsnewDetailsHolder. pricetxt = (TextView) convertView.findViewById(R.id.pricetxt);


            whatsnewDetailsHolder. snotxt.setText(Integer.toString(position + 1));
            whatsnewDetailsHolder. parttxt.setText(partNumberList.get(position));
            whatsnewDetailsHolder.  partType.setText(partTypeList.get(position));
            whatsnewDetailsHolder.sizetxt.setText(sizeList.get(position));
            whatsnewDetailsHolder.  gradetxt.setText(gradeList.get(position));
            whatsnewDetailsHolder.  pricetxt.setText(context.getResources().getString(R.string.Rs)+" "+priceList.get(position));

            whatsnewDetailsHolder. productimage = (ImageView) convertView.findViewById(R.id.image);
            //zoom = (TouchImageView) ((Activity) context).findViewById(R.id.zoomimage);

            Picasso.with(context).load(imageList.get(position)).into( whatsnewDetailsHolder.productimage);

            whatsnewDetailsHolder. productimage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Picasso.with(context).load(imageList.get(position)).into(zoom);
                    zoom.setVisibility(View.VISIBLE);
                }
            });
            convertView.setTag(whatsnewDetailsHolder);

        }
        else
        {
            whatsnewDetailsHolder = (WhatsnewDetailsHolder) convertView.getTag();
        }
        return convertView;
    }




}
class WhatsnewDetailsHolder
{
    TextView snotxt,partType,parttxt,sizetxt,gradetxt,pricetxt;
    ImageView productimage;
}