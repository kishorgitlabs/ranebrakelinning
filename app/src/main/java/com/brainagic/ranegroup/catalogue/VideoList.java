package com.brainagic.ranegroup.catalogue;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.brainagic.catalogue.catalogue.R;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import adapter.Price_Adapter;
import adapter.TrainingVideoAdapter;
import alertbox.Alertbox;
import database.DBHelper;
import dmax.dialog.SpotsDialog;
import home.HomeActivity;
import notification.NotificationActivity;
import persistency.ServerConnection;
import product.PriceList;
import registration.FirstRegistration;

public class VideoList extends Activity {

    private ListView videolist;
    private TrainingVideoAdapter trainingVideoAdapter;
    private SpotsDialog progressDialog;
    private Connection connection;
    private Statement stmt;
    private ArrayList<String> videourl,videodescription,videoname;
    Alertbox alertbox = new Alertbox(VideoList.this);
    private TextView home,back;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_list);

        getActionBar().setLogo(R.drawable.logo);
         home = findViewById(R.id.fontDesign1);
        videolist=findViewById(R.id.videolist);
        back = findViewById(R.id.fontDesign2);
        videolist=findViewById(R.id.videolist);

        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                Intent ho = new Intent(VideoList.this, HomeActivity.class);
                ho.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(ho);
            }
        });
        back.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                onBackPressed();
            }
        });

        new GetVideoList().execute();
    }

    private class GetVideoList extends AsyncTask<String, Void, String> {
        private SpotsDialog progressbar;
        private Connection connection;
        private Statement stmt;
        private ResultSet rset;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressbar = new SpotsDialog(VideoList.this, R.style.Custom);
            progressbar.setCancelable(false);
            progressbar.setTitle("Rane Brake Line");
            progressbar.show();
            videourl=new ArrayList<>();
            videodescription=new ArrayList<>();
            videoname=new ArrayList<>();
        }
        @Override
        protected String doInBackground(String... strings) {
            //int i = 0;
            ServerConnection server = new ServerConnection();
            try {
                connection = server.getConnection();
                stmt = connection.createStatement();
                String query = "select VideoName,Video_Description,VideoUrl from VideoTable";
                //String query = "select description from whats_new where status = 'active'";
                Log.v("Query", query);
                rset = stmt.executeQuery(query);
                while (rset.next()) {
                    videourl.add(rset.getString("VideoUrl"));
                    videodescription.add(rset.getString("Video_Description"));
                    videoname.add(rset.getString("VideoName"));
                }
                if (videourl.isEmpty()) {
                    return "nodata";
                } else {
                    connection.close();
                    stmt.close();
                    rset.close();
                    return "success";
                }
            } catch (Exception e) {
                Log.v("list",e.getMessage());
                return "error";
            }
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressbar.dismiss();
            switch (s) {
                case "success":
                    try {
                        videolist.setAdapter(new TrainingVideoAdapter(VideoList.this,videoname,videodescription,videourl));

                    } catch (Exception e) {
                        alertbox.showAlertbox("Error in adding products!");
                    }
                    break;
                case "nodata":
                    alertbox.showAlertbox("Sorry! No Video available");
                    break;
                default:
                    alertbox.showAlertbox("Please check your network connection and try again.");
                    break;
            }
        }
    }
}
