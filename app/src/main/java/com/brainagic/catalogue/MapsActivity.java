package com.brainagic.catalogue;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.brainagic.catalogue.catalogue.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import alertbox.Alertbox;
import network.NetworkConnection;
import persistency.ServerConnection;
public class MapsActivity extends FragmentActivity implements
        OnMapReadyCallback {


    private ProgressBar pb;
    private LocationManager locationMangaer;
    private boolean flag;
    MyLocationListener locationListener;
    private GoogleMap mMap;
    private double longitude, latitude;
    private Circle mCircle;
    private Marker mMarker;
    private ProgressDialog progressbar;
    private ArrayList<String> LatitudeList,LongtitudeList,ShopnameList;
    private Connection connection;
    private Statement stmt;
    private ResultSet rset;
    private String cityName;
    private Alertbox box = new Alertbox(MapsActivity.this);
    SharedPreferences myshare;
    SharedPreferences.Editor editor;
    private String findertype;
    private Spinner statespinner, cityspinner;
    private String SelectedState, SelectedCity;
    private ArrayList<String>  StatespinnerList, CityspinnerList;
    private TextView tittletxt;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps2);


        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        myshare = this.getSharedPreferences("Registration", this.MODE_PRIVATE);
        editor = myshare.edit();

        findertype = myshare.getString("findertype","") ;

        tittletxt = (TextView) findViewById(R.id.title);
        tittletxt.setText("Find "+findertype);

        statespinner = (Spinner) findViewById(R.id.statespinner);
        cityspinner = (Spinner) findViewById(R.id.cityspinner);

        StatespinnerList = new ArrayList<String>();
        CityspinnerList = new ArrayList<String>();




        LatitudeList = new ArrayList<String>();
        LongtitudeList = new ArrayList<String>();
        ShopnameList= new ArrayList<String>();

        locationMangaer = (LocationManager)
                getSystemService(Context.LOCATION_SERVICE);


        flag = displayGpsStatus();
        if (flag) {

            pb.setVisibility(View.VISIBLE);
            locationListener = new MyLocationListener();

            locationMangaer.requestLocationUpdates(LocationManager
                    .GPS_PROVIDER, 5000, 10, locationListener);

        } else {
            alertbox("Gps Status!!", "Your GPS is: OFF");
        }


        statespinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long l) {

                SelectedState = parent.getItemAtPosition(position).toString();
                if (!SelectedState.equals("Select State")) {

                 //   new GetSpinnerCityData().execute(SelectedState);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        cityspinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long l) {


                SelectedCity = parent.getItemAtPosition(position).toString();


                if (SelectedCity.equals("All")) {
                    String query = "select *  from "+findertype+" where sate =  '"+SelectedState+"'";
                    Log.v("spiner query", query);
                    new GetAddressDetailas().execute(query);
                } else {
                    String query = "select *  from registration  where city = '" + SelectedCity + "' and usertype = '"+findertype+"'";
                    Log.v("spiner query", query);
                    new GetAddressDetailas().execute(query);

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        checkInternet();



    }

    private void checkInternet()
    {
        NetworkConnection net = new NetworkConnection(MapsActivity.this);
        if(net.CheckInternet())
        {
          //  new GetSpinnerStateData().execute();
        }
        else
        {
            box.showAlertbox(getResources().getString(R.string.nointernetmsg));
        }
    }


    /*----Method to Check GPS is enable or disable ----- */
    private Boolean displayGpsStatus() {
        ContentResolver contentResolver = this.getBaseContext()
                .getContentResolver();
        boolean gpsStatus = Settings.Secure
                .isLocationProviderEnabled(contentResolver,
                        LocationManager.GPS_PROVIDER);
        if (gpsStatus) {
            return true;

        } else {
            return false;
        }
    }

    /*----------Method to create an AlertBox ------------- */
    protected void alertbox(String title, String mymessage) {
        AlertDialog.Builder builder = new AlertDialog.Builder(MapsActivity.this);
        builder.setMessage("Your Device's GPS is Disable")
                .setCancelable(false)
                .setTitle("** Gps Status **")
                .setPositiveButton("Settings",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                Intent myIntent = new Intent(
                                        Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                startActivity(myIntent);
                                dialog.cancel();
                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // cancel the dialog box
                                dialog.cancel();
                            }
                        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    /*----------Listener class to get coordinates ------------- */
    private class MyLocationListener implements LocationListener {


        @Override
        public void onLocationChanged(Location loc) {

            pb.setVisibility(View.INVISIBLE);
            Toast.makeText(MapsActivity.this, "Location changed : Lat: " +
                            loc.getLatitude() + " Lng: " + loc.getLongitude(),
                    Toast.LENGTH_SHORT).show();
            longitude = loc.getLongitude();

            latitude = loc.getLatitude();


			/*----------to get City-Name from coordinates ------------- */
            cityName = null;
            Geocoder gcd = new Geocoder(MapsActivity.this.getBaseContext(),
                    Locale.getDefault());
            List<Address> addresses;
            try {
                addresses = gcd.getFromLocation(loc.getLatitude(), loc
                        .getLongitude(), 1);

                if (addresses.size() > 0)
                    System.out.println(addresses.get(0).getLocality());
                cityName = addresses.get(0).getLocality();
            } catch (IOException e) {
                e.printStackTrace();
            }

            String s = longitude + "\n" + latitude +
                    "\n\nMy Currrent City is: " + cityName;




        }

        @Override
        public void onProviderDisabled(String provider) {
            // TODO Auto-generated method stub
        }

        @Override
        public void onProviderEnabled(String provider) {
            // TODO Auto-generated method stub
        }

        @Override
        public void onStatusChanged(String provider,
                                    int status, Bundle extras) {
            // TODO Auto-generated method stub
        }
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        //   LatLng sydney = new LatLng(Float.valueOf(latitude), Float.valueOf(latitude));
        LatLng sydney = new LatLng(973373.77, 7677483.77);
        mMap.addMarker(new MarkerOptions().position(sydney).title("You"));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(sydney, 12.0f));


        Circle circle = mMap.addCircle(new CircleOptions()
                .center(sydney)
                .radius(1000)
                .strokeColor(Color.RED)
                .fillColor(Color.BLUE));

        //new GetAddressDetailas().execute(cityName);
    }


    private void drawMarkerWithCircle(LatLng position) {
        double radiusInMeters = 100.0;
        int strokeColor = 0xffff0000; //red outline
        int shadeColor = 0x44ff0000; //opaque red fill

        CircleOptions circleOptions = new CircleOptions().center(position).radius(radiusInMeters).fillColor(shadeColor).strokeColor(strokeColor).strokeWidth(8);
        mCircle = mMap.addCircle(circleOptions);

        MarkerOptions markerOptions = new MarkerOptions().position(position);
        mMarker = mMap.addMarker(markerOptions);
    }


    class GetAddressDetailas extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressbar = new ProgressDialog(MapsActivity.this);
            progressbar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressbar.setMessage("Loading....");
            progressbar.setCancelable(false);
            progressbar.show();

        }

        @Override
        protected String doInBackground(String... strings) {
            try {

                ServerConnection server = new ServerConnection();
                connection = server.getConnection();
                stmt = connection.createStatement();
                String query = "select distinct shopname,latttitude,longtitude from registration where usertype = '"+findertype+"' and  latttitude != 'notmentioned' and  longtitude != 'notmentioned' ";
                Log.v("Query", query);
                rset = stmt.executeQuery(query);
                while (rset.next())
                {
                    ShopnameList.add(rset.getString("shopname"));
                    LatitudeList.add(rset.getString("latttitude"));
                    LongtitudeList.add(rset.getString("longtitude"));
                }
                if (ShopnameList.isEmpty()) {
                    return "nodata";
                }
                connection.close();
                stmt.close();
                rset.close();
                return "success";
            } catch (Exception e) {
                return "notsuccess";
            }

        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressbar.dismiss();
            if (s.equals("success"))
            {


               /* LatLng sydney = new LatLng(latitude, longitude);
                mMap.addMarker(new MarkerOptions().position(sydney).title("You"));
                mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));

                Circle circle = mMap.addCircle(new CircleOptions()
                        .center(sydney)
                        .radius(1000)
                        .strokeColor(Color.RED)
                        .fillColor(Color.BLUE));

                for (int i =0; i<ShopnameList.size();i++)
                {
                    LatLng locations = new LatLng(Float.valueOf(LatitudeList.get(i)), Float.valueOf(LongtitudeList.get(i)));
                    mMap.addMarker(new MarkerOptions().position(locations).title(ShopnameList.get(i)));

                }*/


            } else if (s.equals("nodata"))
            {
                box.showAlertbox(findertype+"  location not found !");
            } else {
                box.showAlertbox(getResources().getString(R.string.slow_internet));
            }
        }
    }


}
