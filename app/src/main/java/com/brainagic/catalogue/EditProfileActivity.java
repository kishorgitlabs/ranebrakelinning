package com.brainagic.catalogue;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.BroadcastReceiver;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.RequiresApi;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.brainagic.catalogue.catalogue.R;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.appindexing.Thing;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.gson.Gson;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import about.AboutActivity;
import alertbox.Alertbox;
import customer.CustomerActivity;
import dmax.dialog.SpotsDialog;
import engineer.EngineerActivity;
import fcm.Config;
import home.HomeActivity;
import mechanic.MechanicActivity;
import multispinner.MultiSelectionSpinner;
import network.NetworkConnection;
import notification.NotificationActivity;
import notification.NotificationUtil;
import persistency.ServerConnection;
import product.PriceList;
import product.ProductCatalog;
import retailer.RetailerActivity;
import utility.Base64;
import utility.Utility;
import wsd.WSDActivity;

import static com.google.android.gms.plus.PlusOneDummyView.TAG;


public class EditProfileActivity extends Activity {


    EditText name, email, mobileno, city, country, dobtxt, shopname, cityname, pincode, latitude, lontitude, address, doorno, area, street;
    Button okay, cancel, browse;
    Spinner usertype, satespinner;
    SpotsDialog progressDialog;
    String Name, Email = "", Mobno, SelectedState;
    ArrayList<String> types = new ArrayList<String>();
    Connection connection;
    Statement stmt;
    CheckBox checkbox;
    ResultSet resultSet;
    SharedPreferences myshare;
    SharedPreferences.Editor editor;
    String status = "Active";
    String Dbstatus = "";
    String formattedDate;
    String PROJECT_NUMBER = "97108778128";
    String regID, select_email, select_phno, select_name;
    private ArrayAdapter<CharSequence> adapter;

    Calendar myCalendar = Calendar.getInstance();
    private ImageView ivImage;
    private int REQUEST_CAMERA = 0, SELECT_FILE = 1;
    private String userChoosenTask;

    private String updateusertype, updatemobile, updateemail;
    // simply code

    public String UPLOAD_URL = "http://rbl.brainmagicllc.com/Image/UploadToServer.php";
    //Uri to store the image uri
    private String fileName;
    private String token;

    private Bitmap ShopPhoto;
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    Alertbox box = new Alertbox(EditProfileActivity.this);
    private LinearLayout NamaeAreaLayout, LatLongLayout,Typeofchlayout;
    private String Usertype, Useremail, Usermobile;
    Alertbox alertbox = new Alertbox(EditProfileActivity.this);

    private GoogleApiClient client;
    private String OnlineIdstrn = "0";
    private String mobilenostn;

    private MultiSelectionSpinner multiSelectionSpinner;
    private List<String> typeofmech;
    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);


        getActionBar().setIcon(R.drawable.logo);

        name = (EditText) findViewById(R.id.retailernameedittext);
        email = (EditText) findViewById(R.id.emailedittext);
        mobileno = (EditText) findViewById(R.id.phoneedittext);
        city = (EditText) findViewById(R.id.citytext);
        dobtxt = (EditText) findViewById(R.id.dobtext);
        country = (EditText) findViewById(R.id.countryetext);

        shopname = (EditText) findViewById(R.id.shopedittext);
        address = (EditText) findViewById(R.id.address);
        doorno = (EditText) findViewById(R.id.dooredittext);
        area = (EditText) findViewById(R.id.areaedittext);
        street = (EditText) findViewById(R.id.streetedittext);
        pincode = (EditText) findViewById(R.id.pincodetext);
        latitude = (EditText) findViewById(R.id.latitext);
        lontitude = (EditText) findViewById(R.id.longtitext);


        usertype = (Spinner) findViewById(R.id.usertypespinner);
        satespinner = (Spinner) findViewById(R.id.satetspinner);

        myshare = getSharedPreferences("Registration", MODE_PRIVATE);
        editor = myshare.edit();


        Usertype = myshare.getString("Usertype", "");
        Useremail = myshare.getString("Email", "");
        Usermobile = myshare.getString("MobNo", "");


        updateusertype = getIntent().getStringExtra("usertype");
        updatemobile = getIntent().getStringExtra("mobile");
        updateemail = getIntent().getStringExtra("email");
        // Usertype = updateusertype;



        NamaeAreaLayout = (LinearLayout) findViewById(R.id.namearea);
        LatLongLayout = (LinearLayout) findViewById(R.id.latlaong);
        Typeofchlayout = (LinearLayout) findViewById(R.id.typeofchlayout);


        okay = (Button) findViewById(R.id.okay);
        cancel = (Button) findViewById(R.id.cancel);
        ivImage = (ImageView) findViewById(R.id.ivImage);
        browse = (Button) findViewById(R.id.browsebutton);


        /*Gson gson = new Gson();
        String jsonText = myshare.getString("typeofmech", "");
        String[] text = new String[100];
        text = gson.fromJson(jsonText, String[].class);
        int[] selection = new int[10];
        for(int i = 0;i < text.length;i++)
        {
            // Note that this is assuming valid input
            // If you want to check then add a try/catch 
            // and another index for the numbers if to continue adding the others
            selection[i] = Integer.parseInt(text[i]);
        }*/
        
        


        String[] array = {"Truck & Bus", "LCV", "Car", "3W", "2W", "Tractor", "Others"};
        multiSelectionSpinner = (MultiSelectionSpinner) findViewById(R.id.typeofchspinner);

        multiSelectionSpinner.setItems(array);

        multiSelectionSpinner.setListener(new MultiSelectionSpinner.OnMultipleItemsSelectedListener() {
            @Override
            public void selectedIndices(List<Integer> indices)
            {

            }

            @Override
            public void selectedStrings(List<String> strings) {
                // Toast.makeText(FirstRegistration.this, strings.toString(), Toast.LENGTH_LONG).show();
                typeofmech = strings;
            }
        });


      //  multiSelectionSpinner.setSelection(selection);


        if (Usertype.equals("Distributor") || Usertype.equals("Retailer") || Usertype.equals("Mechanic")) {
            NamaeAreaLayout.setVisibility(View.VISIBLE);
            LatLongLayout.setVisibility(View.VISIBLE);

            multiSelectionSpinner.setDialogName("Type of " + Usertype);
            multiSelectionSpinner.setPrompt("Type of " + Usertype);


            if (!Usertype.equals("Distributor"))
                Typeofchlayout.setVisibility(View.VISIBLE);
            else
                Typeofchlayout.setVisibility(View.GONE);

        } else {
            NamaeAreaLayout.setVisibility(View.GONE);
            LatLongLayout.setVisibility(View.GONE);
        }











        browse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage();
            }
        });


        ivImage.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                selectImage();
            }
        });


        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

        };


        Date d = new Date();
        myCalendar.setTime(d);
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        formattedDate = df.format(myCalendar.getTime());

        progressDialog = new SpotsDialog(EditProfileActivity.this, R.style.Custom);
        progressDialog.setCancelable(false);
        //  progressDialog.setTitle("Rane Brake Line");
        //  progressDialog.show();


        myshare = getSharedPreferences("Registration", MODE_PRIVATE);
        editor = myshare.edit();


        satespinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                SelectedState = adapterView.getItemAtPosition(i).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        dobtxt.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                // TODO Auto-generated method stub
                if (dobtxt.isFocused()) {
                    new DatePickerDialog(EditProfileActivity.this, date, myCalendar
                            .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                            myCalendar.get(Calendar.DAY_OF_MONTH)).show();
                }
            }
        });


        okay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (Usertype.equals("Distributor") || Usertype.equals("Retailer") || Usertype.equals("Mechanic")) {

                    Name = name.getText().toString();
                    Email = email.getText().toString();
                    Mobno = mobileno.getText().toString();

                    String PhoneNo = mobileno.getText().toString();
                    String Regex = "[^\\d]";
                    String PhoneDigits = PhoneNo.replaceAll(Regex, "");

                    if (Name.equals("")) {
                        name.setFocusable(true);
                        name.setError("Enter your name");
                    } else if (PhoneDigits.length() != 10) {
                        Toast.makeText(EditProfileActivity.this, "Mobile number must be 10 digits", Toast.LENGTH_SHORT).show();
                        mobileno.setError("Invalid mobile number");
                    } else if (Usertype.equals("Type of User")) {
                        Toast.makeText(EditProfileActivity.this, "Select User Type", Toast.LENGTH_LONG).show();
                    } else if (shopname.getText().length() == 0) {
                        shopname.setFocusable(true);
                        shopname.setError("Enter shop name");
                    } else if (doorno.getText().length() == 0) {
                        doorno.setFocusable(true);
                        doorno.setError("Enter door name");
                    } else if (street.getText().length() == 0) {
                        street.setFocusable(true);
                        street.setError("Enter street name");
                    } else if (area.getText().length() == 0) {
                        area.setFocusable(true);
                        area.setError("Enter area name");
                    } else if (city.getText().length() == 0) {
                        city.setFocusable(true);
                        city.setError("Enter city name");
                    } else if (pincode.getText().length() == 0) {
                        pincode.setFocusable(true);
                        pincode.setError("Enter pincode ");
                    } else if (SelectedState.equals("Select Sate")) {
                        Toast.makeText(EditProfileActivity.this, "Select State", Toast.LENGTH_SHORT).show();
                    } else {

                        if (Usertype.equals("Sales Executives") || Usertype.equals("Rane Employee") || Usertype.equals("MSR")) {
                            if (!Email.equals("")) {
                                if ((CheckGateEmail(email.getText().toString()))) {
                                    checkinternet();
                                } else {
                                    email.setFocusable(true);
                                    email.setError("Please provide your Rane email id for registration");

                                    Toast toast = Toast.makeText(EditProfileActivity.this, "Please provide your Rane email id for registration", Toast.LENGTH_LONG);
                                    toast.setGravity(Gravity.CENTER, 0, 0);
                                    toast.show();
                                }
                            }

                        } else {
                            checkinternet();
                        }

                    }
                } else {

                    Name = name.getText().toString();
                    Email = email.getText().toString();
                    Mobno = mobileno.getText().toString();

                    String PhoneNo = mobileno.getText().toString();
                    String Regex = "[^\\d]";
                    String PhoneDigits = PhoneNo.replaceAll(Regex, "");

                    if (Name.equals("")) {
                        name.setFocusable(true);
                        name.setError("Enter your name");
                    } else if (PhoneDigits.length() != 10) {
                        //Snackbar.make(findViewById(R.id.parentPanel), "Mobile number must be 10 digits", Snackbar.LENGTH_SHORT).show();
                        mobileno.setError("Invalid mobile number");
                    } else if (SelectedState.equals("Select State")) {
                        //Snackbar.make(findViewById(R.id.parentPanel), "Select state", Snackbar.LENGTH_SHORT).show();
                        Toast.makeText(EditProfileActivity.this, "Select State", Toast.LENGTH_LONG).show();
                    } else if (Usertype.equals("Type of User")) {
                        Toast.makeText(EditProfileActivity.this, "Select user type", Toast.LENGTH_LONG).show();
                        //Snackbar.make(findViewById(R.id.parentPanel), "Select user type", Snackbar.LENGTH_LONG).show();
                    } else {

                        if (Usertype.equals("Sales Executives") || Usertype.equals("Rane Employee")) {

                            if ((CheckGateEmail(email.getText().toString()))) {
                                checkinternet();
                            } else {
                                email.setFocusable(true);
                                email.setError("Please provide your Rane email id for registration");

                                Toast.makeText(EditProfileActivity.this, "Please provide your Rane email id for registration", Toast.LENGTH_LONG).show();

                            }

                        } else {
                            checkinternet();
                        }

                    }
                }
            }
        });


        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        // for home abd back
        TextView home = (TextView) findViewById(R.id.fontDesign1);
        TextView back = (TextView) findViewById(R.id.fontDesign2);
        home.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                Intent ho = new Intent(EditProfileActivity.this, HomeActivity.class);
                ho.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(ho);
            }
        });
        back.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                onBackPressed();
            }
        });

        checkinternetforgetUser();
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }


    private void checkinternetforgetUser() {
        NetworkConnection connection = new NetworkConnection(EditProfileActivity.this);

        if (connection.CheckInternet())
        {


            if (Usertype.equals("Distributor") || Usertype.equals("Retailer") || Usertype.equals("Mechanic")) {
                new GetUserDetails().execute();
            } else {
                new GetLocalUserDetails().execute();
            }


        } else {
            // Toast.makeText(UpdateLocation.this,"Not unable to connect please check your Internet connection ! ", Toast.LENGTH_SHORT).show();
            alertbox.showAlertboxwithback(getResources().getString(R.string.nointernetmsg));


        }

    }


    private void updateLabel() {

        String myFormat = "dd/MM/yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        dobtxt.setText(sdf.format(myCalendar.getTime()));
    }

    private boolean CheckGateEmail(String usertype) {

        if (!usertype.equals("")) {


            String[] parts = usertype.split("@");
            String part2 = parts[1];
            if (part2.equals("ranegroup.com") || (part2.equals("rane.co.in"))) {
                return true;
            } else {
                return false;
            }
        } else {
            // Snackbar.make(findViewById(R.id.parentPanel), "Please provide your Rane email id for registration", Snackbar.LENGTH_LONG).show();
            return false;
        }

    }


    // Fetches reg id from shared preferences
    // and displays on the screen
    private void displayFirebaseRegId() {
        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        token = pref.getString("regId", null);

        Log.e(TAG, "Firebase reg id: " + token);

    }


    @Override
    protected void onResume() {
        super.onResume();

        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.REGISTRATION_COMPLETE));

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.PUSH_NOTIFICATION));

        // clear the notification area when the app is opened
        NotificationUtil.clearNotifications(getApplicationContext());
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }


    private void checkinternet() {
        NetworkConnection connection = new NetworkConnection(EditProfileActivity.this);

        if (connection.CheckInternet()) {
            progressDialog.show();

            new AlreadyRegister().execute();

        } else {
            box.showAlertbox(getResources().getString(R.string.nointernetmsg));

        }


    }

    // Shop image


    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case Utility.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (userChoosenTask.equals("Take Photo"))
                        cameraIntent();
                    else if (userChoosenTask.equals("Choose from Library"))
                        galleryIntent();
                } else {
                    //code for deny
                    alertbox.showAlertbox("Gatesloyalty Needs Permission to open Camera !");
                }
                break;
        }
    }


    private void selectImage() {
        final CharSequence[] items = {"Take Photo", "Choose from Library",
                "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(EditProfileActivity.this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result = Utility.checkPermission(EditProfileActivity.this);

                if (items[item].equals("Take Photo")) {
                    userChoosenTask = "Take Photo";
                    if (result)
                        cameraIntent();

                } else if (items[item].equals("Choose from Library")) {
                    userChoosenTask = "Choose from Library";
                    if (result)
                        galleryIntent();

                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void galleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
    }

    private void cameraIntent() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE)
                onSelectFromGalleryResult(data);

            else if (requestCode == REQUEST_CAMERA)
                onCaptureImageResult(data);


        }
    }

    private void onCaptureImageResult(Intent data) {
        ShopPhoto = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        ShopPhoto.compress(Bitmap.CompressFormat.JPEG, 90, bytes);

        File destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");

        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        ivImage.setImageBitmap(ShopPhoto);

    }


    private void onSelectFromGalleryResult(Intent data) {

        ShopPhoto = null;
        if (data != null) {
            try {
                ShopPhoto = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), data.getData());
                Uri path = data.getData();

                File file = new File("" + path);
                fileName = file.getName();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        ivImage.setImageBitmap(ShopPhoto);


    }

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    public Action getIndexApiAction() {
        Thing object = new Thing.Builder()
                .setName("EditProfile Page") // TODO: Define a title for the content shown.
                // TODO: Make sure this auto-generated URL is correct.
                .setUrl(Uri.parse("http://[ENTER-YOUR-URL-HERE]"))
                .build();
        return new Action.Builder(Action.TYPE_VIEW)
                .setObject(object)
                .setActionStatus(Action.STATUS_TYPE_COMPLETED)
                .build();
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        AppIndex.AppIndexApi.start(client, getIndexApiAction());
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        AppIndex.AppIndexApi.end(client, getIndexApiAction());
        client.disconnect();
    }


    class GetUserDetails extends AsyncTask<String, Void, String> {
        String selectquery;
        private String namestrn, emailtrn, shopnametrn, citynametrn, pincodetrn, latitudestrn, lontitudestrn, addressstrn, doornostrn, areastrn, streetstrn, countrystrn, dobstrn, notificationStatus;
        private ArrayList<String> state = new ArrayList<String>();
        String[] types = {""};
        String imageURL = "http://rbl.brainmagicllc.com/Image/";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
           /* progressDialog = new SpotsDialog(EditProfileActivity.this, R.style.Custom);
            progressDialog.setCancelable(false);
            progressDialog.setTitle("Loading....");
            progressDialog.show();*/
        }

        @Override
        protected String doInBackground(String... strings) {

            ServerConnection server = new ServerConnection();
            try {
                connection = server.getConnection();

                stmt = connection.createStatement();

                selectquery = "SELECT * from registration where  mobileno='" + updatemobile + "' and  usertype = '" + updateusertype + "'  ";
                Log.d("selectquery",selectquery);
                resultSet = stmt.executeQuery(selectquery);


                if (resultSet.next()) {

                    OnlineIdstrn = resultSet.getString("registrationid");

                    Dbstatus = resultSet.getString("Status");
                    namestrn = resultSet.getString("name");
                    emailtrn = resultSet.getString("email");
                    shopnametrn = resultSet.getString("shopname");
                    citynametrn = resultSet.getString("city");
                    pincodetrn = resultSet.getString("pincode");
                    addressstrn = resultSet.getString("address");
                    doornostrn = resultSet.getString("doorno");
                    areastrn = resultSet.getString("area");
                    streetstrn = resultSet.getString("street");
                    mobilenostn = resultSet.getString("mobileno");
                    countrystrn = resultSet.getString("country");
                    dobstrn = resultSet.getString("dateofbirth");
                    latitudestrn = resultSet.getString("latttitude");
                    lontitudestrn = resultSet.getString("longtitude");
                    types[0] = resultSet.getString("usertype");
                    state.add(resultSet.getString("state"));
                    imageURL = imageURL + resultSet.getString("shopphoto");
                    resultSet.close();
                    connection.close();
                    stmt.close();

                    return "data";

                } else {
                    resultSet.close();
                    connection.close();
                    stmt.close();

                    return "nodata";

                }
            } catch (Exception e) {
                e.printStackTrace();
                return "error";
            }

        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressDialog.dismiss();
            if (s.equals("data")) {

                name.setText(namestrn);
                email.setText(emailtrn);
                mobileno.setText(mobilenostn);
                shopname.setText(shopnametrn);
                city.setText(citynametrn);
                pincode.setText(pincodetrn);
                country.setText(countrystrn);
                address.setText(addressstrn);
                doorno.setText(doornostrn);
                area.setText(areastrn);
                street.setText(streetstrn);
                dobtxt.setText(dobstrn);

                if (latitudestrn.equals("Not Given") && lontitudestrn.equals("Not Given")) {
                    latitude.setHint(latitudestrn);
                    lontitude.setHint(lontitudestrn);
                } else {
                    latitude.setText(latitudestrn);
                    lontitude.setText(lontitudestrn);
                }
                ivImage.setImageResource(android.R.color.transparent);

                Picasso.with(EditProfileActivity.this).load(imageURL).into(ivImage);


                ArrayAdapter<String> stadapter = new ArrayAdapter<String>(EditProfileActivity.this, R.layout.simple_spinner_item, state);
                satespinner.setAdapter(stadapter);


            } else if (s.equals("nodata")) {
                alertbox.showAlertbox(Usertype + " data not found !");
            } else {
                alertbox.showAlertboxwithback(getResources().getString(R.string.nointernetmsg));
            }


        }
    }


    class GetLocalUserDetails extends AsyncTask<String, Void, String> {
        String selectquery;
        private String namestrn, emailtrn, idstng, citynametrn, pincodetrn, latitudestrn, lontitudestrn, addressstrn, doornostrn, areastrn, streetstrn, countrystrn, dobstrn, notificationStatus;
        private ArrayList<String> state = new ArrayList<String>();
        String[] types = {""};
        String imageURL = "http://rbl.brainmagicllc.com/Image/";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new SpotsDialog(EditProfileActivity.this, R.style.Custom);
            progressDialog.setCancelable(false);
            progressDialog.setTitle("Loading....");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {

            ServerConnection server = new ServerConnection();
            try {
                connection = server.getConnection();

                stmt = connection.createStatement();

                selectquery = "SELECT * from user_registration where  mobileno='" + updatemobile + "' and  usertype = '" + updateusertype + "'  ";
                Log.d("selectquery",selectquery);
                resultSet = stmt.executeQuery(selectquery);


                if (resultSet.next()) {

                    OnlineIdstrn =  resultSet.getString("id");
                    Dbstatus = resultSet.getString("Status");
                    namestrn = resultSet.getString("name");
                    emailtrn = resultSet.getString("email");
                    citynametrn = resultSet.getString("city");
                    mobilenostn = resultSet.getString("mobileno");
                    countrystrn = resultSet.getString("country");
                    dobstrn = resultSet.getString("dateofbirth");

                    if(resultSet.getString("usertype").equals("Sales Executives"))
                    {
                        types[0]  ="Sales Executive";
                    }
                    if(resultSet.getString("usertype").equals("Fleet Operators"))
                    {
                        types[0]  ="Fleet Operator";
                    }

                    state.add(resultSet.getString("state"));
                    imageURL = imageURL + resultSet.getString("shopphoto");

                    resultSet.close();
                    connection.close();
                    stmt.close();
                    return "data";

                } else {
                    resultSet.close();
                    connection.close();
                    stmt.close();
                    return "nodata";

                }
            } catch (Exception e) {
                e.printStackTrace();
                return "error";
            }

        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressDialog.dismiss();
            if (s.equals("data")) {


                name.setText(namestrn);
                email.setText(emailtrn);
                mobileno.setText(mobilenostn);
                city.setText(citynametrn);
                pincode.setText(pincodetrn);
                country.setText(countrystrn);

                dobtxt.setText(dobstrn);

                ivImage.setImageResource(R.drawable.logo);

                Picasso.with(EditProfileActivity.this).load(imageURL).into(ivImage, new Callback() {
                    @Override
                    public void onSuccess() {
                        //Dimiss progress dialog here
                        Log.v("no Error ", "lkdfjdlkfjkl");
                    }

                    @Override
                    public void onError() {
                        Log.v("Error ", "lkdfjdlkfjkl");
                    }
                });
                Picasso.with(EditProfileActivity.this).setLoggingEnabled(true);

                ArrayAdapter<String> stadapter = new ArrayAdapter<String>(EditProfileActivity.this, R.layout.simple_spinner_item, state);
                satespinner.setAdapter(stadapter);



            } else if (s.equals("nodata")) {
                alertbox.showAlertbox(Usertype + " data not found !");
            } else {
                alertbox.showAlertboxwithback(getResources().getString(R.string.nointernetmsg));
            }


        }
    }


    class ImageUploadTask extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            BitmapDrawable drawable = (BitmapDrawable) ivImage.getDrawable();
            ShopPhoto = drawable.getBitmap();
            mobilenostn = mobileno.getText().toString();
        }

        @SuppressWarnings("unused")
        @Override
        protected String doInBackground(Void... unsued) {
            InputStream is;
            BitmapFactory.Options bfo;
            Bitmap bitmapOrg;
            ByteArrayOutputStream bao;

            bfo = new BitmapFactory.Options();
            bfo.inSampleSize = 2;

            bao = new ByteArrayOutputStream();


            ShopPhoto.compress(Bitmap.CompressFormat.JPEG, 90, bao);
            byte[] ba = bao.toByteArray();
            String ba1 = Base64.encodeBytes(ba);

            ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            nameValuePairs.add(new BasicNameValuePair("image", ba1));
            nameValuePairs.add(new BasicNameValuePair("cmd", mobilenostn + ".jpg"));
            Log.v("log_tag", System.currentTimeMillis() + ".jpg");
            try {
                HttpClient httpclient = new DefaultHttpClient();
                HttpPost httppost = new
                        //  Here you need to put your server file address
                        HttpPost(UPLOAD_URL);
                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                HttpResponse response = httpclient.execute(httppost);
                HttpEntity entity = response.getEntity();
                is = entity.getContent();
                Log.v("log_tag", "In the try Loop");
            } catch (Exception e) {
                Log.v("log_tag", "Error in http connection " + e.toString());
                return "notsuccess";
            }
            return "Success";

        }


        @Override
        protected void onPostExecute(String sResponse) {

            if (sResponse.equals("Success"))
                new SaveRegisterDatails().execute();
            else
            {
                progressDialog.dismiss();
                box.showAlertbox(getString(R.string.slow_internet));
            }


        }

    }


    class SaveRegisterDatails extends AsyncTask<String, Void, String> {
        private int i = 0;
        private String namestrn, emailtrn, shopnametrn, citynametrn, pincodetrn, latitudestrn, lontitudestrn, addressstrn, doornostrn, areastrn, streetstrn, mobilestrn, countrystrn, dobeditstrng;
        private boolean truckbus,lcv,car,three,two,tractor,other;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();


            namestrn = name.getText().toString();
            mobilestrn = mobileno.getText().toString();
            emailtrn = email.getText().toString();
            shopnametrn = shopname.getText().toString();
            citynametrn = city.getText().toString();
            pincodetrn = pincode.getText().toString();

            dobeditstrng = dobtxt.getText().toString();
            if (latitude.getText().toString().length() == 0 || lontitude.getText().toString().length() == 0) {
                latitudestrn = "Not Given";
                lontitudestrn = "Not Given";
            } else {
                latitudestrn = latitude.getText().toString();
                lontitudestrn = lontitude.getText().toString();
            }
            typeofmech = multiSelectionSpinner.getSelectedStrings();

            for(int i =0;typeofmech.size()>i;i++)
            {
                if(typeofmech.get(i).toString().equals("Truck & Bus"))
                {
                    truckbus=true;
                }
                else if(typeofmech.get(i).toString().equals("LCV"))
                {
                    lcv=true;
                }
                else if(typeofmech.get(i).toString().equals("Car"))
                {
                    car=true;
                }
                else if(typeofmech.get(i).toString().equals("3W"))
                {
                    three=true;
                }
                else if(typeofmech.get(i).toString().equals("2W"))
                {
                    two=true;
                }
                else if(typeofmech.get(i).toString().equals("Tractor"))
                {
                    tractor=true;
                }
                else
                {
                    other = true;
                }
            }


            doornostrn = doorno.getText().toString();
            areastrn = area.getText().toString();
            streetstrn = street.getText().toString();
            countrystrn = country.getText().toString();

            addressstrn = shopname.getText() + "," + doorno.getText() + "," + street.getText() + "," + area.getText() + "," + city.getText() + "," + SelectedState + "," + countrystrn;


        }

        @Override
        protected String doInBackground(String... strings) {

            try {

                ServerConnection server = new ServerConnection();
                connection = server.getConnection();
                stmt = connection.createStatement();
                String query = "update registration set name = '" + namestrn + "', mobileno = '" + mobilestrn + "',email = '" + emailtrn + "',dateofbirth = '" + dobeditstrng + "',address = '" + addressstrn + "',area  = '" + areastrn + "',shopname = '" + shopnametrn + "',doorno = '" + doornostrn + "',street = '" + streetstrn + "',pincode = '" + pincodetrn + "',city = '" + citynametrn + "',state = '" + SelectedState + "',country = '" + countrystrn + "',latttitude = '" + latitudestrn + "',longtitude = '" + lontitudestrn + "',usertype = '" + Usertype + "',Status = 'Active',insertdate = '" + formattedDate + "' , shopphoto ='" + mobilestrn + ".jpg" + "' , IsTruckBus = '"+truckbus+"' , IsLCV = '"+lcv+"' ,IsCar = '"+car+"' ,Is3W = '"+three+"' ,Is2W = '"+two+"' ,IsTractor = '"+tractor+"' ,IsOthers = '"+other+"' where  registrationid ='" + OnlineIdstrn + "'";
                Log.v("update query ", query);
                i = stmt.executeUpdate(query);

                if (i == 0) {
                    connection.close();
                    stmt.close();
                    return "notinsert";
                } else {
                    connection.close();
                    stmt.close();
                    return "insert";
                }
            } catch (Exception e) {
                e.printStackTrace();
                return "error";
            }


        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressDialog.dismiss();
            if (s.equals("insert")) {

                final AlertDialog alertDialogbox = new AlertDialog.Builder(
                        EditProfileActivity.this).create();

                LayoutInflater inflater = EditProfileActivity.this.getLayoutInflater();
                View dialogView = inflater.inflate(R.layout.alertbox, null);
                alertDialogbox.setView(dialogView);

                TextView log = (TextView) dialogView.findViewById(R.id.textView1);
                Button okay = (Button) dialogView.findViewById(R.id.okay);
                log.setText("Details updated successfully !");
                okay.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View arg0) {
                        // TODO Auto-generated method stub
                        editor.putBoolean("isregister", true);
                        editor.putString("useroremp", "emp");
                        editor.putString("Name", Name);
                        editor.putString("Email", Email);
                        editor.putString("MobNo", Mobno);

                        Gson gson = new Gson();
                        String jsonText = gson.toJson(multiSelectionSpinner.getSelectedIndices());
                        editor.putString("typeofmech",jsonText);

                            editor.putString("Usertype", Usertype);

                        editor.commit();
                        Intent go = new Intent(EditProfileActivity.this, HomeActivity.class);
                        go.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(go);
                        alertDialogbox.dismiss();
                    }
                });
                alertDialogbox.setCancelable(false);
                alertDialogbox.show();

            } else if (s.equals("notinsert")) {

                alertbox.showAlertbox("Details are not updated. Please try again !");
            } else {
                alertbox.showAlertboxwithback(getResources().getString(R.string.slow_internet));
            }

        }
    }


    class BackroundRunning extends AsyncTask<String, Void, String> {
        private String namestrng, mobilestrng, emailstrng, citystrng, countrystrng, dobstrng;


        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            namestrng = name.getText().toString();
            mobilestrng = mobileno.getText().toString();
            emailstrng = email.getText().toString();
            citystrng = city.getText().toString();
            countrystrng = country.getText().toString();
            dobstrng = dobtxt.getText().toString();


        }

        @Override
        protected String doInBackground(String... strings) {

            int i = 0;

            try {

                ServerConnection server = new ServerConnection();
                connection = server.getConnection();
                stmt = connection.createStatement();

                String query ="update user_registration set name = '" + namestrng + "',mobileno = '" + mobilestrng + "',email = '" + emailstrng + "',city = '" + citystrng + "',state = '" + SelectedState + "',date = '" + formattedDate + "',usertype = '" + Usertype + "', dateofbirth= '" + dobstrng + "' , shopphoto ='" + mobilestrng + ".jpg" + "' where   id ='" + OnlineIdstrn + "' ";

                Log.v("query",query);

                i = stmt.executeUpdate(query);


                if (i == 0) {
                    connection.close();
                    stmt.close();
                    return "notinsert";
                } else {


                    connection.close();
                    stmt.close();
                    return "inserted";

                }

            } catch (Exception e) {
                e.printStackTrace();
                return "error_email";
            }


        }


        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressDialog.dismiss();
            if (s.equals("inserted")) {


                final AlertDialog alertDialogbox = new AlertDialog.Builder(
                        EditProfileActivity.this).create();

                LayoutInflater inflater = EditProfileActivity.this.getLayoutInflater();
                View dialogView = inflater.inflate(R.layout.alertbox, null);
                alertDialogbox.setView(dialogView);

                TextView log = (TextView) dialogView.findViewById(R.id.textView1);
                Button okay = (Button) dialogView.findViewById(R.id.okay);
                log.setText("Details updated successfully !");
                okay.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View arg0) {
                        // TODO Auto-generated method stub
                        editor.putBoolean("isregister", true);
                        editor.putString("useroremp", "user");
                        editor.putString("Name", Name);
                        editor.putString("Email", Email);
                        editor.putString("MobNo", Mobno);
                        editor.putString("Usertype", Usertype);

                        Gson gson = new Gson();
                        String jsonText = gson.toJson(multiSelectionSpinner.getSelectedIndices());
                        editor.putString("typeofmech",jsonText);

                        editor.commit();
                        Intent go = new Intent(EditProfileActivity.this, HomeActivity.class);
                        go.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(go);
                        alertDialogbox.dismiss();
                    }
                });
                alertDialogbox.setCancelable(false);
                alertDialogbox.show();
            } else if (s.equals("notinsert")) {
                box.showAlertbox("Details are not updated. Please try again !");
            } else if (s.equals("error_email")) {
                box.showAlertbox(getResources().getString(R.string.slow_internet));

            }
        }

    }


    class AlreadyRegister extends AsyncTask<String, Void, String> {
        String selectquery;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            select_email = email.getText().toString();
            select_phno = mobileno.getText().toString();
            select_name = name.getText().toString();

        }

        @Override
        protected String doInBackground(String... strings) {

            ServerConnection server = new ServerConnection();

            try {

                connection = server.getConnection();

                stmt = connection.createStatement();

                selectquery = "SELECT Status,mobileno,email from user_registration where email='" + select_email + "' and mobileno='" + select_phno + "'";


                resultSet = stmt.executeQuery(selectquery);


                if (resultSet.next()) {

                    Dbstatus = resultSet.getString("Status");
                    mobilenostn = resultSet.getString("mobileno");
                    if (Dbstatus.equals("Active")) {
                        resultSet.close();
                        connection.close();
                        stmt.close();
                        return "Registered";
                    } else {

                        resultSet.close();
                        connection.close();
                        stmt.close();

                        return "UnRegister";

                    }

                } else {
                    resultSet.close();
                    connection.close();
                    stmt.close();

                    return "UnRegister";

                }
            } catch (Exception e) {
                e.printStackTrace();
                return "error";
            }

        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            if (Dbstatus.equals("Active")) {

                // new BackroundRunning().execute();
                if (Usertype.equals("Distributor") || Usertype.equals("Retailer") || Usertype.equals("Mechanic"))
                {
                    new ImageUploadTask().execute();
                }else
                {
                    new BackroundRunning().execute();
                }



            } else if (s.equals("UnRegister")) {
                //  new BackroundRunning().execute();
                if (Usertype.equals("Distributor") || Usertype.equals("Retailer") || Usertype.equals("Mechanic"))
                {
                    new ImageUploadTask().execute();
                }else
                {
                    new BackroundRunning().execute();
                }
            } else {
                progressDialog.dismiss();
                box.showAlertbox(getResources().getString(R.string.slow_internet));
            }


        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.popup, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // ...

        switch (item.getItemId()) {
            case R.id.about:
                // search action
                startActivity(new Intent(EditProfileActivity.this, AboutActivity.class));
                return true;
            case R.id.product:
                // location found
                startActivity(new Intent(EditProfileActivity.this, ProductCatalog.class));
                return true;
            case R.id.price:
                // refresh
                startActivity(new Intent(EditProfileActivity.this, PriceList.class));
                return true;
            case R.id.wsd:
                // help action
                if (!Usertype.equals("Distributor")) {
                    alertbox.showAlertbox("You are not allowed to view this page !","auth");
                } else {
                    startActivity(new Intent(EditProfileActivity.this, WSDActivity.class));
                }
                return true;
            case R.id.retailer:
                // check for updates action
                if (!Usertype.equals("Retailer")) {
                    alertbox.showAlertbox("You are not allowed to view this page !","auth");
                } else {
                    startActivity(new Intent(EditProfileActivity.this, RetailerActivity.class));
                }
                return true;
            case R.id.fieldengi:
                // check for updates action

                if (Usertype.equals("Rane Employee") || Usertype.equals("Sales Executives") || Usertype.equals("MSR")) {

                    startActivity(new Intent(EditProfileActivity.this, EngineerActivity.class));
                } else {
                    alertbox.showAlertbox("You are not allowed to view this page !","auth");
                }
                return true;
            case R.id.mech:
                // check for updates action
                if (!Usertype.equals("Mechanic")) {
                    alertbox.showAlertbox("You are not allowed to view this page !","auth");
                } else {
                    startActivity(new Intent(EditProfileActivity.this, MechanicActivity.class));
                }
                return true;
            case R.id.customer:
                // check for updates action

                if ((Usertype.equals("Others")) || (Usertype.equals("Fleet Operators"))) {

                    startActivity(new Intent(EditProfileActivity.this, CustomerActivity.class));
                } else {
                    alertbox.showAlertbox("You are not allowed to view this page !","auth");
                }
                return true;
            case R.id.notification:
                // check for updates action
                startActivity(new Intent(EditProfileActivity.this, NotificationActivity.class));
                return true;
            case R.id.edit:
                // check for updates action
                Intent edit = new Intent(EditProfileActivity.this, EditProfileActivity.class);
                edit.putExtra("usertype", Usertype);
                edit.putExtra("mobile", Usermobile);
                edit.putExtra("email", Useremail);
                startActivity(edit);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


}
