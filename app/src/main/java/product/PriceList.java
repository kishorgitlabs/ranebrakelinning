package product;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.brainagic.catalogue.EditProfileActivity;
import com.brainagic.catalogue.catalogue.R;

import java.util.ArrayList;

import about.AboutActivity;
import adapter.Price_Adapter;
import alertbox.Alertbox;
import customer.CustomerActivity;
import database.DBHelper;
import dmax.dialog.SpotsDialog;
import engineer.EngineerActivity;
import home.HomeActivity;
import mechanic.MechanicActivity;
import notification.NotificationActivity;
import retailer.RetailerActivity;
import wsd.WSDActivity;

;

public class PriceList extends Activity {

    private Spinner oemspinner, vmodelspinner, Uniquespinner;
    private ArrayList<String> VehicleOem, PartList, VehicleModelList, PartTypeList, SizeList, GradeList, priceList, spinnerOemlist, spinnerUniqelist, spinnermodellist, FRpartList, RpartList, ImageList, Uniqelist;
    private SQLiteDatabase db;
    private String selectedOem;
    private String selectedVmodel,selectedUuniqe;
    private ListView Listview;
    Alertbox alertbox = new Alertbox(PriceList.this);
    private SpotsDialog progressDialog;
    private HorizontalScrollView horilist;
    private String Usertype, Useremail, Usermobile;
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    private ImageView leftarrow, rightarrow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_price_list);

        getActionBar().setLogo(R.drawable.logo);
        oemspinner = (Spinner) findViewById(R.id.oemlistspinner);
        vmodelspinner = (Spinner) findViewById(R.id.vmodellistspinner);
        Uniquespinner = (Spinner) findViewById(R.id.uniquelistspinner);
        Listview = (ListView) findViewById(R.id.listView);

        leftarrow = (ImageView) findViewById(R.id.left);
        rightarrow = (ImageView) findViewById(R.id.right);

        horilist = (HorizontalScrollView) findViewById(R.id.horilist);
        horilist.setVisibility(View.GONE);

        spinnermodellist = new ArrayList<String>();
        spinnerOemlist = new ArrayList<String>();
        VehicleOem = new ArrayList<String>();
        PartList = new ArrayList<String>();
        VehicleModelList = new ArrayList<String>();
        PartTypeList = new ArrayList<String>();
        SizeList = new ArrayList<String>();
        GradeList = new ArrayList<String>();
        priceList = new ArrayList<String>();
        ImageList = new ArrayList<String>();
        FRpartList = new ArrayList<String>();
        RpartList = new ArrayList<String>();
        spinnerUniqelist = new ArrayList<String>();
        Uniqelist = new ArrayList<String>();

        myshare = getSharedPreferences("Registration", MODE_PRIVATE);
        editor = myshare.edit();
        Usertype = myshare.getString("Usertype", "");
        Useremail = myshare.getString("Email", "");
        Usermobile = myshare.getString("MobNo", "");

        spinnerUniqelist.add("Unique Identification");
        ArrayAdapter oeamadapter = new ArrayAdapter<String>(PriceList.this, R.layout.simple_spinner_item, spinnerUniqelist);
        oeamadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        Uniquespinner.setAdapter(oeamadapter);


        String query = "select distinct oemname from product_price  order by oemname asc";
        GetOEMList(query);

        String spinquery = "select distinct vehicle_model from product_price  order by vehicle_model asc";
        GetVTypeList(spinquery);

        // for home abd back
        TextView home = (TextView) findViewById(R.id.fontDesign1);
        TextView back = (TextView) findViewById(R.id.fontDesign2);
        home.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                Intent ho = new Intent(PriceList.this, HomeActivity.class);
                ho.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(ho);
            }
        });
        back.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                onBackPressed();
            }
        });
        leftarrow.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                horilist.smoothScrollTo(horilist.getScrollX() - 3000, horilist.getScrollY());
                leftarrow.setVisibility(View.GONE);
                rightarrow.setVisibility(View.VISIBLE);
            }
        });
        rightarrow.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                horilist.smoothScrollTo(horilist.getScrollX() + 3000, horilist.getScrollY());
                rightarrow.setVisibility(View.GONE);
                leftarrow.setVisibility(View.VISIBLE);
            }
        });

        oemspinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedOem = parent.getItemAtPosition(position).toString();
                if (selectedOem.equals("Vehicle Manufacturer")) {

                    String spinquery = "select distinct vehicle_model from product_price order by vehicle_model asc";
                    GetVTypeList(spinquery);
                } else {

                    String spinquery = "select distinct vehicle_model from product_price where oemname = '" + selectedOem + "' order by vehicle_model asc";
                    GetVTypeList(spinquery);

                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        vmodelspinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedVmodel = parent.getItemAtPosition(position).toString();


                if (selectedOem.equals("Vehicle Manufacturer") && !selectedVmodel.equals("Vehicle Model")) {

                    String query = "select oemname,Unique_Identification, vehicle_model,newpart_no,rear_part_no,front_part_no , grade,part_type,price,size,image,FrontRear from product_price" +
                            " where vehicle_model = '" + selectedVmodel + "' order by FrontRear asc";
                    // "group by oemname, vehicle_model,newpart_no , grade,part_type,price,size,rear_part_no,front_part_no order by vehicle_model asc ";

                    GetIdentificationList("select distinct Unique_Identification from product_price  " +
                            "where vehicle_model = '" + selectedVmodel + "' order by Unique_Identification asc ");

                    new LoadListViewAsyntask().execute(query);


                } else if (!selectedOem.equals("Vehicle Manufacturer") && selectedVmodel.equals("Vehicle Model")) {


                    String query = "select oemname, Unique_Identification,vehicle_model,newpart_no,rear_part_no,front_part_no , grade,part_type,price,size,image,FrontRear from product_price " +
                            "where  oemname = '" + selectedOem + "' order by FrontRear asc";
                    //"group by oemname, vehicle_model,newpart_no , grade,part_type,price,size,rear_part_no,front_part_no order by vehicle_model asc ";
                    GetIdentificationList("select distinct Unique_Identification from product_price  where  oemname = '" + selectedOem + "' order by Unique_Identification asc ");

                    new LoadListViewAsyntask().execute(query);

                } else if (selectedOem.equals("Vehicle Manufacturer") && selectedVmodel.equals("Vehicle Model")) {


                       /* String query = "select product.oemname, product.vehicle_model,product.newpart_no,product.rear_part_no,product.front_part_no , pricelist.grade,pricelist.part_type,pricelist.price,pricelist.size from product INNER JOIN pricelist on pricelist.part_number = product.newpart_no " +
                                "group by product.oemname, product.vehicle_model,product.newpart_no , pricelist.grade,pricelist.part_type,pricelist.price,pricelist.size,product.rear_part_no,product.front_part_no ";
                        new LoadListViewAsyntask().execute(query);*/

                } else {

                    String query = "select oemname,Unique_Identification, vehicle_model,newpart_no,rear_part_no,front_part_no , grade,part_type,price,size,image,FrontRear from product_price " +
                            "where oemname = '" + selectedOem + "' and vehicle_model = '" + selectedVmodel + "' order by FrontRear asc";
                    // "group by oemname, vehicle_model,newpart_no , grade,part_type,price,size,rear_part_no,front_part_no order by vehicle_model asc ";

                    GetIdentificationList("select distinct Unique_Identification from product_price  where oemname = '" + selectedOem + "' and vehicle_model = '" + selectedVmodel + "'  order by Unique_Identification asc ");

                    new LoadListViewAsyntask().execute(query);

                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        Uniquespinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long l)
            {
                selectedUuniqe = parent.getItemAtPosition(position).toString();
               if(!selectedUuniqe.equals("Unique Identification")) {
                   if (selectedOem.equals("Vehicle Manufacturer") && !selectedVmodel.equals("Vehicle Model")) {

                       String query = "select oemname,Unique_Identification, vehicle_model,newpart_no,rear_part_no,front_part_no , grade,part_type,price,size,image,FrontRear from product_price " +
                               "where vehicle_model = '" + selectedVmodel + "' and Unique_Identification = '" + selectedUuniqe + "' order by FrontRear asc";
                       new LoadListViewAsyntask().execute(query);

                   } else if (!selectedOem.equals("Vehicle Manufacturer") && selectedVmodel.equals("Vehicle Model")) {

                       String query = "select oemname, Unique_Identification,vehicle_model,newpart_no,rear_part_no,front_part_no , grade,part_type,price,size,image,FrontRear from product_price " +
                               "where  oemname = '" + selectedOem + "'  and Unique_Identification = '" + selectedUuniqe + "' order by FrontRear asc";

                       new LoadListViewAsyntask().execute(query);

                   } else {
                       String query = "select oemname,Unique_Identification,vehicle_model,newpart_no,rear_part_no,front_part_no , grade,part_type,price,size,image,FrontRear from product_price " +
                               "where oemname = '" + selectedOem + "' and vehicle_model = '" + selectedVmodel + "'  and Unique_Identification = '" + selectedUuniqe + "' order by FrontRear asc";

                       new LoadListViewAsyntask().execute(query);
                   }

               }
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


    }


    private void GetOEMList(String query) {
        DBHelper dbhelper = new DBHelper(PriceList.this);
        db = dbhelper.readDataBase();

        Cursor cursor = db.rawQuery(query, null);
        spinnerOemlist.clear();
        spinnerOemlist.add("Vehicle Manufacturer");
        //spinnerOemlist.add("All");
        if (cursor.moveToFirst()) {
            do {
                spinnerOemlist.add(cursor.getString(cursor.getColumnIndex("oemname")).toString());
            }
            while (cursor.moveToNext());
            db.close();

            ArrayAdapter oeamadapter = new ArrayAdapter<String>(PriceList.this, R.layout.simple_spinner_item, spinnerOemlist);
            oeamadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            oemspinner.setAdapter(oeamadapter);
        }


    }

    private void GetVTypeList(String query) {
        DBHelper dbhelper = new DBHelper(PriceList.this);
        db = dbhelper.readDataBase();
        Cursor cursor = db.rawQuery(query, null);
        spinnermodellist.clear();
        spinnermodellist.add("Vehicle Model");
        //  spinnermodellist.add("All");
        if (cursor.moveToFirst()) {
            do {
                spinnermodellist.add(cursor.getString(cursor.getColumnIndex("vehicle_model")).toString());
            }
            while (cursor.moveToNext());
            db.close();
            ArrayAdapter modeladapter = new ArrayAdapter<String>(PriceList.this, R.layout.simple_spinner_item, spinnermodellist);
            modeladapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            vmodelspinner.setAdapter(modeladapter);
        }
    }


    private void GetIdentificationList(String query) {
        DBHelper dbhelper = new DBHelper(PriceList.this);
        db = dbhelper.readDataBase();
Log.v("query Unique",query);

        Cursor cursor = db.rawQuery(query, null);
        spinnerUniqelist.clear();
        spinnerUniqelist.add("Unique Identification");
        //spinnerOemlist.add("All");
        if (cursor.moveToFirst()) {
            do {
                spinnerUniqelist.add(cursor.getString(cursor.getColumnIndex("Unique_Identification")).toString());
            }
            while (cursor.moveToNext());
            db.close();

            ArrayAdapter oeamadapter = new ArrayAdapter<String>(PriceList.this, R.layout.simple_spinner_item, spinnerUniqelist);
            oeamadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            Uniquespinner.setAdapter(oeamadapter);

        }


    }


    class LoadListViewAsyntask extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new SpotsDialog(PriceList.this, R.style.Custom);
            progressDialog.setCancelable(false);
            progressDialog.show();

            VehicleModelList.clear();
            PartList.clear();
            VehicleOem.clear();
            GradeList.clear();
            SizeList.clear();
            priceList.clear();
            PartTypeList.clear();
            Uniqelist.clear();
            FRpartList.clear();
            RpartList.clear();
            ImageList.clear();
        }
        @Override
        protected String doInBackground(String... query) {

            try {

                DBHelper dbhelper = new DBHelper(PriceList.this);
                db = dbhelper.readDataBase();

                Log.v("Query", query[0]);
                Cursor cursor = db.rawQuery(query[0], null);

                if (cursor.moveToFirst()) {
                    do {

                        VehicleModelList.add(cursor.getString(cursor.getColumnIndex("vehicle_model")).toString());
                        Uniqelist.add(cursor.getString(cursor.getColumnIndex("Unique_Identification")).toString());
                        PartList.add(cursor.getString(cursor.getColumnIndex("newpart_no")).toString());
                        VehicleOem.add(cursor.getString(cursor.getColumnIndex("oemname")).toString());
                        GradeList.add(cursor.getString(cursor.getColumnIndex("grade")).toString());
                        SizeList.add(cursor.getString(cursor.getColumnIndex("size")).toString());
                        priceList.add(cursor.getString(cursor.getColumnIndex("price")).toString());
                        PartTypeList.add(cursor.getString(cursor.getColumnIndex("part_type")).toString());
                        FRpartList.add(cursor.getString(cursor.getColumnIndex("FrontRear")).toString());
                        RpartList.add(cursor.getString(cursor.getColumnIndex("rear_part_no")).toString());
                        ImageList.add(cursor.getString(cursor.getColumnIndex("image")).toString());
                    }
                    while (cursor.moveToNext());
                    db.close();
                    return "success";
                } else {
                    db.close();
                    return "nodata";
                }


            } catch (Exception e) {
                e.printStackTrace();
                db.close();
                return "error";
            }
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            if (s.equals("success")) {
                Listview.setAdapter(new Price_Adapter(PriceList.this, VehicleOem, VehicleModelList, PartList, PartTypeList, GradeList, SizeList, priceList, FRpartList, RpartList,
                        Uniqelist,ImageList));
                progressDialog.dismiss();
                horilist.setVisibility(View.VISIBLE);
                rightarrow.setVisibility(View.VISIBLE);
                leftarrow.setVisibility(View.GONE);

            } else {
                Listview.setAdapter(new Price_Adapter(PriceList.this, VehicleOem, VehicleModelList, PartList, PartTypeList, GradeList, SizeList, priceList, FRpartList, RpartList,
                        Uniqelist,ImageList));
                alertbox.showAlertbox("Data not found !");
                horilist.setVisibility(View.GONE);
                rightarrow.setVisibility(View.GONE);
                leftarrow.setVisibility(View.GONE);
               // Uniquespinner.setVisibility(View.VISIBLE);
                progressDialog.dismiss();
            }
        }
    }


    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        super.onBackPressed();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.popup, menu);


        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // ...

        switch (item.getItemId()) {
            case R.id.about:
                // search action
                startActivity(new Intent(PriceList.this, AboutActivity.class));
                return true;
            case R.id.product:
                // location found
                startActivity(new Intent(PriceList.this, ProductCatalog.class));
                return true;
            case R.id.price:
                // refresh
                startActivity(new Intent(PriceList.this, PriceList.class));
                return true;
            case R.id.wsd:
                // help action
                if (!Usertype.equals("Distributor")) {
                    alertbox.showAlertbox("You are not allowed to view this page !", "auth");
                } else {
                    startActivity(new Intent(PriceList.this, WSDActivity.class));
                }
                return true;
            case R.id.retailer:
                // check for updates action
                if (!Usertype.equals("Retailer")) {
                    alertbox.showAlertbox("You are not allowed to view this page !", "auth");
                } else {
                    startActivity(new Intent(PriceList.this, RetailerActivity.class));
                }
                return true;
            case R.id.fieldengi:
                // check for updates action

                if (Usertype.equals("Rane Employee") || Usertype.equals("Sales Executives") || Usertype.equals("MSR")) {

                    startActivity(new Intent(PriceList.this, EngineerActivity.class));
                } else {
                    alertbox.showAlertbox("You are not allowed to view this page !", "auth");
                }
                return true;
            case R.id.msr:
                // check for updates action

                if (Usertype.equals("MSR")) {

                    startActivity(new Intent(PriceList.this, EngineerActivity.class));
                } else {
                    alertbox.showAlertbox("You are not allowed to view this page !");
                }
                return true;
            case R.id.mech:
                // check for updates action
                if (!Usertype.equals("Mechanic")) {
                    alertbox.showAlertbox("You are not allowed to view this page !", "auth");
                } else {
                    startActivity(new Intent(PriceList.this, MechanicActivity.class));
                }
                return true;
            case R.id.customer:
                // check for updates action

                if ((Usertype.equals("Others")) || (Usertype.equals("Fleet Operators"))) {

                    startActivity(new Intent(PriceList.this, CustomerActivity.class));
                } else {
                    alertbox.showAlertbox("You are not allowed to view this page !", "auth");
                }
                return true;
            case R.id.notification:
                // check for updates action
                startActivity(new Intent(PriceList.this, NotificationActivity.class));
                return true;
            case R.id.edit:
                // check for updates action
                Intent edit = new Intent(PriceList.this, EditProfileActivity.class);
                edit.putExtra("usertype", Usertype);
                edit.putExtra("mobile", Usermobile);
                edit.putExtra("email", Useremail);
                startActivity(edit);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


}



/*if (!selectedVmodel.equals("Vehicle model")) {

                    if (selectedOem.equals("Vehicle manufacturer") && !selectedVmodel.equals("Vehicle model")) {

                        String query = "select product.oemname, product.vehicle_model,product.newpart_no,product.rear_part_no,product.front_part_no , pricelist.grade,pricelist.part_type,pricelist.price,pricelist.size from product INNER JOIN pricelist on pricelist.part_number = product.newpart_no " +
                                "where product.vehicle_model = '" + selectedVmodel + "' " +
                                "group by product.oemname, product.vehicle_model,product.newpart_no , pricelist.grade,pricelist.part_type,pricelist.price,pricelist.size,product.rear_part_no,product.front_part_no ";

                        new LoadListViewAsyntask().execute(query);


                    } else if (!selectedOem.equals("Vehicle manufacturer") && selectedVmodel.equals("Vehicle model")) {


                        String query = "select product.oemname, product.vehicle_model,product.newpart_no,product.rear_part_no,product.front_part_no , pricelist.grade,pricelist.part_type,pricelist.price,pricelist.size from product INNER JOIN pricelist on pricelist.part_number = product.newpart_no " +
                                "where  product.oemname = '" + selectedOem + "' " +
                                "group by product.oemname, product.vehicle_model,product.newpart_no , pricelist.grade,pricelist.part_type,pricelist.price,pricelist.size,product.rear_part_no,product.front_part_no ";
                        new LoadListViewAsyntask().execute(query);

                    } else if (selectedOem.equals("Vehicle manufacturer") && selectedVmodel.equals("Vehicle model")) {


                        String query = "select product.oemname, product.vehicle_model,product.newpart_no,product.rear_part_no,product.front_part_no , pricelist.grade,pricelist.part_type,pricelist.price,pricelist.size from product INNER JOIN pricelist on pricelist.part_number = product.newpart_no " +
                                "group by product.oemname, product.vehicle_model,product.newpart_no , pricelist.grade,pricelist.part_type,pricelist.price,pricelist.size,product.rear_part_no,product.front_part_no ";
                        new LoadListViewAsyntask().execute(query);

                    } else {

                        String query = "select product.oemname, product.vehicle_model,product.newpart_no,product.rear_part_no,product.front_part_no , pricelist.grade,pricelist.part_type,pricelist.price,pricelist.size from product INNER JOIN pricelist on pricelist.part_number = product.newpart_no " +
                                "where product.oemname = '" + selectedOem + "' and product.vehicle_model = '" + selectedVmodel + "' " +
                                "group by product.oemname, product.vehicle_model,product.newpart_no , pricelist.grade,pricelist.part_type,pricelist.price,pricelist.size,product.rear_part_no,product.front_part_no ";

                        new LoadListViewAsyntask().execute(query);

                    }

                } else {
                    if (!selectedOem.equals("Vehicle manufacturer")) {


                        String query = "select product.oemname, product.vehicle_model,product.newpart_no,product.rear_part_no,product.front_part_no , pricelist.grade,pricelist.part_type,pricelist.price,pricelist.size from product INNER JOIN pricelist on pricelist.part_number = product.newpart_no " +
                                "where  product.oemname = '" + selectedOem + "' " +
                                "group by product.oemname, product.vehicle_model,product.newpart_no , pricelist.grade,pricelist.part_type,pricelist.price,pricelist.size,product.rear_part_no,product.front_part_no ";
                        new LoadListViewAsyntask().execute(query);
                    } else {
                        String query = "select product.oemname, product.vehicle_model,product.newpart_no,product.rear_part_no,product.front_part_no , pricelist.grade,pricelist.part_type,pricelist.price,pricelist.size from product INNER JOIN pricelist on pricelist.part_number = product.newpart_no " +
                                "group by product.oemname, product.vehicle_model,product.newpart_no , pricelist.grade,pricelist.part_type,pricelist.price,pricelist.size,product.rear_part_no,product.front_part_no ";
                        new LoadListViewAsyntask().execute(query);
                    }


                }*/