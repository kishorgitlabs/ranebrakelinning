package product;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.brainagic.catalogue.EditProfileActivity;
import com.brainagic.catalogue.catalogue.R;

import java.util.ArrayList;

import about.AboutActivity;
import adapter.ModelListViewAdapter;
import alertbox.Alertbox;
import customer.CustomerActivity;
import database.DBHelper;
import dmax.dialog.SpotsDialog;
import engineer.EngineerActivity;
import home.HomeActivity;
import mechanic.MechanicActivity;
import notification.NotificationActivity;
import retailer.RetailerActivity;
import wsd.WSDActivity;

;

public class OemWithModelActivity extends Activity {



    private ArrayList<String> VModelList,OemlList,OemlListGrid;
    private SQLiteDatabase db;
    private ListView Listview;
    private String productquery,selectedOem,selectedType;
    private Spinner   vOemspinner;
    private SpotsDialog progressDialog;
    Alertbox alertbox = new Alertbox(OemWithModelActivity.this);
    SharedPreferences myshare;
    SharedPreferences.Editor editor;
    private String Usertype,Useremail,Usermobile;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_oem_with_model);

        getActionBar().setLogo(R.drawable.logo);

        OemlList = new ArrayList<String>();
        VModelList = new ArrayList<String>();
        OemlListGrid = new ArrayList<String>();

        Listview = (ListView) findViewById(R.id.listview);
        vOemspinner = (Spinner) findViewById(R.id.oemlistspinner);


        myshare = getSharedPreferences("Registration", MODE_PRIVATE);
        editor = myshare.edit();
        Usertype = myshare.getString("Usertype", "");
        Useremail = myshare.getString("Email","") ;
        Usermobile = myshare.getString("MobNo","") ;

        GetOEMList();

        // for home abd back
        TextView home = (TextView) findViewById(R.id.fontDesign1);
        TextView back = (TextView) findViewById(R.id.fontDesign2);


        Listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                if(!selectedOem.equals("Vehicle Manufacturer"))
                {
                    Intent govehiclemodel = new Intent(OemWithModelActivity.this, GirdActivity.class);
                    govehiclemodel.putExtra("vModel", VModelList.get(position));
                    govehiclemodel.putExtra("oemname", selectedOem);
                    govehiclemodel.putExtra("vType", "");
                    startActivity(govehiclemodel);

                }
                else
                {
                    Intent govehiclemodel = new Intent(OemWithModelActivity.this, GirdActivity.class);
                    govehiclemodel.putExtra("vModel", VModelList.get(position));
                    govehiclemodel.putExtra("oemname", "");
                    govehiclemodel.putExtra("vType", "");
                    startActivity(govehiclemodel);
                }



            }
        });





        vOemspinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedOem = parent.getItemAtPosition(position).toString();
                if (!selectedOem.equals("Vehicle Manufacturer"))
                {
                    String ListQuery = "select distinct oemname,vehicle_model from product_price where oemname = '" + selectedOem + "'";
                    new LoadListViewAsyntask().execute(ListQuery);
                }
                else
                {
                    String ListQuery = "select distinct oemname , vehicle_model from product_price";
                    new LoadListViewAsyntask().execute(ListQuery);

                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });




        home.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                Intent ho = new Intent(OemWithModelActivity.this, HomeActivity.class);
                ho.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(ho);
            }
        });
        back.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                onBackPressed();
            }
        });

       // new LoadListViewAsyntask().execute();

    }






    private void GetOEMList() {
        DBHelper dbhelper = new DBHelper(OemWithModelActivity.this);
        db = dbhelper.readDataBase();
        productquery = "select distinct oemname from product_price order by oemname asc";
        Cursor cursor = db.rawQuery(productquery, null);
        OemlList.clear();
        OemlList.add("Vehicle Manufacturer");
        if (cursor.moveToFirst()) {
            do {

                OemlList.add(cursor.getString(cursor.getColumnIndex("oemname")).toString());

            }
            while (cursor.moveToNext());
            vOemspinner.setAdapter(new ArrayAdapter<String>(OemWithModelActivity.this, R.layout.simple_spinner_item, OemlList));
        }

        String ListQuery = "select distinct oemname,vehicle_model from product_price";
        new LoadListViewAsyntask().execute(ListQuery);
    }


    class LoadListViewAsyntask extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new SpotsDialog(OemWithModelActivity.this,R.style.Custom);
            progressDialog.setCancelable(true);
            progressDialog.show();
            VModelList.clear();
            OemlListGrid.clear();
        }


        @Override
        protected String doInBackground(String... query) {

            try {

                DBHelper dbhelper = new DBHelper(OemWithModelActivity.this);
                db = dbhelper.readDataBase();
                Log.v("Query", query[0]);
                Cursor cursor = db.rawQuery(query[0], null);
                if (cursor.moveToFirst()) {
                    do {

                        VModelList.add(cursor.getString(cursor.getColumnIndex("vehicle_model")).toString());
                        OemlListGrid.add(cursor.getString(cursor.getColumnIndex("oemname")).toString());
                    }
                    while (cursor.moveToNext());

                    return "success";
                } else {
                    return "nodata";
                }


            } catch (Exception e) {
                e.printStackTrace();
                return "error";
            }
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressDialog.dismiss();
            if (s.equals("success")) {
                ModelListViewAdapter adapter = new ModelListViewAdapter(OemWithModelActivity.this, OemlListGrid, VModelList);
                adapter.notifyDataSetChanged();
                Listview.setAdapter(adapter);
            } else if (s.equals("nodata")) {
                alertbox.showAlertbox("Data not found !");
            }
        }
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.popup, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // ...

        switch (item.getItemId()) {
            case R.id.about:
                // search action
                startActivity(new Intent(OemWithModelActivity.this, AboutActivity.class));
                return true;
            case R.id.product:
                // location found
                startActivity(new Intent(OemWithModelActivity.this, ProductCatalog.class));
                return true;
            case R.id.price:
                // refresh
                startActivity(new Intent(OemWithModelActivity.this, PriceList.class));
                return true;
            case R.id.wsd:
                // help action
                if(!Usertype.equals("Distributor"))
                {
                    alertbox.showAlertbox("You are not allowed to view this page !","auth");
                }
                else
                {
                    startActivity(new Intent(OemWithModelActivity.this, WSDActivity.class));
                }
                return true;
            case R.id.retailer:
                // check for updates action
                if(!Usertype.equals("Retailer"))
                {
                    alertbox.showAlertbox("You are not allowed to view this page !","auth");
                }
                else {
                    startActivity(new Intent(OemWithModelActivity.this, RetailerActivity.class));
                }
                return true;
            case R.id.fieldengi:
                // check for updates action

                if(Usertype.equals("Rane Employee") || Usertype.equals("Sales Executives")|| Usertype.equals("MSR"))
                {

                    startActivity(new Intent(OemWithModelActivity.this, EngineerActivity.class));
                }
                else
                {
                    alertbox.showAlertbox("You are not allowed to view this page !","auth");
                }
                return true;
            case R.id.msr:
                // check for updates action

                if(Usertype.equals("MSR"))
                {

                    startActivity(new Intent(OemWithModelActivity.this, EngineerActivity.class));
                }
                else
                {
                    alertbox.showAlertbox("You are not allowed to view this page !");
                }
                return true;
            case R.id.mech:
                // check for updates action
                if(!Usertype.equals("Mechanic"))
                {
                    alertbox.showAlertbox("You are not allowed to view this page !","auth");
                }
                else
                {
                    startActivity(new Intent(OemWithModelActivity.this, MechanicActivity.class));
                }
                return true;
            case R.id.customer:
                // check for updates action

                if((Usertype.equals("Others")) || (Usertype.equals("Fleet Operators")))
                {

                    startActivity(new Intent(OemWithModelActivity.this, CustomerActivity.class));
                }
                else
                {
                    alertbox.showAlertbox("You are not allowed to view this page !","auth");
                }
                return true;
            case R.id.notification:
                // check for updates action
                startActivity(new Intent(OemWithModelActivity.this, NotificationActivity.class));
                return true;
            case R.id.edit:
                // check for updates action
                Intent edit = new Intent(OemWithModelActivity.this,EditProfileActivity.class);
                edit.putExtra("usertype",Usertype);
                edit.putExtra("mobile",Usermobile);
                edit.putExtra("email",Useremail);
                startActivity(edit);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


}