package product;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.brainagic.catalogue.EditProfileActivity;
import com.brainagic.catalogue.catalogue.R;

import java.util.ArrayList;

import about.AboutActivity;
import adapter.ListViewAdapter;
import alertbox.Alertbox;
import customer.CustomerActivity;
import database.DBHelper;
import engineer.EngineerActivity;
import home.HomeActivity;
import mechanic.MechanicActivity;
import notification.NotificationActivity;
import retailer.RetailerActivity;
import wsd.WSDActivity;

;

public class VType_Oem_Activity extends Activity {


    private String selectedType;
    private TextView titletxt,navitxt;
    private SQLiteDatabase db;
    private ArrayList<String> TypeList;
    private ListView Listview;
    private String productquery;
Alertbox box = new Alertbox(VType_Oem_Activity.this);
    SharedPreferences myshare;
    SharedPreferences.Editor editor;
    private Alertbox alertbox = new Alertbox(VType_Oem_Activity.this);
    private String Usertype,Useremail,Usermobile;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vtype__oem_);
        getActionBar().setLogo(R.drawable.logo);
        selectedType = getIntent().getStringExtra("vType");

        myshare = getSharedPreferences("Registration", MODE_PRIVATE);
        editor = myshare.edit();
        Usertype = myshare.getString("Usertype", "");
        Useremail = myshare.getString("Email","") ;
        Usermobile = myshare.getString("MobNo","") ;
        
        
        TypeList = new ArrayList<String>();

        titletxt = (TextView) findViewById(R.id.tittle);
        navitxt = (TextView) findViewById(R.id.navi);
        Listview = (ListView) findViewById(R.id.listview);
        navitxt.setText(" Vehicle Type  >>  "+selectedType);
        titletxt.setText(selectedType);

        // for home abd back
        TextView home = (TextView) findViewById(R.id.fontDesign1);
        TextView back = (TextView) findViewById(R.id.fontDesign2);

        productquery = "select distinct oemname from product_price where vehicle_type = '"+selectedType+"' order by oemname asc ";
        LoadListView();

        Listview.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent govehicletype = new Intent(VType_Oem_Activity.this, GirdActivity.class);
                govehicletype.putExtra("oemname", TypeList.get(position));
                govehicletype.putExtra("vType", selectedType);
                govehicletype.putExtra("vModel", "");
                startActivity(govehicletype);


            }
        });


        home.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                Intent ho = new Intent(VType_Oem_Activity.this, HomeActivity.class);
                ho.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(ho);
            }
        });
        back.setOnClickListener(new View.OnClickListener()
        {

            @Override
            public void onClick(View arg0)
            {
                // TODO Auto-generated method stub
                onBackPressed();
            }
        });

    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.popup, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // ...

        switch (item.getItemId()) {
            case R.id.about:
                // search action
                startActivity(new Intent(VType_Oem_Activity.this, AboutActivity.class));
                return true;
            case R.id.product:
                // location found
                startActivity(new Intent(VType_Oem_Activity.this, ProductCatalog.class));
                return true;
            case R.id.price:
                // refresh
                startActivity(new Intent(VType_Oem_Activity.this, PriceList.class));
                return true;
            case R.id.wsd:
                // help action
                if(!Usertype.equals("Distributor"))
                {
                    alertbox.showAlertbox("You are not allowed to view this page !","auth");
                }
                else
                {
                    startActivity(new Intent(VType_Oem_Activity.this, WSDActivity.class));
                }
                return true;
            case R.id.retailer:
                // check for updates action
                if(!Usertype.equals("Retailer"))
                {
                    alertbox.showAlertbox("You are not allowed to view this page !","auth");
                }
                else {
                    startActivity(new Intent(VType_Oem_Activity.this, RetailerActivity.class));
                }
                return true;
            case R.id.fieldengi:
                // check for updates action

                if(Usertype.equals("Rane Employee") || Usertype.equals("Sales Executives")|| Usertype.equals("MSR"))
                {

                    startActivity(new Intent(VType_Oem_Activity.this, EngineerActivity.class));
                }
                else
                {
                    alertbox.showAlertbox("You are not allowed to view this page !","auth");
                }
                return true;
            case R.id.msr:
                // check for updates action

                if(Usertype.equals("MSR"))
                {

                    startActivity(new Intent(VType_Oem_Activity.this, EngineerActivity.class));
                }
                else
                {
                    alertbox.showAlertbox("You are not allowed to view this page !");
                }
                return true;
            case R.id.mech:
                // check for updates action
                if(!Usertype.equals("Mechanic"))
                {
                    alertbox.showAlertbox("You are not allowed to view this page !","auth");
                }
                else
                {
                    startActivity(new Intent(VType_Oem_Activity.this, MechanicActivity.class));
                }
                return true;
            case R.id.customer:
                // check for updates action
                if((Usertype.equals("Others")) || (Usertype.equals("Fleet Operators")))
                {

                    startActivity(new Intent(VType_Oem_Activity.this, CustomerActivity.class));
                }
                else
                {
                    alertbox.showAlertbox("You are not allowed to view this page !","auth");
                }
                return true;
            case R.id.notification:
                // check for updates action
                startActivity(new Intent(VType_Oem_Activity.this, NotificationActivity.class));
                return true;
            case R.id.edit:
                // check for updates action
                Intent edit = new Intent(VType_Oem_Activity.this,EditProfileActivity.class);
                edit.putExtra("usertype",Usertype);
                edit.putExtra("mobile",Usermobile);
                edit.putExtra("email",Useremail);
                startActivity(edit);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    private void LoadListView() {

        DBHelper dbhelper = new DBHelper(VType_Oem_Activity.this);
        db = dbhelper.readDataBase();


        Cursor cursor = db.rawQuery(productquery, null);

        if (cursor.moveToFirst())
        {
            do
            {
                TypeList.add(cursor.getString(cursor.getColumnIndex("oemname")).toString());
            }
            while (cursor.moveToNext());


            Listview.setAdapter(new ListViewAdapter(VType_Oem_Activity.this, TypeList));

        }
       else
        {
            box.showAlertboxwithback("Data  not found !");
        }


    }

}