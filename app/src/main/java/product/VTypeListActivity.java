package product;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.brainagic.catalogue.EditProfileActivity;
import com.brainagic.catalogue.catalogue.R;

import java.util.ArrayList;

import about.AboutActivity;
import alertbox.Alertbox;
import customer.CustomerActivity;
import engineer.EngineerActivity;
import home.HomeActivity;
import mechanic.MechanicActivity;
import notification.NotificationActivity;
import retailer.RetailerActivity;
import wsd.WSDActivity;

;

public class VTypeListActivity extends Activity {


    private ArrayList<String> VehicleTypeList;
    private SQLiteDatabase db;
    private ArrayList<String> TypeList;
    private ListView Listview;
    private String productquery;
    SharedPreferences myshare;
    SharedPreferences.Editor editor;
    private Alertbox alertbox = new Alertbox(VTypeListActivity.this);
    private String Usertype,Useremail,Usermobile;
    private LinearLayout twoLayout,threeLayout,pcvLayout,tractorLayout,lcvLayout,hcvLayout,windlayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vehicle_model_list);

        getActionBar().setLogo(R.drawable.logo);

        // for home abd back
        TextView home = (TextView) findViewById(R.id.fontDesign1);
        TextView back = (TextView) findViewById(R.id.fontDesign2);


        twoLayout = (LinearLayout) findViewById(R.id.twowlayout);
        threeLayout = (LinearLayout) findViewById(R.id.threewlayout);
        pcvLayout = (LinearLayout) findViewById(R.id.pcvlayout);
        tractorLayout = (LinearLayout) findViewById(R.id.tracklayout);
        lcvLayout = (LinearLayout) findViewById(R.id.lcvlayout);
        hcvLayout = (LinearLayout) findViewById(R.id.hcvlayout);
        windlayout= (LinearLayout) findViewById(R.id.windlayout);

        myshare = getSharedPreferences("Registration", MODE_PRIVATE);
        editor = myshare.edit();
        Usertype = myshare.getString("Usertype", "");
        Useremail = myshare.getString("Email","") ;
        Usermobile = myshare.getString("MobNo","") ;
        
        
        twoLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent goType = new Intent(VTypeListActivity.this, VType_Oem_Activity.class);
                goType.putExtra("vType","TWO WHEELERS");
                startActivity(goType);
            }
        });

        threeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent goType = new Intent(VTypeListActivity.this, VType_Oem_Activity.class);
                goType.putExtra("vType","THREE WHEELERS");
                startActivity(goType);
            }
        });
        pcvLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent goType = new Intent(VTypeListActivity.this,VType_Oem_Activity.class);
                goType.putExtra("vType","PCV");
                startActivity(goType);
            }
        });



        tractorLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent goType = new Intent(VTypeListActivity.this,VType_Oem_Activity.class);
                goType.putExtra("vType","FARM TRACTOR");
                startActivity(goType);
            }
        });

        lcvLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent goType = new Intent(VTypeListActivity.this,VType_Oem_Activity.class);
                goType.putExtra("vType","LCV");
                startActivity(goType);
            }
        });

        hcvLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent goType = new Intent(VTypeListActivity.this,VType_Oem_Activity.class);
                goType.putExtra("vType","HCV");
                startActivity(goType);
            }
        });

        windlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent goType = new Intent(VTypeListActivity.this,VType_Oem_Activity.class);
                goType.putExtra("vType","WIND");
                startActivity(goType);
            }
        });


        home.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                Intent ho = new Intent(VTypeListActivity.this, HomeActivity.class);
                ho.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(ho);
            }
        });
        back.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                onBackPressed();
            }
        });

    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.popup, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // ...

        switch (item.getItemId()) {
            case R.id.about:
                // search action
                startActivity(new Intent(VTypeListActivity.this, AboutActivity.class));
                return true;
            case R.id.product:
                // location found
                startActivity(new Intent(VTypeListActivity.this, ProductCatalog.class));
                return true;
            case R.id.price:
                // refresh
                startActivity(new Intent(VTypeListActivity.this, PriceList.class));
                return true;
            case R.id.wsd:
                // help action
                if(!Usertype.equals("Distributor"))
                {
                    alertbox.showAlertbox("You are not allowed to view this page !","auth");
                }
                else
                {
                    startActivity(new Intent(VTypeListActivity.this, WSDActivity.class));
                }
                return true;
            case R.id.retailer:
                // check for updates action
                if(!Usertype.equals("Retailer"))
                {
                    alertbox.showAlertbox("You are not allowed to view this page !","auth");
                }
                else {
                    startActivity(new Intent(VTypeListActivity.this, RetailerActivity.class));
                }
                return true;
            case R.id.fieldengi:
                // check for updates action

                if(Usertype.equals("Rane Employee") || Usertype.equals("Sales Executives")|| Usertype.equals("MSR"))
                {

                    startActivity(new Intent(VTypeListActivity.this, EngineerActivity.class));
                }
                else
                {
                    alertbox.showAlertbox("You are not allowed to view this page !","auth");
                }
                return true;
            case R.id.msr:
                // check for updates action

                if(Usertype.equals("MSR"))
                {

                    startActivity(new Intent(VTypeListActivity.this, EngineerActivity.class));
                }
                else
                {
                    alertbox.showAlertbox("You are not allowed to view this page !");
                }
                return true;
            case R.id.mech:
                // check for updates action
                if(!Usertype.equals("Mechanic"))
                {
                    alertbox.showAlertbox("You are not allowed to view this page !","auth");
                }
                else
                {
                    startActivity(new Intent(VTypeListActivity.this, MechanicActivity.class));
                }
                return true;
            case R.id.customer:
                // check for updates action

                if((Usertype.equals("Others")) || (Usertype.equals("Fleet Operators")))
                {

                    startActivity(new Intent(VTypeListActivity.this, CustomerActivity.class));
                }
                else
                {
                    alertbox.showAlertbox("You are not allowed to view this page !","auth");
                }
                return true;
            case R.id.notification:
                // check for updates action
                startActivity(new Intent(VTypeListActivity.this, NotificationActivity.class));
                return true;
            case R.id.edit:
                // check for updates action
                Intent edit = new Intent(VTypeListActivity.this,EditProfileActivity.class);
                edit.putExtra("usertype",Usertype);
                edit.putExtra("mobile",Usermobile);
                edit.putExtra("email",Useremail);
                startActivity(edit);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    


 /*   private void LoadListView() {

        DBHelper dbhelper = new DBHelper(VTypeListActivity.this);
        db = dbhelper.readDataBase();
        productquery = "select distinct vehicle_type from product";
        Cursor cursor = db.rawQuery(productquery, null);

        if (cursor.moveToFirst()) {
            do {

                VehicleTypeList.add(cursor.getString(cursor.getColumnIndex("vehicle_type")).toString());

            }
            while (cursor.moveToNext());

        }
        Listview.setAdapter(new ListViewAdapter(VTypeListActivity.this, VehicleTypeList));


    }*/
}
