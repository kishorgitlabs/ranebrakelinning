package product;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.ListView;
import android.widget.TextView;

import com.brainagic.catalogue.EditProfileActivity;
import com.brainagic.catalogue.catalogue.R;

import java.util.ArrayList;

import about.AboutActivity;
import adapter.Search_Product_Adapter;
import alertbox.Alertbox;
import customer.CustomerActivity;
import database.DBHelper;
import dmax.dialog.SpotsDialog;
import engineer.EngineerActivity;
import home.HomeActivity;
import mechanic.MechanicActivity;
import notification.NotificationActivity;
import retailer.RetailerActivity;
import wsd.WSDActivity;

;


public class Search_Grid_Activity extends Activity {


    private ArrayList<String> OemList, PartList, DescriptionList, VehicleModelList, FrontRearPartList,VehicleMGridList,VehicleTypeList,ProductList;
    private String selectedOem, selectedVtype;
    private TextView oemtxt,vtypetxt,vfront,vrear;
    private SQLiteDatabase db;
    private ListView Listview;
    Alertbox alertbox = new Alertbox(Search_Grid_Activity.this);
    private EditText SearchEdit;
    private Button searchbtn;
    private SpotsDialog progressDialog;
    private Button SearchBtn;
    private HorizontalScrollView horlist;
    SharedPreferences myshare;
    SharedPreferences.Editor editor;
    private String Usertype,Useremail,Usermobile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search__grid_);

        getActionBar().setLogo(R.drawable.logo);
        Listview = (ListView)findViewById(R.id.listview);

        VehicleTypeList = new ArrayList<String>();
        VehicleTypeList = new ArrayList<String>();
        OemList = new ArrayList<String>();
        PartList = new ArrayList<String>();
        VehicleMGridList = new ArrayList<String>();

        DescriptionList = new ArrayList<String>();
        FrontRearPartList = new ArrayList<String>();
        ProductList= new ArrayList<String>();

        SearchEdit = (EditText) findViewById(R.id.searchwordedit);
        SearchBtn = (Button) findViewById(R.id.searchbtn);


        TextView home = (TextView) findViewById(R.id.fontDesign1);
        TextView back = (TextView) findViewById(R.id.fontDesign2);


        // for home abd back
         oemtxt = (TextView) findViewById(R.id.vmanu);
         vtypetxt = (TextView) findViewById(R.id.vtype);
        vfront = (TextView) findViewById(R.id.fontDesign1);
        vrear = (TextView) findViewById(R.id.fontDesign2);


        myshare = getSharedPreferences("Registration", MODE_PRIVATE);
        editor = myshare.edit();
        Usertype = myshare.getString("Usertype", "");
        Useremail = myshare.getString("Email","") ;
        Usermobile = myshare.getString("MobNo","") ;
        

        home.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                Intent ho = new Intent(Search_Grid_Activity.this, HomeActivity.class);
                ho.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(ho);
            }
        });
        back.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                onBackPressed();
            }
        });



        home.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                Intent ho = new Intent(Search_Grid_Activity.this, HomeActivity.class);
                ho.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(ho);
            }
        });
        back.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                onBackPressed();
            }
        });



        SearchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if(SearchEdit.getText().length()==0)
                {
                    SearchEdit.setError("Enter search word !");
                }
                else
                {
                   String word =  SearchEdit.getText().toString();
                    String query = "select  oemname,vehicle_model,vehicle_type,newpart_no,front_part_no,rear_part_no,productname,FrontRear from product_price where front_part_no like '%"+ word +"%' or rear_part_no like '%"+ word +"%' or Unique_Identification like '%"+ word +"%' or oemname like '%"+ word +"%' or vehicle_model like '%"+ word +"%' or vehicle_type like '%"+ word +"%' or newpart_no like '%"+ word +"%' order by FrontRear asc";
                    new LoadListViewAsyntask().execute(query);
                }
            }
        });


    }



    class LoadListViewAsyntask extends AsyncTask<String,Void,String>
    {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new SpotsDialog(Search_Grid_Activity.this,R.style.Custom);
            progressDialog.setCancelable(false);
            progressDialog.show();

            VehicleMGridList.clear();
            PartList.clear();
            FrontRearPartList.clear();
            OemList.clear();
            ProductList.clear();
            VehicleTypeList.clear();
        }


        @Override
        protected String doInBackground(String... query) {

            try {

                DBHelper dbhelper = new DBHelper(Search_Grid_Activity.this);
                db = dbhelper.readDataBase();

                Log.v("Query" , query[0]);
                Cursor cursor = db.rawQuery(query[0], null);

                if (cursor.moveToFirst())
                {
                    do {

                        VehicleMGridList.add(cursor.getString(cursor.getColumnIndex("vehicle_model")).toString());
                        PartList.add(cursor.getString(cursor.getColumnIndex("newpart_no")).toString());
                        FrontRearPartList.add(cursor.getString(cursor.getColumnIndex("FrontRear")).toString());
                        OemList.add(cursor.getString(cursor.getColumnIndex("oemname")).toString());
                        ProductList.add(cursor.getString(cursor.getColumnIndex("productname")).toString());
                        VehicleTypeList.add(cursor.getString(cursor.getColumnIndex("vehicle_type")).toString());
                    }
                    while (cursor.moveToNext());

                    return "success";
                }
                else
                {
                    return "nodata";
                }


            }catch (Exception e)
            {
                e.printStackTrace();
                return "error";
            }
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressDialog.dismiss();
            if(s.equals("success"))
            {
                Listview.setAdapter(new Search_Product_Adapter(Search_Grid_Activity.this, OemList, VehicleMGridList, VehicleTypeList, PartList, FrontRearPartList,ProductList));

            }
            else
            {
                Listview.setAdapter(new Search_Product_Adapter(Search_Grid_Activity.this, OemList, VehicleMGridList, VehicleTypeList, PartList, FrontRearPartList,ProductList));
                alertbox.showAlertbox("Data not found !");

            }
        }
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.popup, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // ...

        switch (item.getItemId()) {
            case R.id.about:
                // search action
                startActivity(new Intent(Search_Grid_Activity.this, AboutActivity.class));
                return true;
            case R.id.product:
                // location found
                startActivity(new Intent(Search_Grid_Activity.this, ProductCatalog.class));
                return true;
            case R.id.price:
                // refresh
                startActivity(new Intent(Search_Grid_Activity.this, PriceList.class));
                return true;
            case R.id.wsd:
                // help action
                if(!Usertype.equals("Distributor"))
                {
                    alertbox.showAlertbox("You are not allowed to view this page !","auth");
                }
                else
                {
                    startActivity(new Intent(Search_Grid_Activity.this, WSDActivity.class));
                }
                return true;
            case R.id.retailer:
                // check for updates action
                if(!Usertype.equals("Retailer"))
                {
                    alertbox.showAlertbox("You are not allowed to view this page !","auth");
                }
                else {
                    startActivity(new Intent(Search_Grid_Activity.this, RetailerActivity.class));
                }
                return true;
            case R.id.fieldengi:
                // check for updates action

                if(Usertype.equals("Rane Employee") || Usertype.equals("Sales Executives")|| Usertype.equals("MSR"))
                {

                    startActivity(new Intent(Search_Grid_Activity.this, EngineerActivity.class));
                }
                else
                {
                    alertbox.showAlertbox("You are not allowed to view this page !","auth");
                }
                return true;
            case R.id.msr:
                // check for updates action

                if(Usertype.equals("MSR"))
                {

                    startActivity(new Intent(Search_Grid_Activity.this, EngineerActivity.class));
                }
                else
                {
                    alertbox.showAlertbox("You are not allowed to view this page !");
                }
                return true;
            case R.id.mech:
                // check for updates action
                if(!Usertype.equals("Mechanic"))
                {
                    alertbox.showAlertbox("You are not allowed to view this page !","auth");
                }
                else
                {
                    startActivity(new Intent(Search_Grid_Activity.this, MechanicActivity.class));
                }
                return true;
            case R.id.customer:
                // check for updates action

                if((Usertype.equals("Others")) || (Usertype.equals("Fleet Operators")))
                {

                    startActivity(new Intent(Search_Grid_Activity.this, CustomerActivity.class));
                }
                else
                {
                    alertbox.showAlertbox("You are not allowed to view this page !","auth");
                }
                return true;
            case R.id.notification:
                // check for updates action
                startActivity(new Intent(Search_Grid_Activity.this, NotificationActivity.class));
                return true;
            case R.id.edit:
                // check for updates action
                Intent edit = new Intent(Search_Grid_Activity.this,EditProfileActivity.class);
                edit.putExtra("usertype",Usertype);
                edit.putExtra("mobile",Usermobile);
                edit.putExtra("email",Useremail);
                startActivity(edit);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }




}