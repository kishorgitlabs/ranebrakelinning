package product;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.HorizontalScrollView;
import android.widget.ListView;
import android.widget.TextView;

import com.brainagic.catalogue.EditProfileActivity;
import com.brainagic.catalogue.catalogue.R;

import java.util.ArrayList;

import about.AboutActivity;
import adapter.Product_Adapter;
import adapter.VTypeProduct_Adapter;
import alertbox.Alertbox;
import customer.CustomerActivity;
import database.DBHelper;
import dmax.dialog.SpotsDialog;
import engineer.EngineerActivity;
import home.HomeActivity;
import mechanic.MechanicActivity;
import notification.NotificationActivity;
import retailer.RetailerActivity;
import wsd.WSDActivity;

public class GirdActivity extends Activity {


    private ArrayList<String> VehicleModelList,OemList,FrontReartList,PartNoList,VehicleTypeList,ProductList,PartTypeList,UniqTypeList,ImageList,SizeList;
    private String selectedOem, selectedVtype, selectedVmodel;
    private TextView titletxt, navitxt,vmodelgridtxt;
    private SQLiteDatabase db;
    private ListView Listview;
    private String productquery;
    Alertbox alertbox = new Alertbox(GirdActivity.this);
    private SpotsDialog progressDialog;
    private String navgationstring;
    private HorizontalScrollView horilist;
    private String Usertype,Useremail,Usermobile;
    SharedPreferences myshare;
    SharedPreferences.Editor editor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gird);

        myshare = getSharedPreferences("Registration", MODE_PRIVATE);
        editor = myshare.edit();
        Usertype = myshare.getString("Usertype", "");
        Useremail = myshare.getString("Email","") ;
        Usermobile = myshare.getString("MobNo","") ;

        getActionBar().setIcon(R.drawable.logo);

        VehicleModelList = new ArrayList<String>();
        VehicleTypeList = new ArrayList<String>();
        OemList = new ArrayList<String>();
        SizeList= new ArrayList<String>();
        PartNoList = new ArrayList<String>();
        FrontReartList = new ArrayList<String>();
        ProductList = new ArrayList<String>();
        PartTypeList = new ArrayList<String>();
        UniqTypeList = new ArrayList<String>();
        ImageList = new ArrayList<String>();

        selectedOem = getIntent().getStringExtra("oemname").toString();
        selectedVtype = getIntent().getStringExtra("vType").toString();
        selectedVmodel = getIntent().getStringExtra("vModel").toString();

        navitxt = (TextView) findViewById(R.id.navi);
        titletxt = (TextView) findViewById(R.id.tittle);
        Listview = (ListView) findViewById(R.id.listView);


        // for home abd back
        TextView home = (TextView) findViewById(R.id.fontDesign1);
        TextView back = (TextView) findViewById(R.id.fontDesign2);


        home.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                Intent ho = new Intent(GirdActivity.this, HomeActivity.class);
                ho.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(ho);
            }
        });
        back.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                onBackPressed();
            }
        });

        if(selectedVtype.equals("") && selectedOem.equals(""))
        {
            productquery = "select distinct  oemname,vehicle_type,vehicle_model,productname,part_type,part_number,FrontRear,Unique_Identification,image,size   from product_price  where vehicle_model ='" + selectedVmodel + "' order by FrontRear asc";
            navitxt.setText(selectedVmodel + "  >>  ");

        }

        else if(selectedVtype.equals(""))
        {
            productquery = "select distinct  oemname,vehicle_type,vehicle_model,productname,part_type,part_number,FrontRear,Unique_Identification,image,size   from product_price  where  oemname ='" + selectedOem + "' and  vehicle_model ='" + selectedVmodel + "' order by FrontRear asc";
            navitxt.setText("Vehicle Manufacturer  >>  "+selectedOem + "  >>   " +  selectedVmodel + "  >> ");

        }
        else  if(selectedVmodel.equals(""))
        {
            productquery = "select distinct  oemname,vehicle_type,vehicle_model,productname,part_type,part_number,FrontRear,Unique_Identification,image,size   from product_price  where oemname ='" + selectedOem + "' and vehicle_type ='" + selectedVtype + "' order by FrontRear asc";
            navitxt.setText( "Vehicle Type  >>  " +selectedVtype + "  >>   " + selectedOem + "  >>  ");

        }
        else  if(selectedOem.equals(""))
        {
            productquery = "select distinct  oemname,vehicle_type,vehicle_model,productname,part_type,part_number,FrontRear,Unique_Identification,image ,size  from product_price  where   vehicle_type ='" + selectedVtype + "' and vehicle_model ='" + selectedVmodel + "' order by FrontRear asc";
            navitxt.setText( selectedVtype + "  >>  " + selectedVmodel + "  >> ");

        }
        else
        {
            productquery = "select distinct  oemname,vehicle_type,vehicle_model,productname,part_type,part_number,FrontRear,Unique_Identification,image,size  from product_price  where oemname ='" + selectedOem + "' and vehicle_type ='" + selectedVtype + "' and vehicle_model ='" + selectedVmodel + "' order by FrontRear asc";
            navitxt.setText(selectedOem + "  >>   " + selectedVtype + "  >>  " + selectedVmodel + "  >> ");

        }



       new LoadListViewAsyntask().execute(productquery);

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.popup, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // ...
        switch (item.getItemId()) {
            case R.id.about:
                // search action
                startActivity(new Intent(GirdActivity.this, AboutActivity.class));
                return true;
            case R.id.product:
                // location found
                startActivity(new Intent(GirdActivity.this, ProductCatalog.class));
                return true;
            case R.id.price:
                // refresh
                startActivity(new Intent(GirdActivity.this, PriceList.class));
                return true;
            case R.id.wsd:
                // help action
                if(!Usertype.equals("Distributor"))
                {
                    alertbox.showAlertbox("You are not allowed to view this page !","auth");
                }
                else
                {
                    startActivity(new Intent(GirdActivity.this, WSDActivity.class));
                }
                return true;
            case R.id.retailer:
                // check for updates action
                if(!Usertype.equals("Retailer"))
                {
                    alertbox.showAlertbox("You are not allowed to view this page !","auth");
                }
                else {
                    startActivity(new Intent(GirdActivity.this, RetailerActivity.class));
                }
                return true;
            case R.id.fieldengi:
                // check for updates action

                if(Usertype.equals("Rane Employee") || Usertype.equals("Sales Executives")|| Usertype.equals("MSR"))
                {

                    startActivity(new Intent(GirdActivity.this, EngineerActivity.class));
                }
                else
                {
                    alertbox.showAlertbox("You are not allowed to view this page !","auth");
                }
                return true;
            case R.id.msr:
                // check for updates action

                if(Usertype.equals("MSR"))
                {

                    startActivity(new Intent(GirdActivity.this, EngineerActivity.class));
                }
                else
                {
                    alertbox.showAlertbox("You are not allowed to view this page !");
                }
                return true;
            case R.id.mech:
                // check for updates action
                if(!Usertype.equals("Mechanic"))
                {
                    alertbox.showAlertbox("You are not allowed to view this page !","auth");
                }
                else
                {
                    startActivity(new Intent(GirdActivity.this, MechanicActivity.class));
                }
                return true;
            case R.id.customer:
                // check for updates action

                if((Usertype.equals("Others")) || (Usertype.equals("Fleet Operators")))
                {

                    startActivity(new Intent(GirdActivity.this, CustomerActivity.class));
                }
                else
                {
                    alertbox.showAlertbox("You are not allowed to view this page !","auth");
                }
                return true;
            case R.id.notification:
                // check for updates action
                startActivity(new Intent(GirdActivity.this, NotificationActivity.class));
                return true;
            case R.id.edit:
                // check for updates action
                Intent edit = new Intent(GirdActivity.this,EditProfileActivity.class);
                edit.putExtra("usertype",Usertype);
                edit.putExtra("mobile",Usermobile);
                edit.putExtra("email",Useremail);
                startActivity(edit);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }







    class LoadListViewAsyntask extends AsyncTask<String,Void,String>
    {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new SpotsDialog(GirdActivity.this,R.style.Custom);
            progressDialog.setCancelable(false);
            progressDialog.show();

            VehicleModelList.clear();
            OemList.clear();
            VehicleTypeList.clear();
            PartNoList.clear();
            FrontReartList.clear();
            ProductList.clear();
            PartTypeList.clear();
            ImageList.clear();
            UniqTypeList.clear();
            SizeList.clear();
        }

        @Override
        protected String doInBackground(String... query) {
            try {
                DBHelper dbhelper = new DBHelper(GirdActivity.this);
                db = dbhelper.readDataBase();
                Log.v("Query" , query[0]);
                Cursor cursor = db.rawQuery(query[0], null);
                if (cursor.moveToFirst())
                {
                    do {

                        VehicleModelList.add(cursor.getString(cursor.getColumnIndex("vehicle_model")).toString());
                        OemList.add(cursor.getString(cursor.getColumnIndex("oemname")).toString());
                        VehicleTypeList.add(cursor.getString(cursor.getColumnIndex("vehicle_type")).toString());
                        PartNoList.add(cursor.getString(cursor.getColumnIndex("part_number")).toString());
                        PartTypeList.add(cursor.getString(cursor.getColumnIndex("part_type")).toString());
                        FrontReartList.add(cursor.getString(cursor.getColumnIndex("FrontRear")).toString());
                        ProductList.add(cursor.getString(cursor.getColumnIndex("productname")).toString());
                        ImageList.add(cursor.getString(cursor.getColumnIndex("image")).toString());
                        UniqTypeList.add(cursor.getString(cursor.getColumnIndex("Unique_Identification")).toString());
                        SizeList.add(cursor.getString(cursor.getColumnIndex("size")).toString());
                    }
                    while (cursor.moveToNext());
                    db.close();
                    return "success";
                }
                else
                {
                    return "nodata";
                }
            }catch (Exception e)
            {
                e.printStackTrace();
                return "error";
            }
        }
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(s.equals("success"))
            {
                progressDialog.dismiss();
                if(selectedVmodel.equals(""))
                {
                    navgationstring = navitxt.getText().toString();
                    Listview.setAdapter(new VTypeProduct_Adapter(GirdActivity.this, OemList,VehicleModelList,VehicleTypeList,PartNoList,PartTypeList,FrontReartList,ProductList,UniqTypeList,SizeList,ImageList,navgationstring));

                }
                else
                {
                    navitxt.setText("Vehicle Model  >>  "+OemList.get(0) + "  >>   " +  selectedVmodel + "  >> ");
                    navgationstring = navitxt.getText().toString();
                    Listview.setAdapter(new Product_Adapter(GirdActivity.this, OemList,VehicleModelList,VehicleTypeList,PartNoList,PartTypeList,FrontReartList,ProductList,UniqTypeList,SizeList,ImageList,navgationstring));
                }
            }
            else
            {
                progressDialog.dismiss();
               // Listview.setAdapter(new Product_Adapter(GirdActivity.this, OemList,VehicleModelList,VehicleTypeList,PartList,DescriptionList,FrontPartList,RearPartList,ProductList));
                alertbox.showAlertbox("Data not found !");

            }
        }
    }


}
