package product;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.brainagic.catalogue.EditProfileActivity;
import com.brainagic.catalogue.catalogue.R;

import java.util.ArrayList;

import about.AboutActivity;
import adapter.ListViewAdapter;
import alertbox.Alertbox;
import customer.CustomerActivity;
import database.DBHelper;
import dmax.dialog.SpotsDialog;
import engineer.EngineerActivity;
import home.HomeActivity;
import mechanic.MechanicActivity;
import notification.NotificationActivity;
import retailer.RetailerActivity;
import wsd.WSDActivity;

;

public class VehicleModel_oem_Activity extends Activity {

    private ArrayList<String> VehicleModelList,OemlList,OemListGrid;
    private TextView titletxt, navitxt;
    private SQLiteDatabase db;
    private ArrayList<String> TypeList;
    private ListView Listview;
    private String productquery,selectedVtype,selectedOem;
    private Spinner oemspinner;

    private String Usertype,Useremail,Usermobile;
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    Alertbox alertbox = new Alertbox(VehicleModel_oem_Activity.this);
    private SpotsDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vehicle_model_oem_);

        getActionBar().setLogo(R.drawable.logo);
        OemlList= new ArrayList<String>();
        VehicleModelList = new ArrayList<String>();
        OemListGrid = new ArrayList<String>();

        Log.v("V Type" , getIntent().getStringExtra("vType").toString());
        selectedVtype = getIntent().getStringExtra("vType").toString();

        myshare = getSharedPreferences("Registration", MODE_PRIVATE);
        editor = myshare.edit();
        Usertype = myshare.getString("Usertype", "");
        Useremail = myshare.getString("Email","") ;
        Usermobile = myshare.getString("MobNo","") ;

        navitxt = (TextView) findViewById(R.id.navi);
        titletxt = (TextView) findViewById(R.id.tittle);
        Listview = (ListView) findViewById(R.id.listview);
        oemspinner = (Spinner) findViewById(R.id.oemlistspinner);

        navitxt.setText(selectedVtype + " >>   ");

        GetOEMList();

        // for home abd back
        TextView home = (TextView) findViewById(R.id.fontDesign1);
        TextView back = (TextView) findViewById(R.id.fontDesign2);


        Listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent govehiclemodel = new Intent(VehicleModel_oem_Activity.this, GirdActivity.class);
                govehiclemodel.putExtra("vModel", VehicleModelList.get(position));
                govehiclemodel.putExtra("oemname", selectedOem);
                govehiclemodel.putExtra("vType", selectedVtype);
                startActivity(govehiclemodel);


            }
        });


        home.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                Intent ho = new Intent(VehicleModel_oem_Activity.this, HomeActivity.class);
                ho.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(ho);
            }
        });
        back.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                onBackPressed();
            }
        });


        oemspinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
            {
                selectedOem = parent.getItemAtPosition(position).toString();
                if(!selectedOem.equals("Select OEM"))
                {
                    productquery = "select distinct vehicle_model from product where oem_name = '" + selectedOem + "' and vehicle_type ='" + selectedVtype + "'";
                    new LoadListViewAsyntask().execute(productquery);
                }
                else
                {
                    productquery = "select distinct vehicle_model from product where vehicle_type ='" + selectedVtype + "'";
                    new LoadListViewAsyntask().execute(productquery);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
       //new LoadListViewAsyntask().execute();

    }


    private void GetOEMList()
    {
        DBHelper dbhelper = new DBHelper(VehicleModel_oem_Activity.this);
        db = dbhelper.readDataBase();
        productquery = "select distinct oem_name from product order by oem_name asc";
        Cursor cursor = db.rawQuery(productquery, null);
        OemlList.clear();
        OemlList.add("Select OEM");
        if (cursor.moveToFirst()) {
            do {

                OemlList.add(cursor.getString(cursor.getColumnIndex("oem_name")).toString());

            }
            while (cursor.moveToNext());
            oemspinner.setAdapter(new ArrayAdapter<String>(VehicleModel_oem_Activity.this,R.layout.simple_spinner_item,OemlList));
        }
    }






    class LoadListViewAsyntask extends AsyncTask<String,Void,String>
    {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new SpotsDialog(VehicleModel_oem_Activity.this,R.style.Custom);
            progressDialog.setCancelable(false);
            progressDialog.show();
            VehicleModelList.clear();
        }


        @Override
        protected String doInBackground(String... query) {

            try {

                DBHelper dbhelper = new DBHelper(VehicleModel_oem_Activity.this);
                db = dbhelper.readDataBase();
                if (selectedOem.equals("Select OEM")) {
                    productquery = "select distinct vehicle_model from product where vehicle_type ='" + selectedVtype + "'";
                } else {
                    productquery = "select distinct vehicle_model from product where oem_name = '" + selectedOem + "' and vehicle_type ='" + selectedVtype + "'";
                }

                Log.v("Query" , query[0]);
                Cursor cursor = db.rawQuery(query[0], null);

                if (cursor.moveToFirst())
                {
                    do {

                        VehicleModelList.add(cursor.getString(cursor.getColumnIndex("vehicle_model")).toString());

                    }
                    while (cursor.moveToNext());

                    return "success";
                }
                else
                {
                    return "nodata";
                }


            }catch (Exception e)
            {
                e.printStackTrace();
                return "error";
            }
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressDialog.dismiss();
            if(s.equals("success"))
            {
                ListViewAdapter adapter = new ListViewAdapter(VehicleModel_oem_Activity.this, VehicleModelList);
                adapter.notifyDataSetChanged();
                Listview.setAdapter(adapter);
            }
            else
            {
                ListViewAdapter adapter = new ListViewAdapter(VehicleModel_oem_Activity.this, VehicleModelList);
                adapter.notifyDataSetChanged();
                Listview.setAdapter(adapter);
                alertbox.showAlertbox("Data not found !");
            }
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.popup, menu);


        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // ...

        switch (item.getItemId()) {
            case R.id.about:
                // search action
                startActivity(new Intent(VehicleModel_oem_Activity.this, AboutActivity.class));
                return true;
            case R.id.product:
                // location found
                startActivity(new Intent(VehicleModel_oem_Activity.this, ProductCatalog.class));
                return true;
            case R.id.price:
                // refresh
                startActivity(new Intent(VehicleModel_oem_Activity.this, PriceList.class));
                return true;
            case R.id.wsd:
                // help action
                if(!Usertype.equals("Distributor"))
                {
                    alertbox.showAlertbox("You are not allowed to view this page !","auth");
                }
                else
                {
                    startActivity(new Intent(VehicleModel_oem_Activity.this, WSDActivity.class));
                }
                return true;
            case R.id.retailer:
                // check for updates action
                if(!Usertype.equals("Retailer"))
                {
                    alertbox.showAlertbox("You are not allowed to view this page !","auth");
                }
                else {
                    startActivity(new Intent(VehicleModel_oem_Activity.this, RetailerActivity.class));
                }
                return true;
            case R.id.fieldengi:
                // check for updates action

                if(Usertype.equals("Rane Employee") || Usertype.equals("Sales Executives")|| Usertype.equals("MSR"))
                {

                    startActivity(new Intent(VehicleModel_oem_Activity.this, EngineerActivity.class));
                }
                else
                {
                    alertbox.showAlertbox("You are not allowed to view this page !","auth");
                }
                return true;
            case R.id.msr:
                // check for updates action

                if(Usertype.equals("MSR"))
                {

                    startActivity(new Intent(VehicleModel_oem_Activity.this, EngineerActivity.class));
                }
                else
                {
                    alertbox.showAlertbox("You are not allowed to view this page !");
                }
                return true;
            case R.id.mech:
                // check for updates action
                if(!Usertype.equals("Mechanic"))
                {
                    alertbox.showAlertbox("You are not allowed to view this page !","auth");
                }
                else
                {
                    startActivity(new Intent(VehicleModel_oem_Activity.this, MechanicActivity.class));
                }
                return true;
            case R.id.customer:
                // check for updates action

                if((Usertype.equals("Others")) || (Usertype.equals("Fleet Operators")))
                {

                    startActivity(new Intent(VehicleModel_oem_Activity.this, CustomerActivity.class));
                }
                else
                {
                    alertbox.showAlertbox("You are not allowed to view this page !","auth");
                }
                return true;
            case R.id.notification:
                // check for updates action
                startActivity(new Intent(VehicleModel_oem_Activity.this, NotificationActivity.class));
                return true;
            case R.id.edit:
                // check for updates action
                Intent edit = new Intent(VehicleModel_oem_Activity.this,EditProfileActivity.class);
                edit.putExtra("usertype",Usertype);
                edit.putExtra("mobile",Usermobile);
                edit.putExtra("email",Useremail);
                startActivity(edit);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }



}
