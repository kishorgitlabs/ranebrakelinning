package product;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.brainagic.catalogue.EditProfileActivity;
import com.brainagic.catalogue.catalogue.R;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import about.AboutActivity;
import adapter.WhatNewProduct_details_Adapter;
import alertbox.Alertbox;
import customer.CustomerActivity;
import dmax.dialog.SpotsDialog;
import engineer.EngineerActivity;
import home.HomeActivity;
import mechanic.MechanicActivity;
import network.NetworkConnection;
import notification.NotificationActivity;
import persistency.ServerConnection;
import retailer.RetailerActivity;
import wsd.WSDActivity;

;

public class WhatsNewProduct_More_Details extends Activity {

    private String PartNo, productquery;
    private ArrayList<String> PartTypeList, PartNumberList, SizeList, GradeList, PriceList, ImageList;
    private ImageView productImage;
    private SQLiteDatabase db;
    private TextView navitxt, titletxt;
    private ListView Listview;
    Alertbox alertbox = new Alertbox(WhatsNewProduct_More_Details.this);
    private SpotsDialog progressDialog;
    Connection connection;
    Statement stmt;
    private ResultSet resultSet;
String URL = "http://rbl.brainmagicllc.com/Image/"; //http://rbl.brainmagicllc.com/Image
    private HorizontalScrollView horilist;
    SharedPreferences myshare;
    SharedPreferences.Editor editor;
    private String Usertype,Useremail,Usermobile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product__more__details);

        getActionBar().setLogo(R.drawable.logo);
        PartNo = getIntent().getStringExtra("partno").toString();
        PartTypeList = new ArrayList<String>();
        PartNumberList = new ArrayList<String>();
        SizeList = new ArrayList<String>();
        GradeList = new ArrayList<String>();
        PriceList = new ArrayList<String>();
        ImageList = new ArrayList<String>();

        productquery = "select distinct * from whatsNew  where   Part_Number = '" + PartNo + "'";

        myshare = getSharedPreferences("Registration", MODE_PRIVATE);
        editor = myshare.edit();
        Usertype = myshare.getString("Usertype", "");
        Useremail = myshare.getString("Email","") ;
        Usermobile = myshare.getString("MobNo","") ;
        

        navitxt = (TextView) findViewById(R.id.navi);
        titletxt = (TextView) findViewById(R.id.tittle);
        Listview = (ListView) findViewById(R.id.listView);

        horilist = (HorizontalScrollView) findViewById(R.id.horilist);
        horilist.setVisibility(View.GONE);
        navitxt.setText("What's New  >>  "+PartNo);

        // for home abd back
        TextView home = (TextView) findViewById(R.id.fontDesign1);
        TextView back = (TextView) findViewById(R.id.fontDesign2);


        home.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                Intent ho = new Intent(WhatsNewProduct_More_Details.this, HomeActivity.class);
                ho.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(ho);
            }
        });
        back.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                onBackPressed();
            }
        });

        checkinternet();


    }


    private void checkinternet() {
        NetworkConnection connection = new NetworkConnection(WhatsNewProduct_More_Details.this);

        if (connection.CheckInternet()) {

            new LoadListViewAsyntask().execute(productquery);

        } else {
            // Toast.makeText(LoginActivity.this,"Not unable to connect please check your Internet connection ! ", Toast.LENGTH_SHORT).show();
            alertbox.showAlertboxwithback(getResources().getString(R.string.nointernetmsg));


        }
    }





    class LoadListViewAsyntask extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new SpotsDialog(WhatsNewProduct_More_Details.this,R.style.Custom);
            progressDialog.setCancelable(false);
            progressDialog.show();
            PartTypeList.clear();
            PartNumberList.clear();
            SizeList.clear();
            GradeList.clear();
            PriceList.clear();
            ImageList.clear();

        }


        @Override
        protected String doInBackground(String... query) {

            try {

                ServerConnection server = new ServerConnection();
                connection = server.getConnection();
                stmt = connection.createStatement();


                resultSet = stmt.executeQuery(query[0]);

                Log.v("Query", query[0]);

                while (resultSet.next()) {


                    PartTypeList.add(resultSet.getString("Part_Type").toString());
                    PartNumberList.add(resultSet.getString("Part_Number").toString());
                    SizeList.add(resultSet.getString("Size").toString());
                    GradeList.add(resultSet.getString("Grade").toString());
                    PriceList.add(resultSet.getString("Price").toString());
                    ImageList.add(URL+resultSet.getString("image").toString());

                }

                if (PartTypeList.isEmpty()) {
                    return "nodata";

                } else {
                    return "success";
                }


            } catch (Exception e) {
                e.printStackTrace();
                return "error";
            }
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressDialog.dismiss();
            if (s.equals("success")) {
                Listview.setAdapter(new WhatNewProduct_details_Adapter(WhatsNewProduct_More_Details.this, PartTypeList, PartNumberList, SizeList, GradeList, PriceList, ImageList));
                horilist.setVisibility(View.VISIBLE);
            } else if (s.equals("nodata")) {
                Listview.setAdapter(new WhatNewProduct_details_Adapter(WhatsNewProduct_More_Details.this, PartTypeList, PartNumberList, SizeList, GradeList, PriceList, ImageList));
                alertbox.showAlertboxwithback("Data not found !");
                horilist.setVisibility(View.GONE);
            }
            else
            {
                alertbox.showAlertboxwithback("Data not found !");
            }
        }
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.popup, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // ...

        switch (item.getItemId()) {
            case R.id.about:
                // search action
                startActivity(new Intent(WhatsNewProduct_More_Details.this, AboutActivity.class));
                return true;
            case R.id.product:
                // location found
                startActivity(new Intent(WhatsNewProduct_More_Details.this, ProductCatalog.class));
                return true;
            case R.id.price:
                // refresh
                startActivity(new Intent(WhatsNewProduct_More_Details.this, PriceList.class));
                return true;
            case R.id.wsd:
                // help action
                if(!Usertype.equals("Distributor"))
                {
                    alertbox.showAlertbox("You are not allowed to view this page !","auth");
                }
                else
                {
                    startActivity(new Intent(WhatsNewProduct_More_Details.this, WSDActivity.class));
                }
                return true;
            case R.id.retailer:
                // check for updates action
                if(!Usertype.equals("Retailer"))
                {
                    alertbox.showAlertbox("You are not allowed to view this page !","auth");
                }
                else {
                    startActivity(new Intent(WhatsNewProduct_More_Details.this, RetailerActivity.class));
                }
                return true;
            case R.id.fieldengi:
                // check for updates action

                if(Usertype.equals("Rane Employee") || Usertype.equals("Sales Executives")|| Usertype.equals("MSR"))
                {

                    startActivity(new Intent(WhatsNewProduct_More_Details.this, EngineerActivity.class));
                }
                else
                {
                    alertbox.showAlertbox("You are not allowed to view this page !","auth");
                }
                return true;
            case R.id.msr:
                // check for updates action

                if(Usertype.equals("MSR"))
                {

                    startActivity(new Intent(WhatsNewProduct_More_Details.this, EngineerActivity.class));
                }
                else
                {
                    alertbox.showAlertbox("You are not allowed to view this page !");
                }
                return true;
            case R.id.mech:
                // check for updates action
                if(!Usertype.equals("Mechanic"))
                {
                    alertbox.showAlertbox("You are not allowed to view this page !","auth");
                }
                else
                {
                    startActivity(new Intent(WhatsNewProduct_More_Details.this, MechanicActivity.class));
                }
                return true;
            case R.id.customer:
                // check for updates action

                if((Usertype.equals("Others")) || (Usertype.equals("Fleet Operators")))
                {

                    startActivity(new Intent(WhatsNewProduct_More_Details.this, CustomerActivity.class));
                }
                else
                {
                    alertbox.showAlertbox("You are not allowed to view this page !","auth");
                }
                return true;
            case R.id.notification:
                // check for updates action
                startActivity(new Intent(WhatsNewProduct_More_Details.this, NotificationActivity.class));
                return true;
            case R.id.edit:
                // check for updates action
                Intent edit = new Intent(WhatsNewProduct_More_Details.this,EditProfileActivity.class);
                edit.putExtra("usertype",Usertype);
                edit.putExtra("mobile",Usermobile);
                edit.putExtra("email",Useremail);
                startActivity(edit);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


}
