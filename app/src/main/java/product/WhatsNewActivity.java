package product;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.ListView;
import android.widget.TextView;

import com.brainagic.catalogue.EditProfileActivity;
import com.brainagic.catalogue.catalogue.R;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import about.AboutActivity;
import adapter.WhatsNewProduct_Adapter;
import alertbox.Alertbox;
import customer.CustomerActivity;
import dmax.dialog.SpotsDialog;
import engineer.EngineerActivity;
import home.HomeActivity;
import mechanic.MechanicActivity;
import network.NetworkConnection;
import notification.NotificationActivity;
import persistency.ServerConnection;
import retailer.RetailerActivity;
import wsd.WSDActivity;

;

public class WhatsNewActivity extends Activity {


    private ArrayList<String> OemList, PartList, DescriptionList, VehicleModelList, RearPartList, FrontPartList,VehicleTypeList,ProductList,ImageList,ProductTypeList;
    private String selectedOem, selectedVtype;
    private TextView titletxt, navitxt;
    private SQLiteDatabase db;
    private ListView Listview;
    Alertbox alertbox = new Alertbox(WhatsNewActivity.this);
    private EditText SearchEdit;
    private Button searchbtn;
    private Alertbox box;
    private SpotsDialog progressDialog;
    Connection connection;
    Statement stmt;
    private ResultSet resultSet;
    String URL ="http://rbl.brainmagicllc.com/Image/";
    private HorizontalScrollView horilist;
    SharedPreferences myshare;
    SharedPreferences.Editor editor;
    private String Usertype,Useremail,Usermobile;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_whats_new);

        getActionBar().setLogo(R.drawable.logo);
        VehicleModelList = new ArrayList<String>();
        OemList = new ArrayList<String>();
        PartList = new ArrayList<String>();
        VehicleTypeList = new ArrayList<String>();
        PartList = new ArrayList<String>();
        ProductList= new ArrayList<String>();
        DescriptionList = new ArrayList<String>();
        RearPartList = new ArrayList<String>();
        FrontPartList = new ArrayList<String>();
        ImageList= new ArrayList<String>();
        Listview = (ListView) findViewById(R.id.listView);

        horilist = (HorizontalScrollView) findViewById(R.id.horilist);
        horilist.setVisibility(View.GONE);
        // for home abd back
        TextView home = (TextView) findViewById(R.id.fontDesign1);
        TextView back = (TextView) findViewById(R.id.fontDesign2);


        myshare = getSharedPreferences("Registration", MODE_PRIVATE);
        editor = myshare.edit();
        Usertype = myshare.getString("Usertype", "");
        Useremail = myshare.getString("Email","") ;
        Usermobile = myshare.getString("MobNo","") ;
        

        home.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                Intent ho = new Intent(WhatsNewActivity.this, HomeActivity.class);
                ho.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(ho);
            }
        });
        back.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                onBackPressed();
            }
        });

        checkinternet();
    }

    private void checkinternet() {
        NetworkConnection connection = new NetworkConnection(WhatsNewActivity.this);

        if (connection.CheckInternet()) {

            new LoadListViewAsyntask().execute();

        } else {
            // Toast.makeText(LoginActivity.this,"Not unable to connect please check your Internet connection ! ", Toast.LENGTH_SHORT).show();
            box.showAlertboxwithback(getResources().getString(R.string.nointernetmsg));


        }
    }


    class LoadListViewAsyntask extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new SpotsDialog(WhatsNewActivity.this,R.style.Custom);
            progressDialog.setCancelable(false);
            progressDialog.show();


            OemList.clear();
            VehicleModelList.clear();
            VehicleTypeList.clear();
            PartList.clear();
            DescriptionList.clear();
            RearPartList.clear();
            FrontPartList.clear();
            ProductList.clear();
            FrontPartList.clear();

        }


        @Override
        protected String doInBackground(String... query) {

            try {


                ServerConnection server = new ServerConnection();
                connection = server.getConnection();
                stmt = connection.createStatement();

                String selectquery = " SELECT * from whatsNew where Status = 'Active' order by WhatsNew_id desc ";

                resultSet = stmt.executeQuery(selectquery);

                Log.v("Query", selectquery);

                while (resultSet.next()) {

                    OemList.add(resultSet.getString("Vehicle_manufacturer"));
                    VehicleModelList.add(resultSet.getString("Vehicle_Model"));
                    VehicleTypeList.add(resultSet.getString("Vehicle_Type"));
                    PartList.add(resultSet.getString("Part_Number"));
                    DescriptionList.add(resultSet.getString("Description"));
                    RearPartList.add(resultSet.getString("Rear_PartNo"));
                    FrontPartList.add(resultSet.getString("Front_PartNo"));


                    ProductList.add(resultSet.getString("Product"));
                    ImageList.add(URL+resultSet.getString("image"));

                }

                if (VehicleModelList.isEmpty()) {
                    return "nodata";
                } else

                {

                    return "success";
                }


            } catch (

        Exception e
        )

        {
            e.printStackTrace();
            return "error";
        }

    }

    @Override
    protected void onPostExecute(String s) {
            super.onPostExecute(s);

            if (s.equals("success")) {
                Listview.setAdapter(new WhatsNewProduct_Adapter(WhatsNewActivity.this,OemList, VehicleTypeList, VehicleModelList, PartList,FrontPartList,RearPartList,DescriptionList,ProductList,ImageList));
                horilist.setVisibility(View.VISIBLE);
                progressDialog.dismiss();
            } else if (s.equals("nodata")) {
                Listview.setAdapter(new WhatsNewProduct_Adapter(WhatsNewActivity.this,OemList, VehicleTypeList, VehicleModelList, PartList,FrontPartList,RearPartList,DescriptionList,ProductList,ImageList));
                alertbox.showAlertbox("Data not found !");
                horilist.setVisibility(View.GONE);
                progressDialog.dismiss();
            }
        else
            {
                box.showAlertboxwithback(getResources().getString(R.string.nointernetmsg));
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.popup, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // ...

        switch (item.getItemId()) {
            case R.id.about:
                // search action
                startActivity(new Intent(WhatsNewActivity.this, AboutActivity.class));
                return true;
            case R.id.product:
                // location found
                startActivity(new Intent(WhatsNewActivity.this, ProductCatalog.class));
                return true;
            case R.id.price:
                // refresh
                startActivity(new Intent(WhatsNewActivity.this, PriceList.class));
                return true;
            case R.id.wsd:
                // help action
                if(!Usertype.equals("Distributor"))
                {
                    alertbox.showAlertbox("You are not allowed to view this page !","auth");
                }
                else
                {
                    startActivity(new Intent(WhatsNewActivity.this, WSDActivity.class));
                }
                return true;
            case R.id.retailer:
                // check for updates action
                if(!Usertype.equals("Retailer"))
                {
                    alertbox.showAlertbox("You are not allowed to view this page !","auth");
                }
                else {
                    startActivity(new Intent(WhatsNewActivity.this, RetailerActivity.class));
                }
                return true;
            case R.id.fieldengi:
                // check for updates action

                if(Usertype.equals("Rane Employee") || Usertype.equals("Sales Executives")|| Usertype.equals("MSR"))
                {

                    startActivity(new Intent(WhatsNewActivity.this, EngineerActivity.class));
                }
                else
                {
                    alertbox.showAlertbox("You are not allowed to view this page !","auth");
                }
                return true;
            case R.id.msr:
                // check for updates action

                if(Usertype.equals("MSR"))
                {

                    startActivity(new Intent(WhatsNewActivity.this, EngineerActivity.class));
                }
                else
                {
                    alertbox.showAlertbox("You are not allowed to view this page !");
                }
                return true;
            case R.id.mech:
                // check for updates action
                if(!Usertype.equals("Mechanic"))
                {
                    alertbox.showAlertbox("You are not allowed to view this page !","auth");
                }
                else
                {
                    startActivity(new Intent(WhatsNewActivity.this, MechanicActivity.class));
                }
                return true;
            case R.id.customer:
                // check for updates action

                if((Usertype.equals("Others")) || (Usertype.equals("Fleet Operators")))
                {

                    startActivity(new Intent(WhatsNewActivity.this, CustomerActivity.class));
                }
                else
                {
                    alertbox.showAlertbox("You are not allowed to view this page !","auth");
                }
                return true;
            case R.id.notification:
                // check for updates action
                startActivity(new Intent(WhatsNewActivity.this, NotificationActivity.class));
                return true;
            case R.id.edit:
                // check for updates action
                Intent edit = new Intent(WhatsNewActivity.this,EditProfileActivity.class);
                edit.putExtra("usertype",Usertype);
                edit.putExtra("mobile",Usermobile);
                edit.putExtra("email",Useremail);
                startActivity(edit);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }



}
