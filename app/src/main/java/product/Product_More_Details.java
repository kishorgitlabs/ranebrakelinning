package product;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.brainagic.catalogue.EditProfileActivity;
import com.brainagic.catalogue.catalogue.R;

import java.util.ArrayList;

import about.AboutActivity;
import adapter.Product_details_Adapter;
import alertbox.Alertbox;
import customer.CustomerActivity;
import database.DBHelper;
import dmax.dialog.SpotsDialog;
import engineer.EngineerActivity;
import home.HomeActivity;
import mechanic.MechanicActivity;
import notification.NotificationActivity;
import retailer.RetailerActivity;
import wsd.WSDActivity;

public class Product_More_Details extends Activity {

    private String PartNo,productquery,model;
    private ArrayList<String> PartTypeList,PartNumberList,SizeList,GradeList,PriceList,ImageList,FRList;
    private ImageView productImage;
    private SQLiteDatabase db;
    private TextView navitxt,titletxt;
    private ListView Listview;
    private Alertbox alertbox = new Alertbox(Product_More_Details.this);
    private SpotsDialog progressDialog;
    private String navigationstring;
    private HorizontalScrollView horilist;
    private String Usertype,Useremail,Usermobile;
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    private ImageView leftarrow,rightarrow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product__more__details);
        getActionBar().setLogo(R.drawable.logo);
        PartNo =   getIntent().getStringExtra("partno").toString();
        navigationstring =   getIntent().getStringExtra("navigation").toString();
        model =   getIntent().getStringExtra("model").toString();
        PartTypeList = new ArrayList<String>();
        PartNumberList = new ArrayList<String>();
        SizeList = new ArrayList<String>();
        GradeList = new ArrayList<String>();
        PriceList = new ArrayList<String>();
        ImageList = new ArrayList<String>();
        FRList = new ArrayList<String>();

        productquery = "select distinct * from product_price where part_number = '"+PartNo+"'  and vehicle_model ='"+model+"' order by FrontRear asc";

        navitxt = (TextView) findViewById(R.id.navi);
        titletxt = (TextView) findViewById(R.id.tittle);
        Listview = (ListView) findViewById(R.id.listView);

        horilist = (HorizontalScrollView) findViewById(R.id.horilist);
        horilist.setVisibility(View.GONE);
        navitxt.setText(navigationstring + " "+PartNo);

        leftarrow = (ImageView) findViewById(R.id.left);
        rightarrow = (ImageView) findViewById(R.id.right);

        myshare = getSharedPreferences("Registration", MODE_PRIVATE);
        editor = myshare.edit();
        Usertype = myshare.getString("Usertype", "");
        Useremail = myshare.getString("Email","") ;
        Usermobile = myshare.getString("MobNo","") ;


        leftarrow.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                horilist.smoothScrollTo(horilist.getScrollX() - 3000, horilist.getScrollY());
                leftarrow.setVisibility(View.GONE);
                rightarrow.setVisibility(View.VISIBLE);
            }
        });
                rightarrow.setOnClickListener(new View.OnClickListener() {

                    public void onClick(View v) {
                horilist.smoothScrollTo(horilist.getScrollX() + 3000, horilist.getScrollY());
                rightarrow.setVisibility(View.GONE);
                leftarrow.setVisibility(View.VISIBLE);
            }
        });


        // for home abd back
        TextView home = (TextView) findViewById(R.id.fontDesign1);
        TextView back = (TextView) findViewById(R.id.fontDesign2);

        home.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                Intent ho = new Intent(Product_More_Details.this, HomeActivity.class);
                ho.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(ho);
            }
        });
        back.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                onBackPressed();
            }
        });


       new  LoadListViewAsyntask().execute(productquery);

    }



    private void LoadListView() {

        DBHelper dbhelper = new DBHelper(Product_More_Details.this);
        db = dbhelper.readDataBase();
        Cursor cursor = db.rawQuery(productquery, null);
        Log.v("Grid query",productquery);
        if (cursor.moveToFirst()) {
            do {
                PartTypeList.add(cursor.getString(cursor.getColumnIndex("part_type")).toString());
                PartNumberList.add(cursor.getString(cursor.getColumnIndex("part_number")).toString());
                SizeList.add(cursor.getString(cursor.getColumnIndex("size")).toString());
                GradeList.add(cursor.getString(cursor.getColumnIndex("grade")).toString());
                PriceList.add(cursor.getString(cursor.getColumnIndex("price")).toString());
                ImageList.add(cursor.getString(cursor.getColumnIndex("image")).toString());
                FRList.add(cursor.getString(cursor.getColumnIndex("Front/Rear")).toString());
            }
            while (cursor.moveToNext());

          //  Listview.setAdapter(new Product_details_Adapter(Product_More_Details.this, PartTypeList,PartNumberList,SizeList,GradeList,PriceList,ImageList,FRList));

        }
        else
        {
            alertbox.showAlertboxwithback("Data not found !");
        }


    }



    class LoadListViewAsyntask extends AsyncTask<String,Void,String>
    {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new SpotsDialog(Product_More_Details.this,R.style.Custom);
            progressDialog.setCancelable(false);
            progressDialog.show();

            PartTypeList.clear();
            PartNumberList.clear();
            SizeList.clear();
            GradeList.clear();
            PriceList.clear();
            ImageList.clear();
            FRList.clear();

        }


        @Override
        protected String doInBackground(String... query) {

            try {

                DBHelper dbhelper = new DBHelper(Product_More_Details.this);
                db = dbhelper.readDataBase();

                Log.v("Query" , query[0]);
                Cursor cursor = db.rawQuery(query[0], null);

                if (cursor.moveToFirst())
                {
                    do {

                        PartTypeList.add(cursor.getString(cursor.getColumnIndex("part_type")).toString());
                        PartNumberList.add(cursor.getString(cursor.getColumnIndex("part_number")).toString());
                        SizeList.add(cursor.getString(cursor.getColumnIndex("size")).toString());
                        GradeList.add(cursor.getString(cursor.getColumnIndex("grade")).toString());
                        PriceList.add(cursor.getString(cursor.getColumnIndex("price")).toString());
                        ImageList.add(cursor.getString(cursor.getColumnIndex("image")).toString());
                        FRList.add(cursor.getString(cursor.getColumnIndex("FrontRear")).toString());
                    }
                    while (cursor.moveToNext());

                    return "success";
                }
                else
                {
                    return "nodata";
                }


            }catch (Exception e)
            {
                e.printStackTrace();
                return "error";
            }
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            if(s.equals("success"))
            {

                Listview.setAdapter(new Product_details_Adapter(Product_More_Details.this, PartTypeList,PartNumberList,SizeList,GradeList,PriceList,ImageList,FRList));
                progressDialog.dismiss();
                horilist.setVisibility(View.VISIBLE);
                rightarrow.setVisibility(View.VISIBLE);
                leftarrow.setVisibility(View.GONE);
            }
            else
            {
                Listview.setAdapter(new Product_details_Adapter(Product_More_Details.this, PartTypeList,PartNumberList,SizeList,GradeList,PriceList,ImageList,FRList));
                horilist.setVisibility(View.GONE);
                rightarrow.setVisibility(View.GONE);
                leftarrow.setVisibility(View.GONE);
                progressDialog.dismiss();
                alertbox.showAlertbox("Data not found !");
            }
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.popup, menu);


        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // ...

        switch (item.getItemId()) {
            case R.id.about:
                // search action
                startActivity(new Intent(Product_More_Details.this, AboutActivity.class));
                return true;
            case R.id.product:
                // location found
                startActivity(new Intent(Product_More_Details.this, ProductCatalog.class));
                return true;
            case R.id.price:
                // refresh
                startActivity(new Intent(Product_More_Details.this, PriceList.class));
                return true;
            case R.id.wsd:
                // help action
                if(!Usertype.equals("Distributor"))
                {
                    alertbox.showAlertbox("You are not allowed to view this page !","auth");
                }
                else
                {
                    startActivity(new Intent(Product_More_Details.this, WSDActivity.class));
                }
                return true;
            case R.id.retailer:
                // check for updates action
                if(!Usertype.equals("Retailer"))
                {
                    alertbox.showAlertbox("You are not allowed to view this page !","auth");
                }
                else {
                    startActivity(new Intent(Product_More_Details.this, RetailerActivity.class));
                }
                return true;
            case R.id.fieldengi:
                // check for updates action

                if(Usertype.equals("Rane Employee") || Usertype.equals("Sales Executives")|| Usertype.equals("MSR"))
                {

                    startActivity(new Intent(Product_More_Details.this, EngineerActivity.class));
                }
                else
                {
                    alertbox.showAlertbox("You are not allowed to view this page !","auth");
                }
                return true;
            case R.id.msr:
                // check for updates action

                if(Usertype.equals("MSR"))
                {

                    startActivity(new Intent(Product_More_Details.this, EngineerActivity.class));
                }
                else
                {
                    alertbox.showAlertbox("You are not allowed to view this page !");
                }
                return true;
            case R.id.mech:
                // check for updates action
                if(!Usertype.equals("Mechanic"))
                {
                    alertbox.showAlertbox("You are not allowed to view this page !","auth");
                }
                else
                {
                    startActivity(new Intent(Product_More_Details.this, MechanicActivity.class));
                }
                return true;
            case R.id.customer:
                // check for updates action

                if((Usertype.equals("Others")) || (Usertype.equals("Fleet Operators")))
                {

                    startActivity(new Intent(Product_More_Details.this, CustomerActivity.class));
                }
                else
                {
                    alertbox.showAlertbox("You are not allowed to view this page !","auth");
                }
                return true;
            case R.id.notification:
                // check for updates action
                startActivity(new Intent(Product_More_Details.this, NotificationActivity.class));
                return true;
            case R.id.edit:
                // check for updates action
                Intent edit = new Intent(Product_More_Details.this,EditProfileActivity.class);
                edit.putExtra("usertype",Usertype);
                edit.putExtra("mobile",Usermobile);
                edit.putExtra("email",Useremail);
                startActivity(edit);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


}
