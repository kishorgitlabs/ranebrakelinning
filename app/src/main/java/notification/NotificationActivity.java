package notification;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.brainagic.catalogue.EditProfileActivity;
import com.brainagic.catalogue.catalogue.R;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import about.AboutActivity;
import adapter.Notification_Adapter;
import alertbox.Alertbox;
import customer.CustomerActivity;
import dmax.dialog.SpotsDialog;
import engineer.EngineerActivity;
import home.HomeActivity;
import mechanic.MechanicActivity;
import network.NetworkConnection;
import persistency.ServerConnection;
import product.PriceList;
import product.ProductCatalog;
import retailer.RetailerActivity;
import wsd.WSDActivity;

;

public class NotificationActivity extends Activity {

    private ArrayList<String> DiscriptionList;
    private Alertbox alertbox = new Alertbox(NotificationActivity.this);
    Connection connection;
    Statement stmt;
    private ResultSet resultSet;
    private SpotsDialog progressDialog;
    private ListView Listview;
    SharedPreferences myshare;
    SharedPreferences.Editor editor;
    Alertbox box = new Alertbox(NotificationActivity.this);
    private String Usertype,Useremail,Usermobile;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);

        getActionBar().setLogo(R.drawable.logo);

        DiscriptionList = new ArrayList<String>();
        Listview = (ListView) findViewById(R.id.listView);
        
        myshare = getSharedPreferences("Registration", MODE_PRIVATE);
        editor = myshare.edit();
        Usertype = myshare.getString("Usertype", "");
        Useremail = myshare.getString("Email","") ;
        Usermobile = myshare.getString("MobNo","") ;
        // for home abd back
        TextView home = (TextView) findViewById(R.id.fontDesign1);
        TextView back = (TextView) findViewById(R.id.fontDesign2);

        home.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                Intent ho = new Intent(NotificationActivity.this, HomeActivity.class);
                ho.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(ho);
            }
        });
        back.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                onBackPressed();
            }
        });

        checkinternet();
    }


    private void checkinternet() {
        NetworkConnection connection = new NetworkConnection(NotificationActivity.this);

        if (connection.CheckInternet()) {

            new LoadListViewAsyntask().execute();

        } else {
            // Toast.makeText(LoginActivity.this,"Not unable to connect please check your Internet connection ! ", Toast.LENGTH_SHORT).show();
            box.showAlertboxwithback(getResources().getString(R.string.nointernetmsg));


        }
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.popup, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // ...

        switch (item.getItemId()) {
            case R.id.about:
                // search action

                startActivity(new Intent(NotificationActivity.this, AboutActivity.class));
                return true;
            case R.id.product:
                // location found
                startActivity(new Intent(NotificationActivity.this, ProductCatalog.class));
                return true;
            case R.id.price:
                // refresh
                startActivity(new Intent(NotificationActivity.this, PriceList.class));
                return true;
            case R.id.wsd:
                // help action
                if(!Usertype.equals("Distributor"))
                {
                    alertbox.showAlertbox("You are not allowed to view this page !","auth");
                }
                else
                {
                    startActivity(new Intent(NotificationActivity.this, WSDActivity.class));
                }
                return true;
            case R.id.retailer:
                // check for updates action
                if(!Usertype.equals("Retailer"))
                {
                    alertbox.showAlertbox("You are not allowed to view this page !","auth");
                }
                else {
                    startActivity(new Intent(NotificationActivity.this, RetailerActivity.class));
                }
                return true;
            case R.id.fieldengi:
                // check for updates action

                if(Usertype.equals("Rane Employee") || Usertype.equals("Sales Executives")|| Usertype.equals("MSR"))
                {

                    startActivity(new Intent(NotificationActivity.this, EngineerActivity.class));
                }
                else
                {
                    alertbox.showAlertbox("You are not allowed to view this page !","auth");
                }
                return true;
            case R.id.msr:
                // check for updates action

                if(Usertype.equals("MSR"))
                {

                    startActivity(new Intent(NotificationActivity.this, EngineerActivity.class));
                }
                else
                {
                    alertbox.showAlertbox("You are not allowed to view this page !");
                }
                return true;
            case R.id.mech:
                // check for updates action
                if(!Usertype.equals("Mechanic"))
                {
                    alertbox.showAlertbox("You are not allowed to view this page !","auth");
                }
                else
                {
                    startActivity(new Intent(NotificationActivity.this, MechanicActivity.class));
                }
                return true;
            case R.id.customer:
                // check for updates action

                if((Usertype.equals("Others")) || (Usertype.equals("Fleet Operators")))
                {

                    startActivity(new Intent(NotificationActivity.this, CustomerActivity.class));
                }
                else
                {
                    alertbox.showAlertbox("You are not allowed to view this page !","auth");
                }
                return true;
            case R.id.notification:
                // check for updates action
                startActivity(new Intent(NotificationActivity.this, NotificationActivity.class));
                return true;
            case R.id.edit:
                // check for updates action
                Intent edit = new Intent(NotificationActivity.this,EditProfileActivity.class);
                edit.putExtra("usertype",Usertype);
                edit.putExtra("mobile",Usermobile);
                edit.putExtra("email",Useremail);
                startActivity(edit);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }





    class LoadListViewAsyntask extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new SpotsDialog(NotificationActivity.this, R.style.Custom);
            progressDialog.setCancelable(false);
            progressDialog.show();

            DiscriptionList.clear();


        }


        @Override
        protected String doInBackground(String... query) {

            try {


                ServerConnection server = new ServerConnection();
                connection = server.getConnection();
                stmt = connection.createStatement();

                String selectquery = " SELECT * from Notification where Status = 'Active' order by id desc ";

                resultSet = stmt.executeQuery(selectquery);

                Log.v("Query", selectquery);

                while (resultSet.next()) {

                    DiscriptionList.add(resultSet.getString("Description"));


                }

                if (DiscriptionList.isEmpty()) {
                    return "nodata";
                } else

                {

                    return "success";
                }


            } catch (

                    Exception e
                    )

            {
                e.printStackTrace();
                return "error";
            }

        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressDialog.dismiss();
            if (s.equals("success")) {
                Listview.setAdapter(new Notification_Adapter(NotificationActivity.this, DiscriptionList));
            } else if (s.equals("nodata")) {
                box.showAlertbox("Notification not found !");
                Listview.setAdapter(new Notification_Adapter(NotificationActivity.this, DiscriptionList));

            }
            else {
                box.showAlertboxwithback(getResources().getString(R.string.slow_internet));
            }
        }
    }


}
