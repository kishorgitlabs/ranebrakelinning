package login;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.brainagic.catalogue.EditProfileActivity;
import com.brainagic.catalogue.catalogue.R;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import about.AboutActivity;
import alertbox.Alertbox;
import customer.CustomerActivity;
import dmax.dialog.SpotsDialog;
import engineer.EngineerActivity;
import home.HomeActivity;
import mechanic.MechanicActivity;
import network.NetworkConnection;
import notification.NotificationActivity;
import persistency.ServerConnection;
import product.PriceList;
import product.ProductCatalog;
import registration.UpdateLocation;
import retailer.RetailerActivity;
import wsd.WSDActivity;

;


public class LoginActivity extends Activity {

    private Button login, cancel;
    private String updateusertype, updatemobile, updateemail;
    private TextView UserName, Password;
    private Alertbox box;
    private SpotsDialog progressDialog;
    Connection connection;
    Statement stmt;
    private ResultSet resultSet;
    SharedPreferences myshare;
    SharedPreferences.Editor editor;
    private TextView forget;
    Alertbox alertbox = new Alertbox(LoginActivity.this);
    private String Usertype,nameString,emailString,mobileString;
    private String Useremail,Usermobile;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getActionBar().setIcon(R.drawable.logo);


        UserName = (TextView) findViewById(R.id.useredit);
        Password = (TextView) findViewById(R.id.passwordedit);

        login = (Button) findViewById(R.id.login);
        cancel = (Button) findViewById(R.id.cancel);
        forget = (TextView) findViewById(R.id.forget);

        updateusertype = getIntent().getStringExtra("usertype");
        updatemobile = getIntent().getStringExtra("mobile");
        updateemail = getIntent().getStringExtra("email");

        box = new Alertbox(LoginActivity.this);

        myshare = getSharedPreferences("Registration", MODE_PRIVATE);
        editor = myshare.edit();
        Usertype = myshare.getString("Usertype", "");
        nameString = myshare.getString("Name", "");
        emailString = myshare.getString("Email", "");
        mobileString = myshare.getString("MobNo", "");

        forget.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this, ForgetPassActivity.class));
            }
        });


        login.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (UserName.length() == 0) {
                    UserName.setFocusable(true);
                    UserName.setError("Enter your username");
                } else if (Password.length() == 0) {
                    Password.setFocusable(true);
                    Password.setError("Enter your password");
                } else {
                    checkinternet();
                }
            }
        });

        // for home abd back
        TextView home = (TextView) findViewById(R.id.fontDesign1);
        TextView back = (TextView) findViewById(R.id.fontDesign2);
        home.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                Intent ho = new Intent(LoginActivity.this, HomeActivity.class);
                ho.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(ho);
            }
        });
        back.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                onBackPressed();
            }
        });

        cancel.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                onBackPressed();
            }
        });
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.popup, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // ...

        switch (item.getItemId()) {
            case R.id.about:
                // search action
                startActivity(new Intent(LoginActivity.this, AboutActivity.class));
                return true;
            case R.id.product:
                // location found
                startActivity(new Intent(LoginActivity.this, ProductCatalog.class));
                return true;
            case R.id.price:
                // refresh
                startActivity(new Intent(LoginActivity.this, PriceList.class));
                return true;
            case R.id.wsd:
                // help action
                if(!Usertype.equals("Distributor"))
                {
                    alertbox.showAlertbox("You are not allowed to view this page !","auth");
                }
                else
                {
                    startActivity(new Intent(LoginActivity.this, WSDActivity.class));
                }
                return true;
            case R.id.retailer:
                // check for updates action
                if(!Usertype.equals("Retailer"))
                {
                    alertbox.showAlertbox("You are not allowed to view this page !","auth");
                }
                else {
                    startActivity(new Intent(LoginActivity.this, RetailerActivity.class));
                }
                return true;
            case R.id.fieldengi:
                // check for updates action

                if(Usertype.equals("Rane Employee") || Usertype.equals("Sales Executives")|| Usertype.equals("MSR"))
                {

                    startActivity(new Intent(LoginActivity.this, EngineerActivity.class));
                }
                else
                {
                    alertbox.showAlertbox("You are not allowed to view this page !","auth");
                }
                return true;
            case R.id.msr:
                // check for updates action

                if(Usertype.equals("MSR"))
                {

                    startActivity(new Intent(LoginActivity.this, EngineerActivity.class));
                }
                else
                {
                    alertbox.showAlertbox("You are not allowed to view this page !");
                }
                return true;
            case R.id.mech:
                // check for updates action
                if(!Usertype.equals("Mechanic"))
                {
                    alertbox.showAlertbox("You are not allowed to view this page !","auth");
                }
                else
                {
                    startActivity(new Intent(LoginActivity.this, MechanicActivity.class));
                }
                return true;
            case R.id.customer:
                // check for updates action
                if((Usertype.equals("Others")) || (Usertype.equals("Fleet Operators")))
                {

                    startActivity(new Intent(LoginActivity.this, CustomerActivity.class));
                }
                else
                {
                    alertbox.showAlertbox("You are not allowed to view this page !","auth");
                }
                return true;
            case R.id.notification:
                // check for updates action
                startActivity(new Intent(LoginActivity.this, NotificationActivity.class));
                return true;
            case R.id.edit:
                // check for updates action
                Intent edit = new Intent(LoginActivity.this,EditProfileActivity.class);
                edit.putExtra("usertype",Usertype);
                edit.putExtra("mobile",Usermobile);
                edit.putExtra("email",Useremail);
                startActivity(edit);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }




    private void checkinternet() {
        NetworkConnection connection = new NetworkConnection(LoginActivity.this);

        if (connection.CheckInternet()) {

            new LoginBackgroud().execute();

        } else {
            // Toast.makeText(LoginActivity.this,"Not unable to connect please check your Internet connection ! ", Toast.LENGTH_SHORT).show();
            box.showAlertboxwithback(getResources().getString(R.string.nointernetmsg));


        }


    }

    class LoginBackgroud extends AsyncTask<String, Void, String> {
        private String namestrng, passwordstrng;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new SpotsDialog(LoginActivity.this, R.style.Custom);
            progressDialog.setCancelable(false);
            progressDialog.show();


            namestrng = UserName.getText().toString();
            passwordstrng = Password.getText().toString();

        }

        @Override
        protected String doInBackground(String... strings) {


            try {

                ServerConnection server = new ServerConnection();
                connection = server.getConnection();
                stmt = connection.createStatement();

                String selectquery = "SELECT * from SalesExecutive where EmailId='" + namestrng + "' and Password='" + passwordstrng + "'";

                resultSet = stmt.executeQuery(selectquery);

                if (resultSet.next()) {
                    connection.close();
                    stmt.close();
                    return "success";
                } else {
                    connection.close();
                    stmt.close();
                    return "notsuccess";
                }


            } catch (Exception e) {
                e.printStackTrace();
                return "error_email";
            }


        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressDialog.dismiss();
            if (s.equals("success")) {


                final AlertDialog alertDialogbox = new AlertDialog.Builder(
                        LoginActivity.this).create();

                LayoutInflater inflater = getLayoutInflater();
                View dialogView = inflater.inflate(R.layout.alertbox, null);
                alertDialogbox.setView(dialogView);

                TextView log = (TextView) dialogView.findViewById(R.id.textView1);
                Button okay = (Button) dialogView.findViewById(R.id.okay);
                log.setText("Logged in Successfully");
                okay.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View arg0) {
                        // TODO Auto-generated method stub

                        editor.putBoolean("islogin", true);
                        editor.commit();

                        Intent updateIntent = new Intent(LoginActivity.this, UpdateLocation.class);
                        updateIntent.putExtra("usertype", updateusertype);
                        updateIntent.putExtra("mobile", updatemobile);
                        updateIntent.putExtra("email", updateemail);
                        startActivity(updateIntent);
                        alertDialogbox.dismiss();
                    }
                });
                alertDialogbox.setCancelable(false);
                alertDialogbox.show();


            } else if (s.equals("notsuccess")) {
                box.showAlertbox("Username or password is incorrect !");
            } else {
                box.showAlertboxwithback(getResources().getString(R.string.slow_internet));
            }
        }

    }


    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        super.onBackPressed();
    }

}