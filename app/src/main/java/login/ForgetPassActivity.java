package login;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.brainagic.catalogue.EditProfileActivity;
import com.brainagic.catalogue.catalogue.R;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import about.AboutActivity;
import alertbox.Alertbox;
import customer.CustomerActivity;
import dmax.dialog.SpotsDialog;
import email.Mail;
import engineer.EngineerActivity;
import home.HomeActivity;
import mechanic.MechanicActivity;
import network.NetworkConnection;
import notification.NotificationActivity;
import persistency.ServerConnection;
import product.PriceList;
import product.ProductCatalog;
import retailer.RetailerActivity;
import wsd.WSDActivity;

;

public class ForgetPassActivity extends Activity {

    private TextView emailId;
    private Button send,cancel;
    private Alertbox box;
    private SpotsDialog progressDialog;
    private Connection connection;
    Statement stmt;
    private ResultSet resultSet;
    private String UserName,Password;
    SharedPreferences myshare;
    SharedPreferences.Editor editor;
    private String Usertype,nameString,emailString,mobileString;
    private String Useremail,Usermobile;
    Alertbox alertbox = new Alertbox(ForgetPassActivity.this);
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_pass);
        emailId = (TextView) findViewById(R.id.emailedit);

        send = (Button) findViewById(R.id.send);
        cancel = (Button) findViewById(R.id.cancel);



        box = new Alertbox(ForgetPassActivity.this);


        myshare = getSharedPreferences("Registration", MODE_PRIVATE);
        editor = myshare.edit();

        Usertype = myshare.getString("Usertype", "");
        nameString = myshare.getString("Name", "");
        emailString = myshare.getString("Email", "");
        mobileString = myshare.getString("MobNo", "");


        send.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v)
            {
                // TODO Auto-generated method stub
                if (emailId.length() == 0) {
                    emailId.setFocusable(true);
                    emailId.setError("Enter your Rane Email id !");
                }  else {
                    checkinternet();
                }
            }
        });

        // for home abd back
        TextView home = (TextView) findViewById(R.id.fontDesign1);
        TextView back = (TextView) findViewById(R.id.fontDesign2);
        home.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                Intent ho = new Intent(ForgetPassActivity.this, HomeActivity.class);
                ho.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(ho);
            }
        });
        back.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                onBackPressed();
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                onBackPressed();
            }
        });
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.popup, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // ...

        switch (item.getItemId()) {
            case R.id.about:
                // search action
                startActivity(new Intent(ForgetPassActivity.this, AboutActivity.class));
                return true;
            case R.id.product:
                // location found
                startActivity(new Intent(ForgetPassActivity.this, ProductCatalog.class));
                return true;
            case R.id.price:
                // refresh
                startActivity(new Intent(ForgetPassActivity.this, PriceList.class));
                return true;
            case R.id.wsd:
                // help action
                if(!Usertype.equals("Distributor"))
                {
                    alertbox.showAlertbox("You are not allowed to view this page !","auth");
                }
                else
                {
                    startActivity(new Intent(ForgetPassActivity.this, WSDActivity.class));
                }
                return true;
            case R.id.retailer:
                // check for updates action
                if(!Usertype.equals("Retailer"))
                {
                    alertbox.showAlertbox("You are not allowed to view this page !","auth");
                }
                else {
                    startActivity(new Intent(ForgetPassActivity.this, RetailerActivity.class));
                }
                return true;
            case R.id.fieldengi:
                // check for updates action

                if(Usertype.equals("Rane Employee") || Usertype.equals("Sales Executives")|| Usertype.equals("MSR"))
                {

                    startActivity(new Intent(ForgetPassActivity.this, EngineerActivity.class));
                }
                else
                {
                    alertbox.showAlertbox("You are not allowed to view this page !","auth");
                }
                return true;
            case R.id.msr:
                // check for updates action

                if(Usertype.equals("MSR"))
                {

                    startActivity(new Intent(ForgetPassActivity.this, EngineerActivity.class));
                }
                else
                {
                    alertbox.showAlertbox("You are not allowed to view this page !");
                }
                return true;
            case R.id.mech:
                // check for updates action
                if(!Usertype.equals("Mechanic"))
                {
                    alertbox.showAlertbox("You are not allowed to view this page !","auth");
                }
                else
                {
                    startActivity(new Intent(ForgetPassActivity.this, MechanicActivity.class));
                }
                return true;
            case R.id.customer:
                // check for updates action

                if((Usertype.equals("Others")) || (Usertype.equals("Fleet Operators")))
                {

                    startActivity(new Intent(ForgetPassActivity.this, CustomerActivity.class));
                }
                else
                {
                    alertbox.showAlertbox("You are not allowed to view this page !","auth");
                }
                return true;
            case R.id.notification:
                // check for updates action
                startActivity(new Intent(ForgetPassActivity.this, NotificationActivity.class));
                return true;
            case R.id.edit:
                // check for updates action
                Intent edit = new Intent(ForgetPassActivity.this,EditProfileActivity.class);
                edit.putExtra("usertype",Usertype);
                edit.putExtra("mobile",Usermobile);
                edit.putExtra("email",Useremail);
                startActivity(edit);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }





    private void checkinternet() {
        NetworkConnection connection = new NetworkConnection(ForgetPassActivity.this);

        if (connection.CheckInternet()) {

            new CheckEmaild().execute();

        } else {
            // Toast.makeText(LoginActivity.this,"Not unable to connect please check your Internet connection ! ", Toast.LENGTH_SHORT).show();
            box.showAlertboxwithback(getResources().getString(R.string.nointernetmsg));


        }


    }

    class CheckEmaild extends AsyncTask<String, Void, String> {
        private String namestrng;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new SpotsDialog(ForgetPassActivity.this,R.style.Custom);
            progressDialog.setCancelable(false);
            progressDialog.show();


            namestrng = emailId.getText().toString();

        }

        @Override
        protected String doInBackground(String... strings) {



            try {

                ServerConnection server = new ServerConnection();
                connection = server.getConnection();
                stmt = connection.createStatement();

                String selectquery = "SELECT * from SalesExecutive where EmailId='" + namestrng + "'";

                resultSet = stmt.executeQuery(selectquery);

                if (resultSet.next()) {
                    UserName = resultSet.getString("Name");
                    Password = resultSet.getString("Password");
                    connection.close();
                    stmt.close();
                    return "success";
                } else {
                    connection.close();
                    stmt.close();
                    return "notsuccess";
                }


            } catch (Exception e) {
                e.printStackTrace();
                return "error_email";
            }


        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressDialog.dismiss();
            if (s.equals("success"))
            {
                new SendPassword().execute();

            } else if (s.equals("notsuccess")) {
                box.showAlertbox("Email id is incorrect !");
            }
            else
            {
                box.showAlertboxwithback(getResources().getString(R.string.slow_internet));
            }
        }

    }


    class SendPassword extends AsyncTask<String, Void, String> {
        private int i = 0;
        private String emailid;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new SpotsDialog(ForgetPassActivity.this,R.style.Custom);
            progressDialog.setCancelable(false);
            progressDialog.show();

            emailid = emailId.getText().toString();

        }

        @Override
        protected String doInBackground(String... strings) {

            try {

                    StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                    StrictMode.setThreadPolicy(policy);
                    String mail = emailid;
                    // String mail = "muthu@brainmagic.info";
                    Mail localMail1 = new Mail("ranebrakeline@gmail.com", "ranegroup");
                    String[] arrayOfString = mail.split(",");

                    localMail1.setTo(arrayOfString);
                    localMail1.setFrom("ranebrakeline@gmail.com");
                    String subject = "Rane Brake Line";
                    localMail1.setSubject(subject);

                    String body =" Dear "+UserName +" Your Password is "+Password;

                    Log.v("body",body);

                    localMail1.setBody(body);
                    if (localMail1.send())
                    {

                        return "send";
                    }
                    else
                    {

                        return "notsend";
                    }


            } catch (Exception e) {
                e.printStackTrace();
                return "error";
            }


        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressDialog.dismiss();
            if (s.equals("send")) {
                box.showAlertbox("Your password has been sent to your email id !");
            } else if (s.equals("notinsert"))
            {
                box.showAlertbox("Your password was not sent !");
            }
            else
            {
                box.showAlertbox(getResources().getString(R.string.slow_internet));
            }

        }
    }


    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        super.onBackPressed();
    }

}
